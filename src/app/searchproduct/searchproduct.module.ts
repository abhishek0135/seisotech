import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchproductPageRoutingModule } from './searchproduct-routing.module';

import { SearchproductPage } from './searchproduct.page';
import { ComponentModule } from 'src/app/components/component.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchproductPageRoutingModule,
    ComponentModule
  ],
  declarations: [SearchproductPage]
})
export class SearchproductPageModule {}
