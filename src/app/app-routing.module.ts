import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'product-listing',
    loadChildren: () => import('./pages/product-listing/product-listing.module').then( m => m.ProductListingPageModule)
  },
  {
    path: 'product-search',
    loadChildren: () => import('./pages/product-search/product-search.module').then( m => m.ProductListingPageModule)
  },
  {
    path: 'category-list',
    loadChildren: () => import('./pages/category-list/category-list.module').then( m => m.CategoryListPageModule)
  },
  {
    path: 'wishlist',
    loadChildren: () => import('./pages/wishlist/wishlist.module').then( m => m.WishlistPageModule)
  },
  {
    path: 'verify-mobile',
    loadChildren: () => import('./pages/verify-mobile/verify-mobile.module').then( m => m.VerifyMobilePageModule)

  },
  {
    path: 'shopping-cart',
    loadChildren: () => import('./pages/shopping-cart/shopping-cart.module').then( m => m.ShoppingCartPageModule)
  },
  {
    path: 'delivery-address',
    loadChildren: () => import('./pages/delivery-address/delivery-address.module').then( m => m.DeliveryAddressPageModule)
  },
  {
    path: 'order-list',
    loadChildren: () => import('./pages/order-list/order-list.module').then( m => m.OrderListPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./pages/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'order-success',
    loadChildren: () => import('./pages/order-success/order-success.module').then( m => m.OrderSuccessPageModule)
  },
  {
    path: 'product-details',
    loadChildren: () => import('./pages/product-details/product-details.module').then( m => m.ProductDetailsPageModule)
  },
  {
    path: 'my-address',
    loadChildren: () => import('./pages/my-address/my-address.module').then( m => m.MyAddressPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./pages/faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'my-wallet',
    loadChildren: () => import('./pages/my-wallet/my-wallet.module').then( m => m.MyWalletPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'page-not-found',
    loadChildren: () => import('./pages/page-not-found/page-not-found.module').then( m => m.PageNotFoundPageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./pages/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'my-direct',
    loadChildren: () => import('./pages/my-direct/my-direct.module').then( m => m.MyDirectPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'direct-business-report',
    loadChildren: () => import('./pages/direct-business-report/direct-business-report.module').then( m => m.DirectBusinessReportPageModule)
  },
  {
    path: 'commission-ledger',
    loadChildren: () => import('./pages/commission-ledger/commission-ledger.module').then( m => m.CommissionLedgerPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./pages/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'compose-message',
    loadChildren: () => import('./pages/compose-message/compose-message.module').then( m => m.ComposeMessagePageModule)
  },
  {
    path: 'add-new-ticket',
    loadChildren: () => import('./pages/add-new-ticket/add-new-ticket.module').then( m => m.AddNewTicketPageModule)
  },
  {
    path: 'add-rating-review',
    loadChildren: () => import('./pages/add-rating-review/add-rating-review.module').then( m => m.AddRatingReviewPageModule)
  },
  {
    path: 'kycupload',
    loadChildren: () => import('./pages/kycupload/kycupload.module').then( m => m.KYCUploadPageModule)
  },
  {
    path: 'set-ewallet-password',
    loadChildren: () => import('./pages/set-ewallet-password/set-ewallet-password.module').then( m => m.SetEwalletPasswordPageModule)
  },
  {
    path: 'add-address',
    loadChildren: () => import('./pages/add-address/add-address.module').then( m => m.AddAddressPageModule)
  },
  {
    path: 'voucher',
    loadChildren: () => import('./pages/voucher/voucher.module').then( m => m.VoucherPageModule)
  },
  {
    path: 'delivery-charges-popup',
    loadChildren: () => import('./pages/delivery-charges-popup/delivery-charges-popup.module').then( m => m.DeliveryChargesPopupPageModule)
  },
  {
    path: 'order-details',
    loadChildren: () => import('./pages/order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'mydownline-report',
    loadChildren: () => import('./pages/mydownline-report/mydownline-report.module').then( m => m.MydownlineReportPageModule)
  },
  {
    path: 'generation-tree',
    loadChildren: () => import('./pages/generation-tree/generation-tree.module').then( m => m.GenerationTreePageModule)
  },
  {
    path: 'shipments',
    loadChildren: () => import('./pages/shipments/shipments.module').then( m => m.ShipmentsPageModule)
  },
  {
    path: 'tds-details',
    loadChildren: () => import('./pages/tds-details/tds-details.module').then( m => m.TdsDetailsPageModule)
  },
  {
    path: 'point-details',
    loadChildren: () => import('./pages/point-details/point-details.module').then( m => m.PointDetailsPageModule)
  },
  {
    path: 'fund-qualification-report',
    loadChildren: () => import('./pages/fund-qualification-report/fund-qualification-report.module').then( m => m.FundQualificationReportPageModule)
  },
  {
    path: 'new-fund-qualification-report',
    loadChildren: () => import('./pages/new-fund-qualification-report/new-fund-qualification-report.module').then( m => m.NewFundQualificationReportPageModule)
  },
  {
    path: 'fresh-business-qualification',
    loadChildren: () => import('./pages/fresh-business-qualification/fresh-business-qualification.module').then( m => m.FreshBusinessQualificationPageModule)
  },
  {
    path: 'referrrel-business-report',
    loadChildren: () => import('./pages/referrrel-business-report/referrrel-business-report.module').then( m => m.ReferrrelBusinessReportPageModule)
  },
  {
    path: 'order-return',
    loadChildren: () => import('./pages/order-return/order-return.module').then( m => m.OrderReturnPageModule)
  },
  {
    path: 'db-qualification-details',
    loadChildren: () => import('./pages/db-qualification-details/db-qualification-details.module').then( m => m.DbQualificationDetailsPageModule)
  },
  {
    path: 'db-income-details',
    loadChildren: () => import('./pages/db-income-details/db-income-details.module').then( m => m.DbIncomeDetailsPageModule)
  },
  {
    path: 'lb-income-details',
    loadChildren: () => import('./pages/lb-income-details/lb-income-details.module').then( m => m.LbIncomeDetailsPageModule)
  },
  {
    path: 'gbv-income-details',
    loadChildren: () => import('./pages/gbv-income-details/gbv-income-details.module').then( m => m.GbvIncomeDetailsPageModule)
  },
  {
    path: 'commission-voucher',
    loadChildren: () => import('./pages/commission-voucher/commission-voucher.module').then( m => m.CommissionVoucherPageModule)
  },
  {
    path: 'download-document',
    loadChildren: () => import('./pages/download-document/download-document.module').then( m => m.DownloadDocumentPageModule)
  },
  {
    path: 'download-tds',
    loadChildren: () => import('./pages/download-tds/download-tds.module').then( m => m.DownloadTdsPageModule)
  },
  {
    path: 'schemes-and-offers',
    loadChildren: () => import('./pages/schemes-and-offers/schemes-and-offers.module').then( m => m.SchemesAndOffersPageModule)
  },
  {
    path: 'fasttrack-scheme-qualifiers',
    loadChildren: () => import('./pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.module').then( m => m.FasttrackSchemeQualifiersPageModule)
  },
  {
    path: 'fasttrack-scheme-details',
    loadChildren: () => import('./pages/fasttrack-scheme-details/fasttrack-scheme-details.module').then( m => m.FasttrackSchemeDetailsPageModule)
  },
  {
    path: 'repurchase-scheme-qualifiers',
    loadChildren: () => import('./pages/repurchase-scheme-qualifiers/repurchase-scheme-qualifiers.module').then( m => m.RepurchaseSchemeQualifiersPageModule)
  },
  {
    path: 'monthly-scheme-qualifiers',
    loadChildren: () => import('./pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.module').then( m => m.MonthlySchemeQualifiersPageModule)
  },
  {
    path: 'monthly-scheme-details',
    loadChildren: () => import('./pages/monthly-scheme-details/monthly-scheme-details.module').then( m => m.MonthlySchemeDetailsPageModule)
  },
  {
    path: 'dashboard-scheme-details',
    loadChildren: () => import('./pages/dashboard-scheme-details/dashboard-scheme-details.module').then( m => m.DashboardSchemeDetailsPageModule)
  },
  {
    path: 'dashboard-repurchase-scheme',
    loadChildren: () => import('./pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.module').then( m => m.DashboardRepurchaseSchemePageModule)
  },
  {
    path: 'inbox',
    loadChildren: () => import('./pages/inbox/inbox.module').then( m => m.InboxPageModule)
  },
  {
    path: 'offer-details',
    loadChildren: () => import('./pages/offer-details/offer-details.module').then( m => m.OfferDetailsPageModule)
  },
  {
    path: 'searchproduct',
    loadChildren: () => import('./searchproduct/searchproduct.module').then( m => m.SearchproductPageModule)
  },
  {
    path: 'filtermodal',
    loadChildren: () => import('./pages/filtermodal/filtermodal.module').then( m => m.FiltermodalPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
