import { Injectable, NgZone } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { AppConstants } from './app.constants';
import { Values } from './app.variables';

@Injectable({
  providedIn: 'root',
})
export class CommonUtility {
  constructor(
    public value: Values,
    private toastCtrl: ToastController,
    private nav: NavController,
    public ngZone: NgZone
  ) {}

  async showSuccessToast(messageText) {
    const toast = await this.toastCtrl.create({
      message: messageText,
      duration: 3000,
      keyboardClose: true,
      color: 'success',
    });
    toast.present();
  }

  async showErrorToast(messageText) {
    const toast = await this.toastCtrl.create({
      message: messageText,
      duration: 3000,
      keyboardClose: true,
      color: 'danger',
    });
    toast.present();
  }

  async showWarningToast(messageText) {
    const toast = await this.toastCtrl.create({
      message: messageText,
      duration: 3000,
      keyboardClose: true,
      color: 'warning',
    });
    toast.present();
  }

  async showInformativeToast(messageText) {
    const toast = await this.toastCtrl.create({
      message: messageText,
      duration: 3000,
      keyboardClose: true,
    });
    toast.present();
  }

  setLocalstorage(key: string, value: string) {
    localStorage.setItem(this.value.appIdToUse + '' + key, value);
  }

  getLocalstorage(key: string) {
    const valueFromLocal = localStorage.getItem(
      this.value.appIdToUse + '' + key
    );
    return valueFromLocal;
  }

  removeLocalstorageItem(key: string) {
    localStorage.removeItem(this.value.appIdToUse + '' + key);
  }

  clearLocalstorage() {
    localStorage.clear();
  }

  isEmptyOrNull(value) {
    if (value === null || value === undefined || value === '') {
      return true;
    } else {
      return false;
    }
  }

  redirectToInitialScreen() {
    this.ngZone.run(() => {
      if (
        !this.isEmptyOrNull(
          this.getLocalstorage(AppConstants.localstorageKeys.isLogin)
        )
      ) {
        const isLogin =
          this.getLocalstorage(AppConstants.localstorageKeys.isLogin) === 'true'
            ? true
            : false;
        if (isLogin) {
          this.nav.navigateRoot(['/tabMenu/tabs/home']);
        } else {
          this.nav.navigateRoot(['/login']);
        }
      } else {
        this.nav.navigateRoot(['/login']);
      }
    });
  }

  trimFormFieldValues(formGroup: any) {
    Object.keys(formGroup.controls).forEach((key) => {
      if (typeof formGroup.controls[key].value === 'string') {
        if (!this.isEmptyOrNull(formGroup.controls[key].value)) {
          formGroup.controls[key].setValue(
            formGroup.controls[key].value.toString().trim()
          );
        }
      }
    });
  }

  checkFormValuesIsChanged(originalFormObject, currentFormObject) {
    let isValueChange = false;
    const fields = Object.getOwnPropertyNames(originalFormObject);
    fields.forEach((element) => {
      if (originalFormObject[element] !== currentFormObject[element]) {
        isValueChange = true;
      }
    });
    return isValueChange;
  }
}
