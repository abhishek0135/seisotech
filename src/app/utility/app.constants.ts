export class AppConstants {
  public static productFullDescription = `<div class="full-description" itemprop="description">
  <p><strong>Liver is the most prime organ and multi functional organ of body serving more than 500 different functions at a time. Most of time it is completely neglected and todays hectic lifestyle like excessive alcohol, antibiotics, food laced with preservatives and colours exert more dangerous threats to Liver.<br>But there is natural solution you can trust upon<br></strong></p>
<p><strong>Milk Thistle:-</strong></p>
<ol>
<li>Powerful antioxidant, helps to repair liver tissue.</li>
<li>Works as Liver Tonic and aids liver functions.</li>
</ol>
<p><strong>&nbsp;</strong><strong>Kutki and Kalmegh</strong>:-</p>
<ol>
<li>Conditions liver against damage due to toxins, alcohol, drugs etc</li>
<li>Hepatoprotective in nature.</li>
</ol>
<p>&nbsp;<strong>Amlatas and Revandchini:-</strong></p>
<ol>
<li>Has detoxification like effect and minimizes load to liver to clean toxins from body.</li>
<li>Has laxative effect as well.</li>
</ol>
<p>&nbsp;<strong>Punarnava:-</strong></p>
<ol>
<li>Helps in regeneration of Liver tissue.</li>
</ol>
<p>&nbsp;<strong>Rohitak and Sharpunkha</strong></p>
<ol>
<li>Useful in liver as well as spleen functioning.</li>
<li>Important in secretion of bile and liver enzymes.</li>
</ol>
<p>&nbsp;<strong>Benefits:</strong></p>
<ol>
<li>Helps to maintain liver functions.</li>
<li>Hepatoprotective in nature.</li>
<li>Helps functioning of spleen also and helps formation of blood cells.</li>
<li>Helps in formation of bile and liver enzymes.</li>
<li>Boosts detoxification function of liver.</li>
</ol>
<p>&nbsp;<strong>Use Instructions:</strong></p>
<ol>
<li>Preventive – 1 capsule on daily basis any time of day.</li>
<li>Curative – 2-3 capsules on daily basis half hour before meals</li>
</ol>
</div>`;
  public static localstorageKeys = {
    mobileNo: 'mobileNo',
    fullname: 'fullname',
    userName: 'userName',
    emailAddress: 'emailAddress',
    customerId: 'customerId',
    roleId: 'roleId',
    isLogin: 'isLogin',
    countryId: 'countryId',
    profilePictureUrl: 'profilePictureUrl',
    dismissUpdateForVersion: 'dismissUpdateForVersion',
    isAppIntroShown: 'isAppIntroShown',
    cartCount: 'cartCount',
  };
  public static Action = {
    getCategoryList: 'category',
    getProductList: 'product',
    getProductSearch: 'productsearch',
    cartAction: 'addtocart',
  };
  public static userId = 3;
  currencyCode = 'USD';
  currencyCodeValue = '1.2-2';
}
