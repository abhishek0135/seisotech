import { Injectable } from '@angular/core';
import { ApiEnvironment } from './enums';

const apiEnvironment: ApiEnvironment = ApiEnvironment.DEV;

@Injectable({
  providedIn: 'root',
})
export class Values {
  public BaseURL = '';
  public APIBASE = '';
  public appIdToUse = 'agrowonnmart2020_';
  cartCount = 0;
  appName = '';
  appPackageName = '';
  appRunningVersion = '';
  appCurrentVersion = '';
  isMandatoryUpdate = false;
  serviceAvailable = false;
  serviceDownMsg1 = '';
  serviceDownMsg2 = '';
  updateMsg1 = '';
  updateMsg2 = '';
  clearLocalstorageNo = 0;
  profilePictureUrl = '';

  public appDefaultConfig = {
    appAndroidVersion: '0.0.1',
    appIOSVersion: '0.0.1',
    appAndroidMandatoryUpdate: 'false',
    appIOSMandatoryUpdate: 'true',
    appAndroidServiceAvbl: 'true',
    appIOSServiceAvbl: 'true',
    appAndroidClearLocalstorageNo: '1',
    appIOSClearLocalstorageNo: '1',
    appAndroidServiceDownMsg1: 'We are under maintenance',
    appAndroidServiceDownMsg2: 'We will be right back within 2 hours',
    appAndroidupdateMsg1: 'New update is available',
    appAndroidupdateMsg2: `The current version of this application is no longer supported.
                            We apologize for any inconvenience we may have caused you`,
    appIOSServiceDownMsg1: 'We are under maintenance',
    appIOSServiceDownMsg2: 'We will be right back within 2 hours',
    appIOSupdateMsg1: 'New update is available',
    appIOSupdateMsg2: `The current version of this application is no longer supported.
    We apologize for any inconvenience we may have caused you`,
  };

  getApiUrl(): string {
    this.setApiLink();
    return this.APIBASE;
  }
  getBaseUrl(): string {
    this.setApiLink();
    return this.BaseURL;
  }
  setApiLink() {
    this.APIBASE = '';
    this.BaseURL = '';
    switch (apiEnvironment) {
      case ApiEnvironment.LOCAL:
        this.BaseURL = 'http://18d5d4fa765d.ngrok.io/factweb/';
        this.APIBASE = 'http://18d5d4fa765d.ngrok.io/factweb/';
        break;
      case ApiEnvironment.DEV:
        this.BaseURL = 'http://dev.agrowonmartpos.com';
        this.APIBASE = 'http://dev.agrowonmartpos.com/public/api';
        break;
      case ApiEnvironment.UAT:
        this.BaseURL = 'http://uat.toyotatsushoindia.com/';
        this.APIBASE = 'http://uat.toyotatsushoindia.com/';
        break;
      case ApiEnvironment.LIVE:
        this.BaseURL = 'http://toyotatsushoindia.com/';
        this.APIBASE = 'http://toyotatsushoindia.com/';
        break;
      case ApiEnvironment.NGROK:
        this.BaseURL = 'http://21c434735f73.ngrok.io/WemechWebAndApi/';
        this.APIBASE = 'http://21c434735f73.ngrok.io/WemechWebAndApi/';
        break;
    }
  }
}
