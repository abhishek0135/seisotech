import { Injectable } from '@angular/core';
import { CommonUtility } from './common.utility';
import { AppConstants } from 'src/app/utility/app.constants';

@Injectable({
  providedIn: 'root',
})
export class LocalstorageUtility {
  constructor(private commonUtility: CommonUtility) {}

  private ProcessValue(value) {
    if (!this.commonUtility.isEmptyOrNull(value)) {
      return value;
    } else {
      return '';
    }
  }

  public getCustomerId() {
    const value = this.commonUtility.getLocalstorage(
      AppConstants.localstorageKeys.customerId
    );
    return this.ProcessValue(value);
  }

  public clearLoginData() {
    this.commonUtility.removeLocalstorageItem(
      AppConstants.localstorageKeys.isLogin
    );
    this.commonUtility.removeLocalstorageItem(
      AppConstants.localstorageKeys.customerId
    );
    this.commonUtility.removeLocalstorageItem(
      AppConstants.localstorageKeys.countryId
    );
    this.commonUtility.removeLocalstorageItem(
      AppConstants.localstorageKeys.roleId
    );
    this.commonUtility.removeLocalstorageItem(
      AppConstants.localstorageKeys.fullname
    );
  }
}
