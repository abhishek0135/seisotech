export enum ApiEnvironment {
  LOCAL = 1,
  DEV = 2,
  UAT = 3,
  LIVE = 4,
  NGROK = 5,
}
export enum ApiType {
  'GET' = 1,
  'POST' = 2,
  'PUT' = 3,
  'DELETE' = 4,
}
export enum HttpStatus {
  Unauthorized = 401,
  BadRequest = 400,
  Forbidden = 403,
}
