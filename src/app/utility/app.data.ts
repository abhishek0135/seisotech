import { Injectable } from '@angular/core';
import { AppConstants } from './app.constants';

@Injectable({
  providedIn: 'root',
})
export class AppData {
  public categories: any[] = [];
  public featuredProducts: any[] = [];
  public products: any[] = [];
  public bestSellerProducts: any[] = [];
  public dashboard: any = {};
  public user: any = {};
  public shoppingCart: any = ([] = []);
  public orderDetails: any = {};
  public billingAddress: any[] = [];
  public paymentInfo: any[] = [];
  public shippingInfo: any[] = [];
  public directReportData = [];
  constructor() {
    this.prepareCategoryData();
    this.prepatreFeaturedProducts();
    this.prepareProducts();
    this.prepatreBestSellerProducts();
    this.prepareUserData();
    this.prepareDashboard();
    this.prepareShoppingCart();
    this.prepareOrderDetails();
    this.getDirectReportData();
  }

  private prepareCategoryData() {
    this.categories = [
      {
        name: 'Health Care',
        image:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000190_health-care_600.jpeg',
      },
      {
        name: 'Personal Care',
        image:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000192_home-care_600.jpeg',
      },
      {
        name: 'Agriculture',
        image:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000194_agriculture_600.jpeg',
      },
      {
        name: 'Home Care',
        image:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000192_home-care_600.jpeg',
      },
    ];
  }

  private prepatreFeaturedProducts() {
    this.featuredProducts = [
      {
        name: 'Nutrimark UniAktiv Liver',
        shortDescription:
          'UniAktiv Liver for complete strengthening of liver, regeneration of liver tissues and maintaining its functions.',
        fullDescription: AppConstants.productFullDescription,
        img:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000376_nutrimark-uniaktiv-liver.png',
        dPrice: '500',
        price: '560',
        off: '5% OFF',
      },
      {
        name: 'Nutrimark UniAktiv Joint',
        description:
          'An effective formulation to counter bone and joint related issues.',
        fullDescription: AppConstants.productFullDescription,
        img:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000375_nutrimark-uniaktiv-joint.png',
        dPrice: '500',
        price: '580',
        off: '5% OFF',
      },
      {
        name: 'Nutrimark UniAktiv Trendz',
        description: `Diabetis is life threatening disease with of complications on body. Allopathic medicines only control blood sugar to some extent and do no exert positive effect on complications and diabetis symptoms. Introducing UniAktiveTrendz and holistic approach to control all type of Diabetis, its symptoms and complications.`,
        fullDescription: AppConstants.productFullDescription,
        img:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000377_nutrimark-uniaktiv-trendz.png',
        dPrice: '500',
        price: '620',
        off: '5% OFF',
      },
      {
        name: 'Myristika Face Wash',
        description:
          'A unique face wash with natural safe ingredients which you can completely rely upon to nourish, cleanse, detoxify your skin and make them glow with natural lustre.',
        fullDescription: AppConstants.productFullDescription,
        img:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000186_myristika-face-wash_600.png',
        dPrice: '210',
        price: '300',
        off: '5% OFF',
      },
      {
        name: 'Myristika Face Scrub',
        description:
          'Myristika Face Scrub provides all in one solution and helps clean skin, unclog pores and removing dead skin cells.',
        fullDescription: AppConstants.productFullDescription,
        img:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000185_myristika-face-scrub.png',
        dPrice: '210',
        price: '290',
        off: '5% OFF',
      },
    ];
  }
  private prepatreBestSellerProducts() {
    this.bestSellerProducts = [
      {
        name: 'Myristika Herbal Toothpaste',
        shortDescription:
          'One of its kind revolutionary toothpaste provides benefits like freedom form plaque, tartar, cavities, bad breath and provides strong white teeth.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000182_myristika-herbal-toothpaste.png',
        price: '149',
        oldPrice: '178',
        off: '5% OFF',
      },
      {
        name: 'AFS',
        description:
          'AFS is comprehensive Feed Supplement designed in a unique way for all domesticated animals.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000176_afs.png',
        price: '600',
        oldPrice: '680',
        off: '5% OFF',
      },
      {
        name: 'Nutrimark UniAktiv Joint',
        description:
          'An effective formulation to counter bone and joint related issues.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000375_nutrimark-uniaktiv-joint.png',
        price: '500',
        oldPrice: '580',
        off: '5% OFF',
      },
      {
        name: 'Myristika Face Wash',
        description:
          'A unique face wash with natural safe ingredients which you can completely rely upon to nourish, cleanse, detoxify your skin and make them glow with natural lustre.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000186_myristika-face-wash_600.png',
        price: '210',
        oldPrice: '300',
        off: '5% OFF',
      },
      {
        name: 'UniAktiv Detox',
        description:
          'UniAktiv Detox is wonderful and effective product which helps body detoxify and strengthen your stomach and digestion.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000343_uniaktiv-detox.png',
        price: '500',
        oldPrice: '550',
        off: '5% OFF',
      },
    ];
  }
  private prepareDashboard() {
    const userData = this.getUserData();
    this.dashboard = {
      highlights: {
        totalEarning: '12932.23',
        companyTurnover: '2690480.00',
        freshBV: '16430.00',
        downlineCount: '10',
        lastMonthDownlineCount: '0',
        currentRank: 'Director',
        nextRank: 'NA',
        remainingBVForNextRank: 'NA',
        lastSigninOn: '12-10-2020',
        lastSigninIp: '122.169.99.96',
      },
      userCurrentStatus: {
        name: `${userData.firstName} ${userData.lastName}`,
        memberNo: userData.memberNo,
        statusImg:
          'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAFF0lEQVR42rWXf0zUZRzH0dC2Wrna4ucJgg70H2cRGf4KUSs0mmZpuVxaWz+2LKazskaiCA4LJiLYxATJUMIKMSytwSiW0yKhciIYvwK6HRwHHMfx6+7T5/3cPd/73on8Ovxun90Nvs/zen/ez+f5PM95eEzgKTEUzKvtrdzd2l9fyFFUZ/57f3lX0SMed/q50n0xst9ibqfbPIPWAVNVT/n6OwKv7q1IspJVgPBZrM+mtJZYSm3eRgVth8hsMSlCmvpunJhUeIWx5E0JP9OWTtGFQfRk/ixadSqQVuUFUlROAK3IDaDM1vcVEddMlw9MCnzlycD7JDxHu4+e/iaIos8GCRH4/tSZWUIMv0eRWTPp44YXFZf4/dDJsD4XE940/0mrzwXRmuJg8ekqAG5AxBOfaejnzkK5FKXu8qe0D7Q2Y7Jd9c8pApwcKHAWABc2VIYKAWZLj5nn8HRHwD0W65CwM+bH2QIssrcLQPaKgDybANQDRKAoMe6t2qXBE6Y//1tIIATw1hNAdbjaDwEoxKjsALEM3B+EgPjGTRHuODBjwNpPELHmfLAAAizhrtlLATYHesQ4fneOOwKmcyGJGvigfq3IVILVa6+2H9lv/MtWA4bBNhOW0a0q/MmQn4tMuN0KEKAKWAWX2S/L0NCl7u+F/dw/SlDI7u4Eb86kHxN+qftEgACVoYYj++R/XxfZG4c6iccGuN0H8nUHw/WD2j64gCep6VUBVMA5AUrlv3NzpdKEjEMG63f648+4af/pGJ6I5BJsqp6ntF0Fnu0oPDiwtmwOXTWWKS782lX89oTgh1t2zmbrBRwHDwAIAVSBl38+U/kfBCxJ86dFn/rRce0euwgDnW0/GjXuDsj7WM/HK7ELtCxT44DboTIE/KgNjsC7EPH4fl9xdmA5eAmt4+qIDN2FTgYHMCkAMkuncAFjByCWpmtocao/RST7UUPfddHILndfyBpH9nVGZI+9j2yEAJdwAmc6wAiMWXLQnxYm+tKGqlCxjHymYFdMG5XO0JCuIb0oICWrTM2twBHACOnAwn2+3JbryGTppkMt21eMZdvFwjKsPYpJ2jpcOIHTHGCMQ0Qc8KNH43wo6784XNfooiEvgxFTR+JP5ctlEQRgvyMDCVGHAnTJWMIBxlgEBLxRu0gI4A5ZNlprnl5hLL2CA2j7P9GikiVguJBQJ7AdjrEICNhcPV8KuMSM+0cUUN51rhQOoACxfmqIK1BttSsYBYjxYR96CwFIipvSqAKmcL9PwRY8rUul8Hgfx+S3i+RhwHb4Y3t9aMFOL0ps2iq2Ije0r0Y9HbfeCIvUDTQTQk4mIa4hoeqMJRgB+x9+z0vcJ9FT9jS+vGUsp+O9PKAbLmRrE4SFmOwWWKJztmownAv7yJvmv/sQ7W3cLLJv6qtBH3hgTJ2ID531GICBp3QpIlsIwaThu30cEW/7RKYIvIOMF+zwEt/TW3aIOdAH+LoeO56zwJPXLYFvQ+JqZRjU0bftR+i1mnBxIUUnFMVnXwZ8oinhlvTS9bni3oAlhItIhH+woA3fPd4DyZNvPDG8deplJ0M2EIRAp8RJZ4tO+99Nyju4lPKxrH3hj5AtE4Grnwf51Fsd17Axhav496qeX9pqeq/21JmvDfBBY2GXLPx9kH8t9/L/9NztKhMaXznMTq3jsV6TcSWTz11cD4vPd5zYdqHjZBJHBrfrYww8xp8ZP3R8kcwR+3VbxvKgZ2dMG+uk/wMX+LuBWJ8tZAAAAABJRU5ErkJggg==',
        status: 'Paid',
        statusDate: '04/05/2018',
      },
      downlineRankQualifierCount: 20,
      schemeDetailCount: 13,
      repurchaseSchemeDetailCount: 0,
      fastTrackSchemeDetailCount: 0,
      currentBusinessCount: 10,
      recentjoiningCount: 5,
    };
  }
  private prepareUserData() {
    this.user = {
      firstName: 'NAZIYA',
      middleName: 'IMRAN',
      lastName: 'KAZI',
      memberNo: 'CMEPK6171N',
      emailId: 'naziyakazi1111@gmail.com',
      phoneNo: '8149624367',
      panCardNo: 'CMEPK6171N',
      aadharCardNo: '893328993522',
      address:
        '1st Floor, Premdeep Building, Lullanagar Chowk, Pune , Maharashtra 411040 India',
    };
  }

  private prepareProducts() {
    this.products = [
      {
        name: 'Nutrimark UniAktiv Liver',
        description:
          'UniAktiv Liver for complete strengthening of liver, regeneration of liver tissues and maintaining its functions.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000376_nutrimark-uniaktiv-liver.png',
        price: '500',
        oldPrice: '560',
      },
      {
        name: 'Nutrimark UniAktiv Joint',
        description:
          'An effective formulation to counter bone and joint related issues.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000375_nutrimark-uniaktiv-joint.png',
        price: '500',
        oldPrice: '580',
      },
      {
        name: 'Nutrimark UniAktiv Trendz',
        description: `Diabetis is life threatening disease with of complications on body. Allopathic medicines only control blood sugar to some extent and do no exert positive effect on complications and diabetis symptoms. Introducing UniAktiveTrendz and holistic approach to control all type of Diabetis, its symptoms and complications.`,
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000377_nutrimark-uniaktiv-trendz.png',
        price: '500',
        oldPrice: '620',
      },
      {
        name: 'Myristika Face Wash',
        description:
          'A unique face wash with natural safe ingredients which you can completely rely upon to nourish, cleanse, detoxify your skin and make them glow with natural lustre.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000186_myristika-face-wash_600.png',
        price: '210',
        oldPrice: '300',
      },
      {
        name: 'Myristika Face Scrub',
        description:
          'Myristika Face Scrub provides all in one solution and helps clean skin, unclog pores and removing dead skin cells.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000185_myristika-face-scrub.png',
        price: '210',
        oldPrice: '290',
      },
      {
        name: 'Myristika Herbal Toothpaste',
        description:
          'One of its kind revolutionary toothpaste provides benefits like freedom form plaque, tartar, cavities, bad breath and provides strong white teeth.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000182_myristika-herbal-toothpaste.png',
        price: '134',
        oldPrice: '200',
      },
      {
        name: 'Uniactiv Protein',
        description:
          'Proper nutrition is way to healthy and active life. Imbalance of nutrition is prime cause of disorder. And it adds up with today’s hectic life style and junk & adulterated food. Look no further as we have one of the best nutritional supplements in the form of protein powder Uniaktiv Protein fortified with premium ingredients.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000218_uniactiv-protein.png',
        price: '900',
        oldPrice: '1140',
      },
    ];
  }

  private prepareShoppingCart() {
    this.shoppingCart = [
      {
        name: 'Uniactiv Protein',
        description:
          'Proper nutrition is way to healthy and active life. Imbalance of nutrition is prime cause of disorder. And it adds up with today’s hectic life style and junk & adulterated food. Look no further as we have one of the best nutritional supplements in the form of protein powder Uniaktiv Protein fortified with premium ingredients.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000218_uniactiv-protein.png',
        price: '900',
        oldPrice: '1140',
      },
      {
        name: 'Myristika Face Scrub',
        description:
          'Myristika Face Scrub provides all in one solution and helps clean skin, unclog pores and removing dead skin cells.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000185_myristika-face-scrub.png',
        price: '210',
        oldPrice: '290',
      },
      {
        name: 'Uniactiv Protein',
        description:
          'Proper nutrition is way to healthy and active life. Imbalance of nutrition is prime cause of disorder. And it adds up with today’s hectic life style and junk & adulterated food. Look no further as we have one of the best nutritional supplements in the form of protein powder Uniaktiv Protein fortified with premium ingredients.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000218_uniactiv-protein.png',
        price: '900',
        oldPrice: '1140',
      },
      {
        name: 'Nutrimark UniAktiv Trendz',
        description: `Diabetis is life threatening disease with of complications on body. Allopathic medicines only control blood sugar to some extent and do no exert positive effect on complications and diabetis symptoms. Introducing UniAktiveTrendz and holistic approach to control all type of Diabetis, its symptoms and complications.`,
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000377_nutrimark-uniaktiv-trendz.png',
        price: '500',
        oldPrice: '620',
      },
      {
        name: 'Myristika Face Wash',
        description:
          'A unique face wash with natural safe ingredients which you can completely rely upon to nourish, cleanse, detoxify your skin and make them glow with natural lustre.',
        fullDescription: AppConstants.productFullDescription,
        imageUrl:
          'https://unikart.unilinkbiz.com/content/images/thumbs/0000186_myristika-face-wash_600.png',
        price: '210',
        oldPrice: '300',
      },
    ];
  }
  prepareOrderDetails() {
    const user = this.getUserData();
    const products = this.getProducts();
    const orderItemList: any[] = [];
    orderItemList.push(products[0]);
    orderItemList.push(products[1]);
    this.orderDetails = {
      paymentInfo: {
        payment: 'Bank Transfer',
        paymentMethod: 'EWallet',
        paymentStatus: 'paid',
        remainingAmount: '100',
      },
      shippingInfo: {
        shippingMethod: 'Ground',
        shippingStatus: 'Completed',
      },
      billingAddress: {
        firstName: user.firstName,
        middleName: user.middleName,
        lastName: user.lastName,
        memberNo: user.memberNo,
        emailId: user.emailId,
        phoneNo: user.phoneNo,
        panCardNo: user.panCardNo,
        aadharCardNo: user.aadharCardNo,
        address: user.address,
      },
      shipmentsInfo: {
        trackingNumber: '2324081001',
        dateShipped: 'August 28, 2018',
        dateDelivered: 'August 30, 2018',
      },
      orderItems: orderItemList,
    };
  }

  getCategories() {
    return this.categories;
  }
  getFeaturedProducts() {
    return this.featuredProducts;
  }
  getProducts() {
    return this.products;
  }
  getBestSellerProducts() {
    return this.bestSellerProducts;
  }
  getDashboardData() {
    return this.dashboard;
  }
  getUserData() {
    return this.user;
  }
  getShoppingCartData() {
    return this.shoppingCart;
  }
  getOrderDetails() {
    return this.orderDetails;
  }
  getDirectReportData() {
    return (this.directReportData = [
      {
        status: 'Paid',
        name: 'Manipur A1',
        id: '84473',
        mobileNumber: '9622881511',
        doj: '13/03/2020',
      },
      {
        status: 'Paid',
        name: 'Komal Harshal Bankar',
        id: '100374',
        mobileNumber: '9822885511',
        doj: '11/03/2020',
      },
      {
        status: 'Paid',
        name: 'Komal Harshal Bankar',
        id: '100374',
        mobileNumber: '8421423445',
        doj: '09/05/2020',
      },
    ]);
  }
  getDirectBusinessReportData() {
    let report = [];
    return (report = [
      {
        status: 'Paid',
        name: 'Manipur A1',
        id: '84473',
        mobileNumber: '9622881511',
        directBusiness: '154',
      },
      {
        status: 'Paid',
        name: 'Komal Harshal Bankar',
        id: '100374',
        mobileNumber: '9822885511',
        directBusiness: '44154',
      },
      {
        status: 'Paid',
        name: 'Komal Harshal Bankar',
        id: '100374',
        mobileNumber: '8421423445',
        directBusiness: '15454',
      },
    ]);
  }
  getCommissionLedger() {
    let report = [];
    return (report = [
      {
        payoutNo: '11',
        amount: '1335',
        date: '03/04/2019',
        remark: 'Against Monthly Bonus Payout No.11',
      },
      {
        payoutNo: '10',
        amount: '1126',
        date: '02/03/2019',
        remark: 'Against Monthly Bonus Payout No.10',
      },
      {
        payoutNo: '09',
        amount: '1735',
        date: '02/02/2019',
        remark: 'Against Monthly Bonus Payout No.09',
      },
      {
        payoutNo: '08',
        amount: '1899',
        date: '02/01/2019',
        remark: 'Against Monthly Bonus Payout No.08',
      },
      {
        payoutNo: '07',
        amount: '1363',
        date: '03/12/208',
        remark: 'Against Monthly Bonus Payout No.07',
      },
    ]);
  }
  getTDSDetails() {
    let report = [];
    return (report = [
      {
        payoutNo: '11',
        grossIncome: '13335',
        fromDate: '03/03/2019',
        toDate: '03/04/2019',
        incomeType: 'None',
        TDSDeducted: '1502',
        TDSPercentage: '10%',
      },
      {
        payoutNo: '09',
        grossIncome: '11335',
        fromDate: '03/02/2019',
        toDate: '03/03/2019',
        incomeType: 'None',
        TDSDeducted: '1502',
        TDSPercentage: '10%',
      },
      {
        payoutNo: '08',
        grossIncome: '10335',
        fromDate: '03/01/2019',
        toDate: '03/02/2019',
        incomeType: 'None',
        TDSDeducted: '1502',
        TDSPercentage: '10%',
      },
    ]);
  }
  getCommissionPointDetails() {
    let report = [];
    return (report = [
      {
        incomeType: 'Car fund',
        heighestLegBV: '55615',
        secondHeighestLegBV: '31205',
        qualificationFactor: '1000.00',
        totalPoints: '31',
        pointValue: '57.95',
        income: '1796.45',
      },
      {
        incomeType: 'Car fund',
        heighestLegBV: '55615',
        secondHeighestLegBV: '31205',
        qualificationFactor: '1000.00',
        totalPoints: '31',
        pointValue: '57.95',
        income: '1796.45',
      },
      {
        incomeType: 'Car fund',
        heighestLegBV: '55615',
        secondHeighestLegBV: '31205',
        qualificationFactor: '1000.00',
        totalPoints: '31',
        pointValue: '57.95',
        income: '1796.45',
      },
    ]);
  }
  getFundQualificationReport() {
    let report = [];
    return (report = [
      {
        name: 'Akshay kailash gaikwad',
        type: 'Car fund',
        qualificationDate: '30-11-2019',
      },
      {
        name: 'Prashant suresh nagane',
        type: 'Car fund',
        qualificationDate: '30-11-2019',
      },
    ]);
  }
  getFreshQualificationReport() {
    let report = [];
    return (report = [
      {
        name: 'Akshay kailash gaikwad',
        id: '10037423',
        qualificationDate: '30-11-2019',
      },
      {
        name: 'Prashant suresh nagane',
        id: '10037432',
        qualificationDate: '03-11-2020',
      },
    ]);
  }
  getReferrelBonusBusinessReport() {
    let report = [];
    return (report = [
      {
        name: 'Ajoy bhattacharjee',
        id: 'DPXPB4927J',
        distributorID: 'AEBPU5586J',
        distributorName: 'Usman Nisar ALi',
        orderNo: '18749',
        orderDate: '04-09-2020',
        orderAmount: '974',
        referrelBouns: '10',
        TDSamount: '30',
        adminAmount: '15',
        netamount: '900',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        distributorID: 'AZIPM2202P',
        distributorName: 'Nitten Mahadik',
        orderNo: '16979',
        orderDate: '29-02-2020',
        orderAmount: '2183.00',
        referrelBouns: '436.60',
        TDSamount: '22.00',
        adminAmount: '22.00',
        netamount: '392.60',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        distributorID: 'DBMPS6323E',
        distributorName: 'Devendra Kumar Sahu',
        orderNo: '16004',
        orderDate: '18-02-2020',
        orderAmount: '1829.00',
        referrelBouns: '365.80',
        TDSamount: '18.00',
        adminAmount: '18.00',
        netamount: '329.80',
      },
    ]);
  }
  getDBQualificationReport() {
    let report = [];
    return (report = [
      {
        name: 'Diamond Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Diamond Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Gold Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Gold Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Superstar Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Superstar Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Superstar Director',
        qualificationDate: '20-02-2020',
      },
      {
        name: 'Superstar Director',
        qualificationDate: '20-02-2020',
      },
    ]);
  }
  getGBVIncomeDetails() {
    let report = [];
    return (report = [
      {
        legDirectorName: 'Imran Moin Kazi(BEOPK6524A)',
        business: '4375.00',
        gbvPercentage: '1.00',
        gbvPoints: '43.75',
        gbvPointValue: '2.84',
        gbvIncome: '124.25',
        capping: 'NO',
        lapsedPoints: '0.00',
      },
      {
        legDirectorName: 'Imran Moin Kazi(BEOPK6524A)',
        business: '4375.00',
        gbvPercentage: '1.00',
        gbvPoints: '43.75',
        gbvPointValue: '2.84',
        gbvIncome: '124.25',
        capping: 'NO',
        lapsedPoints: '0.00',
      },
    ]);
  }
  getLBIncomeDetails() {
    let report = [];
    return (report = [
      {
        legNo: '23',
        legDirectName: 'Imran Moin Kazi',
        business: '43',
        lbPercentage: '10%',
        lbpoints: '43.75',
        lbpointValue: '2.84',
        lbIncome: '124.25',
      },
      {
        legNo: '23',
        legDirectName: 'Imran Moin Kazi',
        business: '43',
        lbPercentage: '20%',
        lbpoints: '43.75',
        lbpointValue: '2.84',
        lbIncome: '124.25',
      },
    ]);
  }
  getDBIncomeDetails() {
    let report = [];
    return (report = [
      {
        id: 'BEOPK6524A',
        name: 'Imran Moin Kazi',
        levelNumber: '3',
        groupBV: '43',
        points: '43.75',
        pointValue: '2.84',
        totalIncome: '124.25',
      },
      {
        id: 'BEOPK6524A',
        name: 'Imran Moin Kazi',
        levelNumber: '3',
        groupBV: '43',
        points: '43.75',
        pointValue: '2.84',
        totalIncome: '124.25',
      },
    ]);
  }
  getDownloadableDocuments() {
    let report = [];
    return (report = [
      {
        DocumentName: 'Downloadable_Document 1',
        FileName: 'File 1.doc',
      },
      {
        DocumentName: 'Downloadable_Document 2',
        FileName: 'File 2.docx',
      },
      {
        DocumentName: 'Downloadable_Document 3',
        FileName: 'File 3.pdf',
      },
      {
        DocumentName: 'Downloadable_Document 4',
        FileName: 'File 4.png',
      },
      {
        DocumentName: 'Downloadable_Document 5',
        FileName: 'File 5.xls',
      },
      {
        DocumentName: 'Downloadable_Document 6',
        FileName: 'File 6.txt',
      },
    ]);
  }
  getTDSDocuments() {
    let report = [];
    return (report = [
      {
        FiledId: '1',
        FiledName: 'CMEPK6171N_TDS_2019_20',
        DownloadFiles: 'File 2.doc',
      },
      {
        FiledId: '2',
        FiledName: 'CMEPK6171N_TDSQ3',
        DownloadFiles: 'File 2.docx',
      },
      {
        FiledId: '3',
        FiledName: 'CMEPK6171N_certificate',
        DownloadFiles: 'File 3.pdf',
      },
      {
        FiledId: '4',
        FiledName: 'CMEPK6171N_TDSQ2',
        DownloadFiles: 'File 4.png',
      },
      {
        FiledId: '5',
        FiledName: 'CMEPK6171N_TDS 4',
        DownloadFiles: 'File 5.xls',
      },
      {
        FiledId: '6',
        FiledName: 'CMEPK6171N_TDS 4',
        DownloadFiles: 'File 6.txt',
      },
    ]);
  }
  getInboxSentMessage() {
    let InboxSentMessage = [];
    return (InboxSentMessage = [
      {
        ID: '1',
        Name: 'Fix',
        Imformation: "Listen, I've had a pretty messed up day...",
        Date: '4 Otc 2020',
        IsRead: true,
      },
      {
        ID: '2',
        Name: 'fieldata',
        Imformation: "Listen, I've had a pretty messed up day...",
        Date: '4 Otc 2020',
        IsRead: false,
      },
      {
        ID: '3',
        Name: 'Ffieldataix',
        Imformation: "Listen, I've had a pretty messed up day...",
        Date: '4 Otc 2020',
        IsRead: true,
      },
      {
        ID: '4',
        Name: 'mahesh',
        Imformation: "Listen, I've had a pretty messed up day...",
        Date: '4 Otc 2020',
        IsRead: false,
      },
      {
        ID: '5',
        Name: 'ummar',
        Imformation: "Listen, I've had a pretty messed up day...",
        Date: '4 Otc 2020',
        IsRead: true,
      },
    ]);
  }
  getFasttrackSchemeQualifiers() {
    let report = [];
    return (report = [
      {
        name: 'Ajoy bhattacharjee',
        id: 'DPXPB4927J',
        qualificationDate: '20-02-2020',
        rank: 'Rank value',
        year: '2020',
        month: 'oct',
      },
      {
        name: 'Imran Moin Kazi',
        id: 'BEOPK6524A',
        qualificationDate: '20-02-2020',
        rank: 'Rank value',
        year: '2020',
        month: 'oct',
      },
    ]);
  }
  getFasttrackSchemeDetails() {
    let report = [];
    return (report = [
      {
        name: 'Ajoy bhattacharjee',
        id: 'DPXPB4927J',
        rank: 'Rank value',
        transactionDate: '20-02-2020',
        requiredGroupBV: '23',
        actualBV: '50',
        year: '2020',
        month: 'oct',
        status: 'Active',
      },
      {
        name: 'Imran Moin Kazi',
        id: 'BEOPK6524A',
        rank: 'Rank value',
        transactionDate: '20-02-2020',
        requiredGroupBV: '23',
        actualBV: '50',
        year: '2019',
        month: 'nov',
        status: 'Active',
      },
      {
        name: 'Ajoy bhattacharjee',
        id: 'DPXPB4927J',
        rank: 'Rank value',
        transactionDate: '20-02-2020',
        requiredGroupBV: '23',
        actualBV: '50',
        year: '2020',
        month: 'oct',
        status: 'Active',
      },
    ]);
  }
  getMonthlySchemeDetails() {
    let report = [];
    return (report = [
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        directId: 'BEOPK6524A',
        directName: 'Imran Moin Kazi',
        transactionDate: '31/07/2019',
        groupBV: '10045.00',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        directId: 'DZCPD1238K',
        directName: 'Pooja Manjit Jajoo',
        transactionDate: '31/07/2019',
        groupBV: '175.00',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        directId: 'BYKPS9192J',
        directName: 'Shenaz Rehan Patel',
        transactionDate: '30/04/2019',
        groupBV: '250.00',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        directId: 'ECJPK1839N	',
        directName: 'AFROZ JAHIRODDIN KAZI',
        transactionDate: '30/04/2019',
        groupBV: '3000.00',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        directId: 'BYKPS9192J',
        directName: 'Shenaz Rehan Patel',
        transactionDate: '30/04/2019',
        groupBV: '250.00',
      },
    ]);
  }
  getRepurchaseSchemeQualifiersDetails() {
    let report = [];
    return (report = [
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '31/12/2019',
        count: '1',
        status: 'In-Process',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '30/09/2020',
        count: '1',
        status: 'In-Process',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '31/12/2019',
        count: '1',
        status: 'In-Process',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '30/09/2020',
        count: '1',
        status: 'In-Process',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '31/12/2019',
        count: '1',
        status: 'In-Process',
      },
    ]);
  }
  getMonthlySchemeQualifications() {
    let report = [];
    return (report = [
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '31/12/2019',
        schemeName: 'Diwali 2020 special scheme',
        offerName: 'Diwali special offer',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '06/12/2019',
        schemeName: 'Diwali 2020 special scheme',
        offerName: 'Diwali special offer',
      },
      {
        name: 'NAZIYA IMRAN KAZI',
        id: 'CMEPK6171N',
        qualificationDate: '06/06/2020',
        schemeName: 'Diwali 2020 special scheme',
        offerName: 'Diwali special offer',
      },
    ]);
  }
  getDashboardSchemeDetails() {
    let schemeDetails = [];
    return (schemeDetails = [
      {
        name: 'Unilink Leisure Label 1',
        startDate: '31/03/2020',
        endDate: '31/03/2020',
        schemeDescription: 'Unilink Leisure Label 1 Jan to March 20',
        offerName: 'Unilink Leisure Label 1',
        groupBV: 100,
        pendingGroupBV: 200,
        numberOfLeg: 3,
      },
      {
        name: 'Unilink Leisure Label 2',
        startDate: '31/03/2020',
        endDate: '01/01/2020',
        schemeDescription: 'Unilink Leisure Label 2',
        groupBV: 200,
        pendingGroupBV: 300,
        numberOfLeg: 5,
      },
      {
        name: 'Lucky Draw Jan to march 19',
        startDate: '31/03/2020',
        endDate: '31/03/2020',
        schemeDescription: 'Lucky Draw Jan to march 19',
        groupBV: 50,
        pendingGroupBV: 30,
        numberOfLeg: 2,
      },
      {
        name: 'Gift Offer Feb 20 to March 20',
        startDate: '31/03/2020',
        endDate: '01/01/2020',
        schemeDescription: 'Gift Offer Feb 20 to March 20',
        groupBV: 10,
        pendingGroupBV: 500,
        numberOfLeg: 10,
      },
      {
        name: 'LOT March 2020',
        startDate: '01/04/2020',
        endDate: '30/04/2020',
        schemeDescription: 'LOT March 2020',
        groupBV: 600,
        pendingGroupBV: 300,
        numberOfLeg: 4,
      },
      {
        name: 'Gift Offer Feb 20 to March 20',
        startDate: '31/03/2020',
        endDate: '01/01/2020',
        schemeDescription: 'Gift Offer Feb 20 to March 20',
        groupBV: 100,
        pendingGroupBV: 40,
        numberOfLeg: 1,
      },
    ]);
  }
}
