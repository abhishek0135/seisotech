import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AppHeaderComponent } from './app-header/app-header.component';
import { RecordCountComponent } from './record-count/record-count.component';
import { ExpandableComponent } from './expandable/expandable.component';
import { TreeElementComponent } from './tree-element/tree-element.component';
import { MenuPopoverComponent } from './menu-popover/menu-popover.component';
import { ButtonSpinnerComponent } from './button-spinner/button-spinner.component';
import { CustomLoaderComponent } from './custom-loader/custom-loader.component';

@NgModule({
  imports: [IonicModule, CommonModule],
  declarations: [
    AppHeaderComponent,
    RecordCountComponent,
    ExpandableComponent,
    TreeElementComponent,
    MenuPopoverComponent,
    ButtonSpinnerComponent,
    CustomLoaderComponent,
  ],
  exports: [
    AppHeaderComponent,
    RecordCountComponent,
    ExpandableComponent,
    TreeElementComponent,
    MenuPopoverComponent,
    ButtonSpinnerComponent,
    CustomLoaderComponent,
  ],
  entryComponents: [],
})
export class ComponentModule {}
