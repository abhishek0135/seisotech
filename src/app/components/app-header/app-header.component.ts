import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Values } from 'src/app/utility/app.variables';
import { MenuPopoverComponent } from '../../components/menu-popover/menu-popover.component';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
})
export class AppHeaderComponent implements OnInit {
  @Input() titleText: string;
  @Input() badgeCount: number = 5;
  @Input() menuButton: boolean;
  @Input() backButton: boolean;
  @Input() toShowCartButton: boolean;
  @Input() toShowAddButton: boolean;
  @Input() toShowProfileButton: boolean;
  @Input() toShowPopoverButton: boolean;

  constructor(
    private navController: NavController,
    public popoverController: PopoverController,
    public values: Values
  ) {}

  ngOnInit() {}
  goBack() {
    this.navController.pop();
  }
  goToCart() {
    this.navController.navigateForward(['shopping-cart']);
  }
  addAddress() {
    this.navController.navigateForward(['add-address']);
  }
  async openPopover(ev: any) {
    console.log(ev.view.location.pathname);
    const popover = await this.popoverController.create({
      component: MenuPopoverComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        pathName: ev.view.location.pathname,
      },
      event: ev,
      translucent: true,
    });
    return await popover.present();
  }
}
