import { Component, Input, NgZone, OnInit, SimpleChanges } from '@angular/core';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-record-count',
  templateUrl: './record-count.component.html',
  styleUrls: ['./record-count.component.scss'],
})
export class RecordCountComponent implements OnInit {
  @Input() totalRecordCount: number = 0;
  @Input() recordCount: number = 0;
  @Input() contentRefrence: IonContent;
  constructor(private ngZone: NgZone) { }

  ngOnInit() { }
  scrollTotop() {
    this.contentRefrence.scrollToTop();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.ngZone.run(() => {
      this.recordCount = changes.recordCount.currentValue;
    });
  }
}
