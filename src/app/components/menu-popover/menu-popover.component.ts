import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-menu-popover',
  templateUrl: './menu-popover.component.html',
  styleUrls: ['./menu-popover.component.scss'],
})
export class MenuPopoverComponent implements OnInit {
  pathName;
  constructor(public navParams:NavParams) {
     this.pathName = this.navParams.get('pathName');
     console.log(this.pathName);
   }

  ngOnInit() {}

}
