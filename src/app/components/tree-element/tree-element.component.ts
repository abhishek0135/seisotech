import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tree-element',
  templateUrl: './tree-element.component.html',
  styleUrls: ['./tree-element.component.scss'],
})
export class TreeElementComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  viewMemberInfo(e) {
    e.stopPropagation();
    alert('Member Info');
  }
  memberTreeView() {
    alert('Member Tree View');
  }

}
