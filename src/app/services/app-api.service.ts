import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstants } from '../utility/app.constants';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root',
})
export class AppApiService {
  constructor(public baseApi: BaseApiService) {}
  getCategories(): Observable<any[]> {
    const formData = new FormData();
    return this.baseApi.postApiCall(
      AppConstants.Action.getCategoryList,
      formData,
      []
    );
  }

  getProducts(productId, brandId, catId, priceRange): Observable<any[]> {
    const formData = new FormData();
    formData.append('user_id', AppConstants.userId.toString());
    formData.append('product_id', productId);
    formData.append('brand_id', brandId);
    formData.append('cat_id', catId);
    formData.append('price_range', priceRange);
    return this.baseApi.postApiCall(
      AppConstants.Action.getProductList,
      formData,
      []
    );
  }

  getProduct_Search(search): Observable<any[]> {
    const formData = new FormData();
    formData.append('user_id', AppConstants.userId.toString());
    formData.append('search', search);
    return this.baseApi.postApiCall(
      AppConstants.Action.getProductSearch,
      formData,
      []
    );
  }

  addProductToCart(productId): Observable<any[]> {
    const formData = new FormData();
    formData.append('user_id', AppConstants.userId.toString());
    formData.append('product_id', productId);
    formData.append('action', 'insert');
    return this.baseApi.postApiCall(
      AppConstants.Action.cartAction,
      formData,
      []
    );
  }
}
