import { Injectable } from '@angular/core';
import { Observable, Subscription, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { NavController, Platform } from '@ionic/angular';
import { CommonUtility } from '../utility/common.utility';
import { Values } from '../utility/app.variables';
import { LocalstorageUtility } from '../utility/localstorage.utility';
import { ApiType, HttpStatus } from '../utility/enums';

@Injectable({
  providedIn: 'root',
})
export class BaseApiService {
  API_URL = '';
  subscription: Subscription;
  isNetAvailable = true;
  constructor(
    public commonUtility: CommonUtility,
    public http: HttpClient,
    private nav: NavController,
    public values: Values,
    public localstorageService: LocalstorageUtility,
    public platform: Platform
  ) {
    this.API_URL = this.values.getApiUrl();
  }

  public getApiCall(action, parameters): Observable<any> {
    const apiCallUrl = this.generateFullURL(ApiType.GET, action, parameters);
    const reqHeaderOption = this.getHeaderRequestOptions();
    console.log(reqHeaderOption);
    return this.http
      .get(apiCallUrl, reqHeaderOption)
      .pipe(retry(3), catchError(this.handleError));
  }

  public postApiCall(action, postData, param): Observable<any> {
    const apiCallUrl = this.generateFullURL(ApiType.POST, action, param);
    const reqHeaderOption = this.getHeaderRequestOptions();
    return this.http
      .post(apiCallUrl, postData)
      .pipe(retry(3), catchError(this.handleError));
  }

  private getHeaderRequestOptions(): any {
    const addHeaders = new Headers();
    // addHeaders.append('Content-Type', 'application/json');
    // addHeaders.append(
    //   'AuthorizedToken',
    //   this.localstorageService.getCustomerId()
    // );
    // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());
    return { headers: addHeaders };
  }

  private generateFullURL(apiType, action, parameters) {
    let apiCallUrl = this.API_URL;
    let queryString = '';
    if (action !== '') {
      apiCallUrl = apiCallUrl + '/' + action;
    }
    switch (apiType) {
      case ApiType.GET:
      case ApiType.PUT:
      case ApiType.DELETE:
        if (parameters.length > 0) {
          queryString = '?';
          for (let index = 0; index < parameters.length; index++) {
            if (index > 0) {
              queryString = queryString + '&';
            }
            const parameterKeyValue = parameters[index];
            queryString =
              queryString +
              '' +
              parameterKeyValue[0] +
              '=' +
              parameterKeyValue[1];
          }
          apiCallUrl = apiCallUrl + '' + queryString;
        }
    }
    return apiCallUrl;
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    switch (error.status) {
      case HttpStatus.BadRequest:
        errorMessage = error._body;
        break;
      case HttpStatus.Unauthorized:
        errorMessage = error._body;
        this.localstorageService.clearLoginData();
        this.nav.navigateRoot(['/login']);
        break;
      case HttpStatus.Forbidden:
        errorMessage = error._body;
        this.localstorageService.clearLoginData();
        this.nav.navigateRoot(['/login']);
        break;
    }
    this.commonUtility.showErrorToast(errorMessage);
    return throwError(errorMessage);
  }
}
