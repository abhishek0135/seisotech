import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  dashboardData: any;
  constructor(private dataService: AppData,
              private navCltr: NavController
    ) {
    this.dashboardData = this.dataService.getDashboardData();
  }

  ngOnInit() {
  }

  dashboardSchemeDetails() {
    this.navCltr.navigateForward('dashboard-scheme-details');
  }
  repurchaseSchemeDetails() {
    this.navCltr.navigateForward('dashboard-repurchase-scheme');
  }
}
