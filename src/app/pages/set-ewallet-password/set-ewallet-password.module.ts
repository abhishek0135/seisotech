import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetEwalletPasswordPageRoutingModule } from './set-ewallet-password-routing.module';

import { SetEwalletPasswordPage } from './set-ewallet-password.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    SetEwalletPasswordPageRoutingModule
  ],
  declarations: [SetEwalletPasswordPage]
})
export class SetEwalletPasswordPageModule {}
