import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetEwalletPasswordPage } from './set-ewallet-password.page';

const routes: Routes = [
  {
    path: '',
    component: SetEwalletPasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetEwalletPasswordPageRoutingModule {}
