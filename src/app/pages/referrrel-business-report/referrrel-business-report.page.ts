import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-referrrel-business-report',
  templateUrl: './referrrel-business-report.page.html',
  styleUrls: ['./referrrel-business-report.page.scss'],
})
export class ReferrrelBusinessReportPage implements OnInit {
  referrelBusinessReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.referrelBusinessReport = this.appData.getReferrelBonusBusinessReport();
  }

  ngOnInit() {
  }

}

