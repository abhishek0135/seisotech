import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReferrrelBusinessReportPageRoutingModule } from './referrrel-business-report-routing.module';

import { ReferrrelBusinessReportPage } from './referrrel-business-report.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    ReferrrelBusinessReportPageRoutingModule
  ],
  declarations: [ReferrrelBusinessReportPage]
})
export class ReferrrelBusinessReportPageModule {}
