import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReferrrelBusinessReportPage } from './referrrel-business-report.page';

const routes: Routes = [
  {
    path: '',
    component: ReferrrelBusinessReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReferrrelBusinessReportPageRoutingModule {}
