import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReferrrelBusinessReportPage } from './referrrel-business-report.page';

describe('ReferrrelBusinessReportPage', () => {
  let component: ReferrrelBusinessReportPage;
  let fixture: ComponentFixture<ReferrrelBusinessReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferrrelBusinessReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReferrrelBusinessReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
