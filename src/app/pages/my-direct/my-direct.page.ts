import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-my-direct',
  templateUrl: './my-direct.page.html',
  styleUrls: ['./my-direct.page.scss'],
})
export class MyDirectPage implements OnInit {
  @ViewChild(IonContent) contentPart: IonContent;
  myDirectReportData = [];

  myDownlineReportData: any = {};
  constructor(private appData: AppData) {
   this.myDirectReportData = this.appData.getDirectReportData();
   debugger
  }

  ngOnInit() {
  }

}

