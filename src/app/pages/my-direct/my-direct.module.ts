import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyDirectPageRoutingModule } from './my-direct-routing.module';

import { MyDirectPage } from './my-direct.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyDirectPageRoutingModule,
    ComponentModule
  ],
  declarations: [MyDirectPage]
})
export class MyDirectPageModule {}
