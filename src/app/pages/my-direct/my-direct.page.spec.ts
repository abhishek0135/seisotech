import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyDirectPage } from './my-direct.page';

describe('MyDirectPage', () => {
  let component: MyDirectPage;
  let fixture: ComponentFixture<MyDirectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDirectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyDirectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
