import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyDirectPage } from './my-direct.page';

const routes: Routes = [
  {
    path: '',
    component: MyDirectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyDirectPageRoutingModule {}
