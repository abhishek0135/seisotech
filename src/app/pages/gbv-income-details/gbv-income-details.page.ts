import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-gbv-income-details',
  templateUrl: './gbv-income-details.page.html',
  styleUrls: ['./gbv-income-details.page.scss'],
})
export class GbvIncomeDetailsPage implements OnInit {
  gbvIncomeDetailsReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.gbvIncomeDetailsReport = this.appData.getGBVIncomeDetails();
  }

  ngOnInit() {
  }

}