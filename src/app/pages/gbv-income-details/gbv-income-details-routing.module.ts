import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GbvIncomeDetailsPage } from './gbv-income-details.page';

const routes: Routes = [
  {
    path: '',
    component: GbvIncomeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GbvIncomeDetailsPageRoutingModule {}
