import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GbvIncomeDetailsPageRoutingModule } from './gbv-income-details-routing.module';

import { GbvIncomeDetailsPage } from './gbv-income-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    GbvIncomeDetailsPageRoutingModule
  ],
  declarations: [GbvIncomeDetailsPage]
})
export class GbvIncomeDetailsPageModule {}
