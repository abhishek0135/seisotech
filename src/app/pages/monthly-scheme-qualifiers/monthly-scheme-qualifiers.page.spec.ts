import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MonthlySchemeQualifiersPage } from './monthly-scheme-qualifiers.page';

describe('MonthlySchemeQualifiersPage', () => {
  let component: MonthlySchemeQualifiersPage;
  let fixture: ComponentFixture<MonthlySchemeQualifiersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlySchemeQualifiersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MonthlySchemeQualifiersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
