import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonthlySchemeQualifiersPage } from './monthly-scheme-qualifiers.page';

const routes: Routes = [
  {
    path: '',
    component: MonthlySchemeQualifiersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonthlySchemeQualifiersPageRoutingModule {}
