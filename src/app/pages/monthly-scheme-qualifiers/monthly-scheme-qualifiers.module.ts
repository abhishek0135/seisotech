import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonthlySchemeQualifiersPageRoutingModule } from './monthly-scheme-qualifiers-routing.module';

import { MonthlySchemeQualifiersPage } from './monthly-scheme-qualifiers.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    MonthlySchemeQualifiersPageRoutingModule
  ],
  declarations: [MonthlySchemeQualifiersPage]
})
export class MonthlySchemeQualifiersPageModule {}
