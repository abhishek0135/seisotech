import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';
@Component({
  selector: 'app-monthly-scheme-qualifiers',
  templateUrl: './monthly-scheme-qualifiers.page.html',
  styleUrls: ['./monthly-scheme-qualifiers.page.scss'],
})
export class MonthlySchemeQualifiersPage implements OnInit {
  repurchaseSchemeQualifiers = []
  constructor(public AppData: AppData, public appConstants: AppConstants) {
    this.repurchaseSchemeQualifiers = this.AppData.getMonthlySchemeQualifications();
  }

  ngOnInit() {
  }
}