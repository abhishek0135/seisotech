import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FiltermodalPage } from './filtermodal.page';

describe('FiltermodalPage', () => {
  let component: FiltermodalPage;
  let fixture: ComponentFixture<FiltermodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltermodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FiltermodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
