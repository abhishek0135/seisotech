import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { AppApiService } from 'src/app/services/app-api.service';
// import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-filtermodal',
  templateUrl: './filtermodal.page.html',
  styleUrls: ['./filtermodal.page.scss'],
})
export class FiltermodalPage implements OnInit {

  categoryArray:any=[];
  brandArray:any=[];
  brandId:any=[];
  catId:any=[];
  selectedCatId;
  selectedBrandId;
  item;
  minimum;
  maximum;
  priceRange;
  clearInput:boolean=false;
  isIndeterminate:boolean;
  masterCheck:boolean;

  constructor(public modalController: ModalController,public httpClient: HttpClient,
    private appApiService: AppApiService) { }

  ngOnInit() {
    // const formData = new FormData();
      // formData.append('product_id','');
      // formData.append('brand_id','');
      // formData.append('cat','');
      // formData.append('price_range','');
      // formData.append('user_id','3');
        this.httpClient.post<any>('http://dev.agrowonmartpos.com/public/api/category', '')
        .subscribe(res => {
          console.log(res);
          this.categoryArray=res.category_list;
          this.categoryArray.forEach(element => {
            element.isChecked=false;
          });
          this.httpClient.post<any>('http://dev.agrowonmartpos.com/public/api/brand', '')
          .subscribe(res => {
            console.log(res);
            this.brandArray=res.brand_list;
            this.brandArray.forEach(element => {
              element.isChecked=false;
            });
          });
        });
  }

  ionchange($event) {
    console.log($event);
    if($event.detail.checked) {
      this.selectedCatId=$event.detail.value;
      this.catId.push(this.selectedCatId);
      console.log(this.catId);
    }else if(!$event.detail.checked) {
      this.selectedCatId=$event.detail.value;
      const index=this.catId.indexOf(this.selectedCatId);
      console.log(index);
      if(index>-1){
        this.catId.splice(index,1);
      }
      console.log(this.catId);
    }else {

    }


    const totalItems = this.categoryArray.length;
    let checked = 0;
    this.categoryArray.map(obj => {
      if (obj.isChecked) checked++;
    });
    if (checked > 0 && checked < totalItems) {
      //If even one item is checked but not all
      this.isIndeterminate = true;
      this.masterCheck = false;
    } else if (checked == totalItems) {
      //If all are checked
      this.masterCheck = true;
      this.isIndeterminate = false;
    } else {
      //If none is checked
      this.isIndeterminate = false;
      this.masterCheck = false;
    }

  }
  ionchange1($event) {
    console.log($event);
    if($event.detail.checked) {
      this.selectedBrandId=$event.detail.value;
      this.brandId.push(this.selectedBrandId);
      console.log(this.brandId);
    }else if(!$event.detail.checked) {
      this.selectedBrandId=$event.detail.value;
      const index=this.brandId.indexOf(this.selectedBrandId);
      console.log(index);
      if(index>-1){
        this.brandId.splice(index,1);
      }
      console.log(this.brandId);
    }else {

    }



    const totalItems = this.categoryArray.length;
    let checked = 0;
    this.categoryArray.map(obj => {
      if (obj.isChecked) checked++;
    });
    if (checked > 0 && checked < totalItems) {
      //If even one item is checked but not all
      this.isIndeterminate = true;
      this.masterCheck = false;
    } else if (checked == totalItems) {
      //If all are checked
      this.masterCheck = true;
      this.isIndeterminate = false;
    } else {
      //If none is checked
      this.isIndeterminate = false;
      this.masterCheck = false;
    }
  }

  ionRange($event) {
    this.clearInput=false;
    console.log($event.detail.value);
    this.minimum=$event.detail.value.lower.toString();
    this.maximum=$event.detail.value.upper.toString();
    console.log(this.minimum);
    console.log(this.maximum);
    console.log(typeof(this.minimum));
    console.log(typeof(this.maximum));
    this.priceRange=(this.minimum+'-'+this.maximum);
    console.log(this.priceRange);
  }

  closeModal(){
    this.modalController.dismiss();  
  }

  apply(){
      // const formData = new FormData();
      // formData.append('product_id','');
      // formData.append('brand_id',(this.brandId));
      // formData.append('cat_id',(this.catId));
      // formData.append('price_range',this.priceRange);
      // formData.append('user_id','3');
      // this.httpClient.post<any>('http://pos.agrowonmartpos.com/public/api/product', formData)
      // .subscribe(res => {
      //   console.log(res);
      // });
      this.appApiService.getProducts('',this.brandId,this.catId,this.priceRange).subscribe(
        (respData: any) => {
          console.log(respData);
          this.modalController.dismiss(respData.ProductWise_List);
        });
  }

  clear(){
    this.clearInput=true;
    setTimeout(()=>{
      this.categoryArray.forEach(obj => {
        obj.isChecked = this.masterCheck;
        this.brandArray.forEach(element => {
          element.isChecked = this.masterCheck;
        });
      });
    });
  } 
  

}
