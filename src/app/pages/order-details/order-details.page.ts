import { Component, OnInit } from '@angular/core'
import { NavController } from '@ionic/angular';
import { AppData } from 'src/app/utility/app.data'

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  orderItemExpanded = false;
  paymentInfoExpanded = false;
  shipmentItemExpanded = false;
  orderDetailsData: any
  constructor(public appData: AppData,
              public navCltr: NavController
    ) {
  }
  ngOnInit() {
    this.orderDetailsData = this.appData.getOrderDetails();
    console.log(this.orderDetailsData);
  }

  expandItem(): void {
    if (this.orderItemExpanded) {
      this.orderItemExpanded = false;
    } else {
      this.orderItemExpanded = true;
    }
  }
  expandedPaymentInfo(): void {
    if (this.paymentInfoExpanded) {
      this.paymentInfoExpanded = false;
    } else {
      this.paymentInfoExpanded = true;
    }
  }
  expandShipmentInfo(): void {
    if (this.shipmentItemExpanded) {
      this.shipmentItemExpanded = false;
    } else {
      this.shipmentItemExpanded = true;
    }
  }
  shpmentDetails(orderInfo) {
    this.navCltr.navigateForward('shipments');
  }
}
