import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LbIncomeDetailsPage } from './lb-income-details.page';

const routes: Routes = [
  {
    path: '',
    component: LbIncomeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LbIncomeDetailsPageRoutingModule {}
