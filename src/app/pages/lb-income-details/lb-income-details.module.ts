import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LbIncomeDetailsPageRoutingModule } from './lb-income-details-routing.module';

import { LbIncomeDetailsPage } from './lb-income-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    LbIncomeDetailsPageRoutingModule
  ],
  declarations: [LbIncomeDetailsPage]
})
export class LbIncomeDetailsPageModule {}
