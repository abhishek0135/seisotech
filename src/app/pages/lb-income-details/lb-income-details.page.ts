import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-lb-income-details',
  templateUrl: './lb-income-details.page.html',
  styleUrls: ['./lb-income-details.page.scss'],
})
export class LbIncomeDetailsPage implements OnInit {
  LBIncomeDetailsReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.LBIncomeDetailsReport = this.appData.getLBIncomeDetails();
  }

  ngOnInit() {
  }

}