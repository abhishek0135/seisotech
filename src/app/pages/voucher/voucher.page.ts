import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.page.html',
  styleUrls: ['./voucher.page.scss'],
})
export class VoucherPage implements OnInit {

  constructor(public toastController: ToastController
    ) { }

  ngOnInit() {
  }
  async copyCode() {
    const toast = await this.toastController.create({
      message: 'Your coupon code copied.',
      duration: 2000,
      cssClass: 'toast-button',
      buttons: [
        {
          side: 'start',
          icon: 'checkmark-circle-outline',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
      ]
    });
    toast.present();
  }

}
