import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SchemesAndOffersPage } from './schemes-and-offers.page';

const routes: Routes = [
  {
    path: '',
    component: SchemesAndOffersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SchemesAndOffersPageRoutingModule {}
