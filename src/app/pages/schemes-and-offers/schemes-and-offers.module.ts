import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SchemesAndOffersPageRoutingModule } from './schemes-and-offers-routing.module';

import { SchemesAndOffersPage } from './schemes-and-offers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SchemesAndOffersPageRoutingModule
  ],
  declarations: [SchemesAndOffersPage]
})
export class SchemesAndOffersPageModule {}
