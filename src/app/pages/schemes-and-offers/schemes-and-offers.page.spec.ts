import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SchemesAndOffersPage } from './schemes-and-offers.page';

describe('SchemesAndOffersPage', () => {
  let component: SchemesAndOffersPage;
  let fixture: ComponentFixture<SchemesAndOffersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchemesAndOffersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SchemesAndOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
