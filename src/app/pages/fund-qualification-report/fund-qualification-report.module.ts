import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FundQualificationReportPageRoutingModule } from './fund-qualification-report-routing.module';

import { FundQualificationReportPage } from './fund-qualification-report.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    FundQualificationReportPageRoutingModule
  ],
  declarations: [FundQualificationReportPage]
})
export class FundQualificationReportPageModule {}
