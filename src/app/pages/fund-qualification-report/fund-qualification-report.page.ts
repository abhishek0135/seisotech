import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-fund-qualification-report',
  templateUrl: './fund-qualification-report.page.html',
  styleUrls: ['./fund-qualification-report.page.scss'],
})
export class FundQualificationReportPage implements OnInit {
  fundQualificationReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.fundQualificationReport = this.appData.getFundQualificationReport();
  }

  ngOnInit() {
  }

}
