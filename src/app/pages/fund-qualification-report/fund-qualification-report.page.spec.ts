import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FundQualificationReportPage } from './fund-qualification-report.page';

describe('FundQualificationReportPage', () => {
  let component: FundQualificationReportPage;
  let fixture: ComponentFixture<FundQualificationReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundQualificationReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FundQualificationReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
