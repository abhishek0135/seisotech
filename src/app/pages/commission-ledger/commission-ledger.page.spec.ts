import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommissionLedgerPage } from './commission-ledger.page';

describe('CommissionLedgerPage', () => {
  let component: CommissionLedgerPage;
  let fixture: ComponentFixture<CommissionLedgerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionLedgerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommissionLedgerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
