import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-commission-ledger',
  templateUrl: './commission-ledger.page.html',
  styleUrls: ['./commission-ledger.page.scss'],
})
export class CommissionLedgerPage implements OnInit {
  commissionLedgerList = []
  constructor(private navController: NavController, private AppData: AppData, public appConstants: AppConstants) {
    this.commissionLedgerList = this.AppData.getCommissionLedger();
  }

  ngOnInit() {
  }
  commissionVoucher() {
    this.navController.navigateForward(['commission-voucher']);
  }

}
