import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommissionLedgerPage } from './commission-ledger.page';

const routes: Routes = [
  {
    path: '',
    component: CommissionLedgerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommissionLedgerPageRoutingModule {}
