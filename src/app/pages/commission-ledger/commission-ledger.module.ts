import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommissionLedgerPageRoutingModule } from './commission-ledger-routing.module';

import { CommissionLedgerPage } from './commission-ledger.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommissionLedgerPageRoutingModule,
    ComponentModule
  ],
  declarations: [CommissionLedgerPage]
})
export class CommissionLedgerPageModule {}
