import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddNewTicketPage } from './add-new-ticket.page';

describe('AddNewTicketPage', () => {
  let component: AddNewTicketPage;
  let fixture: ComponentFixture<AddNewTicketPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewTicketPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddNewTicketPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
