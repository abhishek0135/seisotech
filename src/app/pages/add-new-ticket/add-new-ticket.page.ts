import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-new-ticket',
  templateUrl: './add-new-ticket.page.html',
  styleUrls: ['./add-new-ticket.page.scss'],
})
export class AddNewTicketPage implements OnInit {
  department = 'DataWise';
  constructor() { }

  ngOnInit() {
  }

}
