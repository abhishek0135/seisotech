import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNewTicketPageRoutingModule } from './add-new-ticket-routing.module';

import { AddNewTicketPage } from './add-new-ticket.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    AddNewTicketPageRoutingModule
  ],
  declarations: [AddNewTicketPage]
})
export class AddNewTicketPageModule {}
