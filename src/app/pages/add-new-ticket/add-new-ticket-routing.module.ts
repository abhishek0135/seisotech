import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewTicketPage } from './add-new-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: AddNewTicketPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNewTicketPageRoutingModule {}
