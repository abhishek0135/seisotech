import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSelect } from '@ionic/angular';

@Component({
  selector: 'app-order-return',
  templateUrl: './order-return.page.html',
  styleUrls: ['./order-return.page.scss'],
})
export class OrderReturnPage implements OnInit {
  hideList = true;
  resturnReason: any;
  resturnAction: any;
  quantity: any = 0;
  @ViewChild('QtyNumber') countrySelectRef: IonSelect;
  constructor() { }

  ngOnInit() {
  }
  selectQuantity() {
    this.countrySelectRef.open();
 }
 changeQuantity(qty) {
  console.log(qty);
 }
}
