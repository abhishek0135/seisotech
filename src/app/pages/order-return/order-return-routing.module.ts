import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderReturnPage } from './order-return.page';

const routes: Routes = [
  {
    path: '',
    component: OrderReturnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderReturnPageRoutingModule {}
