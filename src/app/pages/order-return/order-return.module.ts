import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderReturnPageRoutingModule } from './order-return-routing.module';

import { OrderReturnPage } from './order-return.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    OrderReturnPageRoutingModule
  ],
  declarations: [OrderReturnPage]
})
export class OrderReturnPageModule {}
