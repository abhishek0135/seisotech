import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-point-details',
  templateUrl: './point-details.page.html',
  styleUrls: ['./point-details.page.scss'],
})
export class PointDetailsPage implements OnInit {
  pointDetails = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.pointDetails = this.appData.getCommissionPointDetails();
  }

  ngOnInit() {
  }

}
