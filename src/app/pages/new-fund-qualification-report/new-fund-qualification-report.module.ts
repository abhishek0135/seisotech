import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewFundQualificationReportPageRoutingModule } from './new-fund-qualification-report-routing.module';

import { NewFundQualificationReportPage } from './new-fund-qualification-report.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    NewFundQualificationReportPageRoutingModule
  ],
  declarations: [NewFundQualificationReportPage]
})
export class NewFundQualificationReportPageModule {}
