import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewFundQualificationReportPage } from './new-fund-qualification-report.page';

describe('NewFundQualificationReportPage', () => {
  let component: NewFundQualificationReportPage;
  let fixture: ComponentFixture<NewFundQualificationReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFundQualificationReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewFundQualificationReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
