import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-new-fund-qualification-report',
  templateUrl: './new-fund-qualification-report.page.html',
  styleUrls: ['./new-fund-qualification-report.page.scss'],
})
export class NewFundQualificationReportPage implements OnInit {
  newFundQualificationReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.newFundQualificationReport = this.appData.getFundQualificationReport();
  }

  ngOnInit() {
  }

}
