import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewFundQualificationReportPage } from './new-fund-qualification-report.page';

const routes: Routes = [
  {
    path: '',
    component: NewFundQualificationReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewFundQualificationReportPageRoutingModule {}
