import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-dashboard-scheme-details',
  templateUrl: './dashboard-scheme-details.page.html',
  styleUrls: ['./dashboard-scheme-details.page.scss'],
})
export class DashboardSchemeDetailsPage implements OnInit {
  schemeDetails: any = [];
  constructor(
    private appData: AppData,
    private navCltr: NavController
  ) { 
    this.schemeDetails = this.appData.getDashboardSchemeDetails();
  }

  ngOnInit() {
  }
  offerDetails(details) {
    console.log(details);
    let navigationExtras: NavigationExtras = {
      queryParams: {
          offerDetails: JSON.stringify(details),
      }
    }
    this.navCltr.navigateForward(['offer-details'], navigationExtras);
  }
}
