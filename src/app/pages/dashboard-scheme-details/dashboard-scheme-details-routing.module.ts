import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardSchemeDetailsPage } from './dashboard-scheme-details.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardSchemeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardSchemeDetailsPageRoutingModule {}
