import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardSchemeDetailsPageRoutingModule } from './dashboard-scheme-details-routing.module';

import { DashboardSchemeDetailsPage } from './dashboard-scheme-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DashboardSchemeDetailsPageRoutingModule
  ],
  declarations: [DashboardSchemeDetailsPage]
})
export class DashboardSchemeDetailsPageModule {}
