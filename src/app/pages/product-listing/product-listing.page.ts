import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppApiService } from 'src/app/services/app-api.service';
import { AppConstants } from 'src/app/utility/app.constants';
import { Values } from 'src/app/utility/app.variables';
import { CommonUtility } from 'src/app/utility/common.utility';
import { AppData } from '../../utility/app.data';
import { ModalController,ActionSheetController } from '@ionic/angular';
import { FiltermodalPage } from '../../pages/filtermodal/filtermodal.page';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.page.html',
  styleUrls: ['./product-listing.page.scss'],
})
export class ProductListingPage implements OnInit {
  categoryId = '';
  products: any = [];
  emptyProduct = [1, 1, 1, 1, 1, 1, 1, 1];
  isDataLoaded = false;
  data:boolean=false;

  constructor(
    public navCltr: NavController,
    public appData: AppData,
    private appApiService: AppApiService,
    private router: Router,
    private values: Values,
    private commonUtility: CommonUtility,
    public modalController: ModalController,
    public actionSheetController: ActionSheetController
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.categoryId = this.router.getCurrentNavigation().extras.state.categoryId;
    }



    
  }
  
  private getProducts() {
    this.appApiService.getProducts('', '', this.categoryId, '').subscribe(
      (respData: any) => {
        this.isDataLoaded = true;
        this.data=true;
        this.products = respData.ProductWise_List;
        console.log(this.products);
      },
      (err) => {
        this.isDataLoaded = true;
        this.data=true
        this.products = this.appData.getProducts();
      }
    );
  }
  ngOnInit() {
    this.getProducts();
  }
  productDetails(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product: JSON.stringify(product),
      },
    };
    this.navCltr.navigateForward(['product-details'], navigationExtras);
  }
  addToCart(event, product) {
    debugger;
    event.stopPropagation();
    product.addToCartInProgress = true;
    this.appApiService.addProductToCart(product.product_id).subscribe(
      (respData: any) => {
        product.addToCartInProgress = false;
        this.values.cartCount++;
        this.commonUtility.setLocalstorage(
          AppConstants.localstorageKeys.cartCount,
          this.values.cartCount.toString()
        );
        this.commonUtility.showSuccessToast('Product added to cart');
      },
      (err) => {
        product.addToCartInProgress = false;
      }
    );
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: FiltermodalPage,
      cssClass: 'filterModal',
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
          if (data) {
            console.log(data);
            this.products=[];
            this.products=data
          }else{
            console.log("goes in else");
          }
  } 

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
              text: 'A-Z',
              handler: () => {
                function dynamicSort(property) {
                  var sortOrder = 1;
              
                  if(property[0] === "-") {
                      sortOrder = -1;
                      property = property.substr(1);
                  }
              
                  return function (a,b) {
                      if(sortOrder == -1){
                          return b[property].localeCompare(a[property]);
                      }else{
                          return a[property].localeCompare(b[property]);
                      }        
                  }
                }
                this.products.sort(dynamicSort("name"));
                console.log(this.products);
              }
          },
          {
              text: 'Z-A',
              handler: () => {
                function dynamicSort(property) {
                  var sortOrder = 1;
              
                  if(property[0] === "-") {
                      sortOrder = -1;
                      property = property.substr(1);
                  }
              
                  return function (a,b) {
                      if(sortOrder == -1){
                          return b[property].localeCompare(a[property]);
                      }else{
                          return a[property].localeCompare(b[property]);
                      }        
                  }
                }
                this.products.sort(dynamicSort("-name"));
                console.log(this.products);
              }
          },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
    await actionSheet.present();
  }



  back(){
    this.router.navigateByUrl('home');
  }

  

}
  

