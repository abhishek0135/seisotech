import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  sponsonIDError: boolean = false;
  
  country = 'ind';
  state = 'mh';
  city = 'pune';
  constructor() { }

  ngOnInit() {
  }

  register() {
    this.sponsonIDError = true;
  }

}
