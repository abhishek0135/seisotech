import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-db-qualification-details',
  templateUrl: './db-qualification-details.page.html',
  styleUrls: ['./db-qualification-details.page.scss'],
})
export class DbQualificationDetailsPage implements OnInit {
  DBQualificationReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.DBQualificationReport = this.appData.getDBQualificationReport();
  }

  ngOnInit() {
  }

}