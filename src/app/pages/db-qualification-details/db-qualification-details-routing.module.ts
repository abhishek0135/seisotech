import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DbQualificationDetailsPage } from './db-qualification-details.page';

const routes: Routes = [
  {
    path: '',
    component: DbQualificationDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DbQualificationDetailsPageRoutingModule {}
