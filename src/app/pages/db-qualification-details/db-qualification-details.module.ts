import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DbQualificationDetailsPageRoutingModule } from './db-qualification-details-routing.module';

import { DbQualificationDetailsPage } from './db-qualification-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DbQualificationDetailsPageRoutingModule
  ],
  declarations: [DbQualificationDetailsPage]
})
export class DbQualificationDetailsPageModule {}
