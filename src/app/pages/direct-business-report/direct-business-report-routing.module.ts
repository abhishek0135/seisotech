import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DirectBusinessReportPage } from './direct-business-report.page';

const routes: Routes = [
  {
    path: '',
    component: DirectBusinessReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DirectBusinessReportPageRoutingModule {}
