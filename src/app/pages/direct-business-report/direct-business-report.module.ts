import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DirectBusinessReportPageRoutingModule } from './direct-business-report-routing.module';

import { DirectBusinessReportPage } from './direct-business-report.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectBusinessReportPageRoutingModule,
    ComponentModule
  ],
  declarations: [DirectBusinessReportPage]
})
export class DirectBusinessReportPageModule {}
