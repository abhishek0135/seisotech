import { Component, OnInit } from '@angular/core';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-direct-business-report',
  templateUrl: './direct-business-report.page.html',
  styleUrls: ['./direct-business-report.page.scss'],
})
export class DirectBusinessReportPage implements OnInit {
  myDirectBusinessReport = [];
  myDownlineReportData: any = {};
  constructor(private appData: AppData) {
   this.myDirectBusinessReport = this.appData.getDirectBusinessReportData();
   debugger
  }

  ngOnInit() {
  }

}


