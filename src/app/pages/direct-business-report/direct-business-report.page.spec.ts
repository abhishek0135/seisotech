import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DirectBusinessReportPage } from './direct-business-report.page';

describe('DirectBusinessReportPage', () => {
  let component: DirectBusinessReportPage;
  let fixture: ComponentFixture<DirectBusinessReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectBusinessReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DirectBusinessReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
