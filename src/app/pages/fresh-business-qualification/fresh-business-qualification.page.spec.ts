import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreshBusinessQualificationPage } from './fresh-business-qualification.page';

describe('FreshBusinessQualificationPage', () => {
  let component: FreshBusinessQualificationPage;
  let fixture: ComponentFixture<FreshBusinessQualificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreshBusinessQualificationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreshBusinessQualificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
