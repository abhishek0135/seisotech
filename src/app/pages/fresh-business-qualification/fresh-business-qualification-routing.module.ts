import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreshBusinessQualificationPage } from './fresh-business-qualification.page';

const routes: Routes = [
  {
    path: '',
    component: FreshBusinessQualificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreshBusinessQualificationPageRoutingModule {}
