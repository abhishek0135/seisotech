import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-fresh-business-qualification',
  templateUrl: './fresh-business-qualification.page.html',
  styleUrls: ['./fresh-business-qualification.page.scss'],
})
export class FreshBusinessQualificationPage implements OnInit {

  freshBusinessQualificationReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.freshBusinessQualificationReport = this.appData.getFreshQualificationReport();
  }

  ngOnInit() {
  }

}

