import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreshBusinessQualificationPageRoutingModule } from './fresh-business-qualification-routing.module';

import { FreshBusinessQualificationPage } from './fresh-business-qualification.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    FreshBusinessQualificationPageRoutingModule
  ],
  declarations: [FreshBusinessQualificationPage]
})
export class FreshBusinessQualificationPageModule {}
