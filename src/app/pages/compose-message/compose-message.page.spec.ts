import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComposeMessagePage } from './compose-message.page';

describe('ComposeMessagePage', () => {
  let component: ComposeMessagePage;
  let fixture: ComponentFixture<ComposeMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComposeMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComposeMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
