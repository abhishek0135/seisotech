import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComposeMessagePageRoutingModule } from './compose-message-routing.module';

import { ComposeMessagePage } from './compose-message.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    ComposeMessagePageRoutingModule
  ],
  declarations: [ComposeMessagePage]
})
export class ComposeMessagePageModule {}
