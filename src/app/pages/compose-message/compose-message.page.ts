import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compose-message',
  templateUrl: './compose-message.page.html',
  styleUrls: ['./compose-message.page.scss'],
})
export class ComposeMessagePage implements OnInit {
  selectedMessageMode = 'Admin';
  constructor() { }

  ngOnInit() {
  }

}
