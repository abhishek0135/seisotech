import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-db-income-details',
  templateUrl: './db-income-details.page.html',
  styleUrls: ['./db-income-details.page.scss'],
})
export class DbIncomeDetailsPage implements OnInit {
  dBIncomeDetailsReport = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.dBIncomeDetailsReport = this.appData.getDBIncomeDetails();
  }

  ngOnInit() {
  }

}
