import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DbIncomeDetailsPageRoutingModule } from './db-income-details-routing.module';

import { DbIncomeDetailsPage } from './db-income-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DbIncomeDetailsPageRoutingModule
  ],
  declarations: [DbIncomeDetailsPage]
})
export class DbIncomeDetailsPageModule {}
