import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppData } from '../../utility/app.data';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {
  user: any;
  constructor(
    public navCltr: NavController,
    public appData: AppData
  ) { 
    this.user = this.appData.getUserData();
    console.log(this.user);
  }

  ngOnInit() {
  }

  myAddress() {
    this.navCltr.navigateForward('my-address');
  }

  wishlist() {
    this.navCltr.navigateForward('wishlist');
  }

  orderList() {
    this.navCltr.navigateForward('order-list');
  }

  myWallet() {
    this.navCltr.navigateForward('my-wallet');
  }

  editProfile() {
    this.navCltr.navigateForward('edit-profile');
  }
  editAddress() {
    this.navCltr.navigateForward('add-address');
  }
  
}
