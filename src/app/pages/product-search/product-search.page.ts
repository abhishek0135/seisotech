import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppApiService } from 'src/app/services/app-api.service';
import { AppConstants } from 'src/app/utility/app.constants';
import { Values } from 'src/app/utility/app.variables';
import { CommonUtility } from 'src/app/utility/common.utility';
import { AppData } from '../../utility/app.data';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-search.page.html',
  styleUrls: ['./product-search.page.scss'],
})
export class ProductListingPage implements OnInit {
  categoryId = '';
  products: any = [];
  emptyProduct = [1, 1, 1, 1, 1, 1, 1, 1];
  isDataLoaded = false;

  constructor(
    public navCltr: NavController,
    public appData: AppData,
    private appApiService: AppApiService,
    private router: Router,
    private values: Values,
    private commonUtility: CommonUtility
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.categoryId = this.router.getCurrentNavigation().extras.state.categoryId;
    }
  }

  ngOnInit() {
    this.getProducts();
  }

  private getProducts() {
    this.appApiService.getProducts('', '','', '').subscribe(
      (respData: any) => {
        this.isDataLoaded = true;
        this.products = respData.ProductWise_List;
      },
      (err) => {
        this.isDataLoaded = true;
        this.products = this.appData.getProducts();
      }
    );
  }

  ionChange(event) {
    console.log(event.detail.value);
    if(event.detail.value.length>2){
    this.getProducts_search(event.detail.value)
  }else{
    this.getProducts_search('')
  }
  }

  private getProducts_search(strSearch) {
    this.isDataLoaded = false;
    this.appApiService.getProduct_Search(strSearch).subscribe(
      (respData: any) => {
        this.isDataLoaded = true;
        this.products = respData.ProductSearch_List;
      },
      (err) => {
        this.isDataLoaded = true;
        this.products = this.appData.getProducts();
      }
    );
  }

  productDetails(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product: JSON.stringify(product),
      },
    };
    this.navCltr.navigateForward(['product-details'], navigationExtras);
  }
  addToCart(event, product) {
    debugger;
    event.stopPropagation();
    product.addToCartInProgress = true;
    this.appApiService.addProductToCart(product.product_id).subscribe(
      (respData: any) => {
        product.addToCartInProgress = false;
        this.values.cartCount++;
        this.commonUtility.setLocalstorage(
          AppConstants.localstorageKeys.cartCount,
          this.values.cartCount.toString()
        );
        this.commonUtility.showSuccessToast('Product added to cart');
      },
      (err) => {
        product.addToCartInProgress = false;
      }
    );
  }
  
}
