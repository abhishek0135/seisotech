import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AppApiService } from 'src/app/services/app-api.service';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from '../../utility/app.data';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage implements OnInit {
  categoryData: any[];
  emptyCategory = [1, 1, 1, 1, 1, 1, 1, 1];
  isDataLoaded = false;
  constructor(
    public navCltr: NavController,
    public appData: AppData,
    private appApiService: AppApiService
  ) {
    console.log(this.categoryData);
  }
  private getCategories() {
    this.appApiService.getCategories().subscribe(
      (respData: any) => {
        this.isDataLoaded = true;
        this.categoryData = respData.category_list;
      },
      (err) => {
        this.isDataLoaded = true;
        this.categoryData = this.appData.getCategories();
      }
    );
  }
  ngOnInit() {
    this.getCategories();
  }

  productList() {
    this.navCltr.navigateForward('product-listing');
  }
}
