import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { DeliveryChargesPopupPage } from '../delivery-charges-popup/delivery-charges-popup.page';
import { AppData } from '../../utility/app.data';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.page.html',
  styleUrls: ['./shopping-cart.page.scss'],
})
export class ShoppingCartPage implements OnInit {
  showBackButton = false;
  showMenuButton = false;
  cartData: any = [];
  constructor(
    public navCltr: NavController,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public modalController: ModalController,
    private appData: AppData,
    public alertController: AlertController
  ) {
    this.cartData = this.appData.getShoppingCartData();
    console.log(this.cartData);
   }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        debugger
        const showBackValue = this.router.getCurrentNavigation().extras.state.showBack;
        if (showBackValue) {
          this.showBackButton = true;

        }
      } else {
        this.showMenuButton = true;
      }
    });
  }

  productDetails(cartItem) {
    const navigationExtras: NavigationExtras = {
      state: {
        product: JSON.stringify(cartItem),
      }
    };
    this.navCltr.navigateForward(['product-details'], navigationExtras);
  }
  verifyMobile() {
    this.navCltr.navigateForward('verify-mobile');
  }
  async helpPopup() {
    const modal = await this.modalController.create({
      component: DeliveryChargesPopupPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async delete(evnt) {
    evnt.preventDefault();
    evnt.stopPropagation();
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Warning!',
      message: '<strong>Are You sure To Delete </strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
