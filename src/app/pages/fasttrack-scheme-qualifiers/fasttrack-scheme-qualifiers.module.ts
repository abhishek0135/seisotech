import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FasttrackSchemeQualifiersPageRoutingModule } from './fasttrack-scheme-qualifiers-routing.module';

import { FasttrackSchemeQualifiersPage } from './fasttrack-scheme-qualifiers.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    FasttrackSchemeQualifiersPageRoutingModule
  ],
  declarations: [FasttrackSchemeQualifiersPage]
})
export class FasttrackSchemeQualifiersPageModule {}
