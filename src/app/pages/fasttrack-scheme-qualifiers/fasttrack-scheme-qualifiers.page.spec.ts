import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FasttrackSchemeQualifiersPage } from './fasttrack-scheme-qualifiers.page';

describe('FasttrackSchemeQualifiersPage', () => {
  let component: FasttrackSchemeQualifiersPage;
  let fixture: ComponentFixture<FasttrackSchemeQualifiersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FasttrackSchemeQualifiersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FasttrackSchemeQualifiersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
