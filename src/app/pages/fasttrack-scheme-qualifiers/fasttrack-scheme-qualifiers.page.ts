import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-fasttrack-scheme-qualifiers',
  templateUrl: './fasttrack-scheme-qualifiers.page.html',
  styleUrls: ['./fasttrack-scheme-qualifiers.page.scss'],
})
export class FasttrackSchemeQualifiersPage implements OnInit {

  fastTrackSchemeQualifiers = []
  constructor(private AppData: AppData, public appConstants: AppConstants) {
    this.fastTrackSchemeQualifiers = this.AppData.getFasttrackSchemeQualifiers();
  }

  ngOnInit() {
  }

}
