import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AppApiService } from 'src/app/services/app-api.service';
import { CommonUtility } from 'src/app/utility/common.utility';
import { Values } from 'src/app/utility/app.variables';
import { AppConstants } from 'src/app/utility/app.constants';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  product: any;
  productDetails: any = {};
  addToCartInProgress = false;
  data:boolean=false;
  constructor(
    public navCltr: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private appApiService: AppApiService,
    private commonUtility: CommonUtility,
    private values: Values
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.product = JSON.parse(
          this.router.getCurrentNavigation().extras.state.product
        );
      }
    });
  }
  private getProductDetail() {
    this.appApiService
      .getProducts(this.product.product_id, '', '', '')
      .subscribe(
        (respData: any) => {
          this.data=true;
          // this.isDataLoaded = true;
          this.productDetails = respData.ProductWise_List[0];
        },
        (err) => {
          
          // this.isDataLoaded = true;
        }
      );
  }
  public addToCart() {
    this.addToCartInProgress = true;
    this.appApiService.addProductToCart(this.product.product_id).subscribe(
      (respData: any) => {
        this.addToCartInProgress = false;
        this.values.cartCount++;
        this.commonUtility.setLocalstorage(
          AppConstants.localstorageKeys.cartCount,
          this.values.cartCount.toString()
        );
        this.commonUtility.showSuccessToast('Product added to cart');
        this.verifyMobile();
      },
      (err) => {
        this.addToCartInProgress = false;
      }
    );
  }
  ngOnInit() {
    this.getProductDetail();
  }

  verifyMobile() {
    this.navCltr.navigateForward('verify-mobile');
  }
}
