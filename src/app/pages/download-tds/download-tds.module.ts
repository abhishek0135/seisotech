import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DownloadTdsPageRoutingModule } from './download-tds-routing.module';

import { DownloadTdsPage } from './download-tds.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DownloadTdsPageRoutingModule
  ],
  declarations: [DownloadTdsPage]
})
export class DownloadTdsPageModule {}
