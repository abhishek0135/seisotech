import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DownloadTdsPage } from './download-tds.page';

describe('DownloadTdsPage', () => {
  let component: DownloadTdsPage;
  let fixture: ComponentFixture<DownloadTdsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadTdsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DownloadTdsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
