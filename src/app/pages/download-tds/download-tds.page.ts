import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-download-tds',
  templateUrl: './download-tds.page.html',
  styleUrls: ['./download-tds.page.scss'],
})
export class DownloadTdsPage implements OnInit {
  tdsDocuments = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.tdsDocuments = this.appData.getTDSDocuments();
  }

  ngOnInit() {
  }
  // getFileExtension(filename){
  //   let fileExtension = filename.split('.').pop();
  //   debugger
  //   return fileExtension;

  // }

}