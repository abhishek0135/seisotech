import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerationTreePageRoutingModule } from './generation-tree-routing.module';

import { GenerationTreePage } from './generation-tree.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    GenerationTreePageRoutingModule,
  ],
  declarations: [GenerationTreePage]
})
export class GenerationTreePageModule {}
