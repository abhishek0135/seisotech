import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GenerationTreePage } from './generation-tree.page';

describe('GenerationTreePage', () => {
  let component: GenerationTreePage;
  let fixture: ComponentFixture<GenerationTreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerationTreePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GenerationTreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
