import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenerationTreePage } from './generation-tree.page';

const routes: Routes = [
  {
    path: '',
    component: GenerationTreePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenerationTreePageRoutingModule {}
