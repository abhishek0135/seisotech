import { Component, OnInit } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-generation-tree',
  templateUrl: './generation-tree.page.html',
  styleUrls: ['./generation-tree.page.scss'],
})
export class GenerationTreePage implements OnInit {
  isLandscap: any = 'L';
  constructor(
    private navController: NavController,
    public screenOrientation: ScreenOrientation,
  ) {}

  ngOnInit() {
    this.isLandscap = 'L';
    this.lockScreenOrientation();
  }
  ionViewWillLeave() {
    this.isLandscap = 'P';
    this.lockScreenOrientation();
  }
  lockScreenOrientation() {
    switch (this.isLandscap) {
      case 'L':
        this.screenOrientation.unlock();
        this.screenOrientation.lock(
          this.screenOrientation.ORIENTATIONS.LANDSCAPE
        );
        this.isLandscap = 'P';
        break;

      case 'P':
        this.screenOrientation.unlock();
        this.screenOrientation.lock(
          this.screenOrientation.ORIENTATIONS.PORTRAIT
        );
        this.isLandscap = 'L';
        break;

      default:
        break;
    }
  }
}
