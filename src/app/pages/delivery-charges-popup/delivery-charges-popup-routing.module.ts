import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryChargesPopupPage } from './delivery-charges-popup.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveryChargesPopupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveryChargesPopupPageRoutingModule {}
