import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-delivery-charges-popup',
  templateUrl: './delivery-charges-popup.page.html',
  styleUrls: ['./delivery-charges-popup.page.scss'],
})
export class DeliveryChargesPopupPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  closeModel() {
    this.modalController.dismiss();
  }

}
