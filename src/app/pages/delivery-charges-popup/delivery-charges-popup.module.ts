import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveryChargesPopupPageRoutingModule } from './delivery-charges-popup-routing.module';

import { DeliveryChargesPopupPage } from './delivery-charges-popup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryChargesPopupPageRoutingModule
  ],
  declarations: [DeliveryChargesPopupPage]
})
export class DeliveryChargesPopupPageModule {}
