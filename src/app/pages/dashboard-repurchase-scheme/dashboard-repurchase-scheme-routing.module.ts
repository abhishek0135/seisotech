import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardRepurchaseSchemePage } from './dashboard-repurchase-scheme.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardRepurchaseSchemePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRepurchaseSchemePageRoutingModule {}
