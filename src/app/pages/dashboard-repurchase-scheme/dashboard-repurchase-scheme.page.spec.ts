import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardRepurchaseSchemePage } from './dashboard-repurchase-scheme.page';

describe('DashboardRepurchaseSchemePage', () => {
  let component: DashboardRepurchaseSchemePage;
  let fixture: ComponentFixture<DashboardRepurchaseSchemePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRepurchaseSchemePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardRepurchaseSchemePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
