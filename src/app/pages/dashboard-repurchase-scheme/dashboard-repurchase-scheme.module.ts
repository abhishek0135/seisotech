import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardRepurchaseSchemePageRoutingModule } from './dashboard-repurchase-scheme-routing.module';

import { DashboardRepurchaseSchemePage } from './dashboard-repurchase-scheme.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    DashboardRepurchaseSchemePageRoutingModule
  ],
  declarations: [DashboardRepurchaseSchemePage]
})
export class DashboardRepurchaseSchemePageModule {}
