import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { monthlySchemeDetailsPage } from './monthly-scheme-details.page';

const routes: Routes = [
  {
    path: '',
    component: monthlySchemeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonthlySchemeDetailsPageRoutingModule {}
