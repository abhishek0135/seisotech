import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonthlySchemeDetailsPageRoutingModule } from './monthly-scheme-details-routing.module';

import { ComponentModule } from 'src/app/components/component.module';
import { monthlySchemeDetailsPage } from './monthly-scheme-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    MonthlySchemeDetailsPageRoutingModule
  ],
  declarations: [monthlySchemeDetailsPage]
})
export class MonthlySchemeDetailsPageModule {}
