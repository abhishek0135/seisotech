import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-monthly-scheme-details',
  templateUrl: './monthly-scheme-details.page.html',
  styleUrls: ['./monthly-scheme-details.page.scss'],
})
export class monthlySchemeDetailsPage implements OnInit {
  monthlySchemeDetails = []
  constructor(public AppData: AppData, public appConstants: AppConstants) {
    this.monthlySchemeDetails = this.AppData.getMonthlySchemeDetails();
  }

  ngOnInit() {
  }
}
