import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MegagiftSchemeDetailsPage } from './megagift-scheme-details.page';

describe('MegagiftSchemeDetailsPage', () => {
  let component: MegagiftSchemeDetailsPage;
  let fixture: ComponentFixture<MegagiftSchemeDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MegagiftSchemeDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MegagiftSchemeDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
