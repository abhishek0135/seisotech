import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddRatingReviewPageRoutingModule } from './add-rating-review-routing.module';

import { AddRatingReviewPage } from './add-rating-review.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    AddRatingReviewPageRoutingModule
  ],
  declarations: [AddRatingReviewPage]
})
export class AddRatingReviewPageModule {}
