import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddRatingReviewPage } from './add-rating-review.page';

describe('AddRatingReviewPage', () => {
  let component: AddRatingReviewPage;
  let fixture: ComponentFixture<AddRatingReviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRatingReviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddRatingReviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
