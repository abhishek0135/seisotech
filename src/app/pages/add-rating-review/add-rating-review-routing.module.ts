import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddRatingReviewPage } from './add-rating-review.page';

const routes: Routes = [
  {
    path: '',
    component: AddRatingReviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddRatingReviewPageRoutingModule {}
