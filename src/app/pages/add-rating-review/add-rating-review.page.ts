import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-rating-review',
  templateUrl: './add-rating-review.page.html',
  styleUrls: ['./add-rating-review.page.scss'],
})
export class AddRatingReviewPage implements OnInit {
  form: any = {};

  constructor() {
    this.form.rating = 0;

  }

  ngOnInit() {
  }
  yourRating(rating) {
    this.form.rating = rating;
  }
}
