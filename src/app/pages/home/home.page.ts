import { Component, OnInit } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppApiService } from 'src/app/services/app-api.service';
import { AppData } from 'src/app/utility/app.data';
import { Values } from 'src/app/utility/app.variables';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  featuredProducts: any[] = [];
  bestSelling: any[] = [];
  categories: any[] = [];
  slides = [
    'assets/slider/1.png',
    'assets/slider/2.png',
    'assets/slider/3.jpg',
    'assets/slider/4.png',
    'assets/slider/5.png',
  ];
  data:boolean=false;
  constructor(
    public navCltr: NavController,
    private apiService: AppApiService,
    private dataService: AppData,
    public values: Values
  ) {
    // this.everydayEssentials = [
    //   {
    //     image: 'assets/small/1.jpg',
    //     name: 'Surf Excel Matic Top Load Detergent Powder (Carton)',
    //     price: '$600.99',
    //     mrp: '$800.99',
    //     off: '50% OFF',
    //     unit: '2 Kg',
    //   },
    //   {
    //     image: 'assets/small/7.jpg',
    //     name: 'Surf Excel Matic Top Load Detergent Powder (Carton)',
    //     price: '$600.99',
    //     mrp: '$800.99',
    //     off: '50% OFF',
    //     unit: '2 Kg',
    //   },
    //   {
    //     image: 'assets/small/3.jpg',
    //     name: 'Hygienix Anti-Bacterial Hand Sanitizer (Bottle) ',
    //     price: '$456.99',
    //     mrp: '$787.99',
    //     off: '5% OFF',
    //     unit: '300 ml',
    //   },
    //   {
    //     image: 'assets/small/6.jpg',
    //     name: 'Hygienix Anti-Bacterial Hand Sanitizer (Bottle) ',
    //     price: '$456.99',
    //     mrp: '$787.99',
    //     off: '5% OFF',
    //     unit: '300 ml',
    //   }
    // ];
    this.featuredProducts = this.dataService.getFeaturedProducts();
    this.bestSelling = this.dataService.getBestSellerProducts();
    this.categories = this.dataService.getCategories();
  }

  productList(categoryId) {
    const navigationExtras: NavigationExtras = {
      state: {
        categoryId,
      },
    };
    this.navCltr.navigateForward('product-listing', navigationExtras);
  }

  searchproductList(categoryId) {
    const navigationExtras: NavigationExtras = {
      state: {
        categoryId,
      },
    };
    this.navCltr.navigateForward('product-search', navigationExtras);
  }

  categoryList() {
    this.navCltr.navigateForward('category-list');
  }

  productDetails(product) {
    const navigationExtras: NavigationExtras = {
      state: {
        product: JSON.stringify(product),
      },
    };
    this.navCltr.navigateForward(['product-details'], navigationExtras);
  }

  goToCart() {
    const navigationExtras: NavigationExtras = {
      // queryParams: {
      //   showBack: true
      // },
      state: {
        showBack: true,
      },
    };
    this.navCltr.navigateForward(['shopping-cart'], navigationExtras);
  }

  ngOnInit() {
    this.getCategories();
    this.getProducts();
  }

  private getProducts() {
    this.apiService.getProducts('', '', '', '').subscribe(
      (respData: any) => {
        this.data=true;
        this.featuredProducts = respData.ProductWise_List.slice(0, 5);
      },
      (err) => {}
    );
  }
  private getCategories() {
    this.apiService.getCategories().subscribe(
      (respData: any) => {
        this.categories = respData.category_list;
        console.log(this.categories);
        this.data=false;
      },
      (err) => {}
    );
  }
}
