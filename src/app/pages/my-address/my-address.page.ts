import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-my-address',
  templateUrl: './my-address.page.html',
  styleUrls: ['./my-address.page.scss'],
})
export class MyAddressPage implements OnInit {

  constructor(
    public navCltr: NavController
  ) { }

  ngOnInit() {
  }
  editAddress() {
    this.navCltr.navigateForward('add-address')
  }

}
