import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-verify-mobile',
  templateUrl: './verify-mobile.page.html',
  styleUrls: ['./verify-mobile.page.scss'],
})
export class VerifyMobilePage implements OnInit {

  constructor(
    public navCltr: NavController
  ) { }

  ngOnInit() {
  }

  deliiveryAddress() {
    this.navCltr.navigateForward('delivery-address');
  }

}
