import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerifyMobilePageRoutingModule } from './verify-mobile-routing.module';

import { VerifyMobilePage } from './verify-mobile.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerifyMobilePageRoutingModule,
    ComponentModule
  ],
  declarations: [VerifyMobilePage]
})
export class VerifyMobilePageModule {}
