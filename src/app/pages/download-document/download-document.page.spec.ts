import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DownloadDocumentPage } from './download-document.page';

describe('DownloadDocumentPage', () => {
  let component: DownloadDocumentPage;
  let fixture: ComponentFixture<DownloadDocumentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadDocumentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DownloadDocumentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
