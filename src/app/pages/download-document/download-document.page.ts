import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-download-document',
  templateUrl: './download-document.page.html',
  styleUrls: ['./download-document.page.scss'],
})
export class DownloadDocumentPage implements OnInit {
  downloadableDocuments = []
  constructor(private appData: AppData, public appConstants: AppConstants) {
    this.downloadableDocuments = this.appData.getDownloadableDocuments();
  }

  ngOnInit() {
  }
  getFileExtension(filename){
    const fileExtension = filename.split('.').pop();
    return fileExtension;

  }

}