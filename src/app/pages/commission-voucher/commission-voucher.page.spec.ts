import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommissionVoucherPage } from './commission-voucher.page';

describe('CommissionVoucherPage', () => {
  let component: CommissionVoucherPage;
  let fixture: ComponentFixture<CommissionVoucherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionVoucherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommissionVoucherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
