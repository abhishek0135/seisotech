import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommissionVoucherPageRoutingModule } from './commission-voucher-routing.module';

import { CommissionVoucherPage } from './commission-voucher.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    CommissionVoucherPageRoutingModule
  ],
  declarations: [CommissionVoucherPage]
})
export class CommissionVoucherPageModule {}
