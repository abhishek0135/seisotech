import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommissionVoucherPage } from './commission-voucher.page';

const routes: Routes = [
  {
    path: '',
    component: CommissionVoucherPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommissionVoucherPageRoutingModule {}
