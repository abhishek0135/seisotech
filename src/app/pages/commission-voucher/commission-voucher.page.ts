import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';

@Component({
  selector: 'app-commission-voucher',
  templateUrl: './commission-voucher.page.html',
  styleUrls: ['./commission-voucher.page.scss'],
})
export class CommissionVoucherPage implements OnInit {

  constructor(public appValues: AppConstants) { }

  ngOnInit() {
  }

}
