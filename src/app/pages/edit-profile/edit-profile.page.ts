import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  country = 'ind';
  state = 'mh';
  city = 'pune';
  
  sponsonIDError: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  save() {
    this.sponsonIDError = true;
  }
}
