import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KYCUploadPage } from './kycupload.page';

const routes: Routes = [
  {
    path: '',
    component: KYCUploadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KYCUploadPageRoutingModule {}
