import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KYCUploadPage } from './kycupload.page';

describe('KYCUploadPage', () => {
  let component: KYCUploadPage;
  let fixture: ComponentFixture<KYCUploadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KYCUploadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KYCUploadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
