import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.page.html',
  styleUrls: ['./delivery-address.page.scss'],
})
export class DeliveryAddressPage implements OnInit {

  constructor(
    public navCltr: NavController
  ) { }

  ngOnInit() {
  }
  payment() {
    this.navCltr.navigateForward('payment');
  }
}
