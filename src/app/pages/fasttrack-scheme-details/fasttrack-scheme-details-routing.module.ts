import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FasttrackSchemeDetailsPage } from './fasttrack-scheme-details.page';

const routes: Routes = [
  {
    path: '',
    component: FasttrackSchemeDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FasttrackSchemeDetailsPageRoutingModule {}
