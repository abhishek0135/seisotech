import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FasttrackSchemeDetailsPage } from './fasttrack-scheme-details.page';

describe('FasttrackSchemeDetailsPage', () => {
  let component: FasttrackSchemeDetailsPage;
  let fixture: ComponentFixture<FasttrackSchemeDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FasttrackSchemeDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FasttrackSchemeDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
