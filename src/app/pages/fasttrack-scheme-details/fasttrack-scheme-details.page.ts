import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-fasttrack-scheme-details',
  templateUrl: './fasttrack-scheme-details.page.html',
  styleUrls: ['./fasttrack-scheme-details.page.scss'],
})
export class FasttrackSchemeDetailsPage implements OnInit {

  fastTrackSchemeDetails = []
  constructor(private AppData: AppData, public appConstants: AppConstants) {
    this.fastTrackSchemeDetails = this.AppData.getFasttrackSchemeDetails();
  }

  ngOnInit() {
  }

}
