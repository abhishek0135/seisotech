import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FasttrackSchemeDetailsPageRoutingModule } from './fasttrack-scheme-details-routing.module';

import { FasttrackSchemeDetailsPage } from './fasttrack-scheme-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    FasttrackSchemeDetailsPageRoutingModule
  ],
  declarations: [FasttrackSchemeDetailsPage]
})
export class FasttrackSchemeDetailsPageModule {}
