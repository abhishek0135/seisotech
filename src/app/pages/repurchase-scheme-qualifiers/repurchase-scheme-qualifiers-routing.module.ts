import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RepurchaseSchemeQualifiersPage } from './repurchase-scheme-qualifiers.page';

const routes: Routes = [
  {
    path: '',
    component: RepurchaseSchemeQualifiersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RepurchaseSchemeQualifiersPageRoutingModule {}
