import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RepurchaseSchemeQualifiersPageRoutingModule } from './repurchase-scheme-qualifiers-routing.module';

import { RepurchaseSchemeQualifiersPage } from './repurchase-scheme-qualifiers.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RepurchaseSchemeQualifiersPageRoutingModule
  ],
  declarations: [RepurchaseSchemeQualifiersPage]
})
export class RepurchaseSchemeQualifiersPageModule {}
