import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RepurchaseSchemeQualifiersPage } from './repurchase-scheme-qualifiers.page';

describe('RepurchaseSchemeQualifiersPage', () => {
  let component: RepurchaseSchemeQualifiersPage;
  let fixture: ComponentFixture<RepurchaseSchemeQualifiersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepurchaseSchemeQualifiersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RepurchaseSchemeQualifiersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
