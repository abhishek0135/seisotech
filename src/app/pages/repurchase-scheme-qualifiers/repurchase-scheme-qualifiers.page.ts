import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-repurchase-scheme-qualifiers',
  templateUrl: './repurchase-scheme-qualifiers.page.html',
  styleUrls: ['./repurchase-scheme-qualifiers.page.scss'],
})
export class RepurchaseSchemeQualifiersPage implements OnInit {
  repurchaseSchemeQualifiers = []
  constructor(public AppData: AppData, public appConstants: AppConstants) {
    this.repurchaseSchemeQualifiers = this.AppData.getRepurchaseSchemeQualifiersDetails();
  }

  ngOnInit() {
  }
}
