import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TdsDetailsPage } from './tds-details.page';

const routes: Routes = [
  {
    path: '',
    component: TdsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TdsDetailsPageRoutingModule {}
