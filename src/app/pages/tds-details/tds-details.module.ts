import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TdsDetailsPageRoutingModule } from './tds-details-routing.module';

import { TdsDetailsPage } from './tds-details.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TdsDetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [TdsDetailsPage]
})
export class TdsDetailsPageModule {}
