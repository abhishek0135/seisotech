import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/utility/app.constants';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-tds-details',
  templateUrl: './tds-details.page.html',
  styleUrls: ['./tds-details.page.scss'],
})
export class TdsDetailsPage implements OnInit {

  tdsDetailsList = []
  constructor(private AppData: AppData, public appConstants: AppConstants) {
    this.tdsDetailsList = this.AppData.getTDSDetails();
  }

  ngOnInit() {
  }

}
