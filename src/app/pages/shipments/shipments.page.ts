import { Component, OnInit } from '@angular/core';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-shipments',
  templateUrl: './shipments.page.html',
  styleUrls: ['./shipments.page.scss'],
})
export class ShipmentsPage implements OnInit {
  orderDetailsData: any;
  orderItemExpanded = false;
  constructor(
    public appData: AppData,
  ) {
    this.orderDetailsData = appData.getOrderDetails();
    console.log(this.orderDetailsData);
  }

  ngOnInit() {
  }

  
  expandItem(): void {
    if (this.orderItemExpanded) {
      this.orderItemExpanded = false;
    } else {
      this.orderItemExpanded = true;
    }
  }
}
