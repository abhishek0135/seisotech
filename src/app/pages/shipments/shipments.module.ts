import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShipmentsPageRoutingModule } from './shipments-routing.module';

import { ShipmentsPage } from './shipments.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    ShipmentsPageRoutingModule
  ],
  declarations: [ShipmentsPage]
})
export class ShipmentsPageModule {}
