import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.page.html',
  styleUrls: ['./order-list.page.scss'],
})
export class OrderListPage implements OnInit {

  constructor(
    public navCltr: NavController
  ) { }

  ngOnInit() {
  }

  orderDetails() {
    this.navCltr.navigateForward('order-details');
  }

  orderReturn() {
    this.navCltr.navigateForward('order-return');
  }

}
