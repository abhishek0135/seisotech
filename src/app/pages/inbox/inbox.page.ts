import { Component, OnInit } from '@angular/core';
import { AppData } from 'src/app/utility/app.data';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage implements OnInit {
  sentInbox = []
  constructor(private appData: AppData) {
    this.sentInbox = this.appData.getInboxSentMessage();
   }

  ngOnInit() {
  }

}
