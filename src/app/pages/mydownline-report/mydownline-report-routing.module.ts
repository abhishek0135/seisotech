import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MydownlineReportPage } from './mydownline-report.page';

const routes: Routes = [
  {
    path: '',
    component: MydownlineReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MydownlineReportPageRoutingModule {}
