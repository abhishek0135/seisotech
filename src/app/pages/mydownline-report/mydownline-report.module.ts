import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MydownlineReportPageRoutingModule } from './mydownline-report-routing.module';

import { MydownlineReportPage } from './mydownline-report.page';
import { ComponentModule } from 'src/app/components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MydownlineReportPageRoutingModule,
    ComponentModule
  ],
  declarations: [MydownlineReportPage]
})
export class MydownlineReportPageModule {}
