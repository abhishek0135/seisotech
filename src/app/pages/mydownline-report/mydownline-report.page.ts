import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mydownline-report',
  templateUrl: './mydownline-report.page.html',
  styleUrls: ['./mydownline-report.page.scss'],
})
export class MydownlineReportPage implements OnInit {

  myDownlineReportData: any = {};
  constructor() {
    this.myDownlineReportData = {
      summary: {
        downlineCount: 9802,
        confirmDownlineCount: 8847,
        downlineBV: 12099954.00,
        confirmDownlineBV: 12101034.00,
        ownBV: 421.00,
        confirmOwnBV: 421.00,
        groupBV: 12100375.00,
        confirmGroupBV: 12101455.00
      },
      downlines: [
        {
          status: 'Paid',
          name: 'Komal Harshal Bankar',
          id: '100374',
          sponsorId: '461861',
          currentPackage: 'Free Package( 0.00 )',
          doj: '09/03/2020',
          level: '26'
        },
        {
          status: 'Paid',
          name: 'Shreekrishn Narayan Londhe',
          id: '100991',
          sponsorId: 'DGYPR3880Q',
          currentPackage: 'Free Package( 0.00 )',
          doj: '12/11/2019',
          level: '15'
        },
        {
          status: 'Paid',
          name: 'POPPY BORAH',
          id: '101122',
          sponsorId: '560427',
          currentPackage: 'Free Package( 0.00 )',
          doj: '05/12/2019',
          level: '17'
        }
      ]
    };
  }

  ngOnInit() {
  }

}
