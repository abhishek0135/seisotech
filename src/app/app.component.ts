import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';

import { Animation, AnimationController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppData } from './utility/app.data';
import { AppMenu } from './menu';
import { Values } from './utility/app.variables';
import { CommonUtility } from './utility/common.utility';
import { AppConstants } from './utility/app.constants';
/*  Menu Class Model*/
export class MemberMenu {
  Name: string;
  Id: number;
  Description: string;
  ActionName: string;
  url: string;
  IconName: string;
  Component: string;
  IsSelected: boolean;
  SubCategory: Array<MemberMenu>;

  constructor(
    name: string,
    id: number,
    description: string,
    actionName: string,
    url: string,
    iconName: string,
    component: string,
    subCategory: Array<MemberMenu>,
    isSelected: boolean = false
  ) {
    this.Name = name;
    this.Id = id;
    this.Description = description;
    this.ActionName = actionName;
    this.url = url;
    this.IconName = iconName;
    this.Component = component;
    this.IsSelected = isSelected;
    this.SubCategory = subCategory;
  }
}
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild('elementMenu', { static: false, read: ElementRef })
  elementMenuRef: ElementRef;
  @ViewChild('expandWrapper', { read: ElementRef }) expandWrapper: ElementRef;

  user: any = {};
  public elementMenuVar: Animation;
  public selectedIndex = 'Home';

  public appPages = [];
  memberMenu: Array<MemberMenu> = [];
  orderItemExpanded = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private animationCtrl: AnimationController,
    private appData: AppData,
    public renderer: Renderer2,
    public values: Values,
    public commonUtility: CommonUtility
  ) {
    this.initializeApp();
    if (
      this.commonUtility.getLocalstorage(
        AppConstants.localstorageKeys.cartCount
      )
    ) {
      this.values.cartCount = Number(
        this.commonUtility.getLocalstorage(
          AppConstants.localstorageKeys.cartCount
        )
      );
    }
    this.appPages = AppMenu.appPages;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString('#003049');
      this.splashScreen.hide();
      this.user = this.appData.getUserData();
    });
  }

  applyAnimation() {
    this.renderer.setStyle(this.expandWrapper.nativeElement, 'max-height', 150);
  }
  expandItem(parentPage) {
    setTimeout(() => {
      if (parentPage.open) {
        parentPage.open = false;
        // this.closeSubMenu(parentPage.childern);
      } else {
        parentPage.open = true;
      }
    }, 2000);
  }
  closeSubMenu(subPage) {
    if (subPage.open) {
      setTimeout((data) => {
        subPage.open = false;
      }, 2000);
    } else {
      subPage.open = true;
    }
  }
}
