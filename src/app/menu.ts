
export class AppMenu {
    public static appPages = [
        {
            id: 'Home',
            title: 'Home',
            url: '/home',
            icon: 'radio-button-on'
        },
        {
            id: 'Dashboard',
            title: 'Dashboard',
            url: '/dashboard',
            icon: 'radio-button-on'
        },
        {
            id: 'CategoryList',
            title: 'Category List',
            url: 'category-list',
            icon: 'radio-button-on'
        },
        {
            id: 'Wishlist',
            title: 'Wishlist',
            url: 'wishlist',
            icon: 'radio-button-on'
        },
        {
            id: 'ShoppingCart',
            title: 'Shopping Cart',
            url: 'shopping-cart',
            icon: 'radio-button-on'
        },

        {
            id: 'FAQ',
            title: 'FAQ',
            url: 'faq',
            icon: 'radio-button-on'
        },

        {
            id: 'Mydetails',
            title: 'My Details',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [
                {
                    id: 'Myprofile',
                    title: 'My Profile',
                    url: "/my-profile",
                    icon: "radio-button-on",
                },
                {
                    id: 'MyWallet',
                    title: 'My Wallet',
                    url: 'my-wallet',
                    icon: 'radio-button-on'
                },
                {
                    id: 'MyAddress',
                    title: 'My Address',
                    url: 'my-address',
                    icon: 'radio-button-on'
                },
                {
                    id: 'KYCUpload',
                    title: 'KYC Upload',
                    url: 'kycupload',
                    icon: 'radio-button-on'
                },
                {
                    id: 'Orderlist',
                    title: 'My Orders',
                    url: 'order-list',
                    icon: 'radio-button-on'
                },
                {
                    id: 'DownloadableDocumentList',
                    title: 'Downloadable Document List',
                    url: 'download-document',
                    icon: 'radio-button-on'
                },
                {
                    id: 'TDSDownloadFiles',
                    title: 'TDS Download Files',
                    url: 'download-tds',
                    icon: 'radio-button-on'
                },
            ]
        },
        {
            id: 'MyNetwork',
            title: 'My Network',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [
                {
                    id: 'Mygeneration',
                    title: 'My Generation',
                    url: "/generation-tree",
                    icon: "radio-button-on",
                },
                {
                    id: 'MyDirect',
                    title: 'My Direct',
                    url: 'my-direct',
                    icon: 'radio-button-on'
                },
                {
                    id: 'MyDirectBusiness',
                    title: 'My Direct Business',
                    url: 'direct-business-report',
                    icon: 'radio-button-on'
                },
                {
                    id: 'MyDownline',
                    title: 'My Downline',
                    url: 'mydownline-report',
                    icon: 'radio-button-on'
                },
            ]
        },

        {
            id: 'SignUp',
            title: 'Sign Up',
            url: 'sign-up',
            icon: 'radio-button-on'
        },
        {
            id: 'PageNotFound',
            title: 'Page Not Found',
            url: 'page-not-found',
            icon: 'radio-button-on'
        },
        {

            id: 'Mycommission',
            title: 'My Commission',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [
                {
                    id: 'CommissionLedger',
                    title: 'Commission Ledger',
                    url: 'commission-ledger',
                    icon: 'radio-button-on'
                },
                {
                    id: 'TDSDetails',
                    title: 'TDS Details',
                    url: 'tds-details',
                    icon: 'radio-button-on'
                },
                {
                    id: 'PointDetails',
                    title: 'Point Details',
                    url: 'point-details',
                    icon: 'radio-button-on'
                },
                {
                    id: 'FundQualification',
                    title: 'Fund Qualification Report',
                    url: 'fund-qualification-report',
                    icon: 'radio-button-on'
                },
                {
                    id: 'NewFundQualification',
                    title: 'New Fund Qualification Report',
                    url: 'new-fund-qualification-report',
                    icon: 'radio-button-on'
                },
                {
                    id: 'FreshBusinessQualificationReport',
                    title: 'Fresh Business Qualification Report',
                    url: 'fresh-business-qualification',
                    icon: 'radio-button-on'
                },
                {
                    id: 'ReferrelBusinessReport',
                    title: 'Referral Business Report',
                    url: 'referrrel-business-report',
                    icon: 'radio-button-on'
                },
                {
                    id: 'DBQualificationDetails',
                    title: 'DB Qualification Details',
                    url: 'db-qualification-details',
                    icon: 'radio-button-on'
                },
                {
                    id: 'DBIncomeDetails',
                    title: 'DB Income Details',
                    url: 'db-income-details',
                    icon: 'radio-button-on'
                },
                {
                    id: 'LBIncomeDetails',
                    title: 'LB Income Details',
                    url: 'lb-income-details',
                    icon: 'radio-button-on'
                },
                {
                    id: 'GBVIncomeDetails',
                    title: 'GBV Income Details',
                    url: 'gbv-income-details',
                    icon: 'radio-button-on'
                },
            ]
        },

        {
            id: 'ForgotPassword',
            title: 'Forgot Password',
            url: 'forgot-password',
            icon: 'radio-button-on'
        },
        {
            id: 'ChangePassword',
            title: 'Change Password',
            url: 'change-password',
            icon: 'radio-button-on'
        },
        {
            id: 'MyAchievements',
            title: 'My Achievements',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [
                {
                    id: 'SchemeOfferDetails',
                    title: 'Scheme & Offer Details',
                    url: "/schemes-and-offers",
                    icon: "radio-button-on",
                },
                {
                    id: 'FastTrackSchemeQualifiers',
                    title: 'Fast Track Scheme Qualifiers',
                    url: "/fasttrack-scheme-qualifiers",
                    icon: "radio-button-on",
                },
                {
                    id: 'FastTrackSchemeDetails',
                    title: 'Fast Track Scheme Details',
                    url: "/fasttrack-scheme-details",
                    icon: "radio-button-on",
                },
                {
                    id: 'MonthlySchemeDetails',
                    title: 'Monthly Scheme Details',
                    url: "/monthly-scheme-details",
                    icon: "radio-button-on",
                },
                {
                    id: 'Repurchase Scheme Qualifiers',
                    title: 'Repurchase Scheme Qualifiers',
                    url: "/repurchase-scheme-qualifiers",
                    icon: "radio-button-on",
                },
                {
                    id: 'MonthlySchemeQualifiers',
                    title: 'Monthly Scheme Qualifiers',
                    url: "/monthly-scheme-qualifiers",
                    icon: "radio-button-on",
                },
            ]
        },
        {
            id: 'Utilities',
            title: 'Utilities',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [{
                id: 'Communicationcenter',
                title: 'Communication Center',
                url: '',
                icon: 'radio-button-on',
                open: false,
                childern: [
                    {
                        id: 'Inbox',
                        title: 'Inbox',
                        url: 'inbox',
                        icon: 'radio-button-on'
                    },
                    {
                        id: 'Composemessage',
                        title: 'Compose Message',
                        url: 'compose-message',
                        icon: 'radio-button-on'
                    },
                    {
                        id: 'Sentemessage',
                        title: 'Sent Message',
                        url: 'compose-message',
                        icon: 'radio-button-on'
                    },
                    {
                        id: 'Folders',
                        title: 'Folders',
                        url: 'compose-message',
                        icon: 'radio-button-on'
                    },
                    {
                        id: 'Addnewticket',
                        title: 'Add New Ticket',
                        url: 'add-new-ticket',
                        icon: 'radio-button-on'
                    },
                ]
            },
            ]
        },

        {
            id: 'Addreview',
            title: 'Add Review',
            url: 'add-rating-review',
            icon: 'radio-button-on'
        },
        {
            id: 'Mywallet',
            title: 'My Wallet',
            url: '',
            icon: 'radio-button-on',
            open: false,
            childern: [
                {
                    id: 'SetE-walletpassword',
                    title: 'Set E-wallet Password',
                    url: 'set-ewallet-password',
                    icon: 'radio-button-on'
                },
            ]
        },
        {
            id: 'Vouchers',
            title: 'Vouchers',
            url: 'voucher',
            icon: 'radio-button-on'
        }
    ];
}