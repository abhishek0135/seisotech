(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-kycupload-kycupload-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/kycupload/kycupload.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/kycupload/kycupload.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesKycuploadKycuploadPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'KYC Upload'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding\">\n  <!-- <div class=\"card\">\n    <div class=\"card-header\">PAN CARD</div>\n    <div class=\"card-body bt0\">\n      <ion-row>\n        <ion-col size=\"4\" class=\"ion-no-padding\">\n          <img class=\"upload-image\" src=\"assets/pan.jpg\" />\n        </ion-col>\n        <div class=\"image-icon\">\n          <ion-row>\n            <ion-col>\n              <ion-button fill=\"clear\"><ion-icon name=\"cloud-upload-outline\"></ion-icon></ion-button>\n            </ion-col>\n            <ion-col >\n              <ion-button fill=\"clear\"><ion-icon name=\"close-circle-outline\"></ion-icon></ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n        <ion-col size=\"8\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label position=\"stacked\"> Pan Card Number </ion-label>\n            <ion-input class=\"placeholder-text\" type=\"text\" placeholder=\"Enter Pan Card Number\">\n            </ion-input>\n          </ion-item>\n          <ion-row>\n            <ion-col>\n              <ion-text class=\"text-wrap\"><ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong> 06/10/2020</ion-text>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <ion-text>\n                <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n              </ion-text>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div> -->\n\n<!-- <ion-card class=\"ion-no-padding\">\n  <ion-card-header>\n    <ion-card-subtitle>PAN CARD</ion-card-subtitle>\n  </ion-card-header>\n  <ion-card-content class=\"ion-no-padding\">\n    <ion-row class=\"img-row ion-no-padding\">\n      <ion-col size=\"8\" class=\"ion-no-padding\">\n        <img class=\"card-image\" src=\"assets/pan.jpg\" />\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-button shape=\"round\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n        <ion-button shape=\"round\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col=\"9\">\n        <ion-item class=\"ion-no-padding\">\n          <ion-label position=\"stacked\"> Pan Card Number </ion-label>\n          <ion-input class=\"placeholder-text\" type=\"text\" placeholder=\"Enter Pan Card Number\">\n          </ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"3\" class=\"ion-text-right\">\n        <ion-chip color=\"primary\" class=\"save-chip\">save</ion-chip>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-text>\n          <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n        </ion-text>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-text>\n          <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n        </ion-text>\n      </ion-col>\n    </ion-row>\n  </ion-card-content>\n</ion-card> -->\n\n<!-- <ion-card  class=\"ion-no-padding\">\n  <ion-card-header>\n    <ion-card-subtitle>PAN CARD</ion-card-subtitle>\n  </ion-card-header>\n  <ion-card-content >\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"6\" class=\"common-col\">\n        <ion-row class=\"ion-no-padding\">\n          <ion-col>\n            <img class=\"pan-card-img\" src=\"assets/pan.jpg\" />\n          </ion-col>\n        </ion-row>\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"6\">\n            <ion-button shape=\"round\" class=\"upload-save-btn\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n          </ion-col>\n          <ion-col size=\"6\">\n            <ion-button shape=\"round\" class=\"upload-save-btn\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"6\" class=\"common-col\">\n        <ion-item class=\"ion-no-padding input-content\">\n          <ion-label position=\"stacked\"> Pan Card Number </ion-label>\n          <ion-input class=\"placeholder-text\" type=\"text\" placeholder=\"Enter Pan Number\">\n          </ion-input>\n        </ion-item>\n        <ion-button shape=\"round\">save</ion-button>\n      </ion-col>\n    </ion-row>\n    <div class=\"\">\n      <ion-row >\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n          </ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n          </ion-text>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-card-content>\n</ion-card> -->\n\n<div class=\"card mb15\">\n  <div class=\"card-header\">PAN CARD</div>\n    <div class=\"card-body bt0\">\n      <ion-row class=\"ion-no-padding img-row-new\">\n        <ion-col size=\"7\" class=\"ion-no-padding\">\n          <img class=\"card-image\" src=\"assets/pan.jpg\" />\n        </ion-col>\n        <ion-col size=\"5\" class=\"remove-upload-btn\">\n          <ion-button shape=\"round\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n          <ion-button shape=\"round\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col=\"9\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label position=\"stacked\"> Pan Card Number </ion-label>\n            <ion-input class=\"placeholder-text\" type=\"text\" placeholder=\"Enter Pan Card Number\">\n            </ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"3\" class=\"ion-text-right\">\n          <ion-chip color=\"primary\" class=\"save-chip\">save</ion-chip>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n          </ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n          </ion-text>\n        </ion-col>\n      </ion-row>\n  </div>\n</div>\n\n<div class=\"card mb15\">\n  <div class=\"card-header\">Aadhar Card Front</div>\n    <div class=\"card-body bt0\">\n      <ion-row class=\"ion-no-padding img-row-new\">\n        <ion-col size=\"7\" class=\"ion-no-padding\">\n          <img class=\"card-image\" src=\"assets/aadhar.jpg\" />\n        </ion-col>\n        <ion-col size=\"5\" class=\"remove-upload-btn\">\n          <ion-button shape=\"round\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n          <ion-button shape=\"round\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col=\"9\">\n          <ion-item class=\"ion-no-padding\">\n            <ion-label position=\"stacked\"> Aadhar Card Number </ion-label>\n            <ion-input class=\"placeholder-text\" type=\"text\" placeholder=\"Enter Aadhar Card Number\">\n            </ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"3\" class=\"ion-text-right\">\n          <ion-chip color=\"primary\" class=\"save-chip\">save</ion-chip>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n          </ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n          </ion-text>\n        </ion-col>\n      </ion-row>\n  </div>\n</div>\n\n<div class=\"card mb15\">\n  <div class=\"card-header\">Aadhar Card Back</div>\n    <div class=\"card-body bt0\">\n      <ion-row class=\"ion-no-padding img-row-new\">\n        <ion-col size=\"7\" class=\"ion-no-padding\">\n          <img class=\"card-image\" src=\"assets/aadhar.jpg\" />\n        </ion-col>\n        <ion-col size=\"5\" class=\"remove-upload-btn\">\n          <ion-button shape=\"round\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n          <ion-button shape=\"round\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n          </ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n          </ion-text>\n        </ion-col>\n      </ion-row>\n  </div>\n</div>\n\n<div class=\"card mb15\">\n  <div class=\"card-header\">Passbook/Cheque Book</div>\n    <div class=\"card-body bt0\">\n      <ion-row class=\"ion-no-padding img-row-new\">\n        <ion-col size=\"7\" class=\"ion-no-padding\">\n          <img class=\"card-image\" src=\"assets/aadhar.jpg\" />\n        </ion-col>\n        <ion-col size=\"5\" class=\"remove-upload-btn\">\n          <ion-button shape=\"round\"><ion-icon name=\"cloud-upload-outline\"></ion-icon>Upload</ion-button>\n          <ion-button shape=\"round\"><ion-icon name=\"close-circle-outline\"></ion-icon>Remove</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"calendar-outline\"></ion-icon><strong>Upload Date:</strong>06/10/2020\n          </ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text><ion-icon name=\"document-text-outline\"></ion-icon><strong>Status:</strong> Pending</ion-text>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-text>\n            <ion-icon name=\"bookmark-outline\"></ion-icon><strong>Remark:</strong>This is Remark Text\n          </ion-text>\n        </ion-col>\n      </ion-row>\n  </div>\n</div>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/kycupload/kycupload-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/kycupload/kycupload-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: KYCUploadPageRoutingModule */

    /***/
    function srcAppPagesKycuploadKycuploadRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "KYCUploadPageRoutingModule", function () {
        return KYCUploadPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _kycupload_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./kycupload.page */
      "./src/app/pages/kycupload/kycupload.page.ts");

      var routes = [{
        path: '',
        component: _kycupload_page__WEBPACK_IMPORTED_MODULE_3__["KYCUploadPage"]
      }];

      var KYCUploadPageRoutingModule = function KYCUploadPageRoutingModule() {
        _classCallCheck(this, KYCUploadPageRoutingModule);
      };

      KYCUploadPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], KYCUploadPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/kycupload/kycupload.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/kycupload/kycupload.module.ts ***!
      \*****************************************************/

    /*! exports provided: KYCUploadPageModule */

    /***/
    function srcAppPagesKycuploadKycuploadModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "KYCUploadPageModule", function () {
        return KYCUploadPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _kycupload_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./kycupload-routing.module */
      "./src/app/pages/kycupload/kycupload-routing.module.ts");
      /* harmony import */


      var _kycupload_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./kycupload.page */
      "./src/app/pages/kycupload/kycupload.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var KYCUploadPageModule = function KYCUploadPageModule() {
        _classCallCheck(this, KYCUploadPageModule);
      };

      KYCUploadPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _kycupload_routing_module__WEBPACK_IMPORTED_MODULE_5__["KYCUploadPageRoutingModule"]],
        declarations: [_kycupload_page__WEBPACK_IMPORTED_MODULE_6__["KYCUploadPage"]]
      })], KYCUploadPageModule);
      /***/
    },

    /***/
    "./src/app/pages/kycupload/kycupload.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/pages/kycupload/kycupload.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesKycuploadKycuploadPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".upload-image {\n  margin: 0 0px 0 0px;\n  width: 115px;\n  flex: none;\n  height: 50%;\n}\n\n.image-icon {\n  position: absolute;\n  top: 165px;\n  width: 110px;\n}\n\n.image-icon ion-button {\n  height: 30px;\n  width: 25px;\n}\n\n.image-icon ion-icon {\n  border: 1px solid;\n  border-radius: 50%;\n  min-width: 25px;\n  min-height: 25px;\n}\n\n.placeholder-text {\n  font-size: 14px;\n}\n\n.bt0 {\n  padding-bottom: 0px !important;\n}\n\n.text-wrap {\n  display: initial;\n  overflow-wrap: break-word;\n  word-spacing: -2px;\n}\n\nion-card ion-card-header {\n  border-bottom: 1px solid;\n}\n\nion-card ion-button {\n  width: 100%;\n}\n\nion-card .img-row {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n\nion-card .img-row ion-button ion-icon {\n  min-width: 15px;\n  min-height: 15px;\n}\n\nion-card .card-image {\n  height: 100px;\n  width: 100%;\n}\n\nion-card .save-chip {\n  border: 1px solid;\n  margin-top: 15px;\n  font-size: 13px;\n}\n\n.pan-card-img {\n  height: 50px;\n  width: 100%;\n  border: 2px solid;\n  border-radius: 5px;\n}\n\n.upload-save-btn {\n  font-size: 10px;\n  height: 25px;\n}\n\n.common-col {\n  border: 1px solid gray;\n  margin-top: 3px;\n  border-radius: 7px;\n}\n\n.common-col .input-content {\n  font-size: 12px;\n}\n\n.img-row-new {\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n\n.img-row-new ion-button {\n  width: 100%;\n}\n\n.img-row-new ion-button ion-icon {\n  min-width: 15px;\n  min-height: 15px;\n}\n\n.img-row-new .card-image {\n  height: 100px;\n  width: 100%;\n}\n\n.save-chip {\n  border: 1px solid;\n  margin-top: 15px;\n  font-size: 13px;\n}\n\n.pd0 {\n  padding: 0 !important;\n}\n\n.mb15 {\n  margin-bottom: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMva3ljdXBsb2FkL2t5Y3VwbG9hZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBQUVKOztBQURJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUFHUjs7QUFESTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFHUjs7QUFBQTtFQUNJLGVBQUE7QUFHSjs7QUFEQTtFQUNJLDhCQUFBO0FBSUo7O0FBRkE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFLSjs7QUFDSTtFQUNJLHdCQUFBO0FBRVI7O0FBQUk7RUFDSSxXQUFBO0FBRVI7O0FBQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFFUjs7QUFFWTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQUFoQjs7QUFJSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0FBRlI7O0FBTUk7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQUpSOztBQVFBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTEo7O0FBT0E7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQUpKOztBQU1BO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFISjs7QUFJSTtFQUNJLGVBQUE7QUFGUjs7QUFRQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQUxKOztBQVFJO0VBQ0ksV0FBQTtBQU5SOztBQU9RO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBTFo7O0FBUUk7RUFDSSxhQUFBO0VBQ0EsV0FBQTtBQU5SOztBQVlBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUFUSjs7QUFXQTtFQUNJLHFCQUFBO0FBUko7O0FBVUE7RUFDSSxtQkFBQTtBQVBKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMva3ljdXBsb2FkL2t5Y3VwbG9hZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudXBsb2FkLWltYWdlIHtcbiAgICBtYXJnaW46IDAgMHB4IDAgMHB4O1xuICAgIHdpZHRoOiAxMTVweDtcbiAgICBmbGV4OiBub25lO1xuICAgIGhlaWdodDogNTAlO1xufVxuLmltYWdlLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDE2NXB4O1xuICAgIHdpZHRoOiAxMTBweDtcbiAgICBpb24tYnV0dG9ue1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgIHdpZHRoOiAyNXB4O1xuICAgIH1cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIG1pbi13aWR0aDogMjVweDtcbiAgICAgICAgbWluLWhlaWdodDogMjVweDtcbiAgICB9XG59XG4ucGxhY2Vob2xkZXItdGV4dCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuLmJ0MCB7XG4gICAgcGFkZGluZy1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuLnRleHQtd3JhcCB7XG4gICAgZGlzcGxheTogaW5pdGlhbDtcbiAgICBvdmVyZmxvdy13cmFwOiBicmVhay13b3JkO1xuICAgIHdvcmQtc3BhY2luZzogLTJweDtcbn1cblxuLy9uZXcgY2FyZFxuXG5pb24tY2FyZCB7XG4gICAgaW9uLWNhcmQtaGVhZGVyIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkO1xuICAgIH1cbiAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5pbWctcm93IHtcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkO1xuICAgICAgICAvLyBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMTVweDtcbiAgICAgICAgICAgICAgICBtaW4taGVpZ2h0OiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIC5jYXJkLWltYWdlIHtcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkO1xuICAgICAgICAvLyBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgfVxuICAgIC5zYXZlLWNoaXAge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgIH1cbn1cbi8vIFRoaXJkIENhcmRcbi5wYW4tY2FyZC1pbWcge1xuICAgIGhlaWdodDogNTBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IDJweCBzb2xpZDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4udXBsb2FkLXNhdmUtYnRuIHtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xufVxuLmNvbW1vbi1jb2wge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xuICAgIGJvcmRlci1yYWRpdXM6IDdweDsgICAgXG4gICAgLmlucHV0LWNvbnRlbnQge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgfVxufVxuXG4vLyBmb3VydGhcblxuLmltZy1yb3ctbmV3IHtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkO1xuICAgIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICAgIG1pbi13aWR0aDogMTVweDtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDE1cHg7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLmNhcmQtaW1hZ2Uge1xuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQ7XG4gICAgICAgIC8vIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG59XG5cbi5zYXZlLWNoaXAge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuLnBkMCB7XG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xufVxuLm1iMTUge1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/kycupload/kycupload.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/kycupload/kycupload.page.ts ***!
      \***************************************************/

    /*! exports provided: KYCUploadPage */

    /***/
    function srcAppPagesKycuploadKycuploadPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "KYCUploadPage", function () {
        return KYCUploadPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KYCUploadPage = /*#__PURE__*/function () {
        function KYCUploadPage() {
          _classCallCheck(this, KYCUploadPage);
        }

        _createClass(KYCUploadPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return KYCUploadPage;
      }();

      KYCUploadPage.ctorParameters = function () {
        return [];
      };

      KYCUploadPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-kycupload',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./kycupload.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/kycupload/kycupload.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./kycupload.page.scss */
        "./src/app/pages/kycupload/kycupload.page.scss"))["default"]]
      })], KYCUploadPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-kycupload-kycupload-module-es5.js.map