(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-referrrel-business-report-referrrel-business-report-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referrrel-business-report/referrrel-business-report.page.html":
    /*!***************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referrrel-business-report/referrrel-business-report.page.html ***!
      \***************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesReferrrelBusinessReportReferrrelBusinessReportPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Referrel Business Report'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of referrelBusinessReport\">\n\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div>Name</div>\n        <div><strong>{{report.name}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">ID</div>\n        <div>{{report.id}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Distributer ID</div>\n        <div>{{report.distributorID}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Order no</div>\n        <div>{{report.orderNo}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Order Date</div>\n        <div>{{report.orderDate}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Order no</div>\n        <div>{{report.orderNo}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Order Date</div>\n        <div>{{report.orderDate}}</div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Order Amount</div>\n        <div>{{report.orderAmount}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Referrel Bonus</div>\n        <div>{{report.referrelBouns}}</div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">TDS Amount</div>\n        <div>{{report.TDSamount}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Admin Bonus</div>\n        <div>{{report.adminAmount}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div class=\"text-secondary\">Net Amount</div>\n        <div>{{report.netamount}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/referrrel-business-report/referrrel-business-report-routing.module.ts":
    /*!*********************************************************************************************!*\
      !*** ./src/app/pages/referrrel-business-report/referrrel-business-report-routing.module.ts ***!
      \*********************************************************************************************/

    /*! exports provided: ReferrrelBusinessReportPageRoutingModule */

    /***/
    function srcAppPagesReferrrelBusinessReportReferrrelBusinessReportRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReferrrelBusinessReportPageRoutingModule", function () {
        return ReferrrelBusinessReportPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _referrrel_business_report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./referrrel-business-report.page */
      "./src/app/pages/referrrel-business-report/referrrel-business-report.page.ts");

      var routes = [{
        path: '',
        component: _referrrel_business_report_page__WEBPACK_IMPORTED_MODULE_3__["ReferrrelBusinessReportPage"]
      }];

      var ReferrrelBusinessReportPageRoutingModule = function ReferrrelBusinessReportPageRoutingModule() {
        _classCallCheck(this, ReferrrelBusinessReportPageRoutingModule);
      };

      ReferrrelBusinessReportPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ReferrrelBusinessReportPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/referrrel-business-report/referrrel-business-report.module.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/referrrel-business-report/referrrel-business-report.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: ReferrrelBusinessReportPageModule */

    /***/
    function srcAppPagesReferrrelBusinessReportReferrrelBusinessReportModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReferrrelBusinessReportPageModule", function () {
        return ReferrrelBusinessReportPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _referrrel_business_report_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./referrrel-business-report-routing.module */
      "./src/app/pages/referrrel-business-report/referrrel-business-report-routing.module.ts");
      /* harmony import */


      var _referrrel_business_report_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./referrrel-business-report.page */
      "./src/app/pages/referrrel-business-report/referrrel-business-report.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var ReferrrelBusinessReportPageModule = function ReferrrelBusinessReportPageModule() {
        _classCallCheck(this, ReferrrelBusinessReportPageModule);
      };

      ReferrrelBusinessReportPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _referrrel_business_report_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReferrrelBusinessReportPageRoutingModule"]],
        declarations: [_referrrel_business_report_page__WEBPACK_IMPORTED_MODULE_6__["ReferrrelBusinessReportPage"]]
      })], ReferrrelBusinessReportPageModule);
      /***/
    },

    /***/
    "./src/app/pages/referrrel-business-report/referrrel-business-report.page.scss":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/referrrel-business-report/referrrel-business-report.page.scss ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesReferrrelBusinessReportReferrrelBusinessReportPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZmVycnJlbC1idXNpbmVzcy1yZXBvcnQvcmVmZXJycmVsLWJ1c2luZXNzLXJlcG9ydC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/pages/referrrel-business-report/referrrel-business-report.page.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/referrrel-business-report/referrrel-business-report.page.ts ***!
      \***********************************************************************************/

    /*! exports provided: ReferrrelBusinessReportPage */

    /***/
    function srcAppPagesReferrrelBusinessReportReferrrelBusinessReportPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReferrrelBusinessReportPage", function () {
        return ReferrrelBusinessReportPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var ReferrrelBusinessReportPage = /*#__PURE__*/function () {
        function ReferrrelBusinessReportPage(appData, appConstants) {
          _classCallCheck(this, ReferrrelBusinessReportPage);

          this.appData = appData;
          this.appConstants = appConstants;
          this.referrelBusinessReport = [];
          this.referrelBusinessReport = this.appData.getReferrelBonusBusinessReport();
        }

        _createClass(ReferrrelBusinessReportPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ReferrrelBusinessReportPage;
      }();

      ReferrrelBusinessReportPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      ReferrrelBusinessReportPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-referrrel-business-report',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./referrrel-business-report.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referrrel-business-report/referrrel-business-report.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./referrrel-business-report.page.scss */
        "./src/app/pages/referrrel-business-report/referrrel-business-report.page.scss"))["default"]]
      })], ReferrrelBusinessReportPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-referrrel-business-report-referrrel-business-report-module-es5.js.map