(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-profile-edit-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header\n  [titleText]=\"'Edit Profile'\"\n  [backButton]=\"true\"\n></app-app-header>\n<!-- <ion-content\n\n  class=\"ion-padding\"\n  color=\"light\"\n  style=\"--offset-top: 0px; --offset-bottom: 0px\"\n>\n  <div class=\"card\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Name\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Phone\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Phone\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Email\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"email\"\n          placeholder=\"Enter Email\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Address\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Address\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n    </ion-list>\n    <ion-list>\n      <ion-radio-group>\n        <ion-list-header>\n          <ion-label class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n            Gender\n          </ion-label>\n        </ion-list-header>\n        <ion-item class=\"item\">\n          <ion-label class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n            Male\n          </ion-label>\n          <ion-radio color=\"primary\" value=\"Male\"> </ion-radio>\n        </ion-item>\n        <ion-item class=\"item\">\n          <ion-label class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n            Female\n          </ion-label>\n          <ion-radio color=\"primary\" value=\"Female\"> </ion-radio>\n        </ion-item>\n        <ion-item class=\"item\">\n          <ion-label class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n            Other\n          </ion-label>\n          <ion-radio color=\"primary\" value=\"Other\"> </ion-radio>\n        </ion-item>\n      </ion-radio-group>\n    </ion-list>\n    <div class=\"ion-padding\">\n      <ion-button class=\"ion-no-margin\" expand=\"block\">\n        SAVE\n      </ion-button>\n    </div>\n  </div>\n</ion-content> -->\n<ion-content\n  color=\"light\"\n  class=\"ion-padding\"\n  style=\"--offset-top: 0px; --offset-bottom: 0px\"\n>\n  <div class=\"card\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          First Name\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter First Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\"\n        >Enter First Name</ion-text\n      >\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Middle Name\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Middle Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Last Name\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Last Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\"\n        >Enter Last Name</ion-text\n      >\n    </ion-list>\n    <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Address\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Full Address\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Pin Code\n        </ion-label>\n        <ion-input type=\"tel\" class=\"sc-ion-input-md-h sc-ion-input-md-s\" \n        placeholder=\"Enter Pin Code\">\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n          >Select Country</ion-label\n        >\n        <ion-select [(ngModel)]=\"country\">\n          <ion-select-option value=\"ind\">India</ion-select-option>\n          <ion-select-option value=\"usa\">USA</ion-select-option>\n          <ion-select-option value=\"uk\">UK</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n          >Select City</ion-label\n        >\n        <ion-select [(ngModel)]=\"city\">\n          <ion-select-option value=\"pune\">Pune</ion-select-option>\n          <ion-select-option value=\"usa\">Nagpur</ion-select-option>\n          <ion-select-option value=\"uk\">Nashik</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n          >Select City</ion-label\n        >\n        <ion-select [(ngModel)]=\"state\">\n          <ion-select-option value=\"mh\">Maharashtra</ion-select-option>\n          <ion-select-option value=\"usa\">Gujrat</ion-select-option>\n          <ion-select-option value=\"uk\">Bihar</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Mobile Number\n        </ion-label>\n        <ion-row>\n          <ion-col>\n            <ion-input\n              [disabled]=\"true\"\n              type=\"tel\"\n              class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n              value=\"India(+91)\"\n            >\n            </ion-input>\n          </ion-col>\n          <ion-col>\n            <ion-input\n              type=\"tel\"\n              class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n              placeholder=\"9999999999\"\n              [disabled]=\"true\"\n              value=\"9561262672\"\n            >\n            </ion-input>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Sponsor ID\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Upline ID\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\"\n        >Enter Sponsor ID</ion-text\n      >\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Referral ID\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Referral ID\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n      <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\"\n        >Enter Referral ID</ion-text\n      >\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Aadhar Card No.\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Aadhar card No\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          A/C Holder Name\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter A/C Holder Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          A/C Number\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter A/C Number\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Bank Name\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Bank Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          IFSC Code\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter IFSC Code \"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Branch Name\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Branch Name\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Bank Address\n        </ion-label>\n        <ion-input\n          type=\"text\"\n          placeholder=\"Enter Bank Address\"\n          class=\"sc-ion-input-md-h sc-ion-input-md-s\"\n        >\n        </ion-input>\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n    Save\n  </ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: EditProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function() { return EditProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/pages/edit-profile/edit-profile.page.ts");




const routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
    }
];
let EditProfilePageRoutingModule = class EditProfilePageRoutingModule {
};
EditProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.module.ts ***!
  \***********************************************************/
/*! exports provided: EditProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-profile-routing.module */ "./src/app/pages/edit-profile/edit-profile-routing.module.ts");
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-profile.page */ "./src/app/pages/edit-profile/edit-profile.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let EditProfilePageModule = class EditProfilePageModule {
};
EditProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
    })
], EditProfilePageModule);



/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".errorText {\n  padding-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZWRpdC1wcm9maWxlL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3JUZXh0IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/edit-profile/edit-profile.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/edit-profile/edit-profile.page.ts ***!
  \*********************************************************/
/*! exports provided: EditProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePage", function() { return EditProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let EditProfilePage = class EditProfilePage {
    constructor() {
        this.country = 'ind';
        this.state = 'mh';
        this.city = 'pune';
        this.sponsonIDError = false;
    }
    ngOnInit() {
    }
    save() {
        this.sponsonIDError = true;
    }
};
EditProfilePage.ctorParameters = () => [];
EditProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./edit-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./edit-profile.page.scss */ "./src/app/pages/edit-profile/edit-profile.page.scss")).default]
    })
], EditProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-edit-profile-edit-profile-module-es2015.js.map