(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-add-rating-review-add-rating-review-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-rating-review/add-rating-review.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-rating-review/add-rating-review.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAddRatingReviewAddRatingReviewPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Write your Own Review'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding\" style=\"--offset-top: 0px; --offset-bottom: 0px\">\n    <div class=\"card mb-2\">\n        <ion-list class=\"ion-no-margin ion-no-padding mb-2\" lines=\"full\">\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Review title\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Enter review title\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n            <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Review Text\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-textarea type=\"text\" rows=\"4\" placeholder=\"Enter review text\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-textarea>\n            </ion-item>\n            <div class=\"ml-3  pt-2 mb-0\">\n                <ion-label position=\"stacked\">\n                    Rating\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n            </div>\n            <div class=\"rating ml-3 mb-0 p-0\">\n                <span tappable (click)=\"yourRating(1)\" class=\"star-icon\" [ngClass]=\"{full: form.rating >= 1}\">&#x2605;</span>\n                <span tappable (click)=\"yourRating(2)\" class=\"star-icon\" [ngClass]=\"{full: form.rating >= 2}\">&#x2605;</span>\n                <span tappable (click)=\"yourRating(3)\" class=\"star-icon\" [ngClass]=\"{full: form.rating >= 3}\">&#x2605;</span>\n                <span tappable (click)=\"yourRating(4)\" class=\"star-icon\" [ngClass]=\"{full: form.rating >= 4}\">&#x2605;</span>\n                <span tappable (click)=\"yourRating(5)\" class=\"star-icon\" [ngClass]=\"{full: form.rating >= 5}\">&#x2605;</span>\n            </div>\n        </ion-list>\n    </div>\n    <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n        Submit review\n    </ion-button>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/add-rating-review/add-rating-review-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/add-rating-review/add-rating-review-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: AddRatingReviewPageRoutingModule */

    /***/
    function srcAppPagesAddRatingReviewAddRatingReviewRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddRatingReviewPageRoutingModule", function () {
        return AddRatingReviewPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _add_rating_review_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-rating-review.page */
      "./src/app/pages/add-rating-review/add-rating-review.page.ts");

      var routes = [{
        path: '',
        component: _add_rating_review_page__WEBPACK_IMPORTED_MODULE_3__["AddRatingReviewPage"]
      }];

      var AddRatingReviewPageRoutingModule = function AddRatingReviewPageRoutingModule() {
        _classCallCheck(this, AddRatingReviewPageRoutingModule);
      };

      AddRatingReviewPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AddRatingReviewPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/add-rating-review/add-rating-review.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/add-rating-review/add-rating-review.module.ts ***!
      \*********************************************************************/

    /*! exports provided: AddRatingReviewPageModule */

    /***/
    function srcAppPagesAddRatingReviewAddRatingReviewModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddRatingReviewPageModule", function () {
        return AddRatingReviewPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _add_rating_review_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./add-rating-review-routing.module */
      "./src/app/pages/add-rating-review/add-rating-review-routing.module.ts");
      /* harmony import */


      var _add_rating_review_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-rating-review.page */
      "./src/app/pages/add-rating-review/add-rating-review.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var AddRatingReviewPageModule = function AddRatingReviewPageModule() {
        _classCallCheck(this, AddRatingReviewPageModule);
      };

      AddRatingReviewPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _add_rating_review_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddRatingReviewPageRoutingModule"]],
        declarations: [_add_rating_review_page__WEBPACK_IMPORTED_MODULE_6__["AddRatingReviewPage"]]
      })], AddRatingReviewPageModule);
      /***/
    },

    /***/
    "./src/app/pages/add-rating-review/add-rating-review.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/add-rating-review/add-rating-review.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAddRatingReviewAddRatingReviewPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "@charset \"UTF-8\";\n.rating .star-icon {\n  color: #ddd;\n  font-size: 40px;\n  position: relative;\n  text-shadow: 2px 2px 2px #a4a4a4;\n}\n.rating .star-icon.full:before {\n  color: #f3c815;\n  content: \"★\";\n  position: absolute;\n  left: 0;\n}\n.rating .star-icon.half:before {\n  color: #f3c815;\n  content: \"★\";\n  position: absolute;\n  left: 0;\n  width: 50%;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRkLXJhdGluZy1yZXZpZXcvYWRkLXJhdGluZy1yZXZpZXcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUNaO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdDQUFBO0FBQ1I7QUFDSTtFQUNJLGNBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0FBQ1I7QUFFSTtFQUNJLGNBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FBQVIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hZGQtcmF0aW5nLXJldmlldy9hZGQtcmF0aW5nLXJldmlldy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmF0aW5nIHtcbiAgICAuc3Rhci1pY29uIHtcbiAgICAgICAgY29sb3I6ICNkZGQ7XG4gICAgICAgIGZvbnQtc2l6ZTogNDBweDtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCAycHggI2E0YTRhNDtcbiAgICB9XG4gICAgLnN0YXItaWNvbi5mdWxsOmJlZm9yZSB7XG4gICAgICAgIGNvbG9yOiAjZjNjODE1O1xuICAgICAgICBjb250ZW50OiAnXFwyNjA1JztcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBcbiAgICB9XG4gICAgLnN0YXItaWNvbi5oYWxmOmJlZm9yZSB7XG4gICAgICAgIGNvbG9yOiAjZjNjODE1O1xuICAgICAgICBjb250ZW50OiAnXFwyNjA1JztcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/add-rating-review/add-rating-review.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/add-rating-review/add-rating-review.page.ts ***!
      \*******************************************************************/

    /*! exports provided: AddRatingReviewPage */

    /***/
    function srcAppPagesAddRatingReviewAddRatingReviewPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddRatingReviewPage", function () {
        return AddRatingReviewPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AddRatingReviewPage = /*#__PURE__*/function () {
        function AddRatingReviewPage() {
          _classCallCheck(this, AddRatingReviewPage);

          this.form = {};
          this.form.rating = 0;
        }

        _createClass(AddRatingReviewPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "yourRating",
          value: function yourRating(rating) {
            this.form.rating = rating;
          }
        }]);

        return AddRatingReviewPage;
      }();

      AddRatingReviewPage.ctorParameters = function () {
        return [];
      };

      AddRatingReviewPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-rating-review',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-rating-review.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-rating-review/add-rating-review.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-rating-review.page.scss */
        "./src/app/pages/add-rating-review/add-rating-review.page.scss"))["default"]]
      })], AddRatingReviewPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-add-rating-review-add-rating-review-module-es5.js.map