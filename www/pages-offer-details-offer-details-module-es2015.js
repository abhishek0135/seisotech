(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-offer-details-offer-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offer-details/offer-details.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offer-details/offer-details.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header\n  [titleText]=\"'Offer Details'\"\n  [backButton]=\"true\"\n></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let offer of offerDetail\">\n    <ion-row class=\"ion-no-padding\" tappable>\n      <ion-col size=\"9\" class=\"text-left\">\n        <strong>{{ offer.name }}</strong>\n      </ion-col>\n      <ion-col size=\"3\" class=\"text-right\">\n        <ion-icon name=\"chevron-forward-outline\" class=\"next-icon\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Start Date</div>\n        <div>{{ offer.startDate }}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">End Date</div>\n        <div>{{ offer.endDate }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col>\n        <div>\n          <ion-label position=\"floating\" class=\"text-secondary\"\n            >Scheme Description</ion-label\n          >\n          <ion-textarea\n            rows=\"4\"\n            class=\"ion-no-padding\"\n            placeholder=\"Enter scheme description...\"\n            [value]=\"offer.schemeDescription\"\n          ></ion-textarea>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/offer-details/offer-details-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/offer-details/offer-details-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: OfferDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferDetailsPageRoutingModule", function() { return OfferDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _offer_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./offer-details.page */ "./src/app/pages/offer-details/offer-details.page.ts");




const routes = [
    {
        path: '',
        component: _offer_details_page__WEBPACK_IMPORTED_MODULE_3__["OfferDetailsPage"]
    }
];
let OfferDetailsPageRoutingModule = class OfferDetailsPageRoutingModule {
};
OfferDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OfferDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/offer-details/offer-details.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/offer-details/offer-details.module.ts ***!
  \*************************************************************/
/*! exports provided: OfferDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferDetailsPageModule", function() { return OfferDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _offer_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./offer-details-routing.module */ "./src/app/pages/offer-details/offer-details-routing.module.ts");
/* harmony import */ var _offer_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./offer-details.page */ "./src/app/pages/offer-details/offer-details.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let OfferDetailsPageModule = class OfferDetailsPageModule {
};
OfferDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _offer_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["OfferDetailsPageRoutingModule"]
        ],
        declarations: [_offer_details_page__WEBPACK_IMPORTED_MODULE_6__["OfferDetailsPage"]]
    })
], OfferDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/offer-details/offer-details.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/offer-details/offer-details.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29mZmVyLWRldGFpbHMvb2ZmZXItZGV0YWlscy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/offer-details/offer-details.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/offer-details/offer-details.page.ts ***!
  \***********************************************************/
/*! exports provided: OfferDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferDetailsPage", function() { return OfferDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let OfferDetailsPage = class OfferDetailsPage {
    constructor(navCltr, route, router) {
        this.navCltr = navCltr;
        this.route = route;
        this.router = router;
        this.route.queryParams.subscribe((params) => {
            debugger;
            // this.offerDetail = params;
        });
        console.log(this.offerDetail);
    }
    ngOnInit() {
    }
};
OfferDetailsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
OfferDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-offer-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./offer-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/offer-details/offer-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./offer-details.page.scss */ "./src/app/pages/offer-details/offer-details.page.scss")).default]
    })
], OfferDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-offer-details-offer-details-module-es2015.js.map