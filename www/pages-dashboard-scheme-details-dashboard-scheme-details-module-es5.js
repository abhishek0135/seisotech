(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-scheme-details-dashboard-scheme-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDashboardSchemeDetailsDashboardSchemeDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Scheme Details'\" [backButton]=\"true\"></app-app-header>\n<ion-content  color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let schemeDetail of schemeDetails\">\n    <ion-row class=\"ion-no-padding\" tappable (click)=\"offerDetails(schemeDetail)\">\n      <ion-col size=\"9\" class=\"text-left\">\n          <strong>{{ schemeDetail.name }}</strong>\n      </ion-col>\n      <ion-col size=\"3\" class=\"text-right\">\n        <ion-icon name=\"chevron-forward-outline\" class=\"next-icon\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"6\" class=\"text-left\">\n          <div class=\"text-secondary\">Start Date</div>\n          <div>{{ schemeDetail.startDate }}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n          <div class=\"text-secondary\">End Date</div>\n          <div>{{ schemeDetail.endDate }}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col>\n        <div >\n          <ion-label position=\"floating\" class=\"text-secondary\">Scheme Description</ion-label>\n          <ion-textarea rows=\"4\" class=\"ion-no-padding\" placeholder=\"Enter scheme description...\" [value]=\"schemeDetail.schemeDescription\"></ion-textarea>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details-routing.module.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/pages/dashboard-scheme-details/dashboard-scheme-details-routing.module.ts ***!
      \*******************************************************************************************/

    /*! exports provided: DashboardSchemeDetailsPageRoutingModule */

    /***/
    function srcAppPagesDashboardSchemeDetailsDashboardSchemeDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardSchemeDetailsPageRoutingModule", function () {
        return DashboardSchemeDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _dashboard_scheme_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./dashboard-scheme-details.page */
      "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.ts");

      var routes = [{
        path: '',
        component: _dashboard_scheme_details_page__WEBPACK_IMPORTED_MODULE_3__["DashboardSchemeDetailsPage"]
      }];

      var DashboardSchemeDetailsPageRoutingModule = function DashboardSchemeDetailsPageRoutingModule() {
        _classCallCheck(this, DashboardSchemeDetailsPageRoutingModule);
      };

      DashboardSchemeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DashboardSchemeDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.module.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: DashboardSchemeDetailsPageModule */

    /***/
    function srcAppPagesDashboardSchemeDetailsDashboardSchemeDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardSchemeDetailsPageModule", function () {
        return DashboardSchemeDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _dashboard_scheme_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./dashboard-scheme-details-routing.module */
      "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details-routing.module.ts");
      /* harmony import */


      var _dashboard_scheme_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./dashboard-scheme-details.page */
      "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var DashboardSchemeDetailsPageModule = function DashboardSchemeDetailsPageModule() {
        _classCallCheck(this, DashboardSchemeDetailsPageModule);
      };

      DashboardSchemeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _dashboard_scheme_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardSchemeDetailsPageRoutingModule"]],
        declarations: [_dashboard_scheme_details_page__WEBPACK_IMPORTED_MODULE_6__["DashboardSchemeDetailsPage"]]
      })], DashboardSchemeDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDashboardSchemeDetailsDashboardSchemeDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1zY2hlbWUtZGV0YWlscy9kYXNoYm9hcmQtc2NoZW1lLWRldGFpbHMucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.ts ***!
      \*********************************************************************************/

    /*! exports provided: DashboardSchemeDetailsPage */

    /***/
    function srcAppPagesDashboardSchemeDetailsDashboardSchemeDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardSchemeDetailsPage", function () {
        return DashboardSchemeDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var DashboardSchemeDetailsPage = /*#__PURE__*/function () {
        function DashboardSchemeDetailsPage(appData, navCltr) {
          _classCallCheck(this, DashboardSchemeDetailsPage);

          this.appData = appData;
          this.navCltr = navCltr;
          this.schemeDetails = [];
          this.schemeDetails = this.appData.getDashboardSchemeDetails();
        }

        _createClass(DashboardSchemeDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "offerDetails",
          value: function offerDetails(details) {
            console.log(details);
            var navigationExtras = {
              queryParams: {
                offerDetails: JSON.stringify(details)
              }
            };
            this.navCltr.navigateForward(['offer-details'], navigationExtras);
          }
        }]);

        return DashboardSchemeDetailsPage;
      }();

      DashboardSchemeDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      DashboardSchemeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard-scheme-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./dashboard-scheme-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./dashboard-scheme-details.page.scss */
        "./src/app/pages/dashboard-scheme-details/dashboard-scheme-details.page.scss"))["default"]]
      })], DashboardSchemeDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-dashboard-scheme-details-dashboard-scheme-details-module-es5.js.map