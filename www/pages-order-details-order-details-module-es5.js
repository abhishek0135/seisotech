(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-order-details-order-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-details/order-details.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-details/order-details.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesOrderDetailsOrderDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Order #52'\" [backButton]=\"true\" [toShowPopoverButton]=\"true\"></app-app-header>\n<ion-toolbar class=\"ion-no-padding\">\n  <div class=\"ion-text-center status-header\">\n    <ion-icon name=\"checkmark-circle-outline\" color=\"success\"></ion-icon>\n    <strong>{{ orderDetailsData.shippingInfo.shippingStatus }}</strong>\n  </div>\n</ion-toolbar>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card\">\n    <div class=\"p-1\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"person-outline\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"10\">\n          <h6 class=\"float-left\">\n            <strong>\n              Billing Address\n            </strong>\n          </h6>\n        </ion-col>\n      </ion-row>\n      <div class=\"card-body card-contain\">\n        <div class=\"p-1\">\n          <div class=\"justify-content-between mb-2\">\n            <div class=\"text-secondary\">Phone Number</div>\n            <div class=\"billing-text\">\n              {{ orderDetailsData.billingAddress.phoneNo}}\n            </div>\n          </div>\n          <div class=\"justify-content-between mb-2\">\n            <div class=\"text-secondary\">Email Address</div>\n            <div class=\"billing-text\">\n              {{ orderDetailsData.billingAddress.emailId }}\n            </div>\n          </div>\n          <div class=\"justify-content-between mb-2\">\n            <div class=\"text-secondary\">Address</div>\n            <div class=\"billing-text\">\n              {{ orderDetailsData.billingAddress.address }}\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"card mt15\">\n    <div class=\"p-1\" tappable (click)=\"expandItem()\">\n      <ion-row>\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"cart-outline\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"5\">\n          <strong class=\"payment-text\">Product(s)</strong>\n        </ion-col>\n        <ion-col size=\"5\" class=\"ion-text-end\">\n          <strong>{{ orderDetailsData.orderItems.length }} Item(s)</strong>\n        </ion-col>\n        <ion-col size=\"1\">\n          <ion-icon\n            *ngIf=\"!orderItemExpanded\"\n            name=\"chevron-down-outline\"\n            class=\"expandable-icon\"\n          ></ion-icon>\n          <ion-icon\n            *ngIf=\"orderItemExpanded\"\n            name=\"chevron-up-outline\"\n            class=\"expandable-icon\"\n          ></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n    <app-expandable\n      [expanded]=\"orderItemExpanded\"\n      *ngFor=\"let product of orderDetailsData.orderItems\"\n    >\n      <div class=\"d-flex p-1 shop-cart-item-bottom\">\n        <div class=\"shop-cart-left\">\n          <img alt=\"img\" class=\"not-found-img\" [src]=\"product.imageUrl\" />\n        </div>\n        <div class=\"shop-cart-right\">\n          <h6 class=\"font-weight-bold text-dark\">{{ product.name }}</h6>\n          <h6 class=\"text-dark mb-2\">${{ product.price }}</h6>\n          <div\n            class=\"small text-gray-500 d-flex align-items-center justify-content-between\"\n          >\n            <small class=\"text-secondary\">BV: 60.00</small>\n          </div>\n        </div>\n      </div>\n    </app-expandable>\n  </div>\n\n  <div class=\"card mt15\">\n    <div class=\"p-1\">\n      <ion-row>\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"card-outline\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"6\">\n          <strong class=\"payment-text\">Payment</strong>\n        </ion-col>\n        <ion-col size=\"5\" class=\"ion-text-end\">\n          <div>\n            <small>{{ orderDetailsData.paymentInfo.payment }}</small>\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class=\"card-body card-contain\">\n      <div class=\"p-1\">\n        <div class=\"d-flex justify-content-between mb-2\">\n          <div class=\"text-secondary\">Payment Method</div>\n          <div class=\"text-secondary\">\n            {{ orderDetailsData.paymentInfo.paymentMethod }}\n          </div>\n        </div>\n        <div class=\"d-flex justify-content-between mb-2\">\n          <div class=\"text-secondary\">Payment Status</div>\n          <div class=\"text-secondary\">\n            {{ orderDetailsData.paymentInfo.paymentStatus }}\n          </div>\n        </div>\n        <div class=\"d-flex justify-content-between mb-2\">\n          <div class=\"text-secondary\">Remaining Amount</div>\n          <div class=\"text-secondary\">\n            {{ orderDetailsData.paymentInfo.remainingAmount }}\n          </div>\n        </div>\n        <div class=\"d-flex justify-content-between mb-2\">\n          <div class=\"text-secondary\">Total BV</div>\n          <div class=\"text-secondary\">517.00</div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"card mb-2 mt15\">\n    <div class=\"p-1\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"logo-usd\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"7\">\n          <strong class=\"payment-text\">Order Amount</strong>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class=\"p-2\">\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Sub-Total</div>\n        <div class=\"text-secondary\">$1520.00</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Shipping</div>\n        <div class=\"text-secondary\">$0.00</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Tax</div>\n        <div class=\"text-secondary\">$0.00</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Total BV</div>\n        <div class=\"text-secondary\">517.00</div>\n      </div>\n    </div>\n    <div\n      class=\"d-flex justify-content-between align-items-center py-2 px-2 border-top text-dark\"\n    >\n      <h6 class=\"font-weight-bold m-0\">Order Total</h6>\n      <h6 class=\"font-weight-bold m-0\">$1520.00</h6>\n    </div>\n  </div>\n\n  <div class=\"card mt15\">\n    <div class=\"p-1\">\n      <ion-row>\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"car-outline\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"7\">\n          <strong class=\"payment-text\">Shipping</strong>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-end\">\n          <!-- <small>{{ orderDetailsData.shippingInfo.shippingMethod }}</small> -->\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class=\"card-body card-contain\">\n      <div class=\"p-1\">\n        <div class=\"justify-content-between mb-2\">\n          <div class=\"text-secondary\">Shipping Method</div>\n          <div class=\"billing-text\">\n            {{ orderDetailsData.shippingInfo.shippingMethod }}\n          </div>\n        </div>\n        <div class=\"justify-content-between mb-2\">\n          <div class=\"text-secondary\">\n            {{ orderDetailsData.shippingInfo.shippingStatus }}\n          </div>\n          <div class=\"billing-text\">\n            Delivered\n          </div>\n        </div>\n        <div class=\"justify-content-between mb-2\">\n          <div class=\"text-secondary\">Shipping Address</div>\n          <div class=\"billing-text\">\n            {{ orderDetailsData.billingAddress.address }}\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"card mt15\">\n    <div class=\"p-1\">\n      <ion-row tappable (click)=\"expandShipmentInfo()\">\n        <ion-col size=\"1\">\n          <ion-icon\n            name=\"car-outline\"\n            color=\"primary\"\n            class=\"icon-size\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col size=\"5\">\n          <strong class=\"payment-text\">Shipments</strong>\n        </ion-col>\n        <ion-col size=\"5\" class=\"ion-text-end\"></ion-col>\n        <ion-col size=\"1\" class=\"ion-text-left\">\n          <ion-icon\n            *ngIf=\"!shipmentItemExpanded\"\n            name=\"chevron-down-outline\"\n            class=\"expandable-payment-icon\"\n          ></ion-icon>\n          <ion-icon\n            *ngIf=\"shipmentItemExpanded\"\n            name=\"chevron-up-outline\"\n            class=\"expandable-payment-icon\"\n          ></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n    <app-expandable [expanded]=\"shipmentItemExpanded\">\n      <ion-list\n        class=\"ion-no-padding p-1 shipment-bottom\"\n        *ngFor=\"let product of orderDetailsData.orderItems\"\n      >\n        <ion-row\n          tappable\n          (click)=\"shpmentDetails(orderDetailsData.shipmentsInfo)\"\n        >\n          <ion-col size=\"12\">\n            <ion-row class=\"shipment-details\">\n              <ion-col size=\"11\">\n                <strong class=\"shipment-id\">Shipment ID:&nbsp;</strong>\n                <span>&nbsp;8</span>\n              </ion-col>\n              <ion-col size=\"1\">\n                <ion-icon\n                  name=\"chevron-forward-outline\"\n                  class=\"detail-icon\"\n                ></ion-icon>\n              </ion-col>\n            </ion-row>\n            <div class=\"p-1\">\n              <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-secondary\">Tracking number</div>\n                <div class=\"text-secondary\">\n                  {{ orderDetailsData.shipmentsInfo.trackingNumber }}\n                </div>\n              </div>\n              <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-secondary\">Date shipped</div>\n                <div class=\"text-secondary\">\n                  {{ orderDetailsData.shipmentsInfo.dateShipped }}\n                </div>\n              </div>\n              <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-secondary\">Date delivered</div>\n                <div class=\"text-secondary\">\n                  {{ orderDetailsData.shipmentsInfo.dateDelivered }}\n                </div>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n    </app-expandable>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n    Re-order\n  </ion-button>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./src/app/pages/order-details/order-details-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/order-details/order-details-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: OrderDetailsPageRoutingModule */

    /***/
    function srcAppPagesOrderDetailsOrderDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderDetailsPageRoutingModule", function () {
        return OrderDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _order_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./order-details.page */
      "./src/app/pages/order-details/order-details.page.ts");

      var routes = [{
        path: '',
        component: _order_details_page__WEBPACK_IMPORTED_MODULE_3__["OrderDetailsPage"]
      }];

      var OrderDetailsPageRoutingModule = function OrderDetailsPageRoutingModule() {
        _classCallCheck(this, OrderDetailsPageRoutingModule);
      };

      OrderDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OrderDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/order-details/order-details.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/order-details/order-details.module.ts ***!
      \*************************************************************/

    /*! exports provided: OrderDetailsPageModule */

    /***/
    function srcAppPagesOrderDetailsOrderDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function () {
        return OrderDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _order_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./order-details-routing.module */
      "./src/app/pages/order-details/order-details-routing.module.ts");
      /* harmony import */


      var _order_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./order-details.page */
      "./src/app/pages/order-details/order-details.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var OrderDetailsPageModule = function OrderDetailsPageModule() {
        _classCallCheck(this, OrderDetailsPageModule);
      };

      OrderDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _order_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderDetailsPageRoutingModule"]],
        declarations: [_order_details_page__WEBPACK_IMPORTED_MODULE_6__["OrderDetailsPage"]]
      })], OrderDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/order-details/order-details.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/pages/order-details/order-details.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesOrderDetailsOrderDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".pt0 {\n  padding-top: 0;\n}\n\n.card-contain {\n  padding: 0px 0px 0.5rem 0.5rem;\n}\n\n.mt15 {\n  margin-top: 15px;\n}\n\n.expandable-icon {\n  color: grey;\n  font-size: 17px;\n}\n\n.old-price {\n  font-size: 0.7rem;\n  text-decoration: line-through;\n}\n\n.payment-text {\n  font-size: 16px;\n}\n\n.mt3 {\n  margin-top: 3px;\n}\n\n.icon-size {\n  font-size: 20px;\n}\n\n.status-header ion-icon {\n  vertical-align: text-top;\n  margin-right: 10px;\n  font-size: 1.1rem;\n}\n\n.status-header strong {\n  font-size: 16px;\n}\n\n.view-detail-btn {\n  height: 20px;\n  width: 50%;\n  font-size: 10px;\n}\n\n.shipments-item {\n  font-size: 13px;\n}\n\n.detail-icon {\n  height: 100%;\n  display: block;\n  vertical-align: middle;\n}\n\n.expandable-payment-icon {\n  color: grey;\n  font-size: 17px;\n}\n\n.shipment-bottom {\n  border-bottom: 1px solid gray;\n}\n\n.shipment-details .shipment-id {\n  color: #003049;\n}\n\n.pl10 {\n  padding-left: 10px;\n}\n\n.billing-text {\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXItZGV0YWlscy9vcmRlci1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSjs7QUFDQTtFQUNJLDhCQUFBO0FBRUo7O0FBQUE7RUFDSSxnQkFBQTtBQUdKOztBQURBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFJSjs7QUFGQTtFQUNJLGlCQUFBO0VBQ0EsNkJBQUE7QUFLSjs7QUFIQTtFQUNJLGVBQUE7QUFNSjs7QUFKQTtFQUNJLGVBQUE7QUFPSjs7QUFMQTtFQUNJLGVBQUE7QUFRSjs7QUFMSTtFQUNJLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQVFSOztBQUxJO0VBQ0ksZUFBQTtBQU9SOztBQUpBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FBT0o7O0FBTEE7RUFFSSxlQUFBO0FBT0o7O0FBTEE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0FBUUo7O0FBTkE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtBQVNKOztBQVBBO0VBQ0ksNkJBQUE7QUFVSjs7QUFQSTtFQUNJLGNBQUE7QUFVUjs7QUFQQTtFQUNJLGtCQUFBO0FBVUo7O0FBTEE7RUFDSSxZQUFBO0FBUUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9vcmRlci1kZXRhaWxzL29yZGVyLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnB0MCB7XG4gICAgcGFkZGluZy10b3A6IDA7XG59XG4uY2FyZC1jb250YWluIHtcbiAgICBwYWRkaW5nOiAwcHggMHB4IDAuNXJlbSAwLjVyZW07XG59XG4ubXQxNSB7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5leHBhbmRhYmxlLWljb24ge1xuICAgIGNvbG9yOiBncmV5OyAgICBcbiAgICBmb250LXNpemU6IDE3cHg7XG59XG4ub2xkLXByaWNlIHtcbiAgICBmb250LXNpemU6IDAuN3JlbTtcbiAgICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbn1cbi5wYXltZW50LXRleHQge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5tdDMge1xuICAgIG1hcmdpbi10b3A6IDNweDtcbn1cbi5pY29uLXNpemUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5zdGF0dXMtaGVhZGVyIHtcbiAgICBpb24taWNvbiB7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICBmb250LXNpemU6IDEuMXJlbTtcbiAgICBcbiAgICB9XG4gICAgc3Ryb25nIHtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgIH1cbn1cbi52aWV3LWRldGFpbC1idG57XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiA1MCU7XG4gICAgZm9udC1zaXplOiAxMHB4O1xufVxuLnNoaXBtZW50cy1pdGVtIHtcbiAgICAvLyBtYXJnaW46IC0xNXB4IDAgLTE1cHggMHB4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn1cbi5kZXRhaWwtaWNvbiB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG4uZXhwYW5kYWJsZS1wYXltZW50LWljb24ge1xuICAgIGNvbG9yOiBncmV5O1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbn1cbi5zaGlwbWVudC1ib3R0b20ge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmF5O1xufVxuLnNoaXBtZW50LWRldGFpbHMge1xuICAgIC5zaGlwbWVudC1pZCB7XG4gICAgICAgIGNvbG9yOiAjMDAzMDQ5O1xuICAgIH1cbn1cbi5wbDEwIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4uYmlsbGluZy1sYWJlbCB7XG4gICAgXG59XG4uYmlsbGluZy10ZXh0IHtcbiAgICBjb2xvcjogYmxhY2s7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/order-details/order-details.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/order-details/order-details.page.ts ***!
      \***********************************************************/

    /*! exports provided: OrderDetailsPage */

    /***/
    function srcAppPagesOrderDetailsOrderDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderDetailsPage", function () {
        return OrderDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var OrderDetailsPage = /*#__PURE__*/function () {
        function OrderDetailsPage(appData, navCltr) {
          _classCallCheck(this, OrderDetailsPage);

          this.appData = appData;
          this.navCltr = navCltr;
          this.orderItemExpanded = false;
          this.paymentInfoExpanded = false;
          this.shipmentItemExpanded = false;
        }

        _createClass(OrderDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.orderDetailsData = this.appData.getOrderDetails();
            console.log(this.orderDetailsData);
          }
        }, {
          key: "expandItem",
          value: function expandItem() {
            if (this.orderItemExpanded) {
              this.orderItemExpanded = false;
            } else {
              this.orderItemExpanded = true;
            }
          }
        }, {
          key: "expandedPaymentInfo",
          value: function expandedPaymentInfo() {
            if (this.paymentInfoExpanded) {
              this.paymentInfoExpanded = false;
            } else {
              this.paymentInfoExpanded = true;
            }
          }
        }, {
          key: "expandShipmentInfo",
          value: function expandShipmentInfo() {
            if (this.shipmentItemExpanded) {
              this.shipmentItemExpanded = false;
            } else {
              this.shipmentItemExpanded = true;
            }
          }
        }, {
          key: "shpmentDetails",
          value: function shpmentDetails(orderInfo) {
            this.navCltr.navigateForward('shipments');
          }
        }]);

        return OrderDetailsPage;
      }();

      OrderDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      OrderDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./order-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-details/order-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./order-details.page.scss */
        "./src/app/pages/order-details/order-details.page.scss"))["default"]]
      })], OrderDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-order-details-order-details-module-es5.js.map