(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-point-details-point-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/point-details/point-details.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/point-details/point-details.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Point Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n    <div class=\"card mt15 mb-2\" *ngFor=\"let pointDetail of pointDetails\">\n        <ion-row>\n            <ion-col size=\"12\" class=\"text-left\">\n                <div class=\"text-secondary\">Income Type</div>\n                <div>{{pointDetail.incomeType}}</div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">Heighest Leg BV</div>\n                <div>{{pointDetail.heighestLegBV}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">Second Heighest Leg BV</div>\n                <div>{{pointDetail.secondHeighestLegBV}}</div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">Qualification Factor</div>\n                <div>{{pointDetail.qualificationFactor}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">Total points</div>\n                <div>{{pointDetail.totalPoints}}</div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">Point value</div>\n                <div>{{pointDetail.pointValue}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">Income</div>\n                <div>{{pointDetail.income}}</div>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/point-details/point-details-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/point-details/point-details-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: PointDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointDetailsPageRoutingModule", function() { return PointDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _point_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./point-details.page */ "./src/app/pages/point-details/point-details.page.ts");




const routes = [
    {
        path: '',
        component: _point_details_page__WEBPACK_IMPORTED_MODULE_3__["PointDetailsPage"]
    }
];
let PointDetailsPageRoutingModule = class PointDetailsPageRoutingModule {
};
PointDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PointDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/point-details/point-details.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/point-details/point-details.module.ts ***!
  \*************************************************************/
/*! exports provided: PointDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointDetailsPageModule", function() { return PointDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _point_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./point-details-routing.module */ "./src/app/pages/point-details/point-details-routing.module.ts");
/* harmony import */ var _point_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./point-details.page */ "./src/app/pages/point-details/point-details.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let PointDetailsPageModule = class PointDetailsPageModule {
};
PointDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _point_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["PointDetailsPageRoutingModule"]
        ],
        declarations: [_point_details_page__WEBPACK_IMPORTED_MODULE_6__["PointDetailsPage"]]
    })
], PointDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/point-details/point-details.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/point-details/point-details.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BvaW50LWRldGFpbHMvcG9pbnQtZGV0YWlscy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/point-details/point-details.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/point-details/point-details.page.ts ***!
  \***********************************************************/
/*! exports provided: PointDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PointDetailsPage", function() { return PointDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let PointDetailsPage = class PointDetailsPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.pointDetails = [];
        this.pointDetails = this.appData.getCommissionPointDetails();
    }
    ngOnInit() {
    }
};
PointDetailsPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
PointDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-point-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./point-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/point-details/point-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./point-details.page.scss */ "./src/app/pages/point-details/point-details.page.scss")).default]
    })
], PointDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-point-details-point-details-module-es2015.js.map