(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-db-qualification-details-db-qualification-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-qualification-details/db-qualification-details.page.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-qualification-details/db-qualification-details.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'DB Qualification Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of DBQualificationReport\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div><strong>{{report.name}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-lefft\">\n        <div class=\"text-secondary\">Qualification Date</div>\n        <div>{{report.qualificationDate}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/db-qualification-details/db-qualification-details-routing.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/db-qualification-details/db-qualification-details-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: DbQualificationDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbQualificationDetailsPageRoutingModule", function() { return DbQualificationDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _db_qualification_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./db-qualification-details.page */ "./src/app/pages/db-qualification-details/db-qualification-details.page.ts");




const routes = [
    {
        path: '',
        component: _db_qualification_details_page__WEBPACK_IMPORTED_MODULE_3__["DbQualificationDetailsPage"]
    }
];
let DbQualificationDetailsPageRoutingModule = class DbQualificationDetailsPageRoutingModule {
};
DbQualificationDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DbQualificationDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/db-qualification-details/db-qualification-details.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/db-qualification-details/db-qualification-details.module.ts ***!
  \***********************************************************************************/
/*! exports provided: DbQualificationDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbQualificationDetailsPageModule", function() { return DbQualificationDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _db_qualification_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./db-qualification-details-routing.module */ "./src/app/pages/db-qualification-details/db-qualification-details-routing.module.ts");
/* harmony import */ var _db_qualification_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./db-qualification-details.page */ "./src/app/pages/db-qualification-details/db-qualification-details.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let DbQualificationDetailsPageModule = class DbQualificationDetailsPageModule {
};
DbQualificationDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _db_qualification_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["DbQualificationDetailsPageRoutingModule"]
        ],
        declarations: [_db_qualification_details_page__WEBPACK_IMPORTED_MODULE_6__["DbQualificationDetailsPage"]]
    })
], DbQualificationDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/db-qualification-details/db-qualification-details.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/db-qualification-details/db-qualification-details.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RiLXF1YWxpZmljYXRpb24tZGV0YWlscy9kYi1xdWFsaWZpY2F0aW9uLWRldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/db-qualification-details/db-qualification-details.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/db-qualification-details/db-qualification-details.page.ts ***!
  \*********************************************************************************/
/*! exports provided: DbQualificationDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbQualificationDetailsPage", function() { return DbQualificationDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let DbQualificationDetailsPage = class DbQualificationDetailsPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.DBQualificationReport = [];
        this.DBQualificationReport = this.appData.getDBQualificationReport();
    }
    ngOnInit() {
    }
};
DbQualificationDetailsPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
DbQualificationDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-db-qualification-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./db-qualification-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-qualification-details/db-qualification-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./db-qualification-details.page.scss */ "./src/app/pages/db-qualification-details/db-qualification-details.page.scss")).default]
    })
], DbQualificationDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-db-qualification-details-db-qualification-details-module-es2015.js.map