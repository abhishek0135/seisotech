(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-verify-mobile-verify-mobile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/verify-mobile/verify-mobile.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/verify-mobile/verify-mobile.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Verify Mobile'\" [backButton]=\"true\">\n</app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding shop-cart-page\">\n    <div class=\"card mb-2\">\n        <div class=\"border-bottom text-center p-3\">We will send an SMS with a confirmation code to this number</div>\n        <div class=\"p-3\">\n            <form class=\"card\">\n                <ion-list lines=\"full\">\n                    <ion-item>\n                        <ion-label position=\"stacked\"> Phone Number\n                            <ion-text color=\"danger\">*</ion-text>\n                        </ion-label>\n                        <ion-input placeholder=\"Enter Phone Number\" type=\"number\"></ion-input>\n                    </ion-item>\n                </ion-list>\n                <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n                    <ion-item>\n                        <ion-label position=\"stacked\">Use your referral code here</ion-label>\n                        <ion-input placeholder=\"Enter Code\" type=\"text\">\n                        </ion-input>\n                    </ion-item>\n                </ion-list>\n            </form>\n        </div>\n        <div class=\"p-3 border-top\">\n            <ion-button expand=\"block\" type=\"submit\" tabindex=\"0\" (click)=\"deliiveryAddress()\">Next</ion-button>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/verify-mobile/verify-mobile-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/verify-mobile/verify-mobile-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: VerifyMobilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyMobilePageRoutingModule", function() { return VerifyMobilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _verify_mobile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verify-mobile.page */ "./src/app/pages/verify-mobile/verify-mobile.page.ts");




const routes = [
    {
        path: '',
        component: _verify_mobile_page__WEBPACK_IMPORTED_MODULE_3__["VerifyMobilePage"]
    }
];
let VerifyMobilePageRoutingModule = class VerifyMobilePageRoutingModule {
};
VerifyMobilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VerifyMobilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/verify-mobile/verify-mobile.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/verify-mobile/verify-mobile.module.ts ***!
  \*************************************************************/
/*! exports provided: VerifyMobilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyMobilePageModule", function() { return VerifyMobilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _verify_mobile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./verify-mobile-routing.module */ "./src/app/pages/verify-mobile/verify-mobile-routing.module.ts");
/* harmony import */ var _verify_mobile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./verify-mobile.page */ "./src/app/pages/verify-mobile/verify-mobile.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let VerifyMobilePageModule = class VerifyMobilePageModule {
};
VerifyMobilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _verify_mobile_routing_module__WEBPACK_IMPORTED_MODULE_5__["VerifyMobilePageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_verify_mobile_page__WEBPACK_IMPORTED_MODULE_6__["VerifyMobilePage"]]
    })
], VerifyMobilePageModule);



/***/ }),

/***/ "./src/app/pages/verify-mobile/verify-mobile.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/verify-mobile/verify-mobile.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZlcmlmeS1tb2JpbGUvdmVyaWZ5LW1vYmlsZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/verify-mobile/verify-mobile.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/verify-mobile/verify-mobile.page.ts ***!
  \***********************************************************/
/*! exports provided: VerifyMobilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyMobilePage", function() { return VerifyMobilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let VerifyMobilePage = class VerifyMobilePage {
    constructor(navCltr) {
        this.navCltr = navCltr;
    }
    ngOnInit() {
    }
    deliiveryAddress() {
        this.navCltr.navigateForward('delivery-address');
    }
};
VerifyMobilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
VerifyMobilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-verify-mobile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./verify-mobile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/verify-mobile/verify-mobile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./verify-mobile.page.scss */ "./src/app/pages/verify-mobile/verify-mobile.page.scss")).default]
    })
], VerifyMobilePage);



/***/ })

}]);
//# sourceMappingURL=pages-verify-mobile-verify-mobile-module-es2015.js.map