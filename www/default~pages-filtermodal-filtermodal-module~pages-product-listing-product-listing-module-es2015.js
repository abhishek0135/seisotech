(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-filtermodal-filtermodal-module~pages-product-listing-product-listing-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"toolbar\">\n    <ion-row style=\"display: flex;flex-direction: row;\n    justify-content: space-between;align-items: center;padding: 2%;\">\n      <ion-text>Filter</ion-text>\n      <ion-icon (click)=\"closeModal()\" name=\"close-outline\"></ion-icon>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n<div >\n  <ion-row style=\"display: flex;\n  flex-direction: column;\n  padding: 5%;\">\n        <ion-text style=\"font-size: 18px;\n        line-height: 18px;\">Price Range</ion-text>\n\n      <ion-range *ngIf=\"clearInput\" (ionChange)=\"ionRange($event)\" style=\"padding: 0;\" dualKnobs=\"true\" min=\"0\" max=\"10000\" step=\"3\" value=\"0\" \n       snaps=\"true\" pin=\"true\"></ion-range>\n       <ion-range *ngIf=\"!clearInput\" (ionChange)=\"ionRange($event)\" style=\"padding: 0;\" dualKnobs=\"true\" min=\"0\" max=\"10000\" step=\"3\" value=\"0\" \n       snaps=\"true\" pin=\"true\"></ion-range>\n  </ion-row>\n  <div>\n    <ion-row>\n      <ion-text style=\"font-size: 18px;\n      padding: 0 20px;font-weight: bolder;\">Categories</ion-text>\n    </ion-row>\n    <ion-row>\n      <ion-list style=\"width: 100%;\">\n        <ion-item style=\"width: 100%;\" *ngFor=\"let item of categoryArray\">\n          <ion-checkbox [(ngModel)]=\"item.isChecked\" value={{item.id}} (ionChange)=\"ionchange($event,i)\" slot=\"start\" mode=\"md\"></ion-checkbox>\n          <ion-label>{{item.name}}({{item.cnt}})</ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n  </div>\n\n  <div style=\"margin-top: 10%;\"> \n    <ion-row>\n      <ion-text style=\"font-size: 18px;\n      padding: 0 20px;font-weight: bolder;\">Brand</ion-text>\n    </ion-row>\n    <ion-row>\n      <ion-list style=\"width: 100%;\">\n        <ion-item style=\"width: 100%;\" *ngFor=\"let item of brandArray\">\n        <ion-checkbox [(ngModel)]=\"item.isChecked\" value={{item.brand_id}} (ionChange)=\"ionchange1($event)\" slot=\"start\" mode=\"md\"></ion-checkbox>\n          <ion-label>{{item.brand_name}}</ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n  </div>\n</div>\n\n\n\n\n\n\n\n\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col (click)=\"apply()\" [size]=\"6\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;border-right: 2px solid lightgrey;\">\n        <ion-text style=\"font-size: 16px;\n        line-height: 18px;\n        padding: 0 6px;\">Apply</ion-text>\n      </ion-col>\n      <ion-col (click)=\"clear()\" [size]=\"6\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;\">\n        <ion-text style=\"font-size: 16px;\n        line-height: 18px;\n        padding: 0 6px;\">Clear</ion-text>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/filtermodal/filtermodal.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/filtermodal/filtermodal.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".toolbar {\n  --background: #003049 !important;\n}\n.toolbar ion-text {\n  color: #FFFFFF;\n  font-size: 20px;\n}\n.toolbar ion-icon {\n  color: #FFFFFF;\n  font-size: 28px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZmlsdGVybW9kYWwvZmlsdGVybW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0NBQUE7QUFDSjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZpbHRlcm1vZGFsL2ZpbHRlcm1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDMwNDkgIWltcG9ydGFudDtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XG4gICAgICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICB9XG4gIH1cbi8vICAgaW9uLXJhbmdlIHtcbi8vICAgICAtLWJhci1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuLy8gICAgIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiAjMDAzMDQ5O1xuLy8gICB9Il19 */");

/***/ }),

/***/ "./src/app/pages/filtermodal/filtermodal.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/filtermodal/filtermodal.page.ts ***!
  \*******************************************************/
/*! exports provided: FiltermodalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltermodalPage", function() { return FiltermodalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/app-api.service */ "./src/app/services/app-api.service.ts");





// import { Storage } from '@ionic/storage';
let FiltermodalPage = class FiltermodalPage {
    constructor(modalController, httpClient, appApiService) {
        this.modalController = modalController;
        this.httpClient = httpClient;
        this.appApiService = appApiService;
        this.categoryArray = [];
        this.brandArray = [];
        this.brandId = [];
        this.catId = [];
        this.clearInput = false;
    }
    ngOnInit() {
        // const formData = new FormData();
        // formData.append('product_id','');
        // formData.append('brand_id','');
        // formData.append('cat','');
        // formData.append('price_range','');
        // formData.append('user_id','3');
        this.httpClient.post('http://dev.agrowonmartpos.com/public/api/category', '')
            .subscribe(res => {
            console.log(res);
            this.categoryArray = res.category_list;
            this.categoryArray.forEach(element => {
                element.isChecked = false;
            });
            this.httpClient.post('http://dev.agrowonmartpos.com/public/api/brand', '')
                .subscribe(res => {
                console.log(res);
                this.brandArray = res.brand_list;
                this.brandArray.forEach(element => {
                    element.isChecked = false;
                });
            });
        });
    }
    ionchange($event) {
        console.log($event);
        if ($event.detail.checked) {
            this.selectedCatId = $event.detail.value;
            this.catId.push(this.selectedCatId);
            console.log(this.catId);
        }
        else if (!$event.detail.checked) {
            this.selectedCatId = $event.detail.value;
            const index = this.catId.indexOf(this.selectedCatId);
            console.log(index);
            if (index > -1) {
                this.catId.splice(index, 1);
            }
            console.log(this.catId);
        }
        else {
        }
        const totalItems = this.categoryArray.length;
        let checked = 0;
        this.categoryArray.map(obj => {
            if (obj.isChecked)
                checked++;
        });
        if (checked > 0 && checked < totalItems) {
            //If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked == totalItems) {
            //If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            //If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    }
    ionchange1($event) {
        console.log($event);
        if ($event.detail.checked) {
            this.selectedBrandId = $event.detail.value;
            this.brandId.push(this.selectedBrandId);
            console.log(this.brandId);
        }
        else if (!$event.detail.checked) {
            this.selectedBrandId = $event.detail.value;
            const index = this.brandId.indexOf(this.selectedBrandId);
            console.log(index);
            if (index > -1) {
                this.brandId.splice(index, 1);
            }
            console.log(this.brandId);
        }
        else {
        }
        const totalItems = this.categoryArray.length;
        let checked = 0;
        this.categoryArray.map(obj => {
            if (obj.isChecked)
                checked++;
        });
        if (checked > 0 && checked < totalItems) {
            //If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked == totalItems) {
            //If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            //If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
    }
    ionRange($event) {
        this.clearInput = false;
        console.log($event.detail.value);
        this.minimum = $event.detail.value.lower.toString();
        this.maximum = $event.detail.value.upper.toString();
        console.log(this.minimum);
        console.log(this.maximum);
        console.log(typeof (this.minimum));
        console.log(typeof (this.maximum));
        this.priceRange = (this.minimum + '-' + this.maximum);
        console.log(this.priceRange);
    }
    closeModal() {
        this.modalController.dismiss();
    }
    apply() {
        // const formData = new FormData();
        // formData.append('product_id','');
        // formData.append('brand_id',(this.brandId));
        // formData.append('cat_id',(this.catId));
        // formData.append('price_range',this.priceRange);
        // formData.append('user_id','3');
        // this.httpClient.post<any>('http://pos.agrowonmartpos.com/public/api/product', formData)
        // .subscribe(res => {
        //   console.log(res);
        // });
        this.appApiService.getProducts('', this.brandId, this.catId, this.priceRange).subscribe((respData) => {
            console.log(respData);
            this.modalController.dismiss(respData.ProductWise_List);
        });
    }
    clear() {
        this.clearInput = true;
        setTimeout(() => {
            this.categoryArray.forEach(obj => {
                obj.isChecked = this.masterCheck;
                this.brandArray.forEach(element => {
                    element.isChecked = this.masterCheck;
                });
            });
        });
    }
};
FiltermodalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"] }
];
FiltermodalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-filtermodal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./filtermodal.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./filtermodal.page.scss */ "./src/app/pages/filtermodal/filtermodal.page.scss")).default]
    })
], FiltermodalPage);



/***/ }),

/***/ "./src/app/services/app-api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/app-api.service.ts ***!
  \*********************************************/
/*! exports provided: AppApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppApiService", function() { return AppApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./base-api.service */ "./src/app/services/base-api.service.ts");




let AppApiService = class AppApiService {
    constructor(baseApi) {
        this.baseApi = baseApi;
    }
    getCategories() {
        const formData = new FormData();
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
    }
    getProducts(productId, brandId, catId, priceRange) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('brand_id', brandId);
        formData.append('cat_id', catId);
        formData.append('price_range', priceRange);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
    }
    getProduct_Search(search) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('search', search);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
    }
    addProductToCart(productId) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('action', 'insert');
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
    }
};
AppApiService.ctorParameters = () => [
    { type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"] }
];
AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AppApiService);



/***/ }),

/***/ "./src/app/services/base-api.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/base-api.service.ts ***!
  \**********************************************/
/*! exports provided: BaseApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseApiService", function() { return BaseApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utility/common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utility/app.variables */ "./src/app/utility/app.variables.ts");
/* harmony import */ var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utility/localstorage.utility */ "./src/app/utility/localstorage.utility.ts");
/* harmony import */ var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utility/enums */ "./src/app/utility/enums.ts");










let BaseApiService = class BaseApiService {
    constructor(commonUtility, http, nav, values, localstorageService, platform) {
        this.commonUtility = commonUtility;
        this.http = http;
        this.nav = nav;
        this.values = values;
        this.localstorageService = localstorageService;
        this.platform = platform;
        this.API_URL = '';
        this.isNetAvailable = true;
        this.API_URL = this.values.getApiUrl();
    }
    getApiCall(action, parameters) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
        const reqHeaderOption = this.getHeaderRequestOptions();
        console.log(reqHeaderOption);
        return this.http
            .get(apiCallUrl, reqHeaderOption)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    postApiCall(action, postData, param) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
        const reqHeaderOption = this.getHeaderRequestOptions();
        return this.http
            .post(apiCallUrl, postData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getHeaderRequestOptions() {
        const addHeaders = new Headers();
        // addHeaders.append('Content-Type', 'application/json');
        // addHeaders.append(
        //   'AuthorizedToken',
        //   this.localstorageService.getCustomerId()
        // );
        // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());
        return { headers: addHeaders };
    }
    generateFullURL(apiType, action, parameters) {
        let apiCallUrl = this.API_URL;
        let queryString = '';
        if (action !== '') {
            apiCallUrl = apiCallUrl + '/' + action;
        }
        switch (apiType) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                    queryString = '?';
                    for (let index = 0; index < parameters.length; index++) {
                        if (index > 0) {
                            queryString = queryString + '&';
                        }
                        const parameterKeyValue = parameters[index];
                        queryString =
                            queryString +
                                '' +
                                parameterKeyValue[0] +
                                '=' +
                                parameterKeyValue[1];
                    }
                    apiCallUrl = apiCallUrl + '' + queryString;
                }
        }
        return apiCallUrl;
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        switch (error.status) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
        }
        this.commonUtility.showErrorToast(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
BaseApiService.ctorParameters = () => [
    { type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"] },
    { type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] }
];
BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], BaseApiService);



/***/ }),

/***/ "./src/app/utility/localstorage.utility.ts":
/*!*************************************************!*\
  !*** ./src/app/utility/localstorage.utility.ts ***!
  \*************************************************/
/*! exports provided: LocalstorageUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function() { return LocalstorageUtility; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");




let LocalstorageUtility = class LocalstorageUtility {
    constructor(commonUtility) {
        this.commonUtility = commonUtility;
    }
    ProcessValue(value) {
        if (!this.commonUtility.isEmptyOrNull(value)) {
            return value;
        }
        else {
            return '';
        }
    }
    getCustomerId() {
        const value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        return this.ProcessValue(value);
    }
    clearLoginData() {
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
    }
};
LocalstorageUtility.ctorParameters = () => [
    { type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"] }
];
LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LocalstorageUtility);



/***/ })

}]);
//# sourceMappingURL=default~pages-filtermodal-filtermodal-module~pages-product-listing-product-listing-module-es2015.js.map