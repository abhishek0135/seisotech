(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"osahan-nav\">\n  <ion-toolbar class=\"in-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n    </ion-buttons>\n    <ion-title>\n      <small>Delivery Location</small> <br />\n      Bengaluru, India\n      <ion-icon name=\"create-outline\"></ion-icon>\n    </ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button color=\"primary\" class=\"top-cart\" (click)=\"goToCart()\">\n        <ion-badge color=\"primary\" mode=\"ios\" *ngIf=\"values.cartCount > 0\"\n          >{{values.cartCount}}</ion-badge\n        >\n        <ion-icon slot=\"start\" name=\"cart-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar>\n    \n    <ion-searchbar (click)=\"searchproductList('')\" \n      class=\"pt-1\"\n      placeholder=\"Search for products\"\n      inputmode=\"text\"\n      type=\"search\"\n    ></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content color=\"light\" *ngIf=\"data\">\n  <!-- Home Page Banner -->\n  <div class=\"homepage-slider\">\n    <ion-slides pager=\"true\">\n      <ion-slide (click)=\"productList('')\" *ngFor=\"let slide of slides\">\n        <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img\" [src]=\"slide\" />\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <!-- Home Page Advertisement -->\n  <!-- <ion-grid>\n        <ion-row>\n            <ion-col>\n                <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img rounded shadow-sm\" src=\"assets/ad/1.jpg\">\n            </ion-col>\n            <ion-col>\n                <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img rounded shadow-sm\" src=\"assets/ad/2.jpg\">\n            </ion-col>\n        </ion-row>\n    </ion-grid> -->\n\n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h5 class=\"mb-0 d-flex align-items-center\">\n            Featured categories\n            <!-- <ion-button size=\"small\" tabindex=\"0\" (click)=\"categoryList() \" class=\"ios button button-small ml-auto\">View All</ion-button> -->\n            <ion-button\n              size=\"small\"\n              tabindex=\"0\"\n              (click)=\"categoryList()\"\n              class=\"view-all-btn ml-auto\"\n            >\n              See All\n              <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let category of categories\">\n          <ion-card\n            class=\"ion-no-margin category-card\"\n            (click)=\"productList(category.id)\"\n          >\n            <div class=\"img-div\">\n              <img\n                [src]=\"category.image\"\n                onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n              />\n            </div>\n            <div class=\"featured-category-name\">{{category.name}}</div>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h5 class=\"mb-0 d-flex align-items-center\">\n            Featured products\n            <!-- <ion-button size=\"small\" tabindex=\"0\" (click)=\"categoryList() \" class=\"ios button button-small ml-auto\">View All</ion-button> -->\n            <ion-button\n              size=\"small\"\n              tabindex=\"0\"\n              (click)=\"productList('')\"\n              class=\"view-all-btn ml-auto\"\n            >\n              See All\n              <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        \n        <ion-col size=\"6\" *ngFor=\"let product of featuredProducts\">\n          <div\n            class=\"p-2 shop-homepage-item bg-white\"\n            tabindex=\"0\"\n            tappable\n            (click)=\"productDetails(product)\"\n          >\n            <div class=\"shop-cart-left\">\n              <img\n                alt=\"img\"\n                class=\"not-found-img\"\n                [src]=\"product.img\"\n                onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n              />\n            </div>\n            <div class=\"shop-cart-right\">\n              <div class=\"mb-2 product-name\">{{product.name}}</div>\n              <h6 class=\"font-weight-bold text-dark mb-2 price\">\n                {{product.dPrice}}\n                <span class=\"regular-price text-secondary font-weight-normal\"\n                  >{{product.price}}</span\n                >\n                <ion-badge color=\"primary\">{{'10%'}}</ion-badge>\n              </h6>\n              <!-- <div\n                class=\"small text-gray-500 d-flex align-items-center justify-content-between\"\n              >\n                <div class=\"input-group shop-cart-value\">\n                  <span class=\"input-group-btn\">\n                    <button\n                      class=\"btn btn-sm\"\n                      disabled=\"disabled\"\n                      type=\"button\"\n                    >\n                      -\n                    </button>\n                  </span>\n                  <input\n                    class=\"form-control border-form-control form-control-sm input-number\"\n                    max=\"10\"\n                    min=\"1\"\n                    name=\"quant[1]\"\n                    type=\"text\"\n                    value=\"1\"\n                  />\n                  <span class=\"input-group-btn\"\n                    ><button class=\"btn btn-sm\" type=\"button\">+</button>\n                  </span>\n                </div>\n              </div> -->\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class=\"bg-primary pt-4\">\n    <h5 class=\"mb-2 text-white text-center font-weight-bold\">\n      Best of Everyday essentials\n    </h5>\n    <div\n      tabindex=\"0\"\n      class=\"text-center text-white-50 h6 mb-3\"\n      tappable\n      (click)=\"categoryList()\"\n    >\n      View All\n    </div>\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let product of featuredProducts\">\n          <div\n            class=\"p-2 shop-homepage-item bg-white\"\n            tabindex=\"0\"\n            tappable\n            (click)=\"productDetails(product)\"\n          >\n            <div class=\"shop-cart-left\">\n              <img\n                alt=\"img\"\n                class=\"not-found-img\"\n                [src]=\"product.img\"\n                onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n              />\n            </div>\n            <div class=\"shop-cart-right\">\n              <div class=\"mb-2 product-name\">{{product.name}}</div>\n              <h6 class=\"font-weight-bold text-dark mb-2 price\">\n                {{product.dPrice}}\n                <span class=\"regular-price text-secondary font-weight-normal\"\n                  >{{product.price}}</span\n                >\n                <ion-badge color=\"primary\">{{product.off}}</ion-badge>\n              </h6>\n              <!-- <div\n                class=\"small text-gray-500 d-flex align-items-center justify-content-between\"\n              >\n                <div class=\"input-group shop-cart-value\">\n                  <span class=\"input-group-btn\">\n                    <button\n                      class=\"btn btn-sm\"\n                      disabled=\"disabled\"\n                      type=\"button\"\n                    >\n                      -\n                    </button>\n                  </span>\n                  <input\n                    class=\"form-control border-form-control form-control-sm input-number\"\n                    max=\"10\"\n                    min=\"1\"\n                    name=\"quant[1]\"\n                    type=\"text\"\n                    value=\"1\"\n                  />\n                  <span class=\"input-group-btn\"\n                    ><button class=\"btn btn-sm\" type=\"button\">+</button>\n                  </span>\n                </div>\n              </div> -->\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>\n\n\n\n\n<ion-content color=\"light\" *ngIf=\"!data\">\n  <!-- Home Page Banner -->\n  <div class=\"homepage-slider\">\n    <ion-slides pager=\"true\">\n      <ion-slide (click)=\"productList('')\" *ngFor=\"let slide of slides\">\n        <ion-skeleton-text alt=\"img\" class=\"single-img\" animated></ion-skeleton-text>\n        <!-- <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img\" [src]=\"slide\" /> -->\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <!-- Home Page Advertisement -->\n  <!-- <ion-grid>\n        <ion-row>\n            <ion-col>\n                <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img rounded shadow-sm\" src=\"assets/ad/1.jpg\">\n            </ion-col>\n            <ion-col>\n                <img _ngcontent-bxr-c1=\"\" alt=\"img\" class=\"single-img rounded shadow-sm\" src=\"assets/ad/2.jpg\">\n            </ion-col>\n        </ion-row>\n    </ion-grid> -->\n\n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h5 class=\"mb-0 d-flex align-items-center\">\n            Featured categories\n            <ion-button\n              size=\"small\"\n              tabindex=\"0\"\n              (click)=\"categoryList()\"\n              class=\"view-all-btn ml-auto\"\n            >\n              See All\n              <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let category of categories\">\n          <ion-card\n            class=\"ion-no-margin category-card\"\n            (click)=\"productList(category.id)\"\n          >\n            <div class=\"img-div\">\n              <ion-skeleton-text class=\"img-div\" animated></ion-skeleton-text>\n            </div>\n            <div class=\"featured-category-name\">\n              <ion-skeleton-text class=\"featured-category-name\" animated></ion-skeleton-text>\n            </div>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <!-- <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h5 class=\"mb-0 d-flex align-items-center\">\n            Featured products\n            <ion-button\n              size=\"small\"\n              tabindex=\"0\"\n              (click)=\"productList('')\"\n              class=\"view-all-btn ml-auto\"\n            >\n              See All\n              <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        \n        <ion-col size=\"6\" *ngFor=\"let product of featuredProducts\">\n          <div\n            class=\"p-2 shop-homepage-item bg-white\"\n            tabindex=\"0\"\n            tappable\n            (click)=\"productDetails(product)\"\n          >\n            <div class=\"shop-cart-left\">\n              <ion-skeleton-text alt=\"img\" class=\"img-div\" animated></ion-skeleton-text>\n            </div>\n            <div class=\"shop-cart-right\">\n              <div class=\"mb-2 product-name\">\n                <ion-skeleton-text animated class=\"mb-2 product-name\"></ion-skeleton-text>\n              </div>\n              <ion-skeleton-text animated class=\"font-weight-bold text-dark mb-2 price\"></ion-skeleton-text>\n              <ion-skeleton-text animated class=\"regular-price text-secondary font-weight-normal\"></ion-skeleton-text>\n              <h6 class=\"font-weight-bold text-dark mb-2 price\">\n                <ion-badge color=\"primary\">{{'10%'}}</ion-badge>\n              </h6>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div> -->\n\n\n  \n  <div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <h5 class=\"mb-0 d-flex align-items-center\">\n            Featured products\n            <ion-button\n              size=\"small\"\n              tabindex=\"0\"\n              (click)=\"productList('')\"\n              class=\"view-all-btn ml-auto\"\n            >\n              See All\n              <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let category of categories\">\n          <ion-card style=\"height: 110px;\"\n            class=\"ion-no-margin category-card\"\n            (click)=\"productList(category.id)\"\n          >\n            <div class=\"img-div\" style=\"position: absolute;width: 100%;\">\n              <ion-skeleton-text class=\"img-div\" animated></ion-skeleton-text>\n              <!-- <ion-badge slot=\"end\" style=\"position: relative;\n              bottom: 100px;\n              left: 60px;width: 40px;\"\n              color=\"primary\">\n              <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-badge> -->\n            </div>\n            <ion-row style=\"margin-top: 32%;\">\n              <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-row>\n            <ion-row>\n              <ion-skeleton-text animated></ion-skeleton-text>\n            </ion-row>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class=\"bg-primary pt-4\">\n    <h5 class=\"mb-2 text-white text-center font-weight-bold\">\n      Best of Everyday essentials\n    </h5>\n    <div\n      tabindex=\"0\"\n      class=\"text-center text-white-50 h6 mb-3\"\n      tappable\n      (click)=\"categoryList()\"\n    >\n      View All\n    </div>\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let product of featuredProducts\">\n          <div\n            class=\"p-2 shop-homepage-item bg-white\"\n            tabindex=\"0\"\n            tappable\n            (click)=\"productDetails(product)\"\n          >\n            <div class=\"shop-cart-left\">\n              <ion-skeleton-text animated></ion-skeleton-text>\n              <!-- <img\n                alt=\"img\"\n                class=\"not-found-img\"\n                [src]=\"product.img\"\n                onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n              /> -->\n            </div>\n            <div class=\"shop-cart-right\">\n              <div class=\"mb-2 product-name\">\n                <ion-skeleton-text animated></ion-skeleton-text>\n              </div>\n              <h6 class=\"font-weight-bold text-dark mb-2 price\">\n                <!-- {{product.dPrice}}\n                <span class=\"regular-price text-secondary font-weight-normal\"\n                  >{{product.price}}</span\n                > -->\n                <ion-skeleton-text animated></ion-skeleton-text>\n                <ion-badge color=\"primary\">{{product.off}}</ion-badge>\n              </h6>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".regular-price {\n  text-decoration: line-through;\n}\n\n.featured-category-name {\n  font-size: 1rem;\n  text-align: center;\n  font-weight: bold;\n  background: #dcdcdc;\n  color: #000000;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n.view-all-btn {\n  --padding-end: 0;\n  --border-radius: 18px;\n}\n\n.view-all-btn ion-icon {\n  padding: 10px;\n  background: #0f9fd7;\n}\n\n.category-card .img-div {\n  height: 100px;\n  text-align: center;\n}\n\n.category-card .img-div img {\n  height: 100px;\n  width: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDZCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQUNGOztBQUVBO0VBQ0UsZ0JBQUE7RUFDQSxxQkFBQTtBQUNGOztBQUFFO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBRUU7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFBSTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FBRU4iLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlZ3VsYXItcHJpY2Uge1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbn1cblxuLmZlYXR1cmVkLWNhdGVnb3J5LW5hbWUge1xuICBmb250LXNpemU6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQ6ICNkY2RjZGM7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuLnZpZXctYWxsLWJ0biB7XG4gIC0tcGFkZGluZy1lbmQ6IDA7XG4gIC0tYm9yZGVyLXJhZGl1czogMThweDtcbiAgaW9uLWljb24ge1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYmFja2dyb3VuZDogIzBmOWZkNztcbiAgfVxufVxuLmNhdGVnb3J5LWNhcmQge1xuICAuaW1nLWRpdiB7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgaW1nIHtcbiAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICB9XG4gIH1cbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/app-api.service */ "./src/app/services/app-api.service.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");
/* harmony import */ var src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/utility/app.variables */ "./src/app/utility/app.variables.ts");






let HomePage = class HomePage {
    constructor(navCltr, apiService, dataService, values) {
        this.navCltr = navCltr;
        this.apiService = apiService;
        this.dataService = dataService;
        this.values = values;
        this.featuredProducts = [];
        this.bestSelling = [];
        this.categories = [];
        this.slides = [
            'assets/slider/1.png',
            'assets/slider/2.png',
            'assets/slider/3.jpg',
            'assets/slider/4.png',
            'assets/slider/5.png',
        ];
        this.data = false;
        // this.everydayEssentials = [
        //   {
        //     image: 'assets/small/1.jpg',
        //     name: 'Surf Excel Matic Top Load Detergent Powder (Carton)',
        //     price: '$600.99',
        //     mrp: '$800.99',
        //     off: '50% OFF',
        //     unit: '2 Kg',
        //   },
        //   {
        //     image: 'assets/small/7.jpg',
        //     name: 'Surf Excel Matic Top Load Detergent Powder (Carton)',
        //     price: '$600.99',
        //     mrp: '$800.99',
        //     off: '50% OFF',
        //     unit: '2 Kg',
        //   },
        //   {
        //     image: 'assets/small/3.jpg',
        //     name: 'Hygienix Anti-Bacterial Hand Sanitizer (Bottle) ',
        //     price: '$456.99',
        //     mrp: '$787.99',
        //     off: '5% OFF',
        //     unit: '300 ml',
        //   },
        //   {
        //     image: 'assets/small/6.jpg',
        //     name: 'Hygienix Anti-Bacterial Hand Sanitizer (Bottle) ',
        //     price: '$456.99',
        //     mrp: '$787.99',
        //     off: '5% OFF',
        //     unit: '300 ml',
        //   }
        // ];
        this.featuredProducts = this.dataService.getFeaturedProducts();
        this.bestSelling = this.dataService.getBestSellerProducts();
        this.categories = this.dataService.getCategories();
    }
    productList(categoryId) {
        const navigationExtras = {
            state: {
                categoryId,
            },
        };
        this.navCltr.navigateForward('product-listing', navigationExtras);
    }
    searchproductList(categoryId) {
        const navigationExtras = {
            state: {
                categoryId,
            },
        };
        this.navCltr.navigateForward('product-search', navigationExtras);
    }
    categoryList() {
        this.navCltr.navigateForward('category-list');
    }
    productDetails(product) {
        const navigationExtras = {
            state: {
                product: JSON.stringify(product),
            },
        };
        this.navCltr.navigateForward(['product-details'], navigationExtras);
    }
    goToCart() {
        const navigationExtras = {
            // queryParams: {
            //   showBack: true
            // },
            state: {
                showBack: true,
            },
        };
        this.navCltr.navigateForward(['shopping-cart'], navigationExtras);
    }
    ngOnInit() {
        this.getCategories();
        this.getProducts();
    }
    getProducts() {
        this.apiService.getProducts('', '', '', '').subscribe((respData) => {
            this.data = true;
            this.featuredProducts = respData.ProductWise_List.slice(0, 5);
        }, (err) => { });
    }
    getCategories() {
        this.apiService.getCategories().subscribe((respData) => {
            this.categories = respData.category_list;
            console.log(this.categories);
            this.data = false;
        }, (err) => { });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_3__["AppApiService"] },
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_4__["AppData"] },
    { type: src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_5__["Values"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    })
], HomePage);



/***/ }),

/***/ "./src/app/services/app-api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/app-api.service.ts ***!
  \*********************************************/
/*! exports provided: AppApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppApiService", function() { return AppApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./base-api.service */ "./src/app/services/base-api.service.ts");




let AppApiService = class AppApiService {
    constructor(baseApi) {
        this.baseApi = baseApi;
    }
    getCategories() {
        const formData = new FormData();
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
    }
    getProducts(productId, brandId, catId, priceRange) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('brand_id', brandId);
        formData.append('cat_id', catId);
        formData.append('price_range', priceRange);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
    }
    getProduct_Search(search) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('search', search);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
    }
    addProductToCart(productId) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('action', 'insert');
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
    }
};
AppApiService.ctorParameters = () => [
    { type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"] }
];
AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AppApiService);



/***/ }),

/***/ "./src/app/services/base-api.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/base-api.service.ts ***!
  \**********************************************/
/*! exports provided: BaseApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseApiService", function() { return BaseApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utility/common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utility/app.variables */ "./src/app/utility/app.variables.ts");
/* harmony import */ var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utility/localstorage.utility */ "./src/app/utility/localstorage.utility.ts");
/* harmony import */ var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utility/enums */ "./src/app/utility/enums.ts");










let BaseApiService = class BaseApiService {
    constructor(commonUtility, http, nav, values, localstorageService, platform) {
        this.commonUtility = commonUtility;
        this.http = http;
        this.nav = nav;
        this.values = values;
        this.localstorageService = localstorageService;
        this.platform = platform;
        this.API_URL = '';
        this.isNetAvailable = true;
        this.API_URL = this.values.getApiUrl();
    }
    getApiCall(action, parameters) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
        const reqHeaderOption = this.getHeaderRequestOptions();
        console.log(reqHeaderOption);
        return this.http
            .get(apiCallUrl, reqHeaderOption)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    postApiCall(action, postData, param) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
        const reqHeaderOption = this.getHeaderRequestOptions();
        return this.http
            .post(apiCallUrl, postData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getHeaderRequestOptions() {
        const addHeaders = new Headers();
        // addHeaders.append('Content-Type', 'application/json');
        // addHeaders.append(
        //   'AuthorizedToken',
        //   this.localstorageService.getCustomerId()
        // );
        // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());
        return { headers: addHeaders };
    }
    generateFullURL(apiType, action, parameters) {
        let apiCallUrl = this.API_URL;
        let queryString = '';
        if (action !== '') {
            apiCallUrl = apiCallUrl + '/' + action;
        }
        switch (apiType) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                    queryString = '?';
                    for (let index = 0; index < parameters.length; index++) {
                        if (index > 0) {
                            queryString = queryString + '&';
                        }
                        const parameterKeyValue = parameters[index];
                        queryString =
                            queryString +
                                '' +
                                parameterKeyValue[0] +
                                '=' +
                                parameterKeyValue[1];
                    }
                    apiCallUrl = apiCallUrl + '' + queryString;
                }
        }
        return apiCallUrl;
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        switch (error.status) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
        }
        this.commonUtility.showErrorToast(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
BaseApiService.ctorParameters = () => [
    { type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"] },
    { type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] }
];
BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], BaseApiService);



/***/ }),

/***/ "./src/app/utility/localstorage.utility.ts":
/*!*************************************************!*\
  !*** ./src/app/utility/localstorage.utility.ts ***!
  \*************************************************/
/*! exports provided: LocalstorageUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function() { return LocalstorageUtility; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");




let LocalstorageUtility = class LocalstorageUtility {
    constructor(commonUtility) {
        this.commonUtility = commonUtility;
    }
    ProcessValue(value) {
        if (!this.commonUtility.isEmptyOrNull(value)) {
            return value;
        }
        else {
            return '';
        }
    }
    getCustomerId() {
        const value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        return this.ProcessValue(value);
    }
    clearLoginData() {
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
    }
};
LocalstorageUtility.ctorParameters = () => [
    { type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"] }
];
LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LocalstorageUtility);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map