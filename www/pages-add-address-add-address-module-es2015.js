(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-add-address-add-address-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-address/add-address.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-address/add-address.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Add Address'\" [backButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding\">\n    <div class=\"card\">\n        <form>\n            <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n                <ion-item>\n                    <ion-label position=\"stacked\"> First name\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter first name\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> First last name\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter last ame\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Email\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter email\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                    <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select Country</ion-label>\n                    <ion-select>\n                        <ion-select-option value=\"India\">India</ion-select-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                    <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select state</ion-label>\n                    <ion-select>\n                        <ion-select-option value=\"state\">Delhi</ion-select-option>\n                        <ion-select-option value=\"PayoutWise\">Maharshtra</ion-select-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                    <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select city</ion-label>\n                    <ion-select>\n                        <ion-select-option value=\"DataWise\">Mumbai</ion-select-option>\n                        <ion-select-option value=\"PayoutWise\">Pune</ion-select-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Flat/House/Office No.\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Flat/House/Office..\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Street/Society/Office Name\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Street/Society/Office...\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Zipcode/ Postal code\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter zip code/Postal code\" type=\"text\"></ion-input>\n                </ion-item>\n            </ion-list>\n            <ion-item>\n                <ion-label position=\"stacked\"> Phone number\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input placeholder=\"Enter phone number\" type=\"text\"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label position=\"stacked\"> Fax number\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input placeholder=\"Enter fax number\" type=\"text\"></ion-input>\n            </ion-item>\n\n        </form>\n    </div>\n\n</ion-content>\n\n<ion-footer class=\"ion-no-border footer-background\">\n    <ion-button shape=\"round\" expand=\"block\" color=\"primary\">SAVE\n    </ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/add-address/add-address-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/add-address/add-address-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: AddAddressPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAddressPageRoutingModule", function() { return AddAddressPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _add_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-address.page */ "./src/app/pages/add-address/add-address.page.ts");




const routes = [
    {
        path: '',
        component: _add_address_page__WEBPACK_IMPORTED_MODULE_3__["AddAddressPage"]
    }
];
let AddAddressPageRoutingModule = class AddAddressPageRoutingModule {
};
AddAddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddAddressPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/add-address/add-address.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/add-address/add-address.module.ts ***!
  \*********************************************************/
/*! exports provided: AddAddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAddressPageModule", function() { return AddAddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _add_address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-address-routing.module */ "./src/app/pages/add-address/add-address-routing.module.ts");
/* harmony import */ var _add_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-address.page */ "./src/app/pages/add-address/add-address.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let AddAddressPageModule = class AddAddressPageModule {
};
AddAddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _add_address_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddAddressPageRoutingModule"]
        ],
        declarations: [_add_address_page__WEBPACK_IMPORTED_MODULE_6__["AddAddressPage"]]
    })
], AddAddressPageModule);



/***/ }),

/***/ "./src/app/pages/add-address/add-address.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/add-address/add-address.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkZC1hZGRyZXNzL2FkZC1hZGRyZXNzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/add-address/add-address.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/add-address/add-address.page.ts ***!
  \*******************************************************/
/*! exports provided: AddAddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAddressPage", function() { return AddAddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let AddAddressPage = class AddAddressPage {
    constructor() { }
    ngOnInit() {
    }
};
AddAddressPage.ctorParameters = () => [];
AddAddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-address',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-address.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-address/add-address.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-address.page.scss */ "./src/app/pages/add-address/add-address.page.scss")).default]
    })
], AddAddressPage);



/***/ })

}]);
//# sourceMappingURL=pages-add-address-add-address-module-es2015.js.map