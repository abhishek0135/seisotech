(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-generation-tree-generation-tree-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/generation-tree/generation-tree.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/generation-tree/generation-tree.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"osahan-nav\">\n    <ion-toolbar class=\"in-toolbar\">\n        <ion-buttons slot=\"start\">\n            <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n        <ion-title>\n            Generation Tree\n        </ion-title>\n        <ion-buttons slot=\"primary\" slot=\"end\">\n            <ion-button color=\"primary\" class=\"top-cart\" (click)=\"lockScreenOrientation()\">\n                <ion-icon [name]=\"isLandscap == 'L'?'phone-landscape-outline':'phone-portrait-outline'\" class=\"options\">\n                </ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-toolbar no-border class=\"scrollable-segments  mt10\">\n        <ion-list>\n            <ion-item>\n                <img src=\"../assets/imgs/available_status.png\"> <span>Available</span>\n            </ion-item>\n            <ion-item>\n                <img src=\"../assets/imgs/paid_status.png\"> <span>Paid</span>\n            </ion-item>\n            <ion-item>\n                <img src=\"../assets/imgs/unpaid_status.png\"> <span>Unpaid</span>\n            </ion-item>\n        </ion-list>\n    </ion-toolbar>\n    <div class=\"tf-tree custom-tree-view\">\n        <ul>\n            <li>\n                <span class=\"tf-nc\">\n                 <app-tree-element></app-tree-element>\n                </span>\n                <ul>\n                    <li>\n                        <span class=\"tf-nc\">\n                          <app-tree-element></app-tree-element>\n                        </span>\n                        <ul>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                        </ul>\n                    </li>\n                    <li>\n                        <span class=\"tf-nc\">\n                          <app-tree-element></app-tree-element>\n                        </span>\n                        <ul>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                        </ul>\n                    </li>\n                    <li>\n                        <span class=\"tf-nc\">\n                          <app-tree-element></app-tree-element>\n                        </span>\n                        <ul>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                            <li>\n                                <span class=\"tf-nc\">\n                                  <app-tree-element></app-tree-element>\n                                </span>\n                            </li>\n                        </ul>\n                    </li>\n                </ul>\n            </li>\n        </ul>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/generation-tree/generation-tree-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/generation-tree/generation-tree-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: GenerationTreePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenerationTreePageRoutingModule", function() { return GenerationTreePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _generation_tree_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./generation-tree.page */ "./src/app/pages/generation-tree/generation-tree.page.ts");




const routes = [
    {
        path: '',
        component: _generation_tree_page__WEBPACK_IMPORTED_MODULE_3__["GenerationTreePage"]
    }
];
let GenerationTreePageRoutingModule = class GenerationTreePageRoutingModule {
};
GenerationTreePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GenerationTreePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/generation-tree/generation-tree.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/generation-tree/generation-tree.module.ts ***!
  \*****************************************************************/
/*! exports provided: GenerationTreePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenerationTreePageModule", function() { return GenerationTreePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _generation_tree_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./generation-tree-routing.module */ "./src/app/pages/generation-tree/generation-tree-routing.module.ts");
/* harmony import */ var _generation_tree_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./generation-tree.page */ "./src/app/pages/generation-tree/generation-tree.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let GenerationTreePageModule = class GenerationTreePageModule {
};
GenerationTreePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _generation_tree_routing_module__WEBPACK_IMPORTED_MODULE_5__["GenerationTreePageRoutingModule"],
        ],
        declarations: [_generation_tree_page__WEBPACK_IMPORTED_MODULE_6__["GenerationTreePage"]]
    })
], GenerationTreePageModule);



/***/ }),

/***/ "./src/app/pages/generation-tree/generation-tree.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/generation-tree/generation-tree.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar.scrollable-segments .toolbar-background {\n  background-color: #F2F1F7 !important;\n}\nion-toolbar.scrollable-segments .toolbar-content {\n  height: 60px !important;\n}\nion-toolbar.scrollable-segments ion-list {\n  display: block;\n  overflow-x: scroll;\n  white-space: nowrap;\n  height: 75px;\n}\nion-toolbar.scrollable-segments ion-list ion-item {\n  display: inline-block;\n  width: auto;\n  margin-left: 3px;\n  height: 40px !important;\n}\nion-toolbar.scrollable-segments ion-list ion-item span {\n  vertical-align: super;\n}\nion-toolbar.scrollable-segments ion-list ion-item img {\n  vertical-align: bottom;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ2VuZXJhdGlvbi10cmVlL2dlbmVyYXRpb24tdHJlZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSxvQ0FBQTtBQUFSO0FBRUk7RUFDSSx1QkFBQTtBQUFSO0FBRUk7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFBUjtBQUNRO0VBQ0kscUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQUNaO0FBQVk7RUFDSSxxQkFBQTtBQUVoQjtBQUFZO0VBQ0ksc0JBQUE7QUFFaEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9nZW5lcmF0aW9uLXRyZWUvZ2VuZXJhdGlvbi10cmVlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyLnNjcm9sbGFibGUtc2VnbWVudHMge1xuICAgIC50b29sYmFyLWJhY2tncm91bmQge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjJGMUY3ICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIC50b29sYmFyLWNvbnRlbnQge1xuICAgICAgICBoZWlnaHQ6IDYwcHggIWltcG9ydGFudDtcbiAgICB9XG4gICAgaW9uLWxpc3Qge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICBoZWlnaHQ6IDc1cHg7XG4gICAgICAgIGlvbi1pdGVtIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDNweDtcbiAgICAgICAgICAgIGhlaWdodDogNDBweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgc3BhbiB7XG4gICAgICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IHN1cGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/generation-tree/generation-tree.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/generation-tree/generation-tree.page.ts ***!
  \***************************************************************/
/*! exports provided: GenerationTreePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenerationTreePage", function() { return GenerationTreePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/screen-orientation/ngx */ "./node_modules/@ionic-native/screen-orientation/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let GenerationTreePage = class GenerationTreePage {
    constructor(navController, screenOrientation) {
        this.navController = navController;
        this.screenOrientation = screenOrientation;
        this.isLandscap = 'L';
    }
    ngOnInit() {
        this.isLandscap = 'L';
        this.lockScreenOrientation();
    }
    ionViewWillLeave() {
        this.isLandscap = 'P';
        this.lockScreenOrientation();
    }
    lockScreenOrientation() {
        switch (this.isLandscap) {
            case 'L':
                this.screenOrientation.unlock();
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
                this.isLandscap = 'P';
                break;
            case 'P':
                this.screenOrientation.unlock();
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
                this.isLandscap = 'L';
                break;
            default:
                break;
        }
    }
};
GenerationTreePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_native_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_2__["ScreenOrientation"] }
];
GenerationTreePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-generation-tree',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./generation-tree.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/generation-tree/generation-tree.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./generation-tree.page.scss */ "./src/app/pages/generation-tree/generation-tree.page.scss")).default]
    })
], GenerationTreePage);



/***/ })

}]);
//# sourceMappingURL=pages-generation-tree-generation-tree-module-es2015.js.map