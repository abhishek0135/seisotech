(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-product-details-product-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-details/product-details.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-details/product-details.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesProductDetailsProductDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header\n  [titleText]=\"'Product Details'\"\n  [backButton]=\"true\"\n  [toShowCartButton]=\"true\"\n></app-app-header>\n<ion-content *ngIf=\"data\" class=\"single-page\" color=\"light\">\n  <ion-slides pager=\"true\">\n    <ion-slide>\n      <img class=\"single-img\" [src]=\"productDetails.img\" />\n    </ion-slide>\n    <!-- <ion-slide>\n            <img class=\"single-img\" src=\"https://unikart.unilinkbiz.com/content/images/thumbs/0000377_nutrimark-uniaktiv-trendz.png\">\n        </ion-slide>\n        <ion-slide>\n            <img class=\"single-img\" src=\"https://unikart.unilinkbiz.com/content/images/thumbs/0000377_nutrimark-uniaktiv-trendz.png\">\n        </ion-slide> -->\n  </ion-slides>\n  <div class=\"p-3\">\n    <div class=\"mb-2 card p-3 single-page-info\">\n      <div>\n        <div class=\"single-page-shop\">\n          <h5 class=\"mb-1\">{{ productDetails.name }}</h5>\n          <small class=\"text-secondary\">\n            <strong>\n              <ion-icon name=\"checkmark-circle-outline\"> </ion-icon>\n              Brand\n            </strong>\n            - {{productDetails.brand_name}}\n          </small>\n          <h6 class=\"font-weight-bold text-dark mb-3 mt-2\">\n            {{ productDetails.dPrice | currency: 'INR'}}\n            <span class=\"regular-price text-secondary font-weight-normal\">\n              {{ productDetails.oldPrice | currency: 'INR'}}\n            </span>\n            <ion-badge color=\"success\">5% OFF</ion-badge>\n          </h6>\n          <div\n            class=\"small text-gray-500 d-flex align-items-center justify-content-between\"\n          >\n            <div class=\"text-success\">\n              <ion-icon name=\"star-outline\"></ion-icon>\n              4.4\n            </div>\n            <!-- <div class=\"input-group shop-cart-value\">\n                            <span class=\"input-group-btn\">\n                                <button class=\"btn btn-sm\" type=\"button\">-</button>\n                            </span>\n                            <input class=\"form-control border-form-control form-control-sm input-number\" max=\"10\" min=\"1\" value=\"1\">\n                            <span class=\"input-group-btn\">\n                                <button class=\"btn btn-sm\" type=\"button\">+</button>\n                            </span>\n                        </div> -->\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"mb-2 card p-3 single-page-info\">\n      <div class=\"short-description\">\n        <small class=\"float-right\">\n          Availability:\n          <span class=\"badge badge-success\">In Stock</span>\n        </small>\n        <h6 class=\"font-weight-bold mb-3\">Quick Overview</h6>\n        <!-- <p class=\"text-secondary\">\n                    <b>{{ productDetails.description }}</b>\n                </p> -->\n        <p\n          class=\"mb-0 text-secondary\"\n          class=\"full-description\"\n          [innerHtml]=\"productDetails.product_description\"\n        ></p>\n      </div>\n    </div>\n  </div>\n</ion-content>\n<ion-footer *ngIf=\"data\" class=\"border-0\">\n  <button\n    type=\"button\"\n    class=\"btn btn-primary btn-lg btn-block fix-btn text-left\"\n    (click)=\"addToCart()\"\n    [disabled]=\"addToCartInProgress\"\n  >\n    <span class=\"float-left\" *ngIf=\"!addToCartInProgress\">\n      <ion-icon name=\"cart-outline\"><ion-footer *ngIf=\"!data\" class=\"border-0\">\n        <button\n          type=\"button\"\n          class=\"btn btn-primary btn-lg btn-block fix-btn text-left\"\n          (click)=\"addToCart()\"\n          [disabled]=\"addToCartInProgress\"\n        >\n          <span class=\"float-left\" *ngIf=\"!addToCartInProgress\">\n            <ion-icon name=\"cart-outline\"> </ion-icon>\n            Add to cart\n          </span>\n          <app-button-spinner *ngIf=\"addToCartInProgress\"></app-button-spinner>\n          <span class=\"float-right\">\n            <strong><ion-skeleton-text animated></ion-skeleton-text></strong>\n            <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n          </span>\n        </button>\n      </ion-footer> </ion-icon>\n      Add to cart\n    </span>\n    <app-button-spinner *ngIf=\"addToCartInProgress\"></app-button-spinner>\n    <span class=\"float-right\">\n      <strong>{{ productDetails.dPrice | currency: 'INR'}}</strong>\n      <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n    </span>\n  </button>\n</ion-footer>\n\n\n\n\n\n\n\n\n\n\n<ion-content *ngIf=\"!data\" class=\"single-page\" color=\"light\">\n  <ion-slides pager=\"true\">\n    <ion-slide>\n      <ion-thumbnail style=\"width: 300px;\n      height: 300px;\n      margin-top: 3%;\">\n        <ion-skeleton-text class=\"single-img\" animated></ion-skeleton-text>\n      </ion-thumbnail>\n    </ion-slide>\n  </ion-slides>\n  <div class=\"p-3\">\n    <div class=\"mb-2 card p-3 single-page-info\">\n      <div>\n        <div class=\"single-page-shop\">\n          <ion-skeleton-text animated></ion-skeleton-text>\n          <ion-skeleton-text animated></ion-skeleton-text>\n          <ion-skeleton-text animated></ion-skeleton-text>\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </div>\n      </div>\n    </div>\n    <div class=\"mb-2 card p-3 single-page-info\">\n      <div class=\"short-description\">\n        <small class=\"float-right\">\n          <ion-skeleton-text animated></ion-skeleton-text>\n        </small>\n        <h6 class=\"font-weight-bold mb-3\">Quick Overview</h6>\n        <p\n          class=\"mb-0 text-secondary\"\n          class=\"full-description\"\n          \n        ><ion-skeleton-text style=\"height: 120px;\" animated></ion-skeleton-text></p>\n      </div>\n    </div>\n  </div>\n</ion-content>\n<ion-footer *ngIf=\"!data\" class=\"border-0\">\n  <button\n    type=\"button\"\n    class=\"btn btn-primary btn-lg btn-block fix-btn text-left\"\n    (click)=\"addToCart()\"\n    [disabled]=\"addToCartInProgress\"\n  >\n    <span class=\"float-left\" *ngIf=\"!addToCartInProgress\">\n      <ion-icon name=\"cart-outline\"> </ion-icon>\n      Add to cart\n    </span>\n    <app-button-spinner *ngIf=\"addToCartInProgress\"></app-button-spinner>\n    <span class=\"float-right\">\n      <strong><ion-skeleton-text animated></ion-skeleton-text></strong>\n      <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n    </span>\n  </button>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./src/app/pages/product-details/product-details-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/product-details/product-details-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: ProductDetailsPageRoutingModule */

    /***/
    function srcAppPagesProductDetailsProductDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailsPageRoutingModule", function () {
        return ProductDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _product_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./product-details.page */
      "./src/app/pages/product-details/product-details.page.ts");

      var routes = [{
        path: '',
        component: _product_details_page__WEBPACK_IMPORTED_MODULE_3__["ProductDetailsPage"]
      }];

      var ProductDetailsPageRoutingModule = function ProductDetailsPageRoutingModule() {
        _classCallCheck(this, ProductDetailsPageRoutingModule);
      };

      ProductDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProductDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/product-details/product-details.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/product-details/product-details.module.ts ***!
      \*****************************************************************/

    /*! exports provided: ProductDetailsPageModule */

    /***/
    function srcAppPagesProductDetailsProductDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function () {
        return ProductDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _product_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./product-details-routing.module */
      "./src/app/pages/product-details/product-details-routing.module.ts");
      /* harmony import */


      var _product_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./product-details.page */
      "./src/app/pages/product-details/product-details.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var ProductDetailsPageModule = function ProductDetailsPageModule() {
        _classCallCheck(this, ProductDetailsPageModule);
      };

      ProductDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _product_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_product_details_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsPage"]]
      })], ProductDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/product-details/product-details.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/product-details/product-details.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesProductDetailsProductDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".single-img {\n  max-height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC1kZXRhaWxzL3Byb2R1Y3QtZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvZHVjdC1kZXRhaWxzL3Byb2R1Y3QtZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2luZ2xlLWltZyB7XG4gICAgbWF4LWhlaWdodDogMzAwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/product-details/product-details.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/product-details/product-details.page.ts ***!
      \***************************************************************/

    /*! exports provided: ProductDetailsPage */

    /***/
    function srcAppPagesProductDetailsProductDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailsPage", function () {
        return ProductDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/app-api.service */
      "./src/app/services/app-api.service.ts");
      /* harmony import */


      var src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");

      var ProductDetailsPage = /*#__PURE__*/function () {
        function ProductDetailsPage(navCltr, route, router, appApiService, commonUtility, values) {
          var _this = this;

          _classCallCheck(this, ProductDetailsPage);

          this.navCltr = navCltr;
          this.route = route;
          this.router = router;
          this.appApiService = appApiService;
          this.commonUtility = commonUtility;
          this.values = values;
          this.productDetails = {};
          this.addToCartInProgress = false;
          this.data = false;
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.product = JSON.parse(_this.router.getCurrentNavigation().extras.state.product);
            }
          });
        }

        _createClass(ProductDetailsPage, [{
          key: "getProductDetail",
          value: function getProductDetail() {
            var _this2 = this;

            this.appApiService.getProducts(this.product.product_id, '', '', '').subscribe(function (respData) {
              _this2.data = true; // this.isDataLoaded = true;

              _this2.productDetails = respData.ProductWise_List[0];
            }, function (err) {// this.isDataLoaded = true;
            });
          }
        }, {
          key: "addToCart",
          value: function addToCart() {
            var _this3 = this;

            this.addToCartInProgress = true;
            this.appApiService.addProductToCart(this.product.product_id).subscribe(function (respData) {
              _this3.addToCartInProgress = false;
              _this3.values.cartCount++;

              _this3.commonUtility.setLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_7__["AppConstants"].localstorageKeys.cartCount, _this3.values.cartCount.toString());

              _this3.commonUtility.showSuccessToast('Product added to cart');

              _this3.verifyMobile();
            }, function (err) {
              _this3.addToCartInProgress = false;
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getProductDetail();
          }
        }, {
          key: "verifyMobile",
          value: function verifyMobile() {
            this.navCltr.navigateForward('verify-mobile');
          }
        }]);

        return ProductDetailsPage;
      }();

      ProductDetailsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"]
        }, {
          type: src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_5__["CommonUtility"]
        }, {
          type: src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__["Values"]
        }];
      };

      ProductDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-details/product-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-details.page.scss */
        "./src/app/pages/product-details/product-details.page.scss"))["default"]]
      })], ProductDetailsPage);
      /***/
    },

    /***/
    "./src/app/services/app-api.service.ts":
    /*!*********************************************!*\
      !*** ./src/app/services/app-api.service.ts ***!
      \*********************************************/

    /*! exports provided: AppApiService */

    /***/
    function srcAppServicesAppApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppApiService", function () {
        return AppApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./base-api.service */
      "./src/app/services/base-api.service.ts");

      var AppApiService = /*#__PURE__*/function () {
        function AppApiService(baseApi) {
          _classCallCheck(this, AppApiService);

          this.baseApi = baseApi;
        }

        _createClass(AppApiService, [{
          key: "getCategories",
          value: function getCategories() {
            var formData = new FormData();
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
          }
        }, {
          key: "getProducts",
          value: function getProducts(productId, brandId, catId, priceRange) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('brand_id', brandId);
            formData.append('cat_id', catId);
            formData.append('price_range', priceRange);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
          }
        }, {
          key: "getProduct_Search",
          value: function getProduct_Search(search) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('search', search);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
          }
        }, {
          key: "addProductToCart",
          value: function addProductToCart(productId) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('action', 'insert');
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
          }
        }]);

        return AppApiService;
      }();

      AppApiService.ctorParameters = function () {
        return [{
          type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"]
        }];
      };

      AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AppApiService);
      /***/
    },

    /***/
    "./src/app/services/base-api.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/base-api.service.ts ***!
      \**********************************************/

    /*! exports provided: BaseApiService */

    /***/
    function srcAppServicesBaseApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BaseApiService", function () {
        return BaseApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../utility/localstorage.utility */
      "./src/app/utility/localstorage.utility.ts");
      /* harmony import */


      var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../utility/enums */
      "./src/app/utility/enums.ts");

      var BaseApiService = /*#__PURE__*/function () {
        function BaseApiService(commonUtility, http, nav, values, localstorageService, platform) {
          _classCallCheck(this, BaseApiService);

          this.commonUtility = commonUtility;
          this.http = http;
          this.nav = nav;
          this.values = values;
          this.localstorageService = localstorageService;
          this.platform = platform;
          this.API_URL = '';
          this.isNetAvailable = true;
          this.API_URL = this.values.getApiUrl();
        }

        _createClass(BaseApiService, [{
          key: "getApiCall",
          value: function getApiCall(action, parameters) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
            var reqHeaderOption = this.getHeaderRequestOptions();
            console.log(reqHeaderOption);
            return this.http.get(apiCallUrl, reqHeaderOption).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "postApiCall",
          value: function postApiCall(action, postData, param) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
            var reqHeaderOption = this.getHeaderRequestOptions();
            return this.http.post(apiCallUrl, postData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "getHeaderRequestOptions",
          value: function getHeaderRequestOptions() {
            var addHeaders = new Headers(); // addHeaders.append('Content-Type', 'application/json');
            // addHeaders.append(
            //   'AuthorizedToken',
            //   this.localstorageService.getCustomerId()
            // );
            // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());

            return {
              headers: addHeaders
            };
          }
        }, {
          key: "generateFullURL",
          value: function generateFullURL(apiType, action, parameters) {
            var apiCallUrl = this.API_URL;
            var queryString = '';

            if (action !== '') {
              apiCallUrl = apiCallUrl + '/' + action;
            }

            switch (apiType) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                  queryString = '?';

                  for (var index = 0; index < parameters.length; index++) {
                    if (index > 0) {
                      queryString = queryString + '&';
                    }

                    var parameterKeyValue = parameters[index];
                    queryString = queryString + '' + parameterKeyValue[0] + '=' + parameterKeyValue[1];
                  }

                  apiCallUrl = apiCallUrl + '' + queryString;
                }

            }

            return apiCallUrl;
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            var errorMessage = '';

            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = "Error: ".concat(error.error.message);
            } else {
              // server-side error
              errorMessage = "Error Code: ".concat(error.status, "\nMessage: ").concat(error.message);
            }

            switch (error.status) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            }

            this.commonUtility.showErrorToast(errorMessage);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
          }
        }]);

        return BaseApiService;
      }();

      BaseApiService.ctorParameters = function () {
        return [{
          type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"]
        }, {
          type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }];
      };

      BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], BaseApiService);
      /***/
    },

    /***/
    "./src/app/utility/localstorage.utility.ts":
    /*!*************************************************!*\
      !*** ./src/app/utility/localstorage.utility.ts ***!
      \*************************************************/

    /*! exports provided: LocalstorageUtility */

    /***/
    function srcAppUtilityLocalstorageUtilityTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function () {
        return LocalstorageUtility;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");

      var LocalstorageUtility = /*#__PURE__*/function () {
        function LocalstorageUtility(commonUtility) {
          _classCallCheck(this, LocalstorageUtility);

          this.commonUtility = commonUtility;
        }

        _createClass(LocalstorageUtility, [{
          key: "ProcessValue",
          value: function ProcessValue(value) {
            if (!this.commonUtility.isEmptyOrNull(value)) {
              return value;
            } else {
              return '';
            }
          }
        }, {
          key: "getCustomerId",
          value: function getCustomerId() {
            var value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            return this.ProcessValue(value);
          }
        }, {
          key: "clearLoginData",
          value: function clearLoginData() {
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
          }
        }]);

        return LocalstorageUtility;
      }();

      LocalstorageUtility.ctorParameters = function () {
        return [{
          type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"]
        }];
      };

      LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LocalstorageUtility);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-product-details-product-details-module-es5.js.map