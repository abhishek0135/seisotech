(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-my-wallet-my-wallet-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-wallet/my-wallet.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-wallet/my-wallet.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'My Wallet'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" style=\"--offset-top:0px; --offset-bottom:0px;\">\n    <div class=\"mywallet text-center\">\n        <ion-icon name=\"wallet\" slot=\"icon-only\"></ion-icon>\n        <p>Total Balance</p>\n        <h1>$24,000</h1>\n        <ion-button class=\"m-3\">\n            Add Money\n        </ion-button>\n    </div>\n    <ion-card class=\"about-card ion-padding p-0\">\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-success font-weight-bold\">+$800</p>\n        </div>\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-danger font-weight-bold\">-$625</p>\n        </div>\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-success font-weight-bold\">+$500</p>\n        </div>\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-danger font-weight-bold\">-$45</p>\n        </div>\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-danger font-weight-bold\">-$678</p>\n        </div>\n        <div class=\"border-bottom d-flex align-items-center p-3\">\n            <p class=\"m-0\">Payment to xyz Shop</p>\n            <p class=\"ml-auto mb-0 text-success font-weight-bold\">+$600</p>\n        </div>\n    </ion-card>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/my-wallet/my-wallet-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/my-wallet/my-wallet-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: MyWalletPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyWalletPageRoutingModule", function() { return MyWalletPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _my_wallet_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-wallet.page */ "./src/app/pages/my-wallet/my-wallet.page.ts");




const routes = [
    {
        path: '',
        component: _my_wallet_page__WEBPACK_IMPORTED_MODULE_3__["MyWalletPage"]
    }
];
let MyWalletPageRoutingModule = class MyWalletPageRoutingModule {
};
MyWalletPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyWalletPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/my-wallet/my-wallet.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-wallet/my-wallet.module.ts ***!
  \*****************************************************/
/*! exports provided: MyWalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyWalletPageModule", function() { return MyWalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _my_wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-wallet-routing.module */ "./src/app/pages/my-wallet/my-wallet-routing.module.ts");
/* harmony import */ var _my_wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-wallet.page */ "./src/app/pages/my-wallet/my-wallet.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let MyWalletPageModule = class MyWalletPageModule {
};
MyWalletPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_wallet_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyWalletPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_my_wallet_page__WEBPACK_IMPORTED_MODULE_6__["MyWalletPage"]]
    })
], MyWalletPageModule);



/***/ }),

/***/ "./src/app/pages/my-wallet/my-wallet.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-wallet/my-wallet.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mywallet {\n  background: #003049;\n  color: #fff;\n  padding: 40px 0 80px 0;\n}\n.mywallet ion-icon {\n  font-size: 102px;\n}\n.mywallet p {\n  font-size: 16px;\n  margin: 0px;\n}\n.mywallet h1 {\n  font-weight: bold;\n}\n.about-card {\n  margin-top: -55px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbXktd2FsbGV0L215LXdhbGxldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQUNKO0FBQUk7RUFDSSxnQkFBQTtBQUVSO0FBQUk7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQUVSO0FBQUk7RUFDSSxpQkFBQTtBQUVSO0FBQ0E7RUFDSSxpQkFBQTtBQUVKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbXktd2FsbGV0L215LXdhbGxldC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXl3YWxsZXQge1xuICAgIGJhY2tncm91bmQ6ICMwMDMwNDk7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgcGFkZGluZzogNDBweCAwIDgwcHggMDtcbiAgICBpb24taWNvbntcbiAgICAgICAgZm9udC1zaXplOiAxMDJweDtcbiAgICB9XG4gICAgcHtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9XG4gICAgaDF7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbn1cbi5hYm91dC1jYXJkIHtcbiAgICBtYXJnaW4tdG9wOiAtNTVweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/my-wallet/my-wallet.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/my-wallet/my-wallet.page.ts ***!
  \***************************************************/
/*! exports provided: MyWalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyWalletPage", function() { return MyWalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let MyWalletPage = class MyWalletPage {
    constructor() { }
    ngOnInit() {
    }
};
MyWalletPage.ctorParameters = () => [];
MyWalletPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-wallet',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./my-wallet.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-wallet/my-wallet.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./my-wallet.page.scss */ "./src/app/pages/my-wallet/my-wallet.page.scss")).default]
    })
], MyWalletPage);



/***/ })

}]);
//# sourceMappingURL=pages-my-wallet-my-wallet-module-es2015.js.map