(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-sign-up-sign-up-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Sign Up'\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding\" style=\"--offset-top: 0px; --offset-bottom: 0px\">\n    <div class=\"card\">\n        <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Sponsor ID\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Upline ID\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n            <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\">Enter Sponsor ID</ion-text>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Referral ID\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Referral ID\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n            <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\">Enter Referral ID</ion-text>\n        </ion-list>\n        <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    First Name\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Enter First Name\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n            <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\">Enter First Name</ion-text>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Middle Name\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Enter Middle Name\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Last Name\n                    <ion-text color=\"danger\">*</ion-text>\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Enter Last Name\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n            <ion-text color=\"danger\" class=\"errorText\" *ngIf=\"sponsonIDError\">Enter Last Name</ion-text>\n        </ion-list>\n\n        <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Address\n                </ion-label>\n                <ion-input type=\"text\" placeholder=\"Full Address\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Pin Code\n                </ion-label>\n                <ion-input type=\"tel\" placeholder=\"Enter Pin Code\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n                </ion-input>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select Country</ion-label>\n                <ion-select [(ngModel)]=\"country\">\n                    <ion-select-option value=\"ind\">India</ion-select-option>\n                    <ion-select-option value=\"usa\">USA</ion-select-option>\n                    <ion-select-option value=\"uk\">UK</ion-select-option>\n                </ion-select>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select City</ion-label>\n                <ion-select [(ngModel)]=\"city\">\n                    <ion-select-option value=\"pune\">Pune</ion-select-option>\n                    <ion-select-option value=\"usa\">Nagpur</ion-select-option>\n                    <ion-select-option value=\"uk\">Nashik</ion-select-option>\n                </ion-select>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select City</ion-label>\n                <ion-select [(ngModel)]=\"state\">\n                    <ion-select-option value=\"mh\">Maharashtra</ion-select-option>\n                    <ion-select-option value=\"usa\">Gujrat</ion-select-option>\n                    <ion-select-option value=\"uk\">Bihar</ion-select-option>\n                </ion-select>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Mobile Number\n                </ion-label>\n                <ion-row>\n                    <ion-col>\n                        <ion-input [disabled]=\"true\" type=\"tel\" class=\"sc-ion-input-md-h sc-ion-input-md-s\" value=\"India(+91)\">\n                        </ion-input>\n                    </ion-col>\n                    <ion-col>\n                        <ion-input type=\"tel\" [disabled]=\"true\" class=\"sc-ion-input-md-h sc-ion-input-md-s\" placeholder=\"9999999999\" value=\"9561625256\">\n                        </ion-input>\n                    </ion-col>\n                </ion-row>\n            </ion-item>\n\n            <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n                <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n                    Pan No.\n                </ion-label>\n                <ion-input type=\"text\" class=\"sc-ion-input-md-h sc-ion-input-md-s\" value=\"Pune\">\n                </ion-input>\n            </ion-item>\n        </ion-list>\n\n    </div>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n    <ion-button (click)=\"register()\" shape=\"round\" expand=\"block\" color=\"primary\">\n        Register\n    </ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/sign-up/sign-up-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/sign-up/sign-up-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SignUpPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPageRoutingModule", function() { return SignUpPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _sign_up_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sign-up.page */ "./src/app/pages/sign-up/sign-up.page.ts");




const routes = [
    {
        path: '',
        component: _sign_up_page__WEBPACK_IMPORTED_MODULE_3__["SignUpPage"]
    }
];
let SignUpPageRoutingModule = class SignUpPageRoutingModule {
};
SignUpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SignUpPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/sign-up/sign-up.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/sign-up/sign-up.module.ts ***!
  \*************************************************/
/*! exports provided: SignUpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function() { return SignUpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sign-up-routing.module */ "./src/app/pages/sign-up/sign-up-routing.module.ts");
/* harmony import */ var _sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sign-up.page */ "./src/app/pages/sign-up/sign-up.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let SignUpPageModule = class SignUpPageModule {
};
SignUpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignUpPageRoutingModule"]
        ],
        declarations: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]]
    })
], SignUpPageModule);



/***/ }),

/***/ "./src/app/pages/sign-up/sign-up.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/sign-up/sign-up.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".errorText {\n  padding-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zaWduLXVwL3NpZ24tdXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yVGV4dCB7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/sign-up/sign-up.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/sign-up/sign-up.page.ts ***!
  \***********************************************/
/*! exports provided: SignUpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPage", function() { return SignUpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SignUpPage = class SignUpPage {
    constructor() {
        this.sponsonIDError = false;
        this.country = 'ind';
        this.state = 'mh';
        this.city = 'pune';
    }
    ngOnInit() {
    }
    register() {
        this.sponsonIDError = true;
    }
};
SignUpPage.ctorParameters = () => [];
SignUpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-up',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sign-up.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/sign-up/sign-up.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sign-up.page.scss */ "./src/app/pages/sign-up/sign-up.page.scss")).default]
    })
], SignUpPage);



/***/ })

}]);
//# sourceMappingURL=pages-sign-up-sign-up-module-es2015.js.map