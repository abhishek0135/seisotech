(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-shipments-shipments-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shipments/shipments.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shipments/shipments.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Shipment #52'\" [backButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n    <div class=\"card mt15\">\n        <div class=\"p-1\">\n            <ion-row class=\"shipment-details\">\n                <ion-col size=\"12\">\n                    <strong class=\"shipment-id\">Shipping Address</strong>\n                </ion-col>\n            </ion-row>\n            <div class=\"card-body card-contain\">\n                <div class=\"p-1\">\n                    <div class=\"justify-content-between mb-2\">\n                        <div class=\"text-secondary\">Phone Number</div>\n                        <div class=\"billing-text\">\n                            {{ orderDetailsData.billingAddress.phoneNo}}\n                        </div>\n                    </div>\n                    <div class=\"justify-content-between mb-2\">\n                        <div class=\"text-secondary\">Email Address</div>\n                        <div class=\"billing-text\">\n                            {{ orderDetailsData.billingAddress.emailId }}\n                        </div>\n                    </div>\n                    <div class=\"justify-content-between mb-2\">\n                        <div class=\"text-secondary\">Address</div>\n                        <div class=\"billing-text\">\n                            {{ orderDetailsData.billingAddress.address }}\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"card mt15\">\n        <ion-list class=\"ion-no-padding p-1 shipment-bottom\">\n            <ion-row tappable>\n                <ion-col size=\"12\">\n                    <ion-row class=\"shipment-details\">\n                        <ion-col size=\"12\">\n                            <strong class=\"shipment-id\">Shipment ID:&nbsp;</strong>\n                            <span>&nbsp;8</span>\n                        </ion-col>\n                    </ion-row>\n                    <div class=\"p-1\">\n                        <div class=\"d-flex justify-content-between mb-2\">\n                            <div class=\"text-secondary\">Shipping Charges</div>\n                            <div class=\"text-secondary\">\n                                {{ orderDetailsData.shippingInfo.shippingMethod }}\n                            </div>\n                        </div>\n                        <div class=\"d-flex justify-content-between mb-2\">\n                            <div class=\"text-secondary\">Tracking number</div>\n                            <div class=\"text-secondary\">\n                                {{ orderDetailsData.shipmentsInfo.trackingNumber }}\n                            </div>\n                        </div>\n                        <div class=\"d-flex justify-content-between mb-2\">\n                            <div class=\"text-secondary\">Date shipped</div>\n                            <div class=\"text-secondary\">\n                                {{ orderDetailsData.shipmentsInfo.dateShipped }}\n                            </div>\n                        </div>\n                        <div class=\"d-flex justify-content-between mb-2\">\n                            <div class=\"text-secondary\">Date delivered</div>\n                            <div class=\"text-secondary\">\n                                {{ orderDetailsData.shipmentsInfo.dateDelivered }}\n                            </div>\n                        </div>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </div>\n\n    <div class=\"card mt15\">\n        <div class=\"p-2\" tappable (click)=\"expandItem()\">\n            <ion-row>\n                <ion-col size=\"11\">\n                    <strong class=\"payment-text\">Shipped product(s)</strong>\n                </ion-col>\n                <ion-col size=\"1\">\n                    <ion-icon *ngIf=\"!orderItemExpanded\" name=\"chevron-down-outline\" class=\"expandable-icon\"></ion-icon>\n                    <ion-icon *ngIf=\"orderItemExpanded\" name=\"chevron-up-outline\" class=\"expandable-icon\"></ion-icon>\n                </ion-col>\n            </ion-row>\n        </div>\n        <app-expandable [expanded]=\"orderItemExpanded\" *ngFor=\"let product of orderDetailsData.orderItems\">\n            <div class=\"p-2 shop-cart-item-bottom shipment-bottom \">\n                <div class=\"shop-cart-right\">\n                    <ion-row>\n                        <ion-col>\n                            <div>\n                                <strong>{{ product.name }}</strong>\n                            </div>\n                        </ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col size=\"7\">\n                            <div class=\"text-secondary\">\n                                <div>\n                                    Net Quantity: 250 GM\n                                </div>\n                            </div>\n                        </ion-col>\n                        <ion-col size=\"5\" class=\"ion-text-right\">\n                            <div>\n                                2 Qty shipped\n                            </div>\n                        </ion-col>\n                    </ion-row>\n                </div>\n            </div>\n        </app-expandable>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/shipments/shipments-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/shipments/shipments-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ShipmentsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShipmentsPageRoutingModule", function() { return ShipmentsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _shipments_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shipments.page */ "./src/app/pages/shipments/shipments.page.ts");




const routes = [
    {
        path: '',
        component: _shipments_page__WEBPACK_IMPORTED_MODULE_3__["ShipmentsPage"]
    }
];
let ShipmentsPageRoutingModule = class ShipmentsPageRoutingModule {
};
ShipmentsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ShipmentsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/shipments/shipments.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/shipments/shipments.module.ts ***!
  \*****************************************************/
/*! exports provided: ShipmentsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShipmentsPageModule", function() { return ShipmentsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shipments_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shipments-routing.module */ "./src/app/pages/shipments/shipments-routing.module.ts");
/* harmony import */ var _shipments_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shipments.page */ "./src/app/pages/shipments/shipments.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let ShipmentsPageModule = class ShipmentsPageModule {
};
ShipmentsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _shipments_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShipmentsPageRoutingModule"]
        ],
        declarations: [_shipments_page__WEBPACK_IMPORTED_MODULE_6__["ShipmentsPage"]]
    })
], ShipmentsPageModule);



/***/ }),

/***/ "./src/app/pages/shipments/shipments.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/shipments/shipments.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mt15 {\n  margin-top: 15px;\n}\n\n.icon-size {\n  font-size: 20px;\n}\n\n.payment-text {\n  font-size: 16px;\n}\n\n.card-contain {\n  padding: 0px 0px 0.5rem 0.5rem;\n}\n\n.shipment-details {\n  font-size: 16px;\n}\n\n.shipment-details ion-col {\n  padding-left: 10px;\n}\n\n.detail-icon {\n  height: 100%;\n  display: block;\n  vertical-align: middle;\n}\n\n.billing-text {\n  color: black;\n}\n\n.shipment-bottom {\n  border-bottom: 1px solid gray;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2hpcG1lbnRzL3NoaXBtZW50cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKOztBQUNBO0VBQ0ksZUFBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtBQUdKOztBQURBO0VBQ0ksOEJBQUE7QUFJSjs7QUFGQTtFQUNJLGVBQUE7QUFLSjs7QUFKSTtFQUNJLGtCQUFBO0FBTVI7O0FBREE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0FBR0o7O0FBREE7RUFDSSxZQUFBO0FBSUo7O0FBRkE7RUFDSSw2QkFBQTtBQUtKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2hpcG1lbnRzL3NoaXBtZW50cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXQxNSB7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5pY29uLXNpemUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5wYXltZW50LXRleHQge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5jYXJkLWNvbnRhaW4ge1xuICAgIHBhZGRpbmc6IDBweCAwcHggMC41cmVtIDAuNXJlbTtcbn1cbi5zaGlwbWVudC1kZXRhaWxzIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICB9XG4gICAgLnNoaXBtZW50LWlkIHtcbiAgICB9XG59XG4uZGV0YWlsLWljb24ge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xufVxuLmJpbGxpbmctdGV4dCB7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLnNoaXBtZW50LWJvdHRvbSB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyYXk7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/shipments/shipments.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/shipments/shipments.page.ts ***!
  \***************************************************/
/*! exports provided: ShipmentsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShipmentsPage", function() { return ShipmentsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");



let ShipmentsPage = class ShipmentsPage {
    constructor(appData) {
        this.appData = appData;
        this.orderItemExpanded = false;
        this.orderDetailsData = appData.getOrderDetails();
        console.log(this.orderDetailsData);
    }
    ngOnInit() {
    }
    expandItem() {
        if (this.orderItemExpanded) {
            this.orderItemExpanded = false;
        }
        else {
            this.orderItemExpanded = true;
        }
    }
};
ShipmentsPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_2__["AppData"] }
];
ShipmentsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shipments',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./shipments.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shipments/shipments.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./shipments.page.scss */ "./src/app/pages/shipments/shipments.page.scss")).default]
    })
], ShipmentsPage);



/***/ })

}]);
//# sourceMappingURL=pages-shipments-shipments-module-es2015.js.map