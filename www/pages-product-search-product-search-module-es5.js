(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-product-search-product-search-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-search/product-search.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-search/product-search.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesProductSearchProductSearchPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header\n  [titleText]=\"'Product Search'\"\n  [backButton]=\"true\"\n  [toShowCartButton]=\"true\">\n\n</app-app-header>\n<ion-content\n  color=\"light\"\n  class=\"ion-padding order-list-page ion-color ion-color-light\"\n>\n  <ion-searchbar\n    class=\"p-0 mb-2\"\n    placeholder=\"Search\"\n    inputmode=\"text\"\n    type=\"search\"\n    (ionChange)=\"ionChange($event)\">\n\n  </ion-searchbar>\n  \n\n  <!-- <div class=\"text-center mb-2\">\n        <small> Placed on Fri, 22 Jun, 8 PM - 11 PM </small>\n    </div> -->\n    \n  <!-- <img class=\"rounded shadow-sm mb-2\" src=\"assets/shop.jpg\" /> -->\n  <div\n    class=\"d-flex p-3 shop-cart-item bg-white mb-2\"\n    *ngFor=\"let product of products\"\n    (click)=\"productDetails(product)\">\n\n    <div class=\"shop-cart-left\">\n      <img\n        class=\"not-found-img\"\n        [src]=\"product.img\"\n        onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n      />\n    </div>\n    <div class=\"shop-cart-right\">\n\n      <h6 class=\"font-weight-bold text-dark mb-2 price\" >{{product.name}}</h6>\n\n      <h6 class=\"font-weight-normal text-dark mb-2 price\">\n        {{ product.dPrice | currency : 'INR' }}\n        <span\n          class=\"regular-price text-secondary font-weight-normal strick-out\"\n        >\n          {{ product.price | currency : 'INR' }}\n        </span>\n      </h6>\n      <ion-button\n        [disabled]=\"product.addToCartInProgress\"\n        class=\"add-to-cart-btn\"\n        (click)=\"addToCart($event, product)\"\n      >\n        <ion-icon\n          slot=\"start\"\n          name=\"cart-outline\"\n          *ngIf=\"!product.addToCartInProgress\"\n        ></ion-icon>\n        <!-- <img src=\"../../../assets/imgs/addtocart.png\" /> -->\n        <ion-note *ngIf=\"!product.addToCartInProgress\">Add to cart</ion-note>\n        <app-button-spinner\n          *ngIf=\"product.addToCartInProgress\"\n        ></app-button-spinner>\n      </ion-button>\n      <!-- <div class=\"mb-2\" [innerHTML]=\"product.product_description\"></div> -->\n    </div>\n  </div>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/product-search/product-search-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/product-search/product-search-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: ProductListingPageRoutingModule */

    /***/
    function srcAppPagesProductSearchProductSearchRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPageRoutingModule", function () {
        return ProductListingPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _product_search_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./product-search.page */
      "./src/app/pages/product-search/product-search.page.ts");

      var routes = [{
        path: '',
        component: _product_search_page__WEBPACK_IMPORTED_MODULE_3__["ProductListingPage"]
      }];

      var ProductListingPageRoutingModule = function ProductListingPageRoutingModule() {
        _classCallCheck(this, ProductListingPageRoutingModule);
      };

      ProductListingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProductListingPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/product-search/product-search.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/product-search/product-search.module.ts ***!
      \***************************************************************/

    /*! exports provided: ProductListingPageModule */

    /***/
    function srcAppPagesProductSearchProductSearchModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPageModule", function () {
        return ProductListingPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _product_search_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./product-search-routing.module */
      "./src/app/pages/product-search/product-search-routing.module.ts");
      /* harmony import */


      var _product_search_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./product-search.page */
      "./src/app/pages/product-search/product-search.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var ProductListingPageModule = function ProductListingPageModule() {
        _classCallCheck(this, ProductListingPageModule);
      };

      ProductListingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _product_search_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductListingPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_product_search_page__WEBPACK_IMPORTED_MODULE_6__["ProductListingPage"]]
      })], ProductListingPageModule);
      /***/
    },

    /***/
    "./src/app/pages/product-search/product-search.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/pages/product-search/product-search.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesProductSearchProductSearchPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".add-to-cart-btn {\n  --background: #00ab00;\n  color: white;\n}\n.add-to-cart-btn ion-icon {\n  font-size: 28px;\n}\n.add-to-cart-btn ion-note {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC1zZWFyY2gvcHJvZHVjdC1zZWFyY2gucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0FBQ0Y7QUFBRTtFQUNFLGVBQUE7QUFFSjtBQUFFO0VBQ0UsWUFBQTtBQUVKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvZHVjdC1zZWFyY2gvcHJvZHVjdC1zZWFyY2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFkZC10by1jYXJ0LWJ0biB7XG4gIC0tYmFja2dyb3VuZDogIzAwYWIwMDtcbiAgY29sb3I6IHdoaXRlO1xuICBpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICB9XG4gIGlvbi1ub3RlIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gIH1cbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/product-search/product-search.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/product-search/product-search.page.ts ***!
      \*************************************************************/

    /*! exports provided: ProductListingPage */

    /***/
    function srcAppPagesProductSearchProductSearchPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPage", function () {
        return ProductListingPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/app-api.service */
      "./src/app/services/app-api.service.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var _utility_app_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../utility/app.data */
      "./src/app/utility/app.data.ts");

      var ProductListingPage = /*#__PURE__*/function () {
        function ProductListingPage(navCltr, appData, appApiService, router, values, commonUtility) {
          _classCallCheck(this, ProductListingPage);

          this.navCltr = navCltr;
          this.appData = appData;
          this.appApiService = appApiService;
          this.router = router;
          this.values = values;
          this.commonUtility = commonUtility;
          this.categoryId = '';
          this.products = [];
          this.emptyProduct = [1, 1, 1, 1, 1, 1, 1, 1];
          this.isDataLoaded = false;

          if (this.router.getCurrentNavigation().extras.state) {
            this.categoryId = this.router.getCurrentNavigation().extras.state.categoryId;
          }
        }

        _createClass(ProductListingPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getProducts();
          }
        }, {
          key: "getProducts",
          value: function getProducts() {
            var _this = this;

            this.appApiService.getProducts('', '', '', '').subscribe(function (respData) {
              _this.isDataLoaded = true;
              _this.products = respData.ProductWise_List;
            }, function (err) {
              _this.isDataLoaded = true;
              _this.products = _this.appData.getProducts();
            });
          }
        }, {
          key: "ionChange",
          value: function ionChange(event) {
            console.log(event.detail.value);

            if (event.detail.value.length > 2) {
              this.getProducts_search(event.detail.value);
            } else {
              this.getProducts_search('');
            }
          }
        }, {
          key: "getProducts_search",
          value: function getProducts_search(strSearch) {
            var _this2 = this;

            this.isDataLoaded = false;
            this.appApiService.getProduct_Search(strSearch).subscribe(function (respData) {
              _this2.isDataLoaded = true;
              _this2.products = respData.ProductSearch_List;
            }, function (err) {
              _this2.isDataLoaded = true;
              _this2.products = _this2.appData.getProducts();
            });
          }
        }, {
          key: "productDetails",
          value: function productDetails(product) {
            var navigationExtras = {
              state: {
                product: JSON.stringify(product)
              }
            };
            this.navCltr.navigateForward(['product-details'], navigationExtras);
          }
        }, {
          key: "addToCart",
          value: function addToCart(event, product) {
            var _this3 = this;

            debugger;
            event.stopPropagation();
            product.addToCartInProgress = true;
            this.appApiService.addProductToCart(product.product_id).subscribe(function (respData) {
              product.addToCartInProgress = false;
              _this3.values.cartCount++;

              _this3.commonUtility.setLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].localstorageKeys.cartCount, _this3.values.cartCount.toString());

              _this3.commonUtility.showSuccessToast('Product added to cart');
            }, function (err) {
              product.addToCartInProgress = false;
            });
          }
        }]);

        return ProductListingPage;
      }();

      ProductListingPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _utility_app_data__WEBPACK_IMPORTED_MODULE_8__["AppData"]
        }, {
          type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__["Values"]
        }, {
          type: src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__["CommonUtility"]
        }];
      };

      ProductListingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-listing',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-search.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-search/product-search.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-search.page.scss */
        "./src/app/pages/product-search/product-search.page.scss"))["default"]]
      })], ProductListingPage);
      /***/
    },

    /***/
    "./src/app/services/app-api.service.ts":
    /*!*********************************************!*\
      !*** ./src/app/services/app-api.service.ts ***!
      \*********************************************/

    /*! exports provided: AppApiService */

    /***/
    function srcAppServicesAppApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppApiService", function () {
        return AppApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./base-api.service */
      "./src/app/services/base-api.service.ts");

      var AppApiService = /*#__PURE__*/function () {
        function AppApiService(baseApi) {
          _classCallCheck(this, AppApiService);

          this.baseApi = baseApi;
        }

        _createClass(AppApiService, [{
          key: "getCategories",
          value: function getCategories() {
            var formData = new FormData();
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
          }
        }, {
          key: "getProducts",
          value: function getProducts(productId, brandId, catId, priceRange) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('brand_id', brandId);
            formData.append('cat_id', catId);
            formData.append('price_range', priceRange);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
          }
        }, {
          key: "getProduct_Search",
          value: function getProduct_Search(search) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('search', search);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
          }
        }, {
          key: "addProductToCart",
          value: function addProductToCart(productId) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('action', 'insert');
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
          }
        }]);

        return AppApiService;
      }();

      AppApiService.ctorParameters = function () {
        return [{
          type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"]
        }];
      };

      AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AppApiService);
      /***/
    },

    /***/
    "./src/app/services/base-api.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/base-api.service.ts ***!
      \**********************************************/

    /*! exports provided: BaseApiService */

    /***/
    function srcAppServicesBaseApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BaseApiService", function () {
        return BaseApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../utility/localstorage.utility */
      "./src/app/utility/localstorage.utility.ts");
      /* harmony import */


      var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../utility/enums */
      "./src/app/utility/enums.ts");

      var BaseApiService = /*#__PURE__*/function () {
        function BaseApiService(commonUtility, http, nav, values, localstorageService, platform) {
          _classCallCheck(this, BaseApiService);

          this.commonUtility = commonUtility;
          this.http = http;
          this.nav = nav;
          this.values = values;
          this.localstorageService = localstorageService;
          this.platform = platform;
          this.API_URL = '';
          this.isNetAvailable = true;
          this.API_URL = this.values.getApiUrl();
        }

        _createClass(BaseApiService, [{
          key: "getApiCall",
          value: function getApiCall(action, parameters) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
            var reqHeaderOption = this.getHeaderRequestOptions();
            console.log(reqHeaderOption);
            return this.http.get(apiCallUrl, reqHeaderOption).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "postApiCall",
          value: function postApiCall(action, postData, param) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
            var reqHeaderOption = this.getHeaderRequestOptions();
            return this.http.post(apiCallUrl, postData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "getHeaderRequestOptions",
          value: function getHeaderRequestOptions() {
            var addHeaders = new Headers(); // addHeaders.append('Content-Type', 'application/json');
            // addHeaders.append(
            //   'AuthorizedToken',
            //   this.localstorageService.getCustomerId()
            // );
            // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());

            return {
              headers: addHeaders
            };
          }
        }, {
          key: "generateFullURL",
          value: function generateFullURL(apiType, action, parameters) {
            var apiCallUrl = this.API_URL;
            var queryString = '';

            if (action !== '') {
              apiCallUrl = apiCallUrl + '/' + action;
            }

            switch (apiType) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                  queryString = '?';

                  for (var index = 0; index < parameters.length; index++) {
                    if (index > 0) {
                      queryString = queryString + '&';
                    }

                    var parameterKeyValue = parameters[index];
                    queryString = queryString + '' + parameterKeyValue[0] + '=' + parameterKeyValue[1];
                  }

                  apiCallUrl = apiCallUrl + '' + queryString;
                }

            }

            return apiCallUrl;
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            var errorMessage = '';

            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = "Error: ".concat(error.error.message);
            } else {
              // server-side error
              errorMessage = "Error Code: ".concat(error.status, "\nMessage: ").concat(error.message);
            }

            switch (error.status) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            }

            this.commonUtility.showErrorToast(errorMessage);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
          }
        }]);

        return BaseApiService;
      }();

      BaseApiService.ctorParameters = function () {
        return [{
          type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"]
        }, {
          type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }];
      };

      BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], BaseApiService);
      /***/
    },

    /***/
    "./src/app/utility/localstorage.utility.ts":
    /*!*************************************************!*\
      !*** ./src/app/utility/localstorage.utility.ts ***!
      \*************************************************/

    /*! exports provided: LocalstorageUtility */

    /***/
    function srcAppUtilityLocalstorageUtilityTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function () {
        return LocalstorageUtility;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");

      var LocalstorageUtility = /*#__PURE__*/function () {
        function LocalstorageUtility(commonUtility) {
          _classCallCheck(this, LocalstorageUtility);

          this.commonUtility = commonUtility;
        }

        _createClass(LocalstorageUtility, [{
          key: "ProcessValue",
          value: function ProcessValue(value) {
            if (!this.commonUtility.isEmptyOrNull(value)) {
              return value;
            } else {
              return '';
            }
          }
        }, {
          key: "getCustomerId",
          value: function getCustomerId() {
            var value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            return this.ProcessValue(value);
          }
        }, {
          key: "clearLoginData",
          value: function clearLoginData() {
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
          }
        }]);

        return LocalstorageUtility;
      }();

      LocalstorageUtility.ctorParameters = function () {
        return [{
          type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"]
        }];
      };

      LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LocalstorageUtility);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-product-search-product-search-module-es5.js.map