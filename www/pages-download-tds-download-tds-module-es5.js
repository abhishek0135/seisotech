(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-download-tds-download-tds-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-tds/download-tds.page.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-tds/download-tds.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDownloadTdsDownloadTdsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header titleText=\"TDS Download Files\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\">\n    <ion-card class=\"download-report-card\" *ngFor=\"let report of tdsDocuments\">\n        <ion-row class=\"text-black-100\">\n            <ion-col size=\"2\" class=\"left-container\">\n                <ion-row class=\"member-status-image-row\">\n                    <ion-col size=\"12\" class=\"member-status-image-col\">\n\n                        <img src=\"../assets/imgs/download_files_icons/tds-icon.png\" />\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n            <ion-col size=\"10\" class=\"f-12 right-container\">\n                <ion-row>\n                    <ion-col size=\"12\" class=\"ion-no-padding font-weight-bolder\">\n                        {{report.FiledName}}\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size=\"9\">\n                        <div class=\"text-secondary ml-1\"><strong>Filed Id: </strong>{{report.FiledId}}</div>\n                    </ion-col>\n                    <ion-col size=\"3\">\n                        <div class=\"text-secondary text-right\">\n                            <ion-icon name=\"cloud-download-outline\"></ion-icon>\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n    </ion-card>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/download-tds/download-tds-routing.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/download-tds/download-tds-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: DownloadTdsPageRoutingModule */

    /***/
    function srcAppPagesDownloadTdsDownloadTdsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadTdsPageRoutingModule", function () {
        return DownloadTdsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _download_tds_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./download-tds.page */
      "./src/app/pages/download-tds/download-tds.page.ts");

      var routes = [{
        path: '',
        component: _download_tds_page__WEBPACK_IMPORTED_MODULE_3__["DownloadTdsPage"]
      }];

      var DownloadTdsPageRoutingModule = function DownloadTdsPageRoutingModule() {
        _classCallCheck(this, DownloadTdsPageRoutingModule);
      };

      DownloadTdsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DownloadTdsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/download-tds/download-tds.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/download-tds/download-tds.module.ts ***!
      \***********************************************************/

    /*! exports provided: DownloadTdsPageModule */

    /***/
    function srcAppPagesDownloadTdsDownloadTdsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadTdsPageModule", function () {
        return DownloadTdsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _download_tds_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./download-tds-routing.module */
      "./src/app/pages/download-tds/download-tds-routing.module.ts");
      /* harmony import */


      var _download_tds_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./download-tds.page */
      "./src/app/pages/download-tds/download-tds.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var DownloadTdsPageModule = function DownloadTdsPageModule() {
        _classCallCheck(this, DownloadTdsPageModule);
      };

      DownloadTdsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _download_tds_routing_module__WEBPACK_IMPORTED_MODULE_5__["DownloadTdsPageRoutingModule"]],
        declarations: [_download_tds_page__WEBPACK_IMPORTED_MODULE_6__["DownloadTdsPage"]]
      })], DownloadTdsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/download-tds/download-tds.page.scss":
    /*!***********************************************************!*\
      !*** ./src/app/pages/download-tds/download-tds.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDownloadTdsDownloadTdsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rvd25sb2FkLXRkcy9kb3dubG9hZC10ZHMucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/download-tds/download-tds.page.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/download-tds/download-tds.page.ts ***!
      \*********************************************************/

    /*! exports provided: DownloadTdsPage */

    /***/
    function srcAppPagesDownloadTdsDownloadTdsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadTdsPage", function () {
        return DownloadTdsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var DownloadTdsPage = /*#__PURE__*/function () {
        function DownloadTdsPage(appData, appConstants) {
          _classCallCheck(this, DownloadTdsPage);

          this.appData = appData;
          this.appConstants = appConstants;
          this.tdsDocuments = [];
          this.tdsDocuments = this.appData.getTDSDocuments();
        }

        _createClass(DownloadTdsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DownloadTdsPage;
      }();

      DownloadTdsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      DownloadTdsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-download-tds',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./download-tds.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-tds/download-tds.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./download-tds.page.scss */
        "./src/app/pages/download-tds/download-tds.page.scss"))["default"]]
      })], DownloadTdsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-download-tds-download-tds-module-es5.js.map