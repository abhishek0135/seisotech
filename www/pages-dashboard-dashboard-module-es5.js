(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-dashboard-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDashboardDashboardPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border osahan-nav\">\n    <ion-toolbar class=\"in-toolbar\">\n        <ion-buttons slot=\"start\">\n            <ion-buttons slot=\"start\">\n                <ion-menu-button></ion-menu-button>\n            </ion-buttons>\n        </ion-buttons>\n        <ion-title>Dashboard</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content color=\"light\">\n    <div style=\"height: 70px;background: #003049;\"></div>\n    <div>\n        <!-- <div class=\"card\" style=\"height: 100px;background: white;margin-top: -70px;\"></div> -->\n        <ion-card class=\"card\" style=\"background: white;margin-top: -70px;\">\n            <ion-row>\n                <ion-col size=\"4\" style=\"padding: 0;\">\n                    <ion-thumbnail style=\"width: 100%;height: 100%;\">\n                        <img src=\"../../../assets/user/1.jpg\" />\n                    </ion-thumbnail>\n                    <!-- <ion-avatar style=\"height: 100%;width: 100%;\">\n                        <img src=\"../../../assets/user/1.jpg\" />\n                    </ion-avatar> -->\n                </ion-col>\n                <ion-col size=\"8\" style=\"padding-top: 9px;\">\n                    <ion-label>{{dashboardData.userCurrentStatus.name}}</ion-label><br/>\n                    <ion-label>{{dashboardData.userCurrentStatus.memberNo}}</ion-label><br/>\n                    <ion-label>\n                        <img style=\"width: 20px;\" [src]=\"dashboardData.userCurrentStatus.statusImg\" /> {{dashboardData.userCurrentStatus.status}} ({{dashboardData.userCurrentStatus.statusDate}})\n                    </ion-label>\n                </ion-col>\n            </ion-row>\n        </ion-card>\n    </div>\n    <ion-list-header>\n        <h6 class=\"font-bold\">Highlights</h6>\n    </ion-list-header>\n    <div class=\"ion-padding highlight-container\">\n        <div class=\"card highlight-card1-color\">\n            <div>\n                <strong>{{dashboardData.highlights.totalEarning}}</strong>\n            </div>\n            <ion-label>Total Earning</ion-label>\n        </div>\n        <div class=\"card highlight-card2-color\">\n            <div>\n                <strong>{{dashboardData.highlights.companyTurnover}}</strong>\n            </div>\n            <ion-label>Company Turnover</ion-label>\n        </div>\n        <div class=\"card highlight-card3-color\">\n            <div>\n                <strong>{{dashboardData.highlights.freshBV}}</strong>\n            </div>\n            <ion-label>Fresh BV</ion-label>\n        </div>\n        <div class=\"card highlight-card4-color\">\n            <div>\n                <strong>{{dashboardData.highlights.downlineCount}}</strong>\n            </div>\n            <ion-label>Downline count</ion-label>\n        </div>\n        <div class=\"card highlight-card5-color\">\n            <div>\n                <strong>{{dashboardData.highlights.lastMonthDownlineCount}}</strong>\n            </div>\n            <ion-label>Last Month Downline Count</ion-label>\n        </div>\n        <div class=\"card highlight-card6-color\">\n            <div>\n                <strong>{{dashboardData.highlights.currentRank}}</strong>\n            </div>\n            <ion-label>Current rank</ion-label>\n        </div>\n        <div class=\"card highlight-card7-color\">\n            <div>\n                <strong>{{dashboardData.highlights.nextRank}}</strong>\n            </div>\n            <ion-label>Next rank</ion-label>\n        </div>\n        <div class=\"card highlight-card8-color\">\n            <div>\n                <strong>{{dashboardData.highlights.remainingBVForNextRank}}</strong>\n            </div>\n            <ion-label>Remaining BV for Next Rank</ion-label>\n        </div>\n        <div class=\"card highlight-card9-color\">\n            <div>\n                <strong>{{dashboardData.highlights.lastSigninOn}}</strong>\n            </div>\n            <ion-label>Last Sign-In On</ion-label>\n        </div>\n        <div class=\"card highlight-card10-color\">\n            <div>\n                <strong>{{dashboardData.highlights.lastSigninIp}}</strong>\n            </div>\n            <ion-label>Last Sign-In IP</ion-label>\n        </div>\n    </div>\n    <div class=\"more-details-links-container\">\n        <ion-card>\n            <ion-list class=\"ion-no-padding\">\n                <!-- <ion-item lines=\"full\"> -->\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <!-- <ion-label>Downline Rank Qualifiers</ion-label> -->\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <!-- <ion-chip color=\"primary\" mode=\"ios\" outline=\"true\">\n                        <ion-label>58</ion-label>\n                    </ion-chip> -->\n                    <!-- <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.downlineRankQualifierCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item> -->\n                <ion-item lines=\"full\" tappable (click)=\"dashboardSchemeDetails()\">\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <ion-label>Scheme Details</ion-label>\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.schemeDetailCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item>\n                <ion-item lines=\"full\" tappable (click)=\"repurchaseSchemeDetails()\">\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <ion-label>Repurchase Scheme Details</ion-label>\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.repurchaseSchemeDetailCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item>\n                <ion-item lines=\"full\">\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <ion-label>Fast Track Scheme Details</ion-label>\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.fastTrackSchemeDetailCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item>\n                <ion-item lines=\"full\">\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <ion-label>Current business</ion-label>\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.currentBusinessCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item>\n                <ion-item lines=\"full\">\n                    <!-- <ion-avatar slot=\"start\">\n                        <div class=\"record-count\">58</div>\n                    </ion-avatar> -->\n                    <ion-label>Recent Joining</ion-label>\n                    <!-- <ion-icon slot=\"end\" name=\"chevron-forward-outline\"></ion-icon> -->\n                    <ion-chip color=\"primary\" outline=\"true\">\n                        <ion-label>{{dashboardData.recentjoiningCount}}</ion-label>\n                        <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                    </ion-chip>\n                </ion-item>\n            </ion-list>\n        </ion-card>\n        <ion-card>\n            <ion-item>\n                <ion-icon name=\"chevron-forward-outline\" slot=\"end\"></ion-icon>\n                <ion-label>More Information</ion-label>\n            </ion-item>\n        </ion-card>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/dashboard/dashboard-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/dashboard/dashboard-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: DashboardPageRoutingModule */

    /***/
    function srcAppPagesDashboardDashboardRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function () {
        return DashboardPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./dashboard.page */
      "./src/app/pages/dashboard/dashboard.page.ts");

      var routes = [{
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPage"]
      }];

      var DashboardPageRoutingModule = function DashboardPageRoutingModule() {
        _classCallCheck(this, DashboardPageRoutingModule);
      };

      DashboardPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DashboardPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard/dashboard.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
      \*****************************************************/

    /*! exports provided: DashboardPageModule */

    /***/
    function srcAppPagesDashboardDashboardModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function () {
        return DashboardPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./dashboard-routing.module */
      "./src/app/pages/dashboard/dashboard-routing.module.ts");
      /* harmony import */


      var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./dashboard.page */
      "./src/app/pages/dashboard/dashboard.page.ts");

      var DashboardPageModule = function DashboardPageModule() {
        _classCallCheck(this, DashboardPageModule);
      };

      DashboardPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"]],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
      })], DashboardPageModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard/dashboard.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/pages/dashboard/dashboard.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDashboardDashboardPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".highlight-container {\n  padding-top: 0;\n  overflow-x: scroll;\n  overflow-y: hidden;\n  white-space: nowrap;\n}\n.highlight-container .card {\n  padding: 10px;\n  margin-right: 10px;\n  display: inline-block;\n  background: #003049;\n  color: white;\n}\n.more-details-links-container ion-avatar {\n  background: black;\n  color: white;\n  display: block;\n}\n.more-details-links-container ion-label {\n  font-size: 13px;\n}\n.more-details-links-container .record-count {\n  text-align: center;\n  margin-top: 20%;\n  font-size: 12px;\n}\n.highlight-card1-color {\n  background-color: #32a086 !important;\n}\n.highlight-card2-color {\n  background-color: #b4899f !important;\n}\n.highlight-card3-color {\n  background-color: #7d6d3c !important;\n}\n.highlight-card4-color {\n  background-color: #43d3b1 !important;\n}\n.highlight-card5-color {\n  background-color: #17c0f2 !important;\n}\n.highlight-card6-color {\n  background-color: #ffda6d !important;\n}\n.highlight-card7-color {\n  background-color: #0c8fb6 !important;\n}\n.highlight-card8-color {\n  background-color: #ee74b2 !important;\n}\n.highlight-card9-color {\n  background-color: #d9bc65 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkL2Rhc2hib2FyZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBQ0o7QUFBSTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBRVI7QUFHSTtFQUNJLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUFBUjtBQUVJO0VBQ0ksZUFBQTtBQUFSO0FBRUk7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQVI7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREo7QUFJQTtFQUNJLG9DQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9kYXNoYm9hcmQvZGFzaGJvYXJkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWdobGlnaHQtY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgLmNhcmQge1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgYmFja2dyb3VuZDogIzAwMzA0OTtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbn1cblxuLm1vcmUtZGV0YWlscy1saW5rcy1jb250YWluZXIge1xuICAgIGlvbi1hdmF0YXIge1xuICAgICAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgIH1cbiAgICAucmVjb3JkLWNvdW50IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMCU7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB9XG59XG5cbi5oaWdobGlnaHQtY2FyZDEtY29sb3Ige1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMmEwODYgIWltcG9ydGFudDtcbn1cblxuLmhpZ2hsaWdodC1jYXJkMi1jb2xvciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2I0ODk5ZiAhaW1wb3J0YW50O1xufVxuXG4uaGlnaGxpZ2h0LWNhcmQzLWNvbG9yIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjN2Q2ZDNjICFpbXBvcnRhbnQ7XG59XG5cbi5oaWdobGlnaHQtY2FyZDQtY29sb3Ige1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM0M2QzYjEgIWltcG9ydGFudDtcbn1cblxuLmhpZ2hsaWdodC1jYXJkNS1jb2xvciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE3YzBmMiAhaW1wb3J0YW50O1xufVxuXG4uaGlnaGxpZ2h0LWNhcmQ2LWNvbG9yIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkYTZkICFpbXBvcnRhbnQ7XG59XG5cbi5oaWdobGlnaHQtY2FyZDctY29sb3Ige1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwYzhmYjYgIWltcG9ydGFudDtcbn1cblxuLmhpZ2hsaWdodC1jYXJkOC1jb2xvciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VlNzRiMiAhaW1wb3J0YW50O1xufVxuXG4uaGlnaGxpZ2h0LWNhcmQ5LWNvbG9yIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDliYzY1ICFpbXBvcnRhbnQ7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/dashboard/dashboard.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/dashboard/dashboard.page.ts ***!
      \***************************************************/

    /*! exports provided: DashboardPage */

    /***/
    function srcAppPagesDashboardDashboardPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardPage", function () {
        return DashboardPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var DashboardPage = /*#__PURE__*/function () {
        function DashboardPage(dataService, navCltr) {
          _classCallCheck(this, DashboardPage);

          this.dataService = dataService;
          this.navCltr = navCltr;
          this.dashboardData = this.dataService.getDashboardData();
        }

        _createClass(DashboardPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "dashboardSchemeDetails",
          value: function dashboardSchemeDetails() {
            this.navCltr.navigateForward('dashboard-scheme-details');
          }
        }, {
          key: "repurchaseSchemeDetails",
          value: function repurchaseSchemeDetails() {
            this.navCltr.navigateForward('dashboard-repurchase-scheme');
          }
        }]);

        return DashboardPage;
      }();

      DashboardPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      DashboardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./dashboard.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./dashboard.page.scss */
        "./src/app/pages/dashboard/dashboard.page.scss"))["default"]]
      })], DashboardPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-dashboard-dashboard-module-es5.js.map