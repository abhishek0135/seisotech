(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["searchproduct-searchproduct-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/searchproduct/searchproduct.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/searchproduct/searchproduct.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header\n  [titleText]=\"'Product Listing'\"\n  [backButton]=\"true\"\n  [toShowCartButton]=\"true\">\n\n</app-app-header>\n<ion-content\n  color=\"light\"\n  class=\"ion-padding order-list-page ion-color ion-color-light\"\n>\n  <ion-searchbar\n    class=\"p-0 mb-2\"\n    placeholder=\"Search Favorites\"\n    inputmode=\"text\"\n    type=\"search\"\n  ></ion-searchbar>\n  <!-- <div class=\"text-center mb-2\">\n        <small> Placed on Fri, 22 Jun, 8 PM - 11 PM </small>\n    </div> -->\n    \n  <img class=\"rounded shadow-sm mb-2\" src=\"assets/shop.jpg\" />\n  <div\n    class=\"d-flex p-3 shop-cart-item bg-white mb-2\"\n    *ngFor=\"let product of products\"\n    (click)=\"productDetails(product)\">\n\n    <div class=\"shop-cart-left\">\n      <img\n        class=\"not-found-img\"\n        [src]=\"product.img\"\n        onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n      />\n    </div>\n    <div class=\"shop-cart-right\">\n      <h6>{{product.name}}</h6>\n      <h6 class=\"font-weight-bold text-dark mb-2 price\">\n        {{ product.dPrice | currency : 'INR' }}\n        <span\n          class=\"regular-price text-secondary font-weight-normal strick-out\"\n        >\n          {{ product.price | currency : 'INR' }}\n        </span>\n      </h6>\n      <ion-button\n        [disabled]=\"product.addToCartInProgress\"\n        class=\"add-to-cart-btn\"\n        (click)=\"addToCart($event, product)\"\n      >\n        <ion-icon\n          slot=\"start\"\n          name=\"cart-outline\"\n          *ngIf=\"!product.addToCartInProgress\"\n        ></ion-icon>\n        <!-- <img src=\"../../../assets/imgs/addtocart.png\" /> -->\n        <ion-note *ngIf=\"!product.addToCartInProgress\">Add to cart</ion-note>\n        <app-button-spinner\n          *ngIf=\"product.addToCartInProgress\"\n        ></app-button-spinner>\n      </ion-button>\n      <!-- <div class=\"mb-2\" [innerHTML]=\"product.product_description\"></div> -->\n    </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/searchproduct/searchproduct-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/searchproduct/searchproduct-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: SearchproductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchproductPageRoutingModule", function() { return SearchproductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _searchproduct_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./searchproduct.page */ "./src/app/searchproduct/searchproduct.page.ts");




const routes = [
    {
        path: '',
        component: _searchproduct_page__WEBPACK_IMPORTED_MODULE_3__["SearchproductPage"]
    }
];
let SearchproductPageRoutingModule = class SearchproductPageRoutingModule {
};
SearchproductPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SearchproductPageRoutingModule);



/***/ }),

/***/ "./src/app/searchproduct/searchproduct.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/searchproduct/searchproduct.module.ts ***!
  \*******************************************************/
/*! exports provided: SearchproductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchproductPageModule", function() { return SearchproductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _searchproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./searchproduct-routing.module */ "./src/app/searchproduct/searchproduct-routing.module.ts");
/* harmony import */ var _searchproduct_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./searchproduct.page */ "./src/app/searchproduct/searchproduct.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let SearchproductPageModule = class SearchproductPageModule {
};
SearchproductPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _searchproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__["SearchproductPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_searchproduct_page__WEBPACK_IMPORTED_MODULE_6__["SearchproductPage"]]
    })
], SearchproductPageModule);



/***/ }),

/***/ "./src/app/searchproduct/searchproduct.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/searchproduct/searchproduct.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".add-to-cart-btn {\n  --background: #00ab00;\n  color: white;\n}\n.add-to-cart-btn ion-icon {\n  font-size: 28px;\n}\n.add-to-cart-btn ion-note {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNocHJvZHVjdC9zZWFyY2hwcm9kdWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtBQUNKO0FBQUk7RUFDRSxlQUFBO0FBRU47QUFBSTtFQUNFLFlBQUE7QUFFTiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaHByb2R1Y3Qvc2VhcmNocHJvZHVjdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWRkLXRvLWNhcnQtYnRuIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMGFiMDA7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGlvbi1pY29uIHtcbiAgICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICB9XG4gICAgaW9uLW5vdGUge1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgIH1cbiAgfVxuICAiXX0= */");

/***/ }),

/***/ "./src/app/searchproduct/searchproduct.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/searchproduct/searchproduct.page.ts ***!
  \*****************************************************/
/*! exports provided: SearchproductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchproductPage", function() { return SearchproductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/app-api.service */ "./src/app/services/app-api.service.ts");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/utility/app.variables */ "./src/app/utility/app.variables.ts");
/* harmony import */ var src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/utility/common.utility */ "./src/app/utility/common.utility.ts");








let SearchproductPage = class SearchproductPage {
    constructor(navCltr, appApiService, router, values, commonUtility) {
        this.navCltr = navCltr;
        this.appApiService = appApiService;
        this.router = router;
        this.values = values;
        this.commonUtility = commonUtility;
        this.categoryId = '';
        this.products = [];
        this.emptyProduct = [1, 1, 1, 1, 1, 1, 1, 1];
        this.isDataLoaded = false;
        if (this.router.getCurrentNavigation().extras.state) {
            this.categoryId = this.router.getCurrentNavigation().extras.state.categoryId;
        }
    }
    getProducts() {
        this.appApiService.getProducts('', '', this.categoryId, '').subscribe((respData) => {
            this.isDataLoaded = true;
            this.products = respData.ProductWise_List;
        }, (err) => {
            this.isDataLoaded = true;
            //this.products = this.appData.getProducts();
        });
    }
    ngOnInit() {
        this.getProducts();
    }
    productDetails(product) {
        let navigationExtras = {
            state: {
                product: JSON.stringify(product),
            },
        };
        this.navCltr.navigateForward(['product-details'], navigationExtras);
    }
    addToCart(event, product) {
        debugger;
        event.stopPropagation();
        product.addToCartInProgress = true;
        this.appApiService.addProductToCart(product.product_id).subscribe((respData) => {
            product.addToCartInProgress = false;
            this.values.cartCount++;
            this.commonUtility.setLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].localstorageKeys.cartCount, this.values.cartCount.toString());
            this.commonUtility.showSuccessToast('Product added to cart');
        }, (err) => {
            product.addToCartInProgress = false;
        });
    }
};
SearchproductPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__["Values"] },
    { type: src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__["CommonUtility"] }
];
SearchproductPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-searchproduct',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./searchproduct.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/searchproduct/searchproduct.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./searchproduct.page.scss */ "./src/app/searchproduct/searchproduct.page.scss")).default]
    })
], SearchproductPage);



/***/ }),

/***/ "./src/app/services/app-api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/app-api.service.ts ***!
  \*********************************************/
/*! exports provided: AppApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppApiService", function() { return AppApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./base-api.service */ "./src/app/services/base-api.service.ts");




let AppApiService = class AppApiService {
    constructor(baseApi) {
        this.baseApi = baseApi;
    }
    getCategories() {
        const formData = new FormData();
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
    }
    getProducts(productId, brandId, catId, priceRange) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('brand_id', brandId);
        formData.append('cat_id', catId);
        formData.append('price_range', priceRange);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
    }
    getProduct_Search(search) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('search', search);
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
    }
    addProductToCart(productId) {
        const formData = new FormData();
        formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
        formData.append('product_id', productId);
        formData.append('action', 'insert');
        return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
    }
};
AppApiService.ctorParameters = () => [
    { type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"] }
];
AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AppApiService);



/***/ }),

/***/ "./src/app/services/base-api.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/base-api.service.ts ***!
  \**********************************************/
/*! exports provided: BaseApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseApiService", function() { return BaseApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utility/common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utility/app.variables */ "./src/app/utility/app.variables.ts");
/* harmony import */ var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utility/localstorage.utility */ "./src/app/utility/localstorage.utility.ts");
/* harmony import */ var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utility/enums */ "./src/app/utility/enums.ts");










let BaseApiService = class BaseApiService {
    constructor(commonUtility, http, nav, values, localstorageService, platform) {
        this.commonUtility = commonUtility;
        this.http = http;
        this.nav = nav;
        this.values = values;
        this.localstorageService = localstorageService;
        this.platform = platform;
        this.API_URL = '';
        this.isNetAvailable = true;
        this.API_URL = this.values.getApiUrl();
    }
    getApiCall(action, parameters) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
        const reqHeaderOption = this.getHeaderRequestOptions();
        console.log(reqHeaderOption);
        return this.http
            .get(apiCallUrl, reqHeaderOption)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    postApiCall(action, postData, param) {
        const apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
        const reqHeaderOption = this.getHeaderRequestOptions();
        return this.http
            .post(apiCallUrl, postData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    }
    getHeaderRequestOptions() {
        const addHeaders = new Headers();
        // addHeaders.append('Content-Type', 'application/json');
        // addHeaders.append(
        //   'AuthorizedToken',
        //   this.localstorageService.getCustomerId()
        // );
        // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());
        return { headers: addHeaders };
    }
    generateFullURL(apiType, action, parameters) {
        let apiCallUrl = this.API_URL;
        let queryString = '';
        if (action !== '') {
            apiCallUrl = apiCallUrl + '/' + action;
        }
        switch (apiType) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                    queryString = '?';
                    for (let index = 0; index < parameters.length; index++) {
                        if (index > 0) {
                            queryString = queryString + '&';
                        }
                        const parameterKeyValue = parameters[index];
                        queryString =
                            queryString +
                                '' +
                                parameterKeyValue[0] +
                                '=' +
                                parameterKeyValue[1];
                    }
                    apiCallUrl = apiCallUrl + '' + queryString;
                }
        }
        return apiCallUrl;
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        switch (error.status) {
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
        }
        this.commonUtility.showErrorToast(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
    }
};
BaseApiService.ctorParameters = () => [
    { type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"] },
    { type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] }
];
BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], BaseApiService);



/***/ }),

/***/ "./src/app/utility/localstorage.utility.ts":
/*!*************************************************!*\
  !*** ./src/app/utility/localstorage.utility.ts ***!
  \*************************************************/
/*! exports provided: LocalstorageUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function() { return LocalstorageUtility; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common.utility */ "./src/app/utility/common.utility.ts");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");




let LocalstorageUtility = class LocalstorageUtility {
    constructor(commonUtility) {
        this.commonUtility = commonUtility;
    }
    ProcessValue(value) {
        if (!this.commonUtility.isEmptyOrNull(value)) {
            return value;
        }
        else {
            return '';
        }
    }
    getCustomerId() {
        const value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        return this.ProcessValue(value);
    }
    clearLoginData() {
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
        this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
    }
};
LocalstorageUtility.ctorParameters = () => [
    { type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"] }
];
LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], LocalstorageUtility);



/***/ })

}]);
//# sourceMappingURL=searchproduct-searchproduct-module-es2015.js.map