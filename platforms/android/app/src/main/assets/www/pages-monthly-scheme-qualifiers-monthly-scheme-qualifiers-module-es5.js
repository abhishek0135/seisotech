(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-monthly-scheme-qualifiers-monthly-scheme-qualifiers-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.html":
    /*!***************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.html ***!
      \***************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMonthlySchemeQualifiersMonthlySchemeQualifiersPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Monthly Scheme Qualifiers'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of repurchaseSchemeQualifiers\">\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <strong>{{report.name}}</strong>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <strong>{{ report.id}}</strong>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Offer name</div>\n        <div>{{ report.offerName}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Qualification date</div>\n        <div>{{ report.qualificationDate}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div class=\"text-secondary\">Scheme name</div>\n        <div>{{ report.schemeName}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers-routing.module.ts":
    /*!*********************************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers-routing.module.ts ***!
      \*********************************************************************************************/

    /*! exports provided: MonthlySchemeQualifiersPageRoutingModule */

    /***/
    function srcAppPagesMonthlySchemeQualifiersMonthlySchemeQualifiersRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MonthlySchemeQualifiersPageRoutingModule", function () {
        return MonthlySchemeQualifiersPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _monthly_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./monthly-scheme-qualifiers.page */
      "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.ts");

      var routes = [{
        path: '',
        component: _monthly_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_3__["MonthlySchemeQualifiersPage"]
      }];

      var MonthlySchemeQualifiersPageRoutingModule = function MonthlySchemeQualifiersPageRoutingModule() {
        _classCallCheck(this, MonthlySchemeQualifiersPageRoutingModule);
      };

      MonthlySchemeQualifiersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MonthlySchemeQualifiersPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.module.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: MonthlySchemeQualifiersPageModule */

    /***/
    function srcAppPagesMonthlySchemeQualifiersMonthlySchemeQualifiersModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MonthlySchemeQualifiersPageModule", function () {
        return MonthlySchemeQualifiersPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _monthly_scheme_qualifiers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./monthly-scheme-qualifiers-routing.module */
      "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers-routing.module.ts");
      /* harmony import */


      var _monthly_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./monthly-scheme-qualifiers.page */
      "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var MonthlySchemeQualifiersPageModule = function MonthlySchemeQualifiersPageModule() {
        _classCallCheck(this, MonthlySchemeQualifiersPageModule);
      };

      MonthlySchemeQualifiersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _monthly_scheme_qualifiers_routing_module__WEBPACK_IMPORTED_MODULE_5__["MonthlySchemeQualifiersPageRoutingModule"]],
        declarations: [_monthly_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_6__["MonthlySchemeQualifiersPage"]]
      })], MonthlySchemeQualifiersPageModule);
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.scss":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.scss ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMonthlySchemeQualifiersMonthlySchemeQualifiersPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vbnRobHktc2NoZW1lLXF1YWxpZmllcnMvbW9udGhseS1zY2hlbWUtcXVhbGlmaWVycy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.ts ***!
      \***********************************************************************************/

    /*! exports provided: MonthlySchemeQualifiersPage */

    /***/
    function srcAppPagesMonthlySchemeQualifiersMonthlySchemeQualifiersPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MonthlySchemeQualifiersPage", function () {
        return MonthlySchemeQualifiersPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var MonthlySchemeQualifiersPage = /*#__PURE__*/function () {
        function MonthlySchemeQualifiersPage(AppData, appConstants) {
          _classCallCheck(this, MonthlySchemeQualifiersPage);

          this.AppData = AppData;
          this.appConstants = appConstants;
          this.repurchaseSchemeQualifiers = [];
          this.repurchaseSchemeQualifiers = this.AppData.getMonthlySchemeQualifications();
        }

        _createClass(MonthlySchemeQualifiersPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return MonthlySchemeQualifiersPage;
      }();

      MonthlySchemeQualifiersPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      MonthlySchemeQualifiersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-monthly-scheme-qualifiers',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./monthly-scheme-qualifiers.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./monthly-scheme-qualifiers.page.scss */
        "./src/app/pages/monthly-scheme-qualifiers/monthly-scheme-qualifiers.page.scss"))["default"]]
      })], MonthlySchemeQualifiersPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-monthly-scheme-qualifiers-monthly-scheme-qualifiers-module-es5.js.map