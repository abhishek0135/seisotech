(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-set-ewallet-password-set-ewallet-password-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/set-ewallet-password/set-ewallet-password.page.html":
    /*!*****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/set-ewallet-password/set-ewallet-password.page.html ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSetEwalletPasswordSetEwalletPasswordPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'E-Wallet Password'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding\" style=\"--offset-top: 0px; --offset-bottom: 0px\">\n  <div class=\"card mb-2\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n      <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n          New password\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"New password\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n        </ion-input>\n      </ion-item>\n      <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n          Confirm password\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"Confirm password\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n        </ion-input>\n      </ion-item>\n      <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n    </ion-list>\n    <div class=\"mb-2\"></div>\n    </div>\n  <ion-button class=\"ion-no-margin\" shape=\"round\" expand=\"block\">\n    Set Password\n  </ion-button>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/set-ewallet-password/set-ewallet-password-routing.module.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/set-ewallet-password/set-ewallet-password-routing.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: SetEwalletPasswordPageRoutingModule */

    /***/
    function srcAppPagesSetEwalletPasswordSetEwalletPasswordRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SetEwalletPasswordPageRoutingModule", function () {
        return SetEwalletPasswordPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _set_ewallet_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./set-ewallet-password.page */
      "./src/app/pages/set-ewallet-password/set-ewallet-password.page.ts");

      var routes = [{
        path: '',
        component: _set_ewallet_password_page__WEBPACK_IMPORTED_MODULE_3__["SetEwalletPasswordPage"]
      }];

      var SetEwalletPasswordPageRoutingModule = function SetEwalletPasswordPageRoutingModule() {
        _classCallCheck(this, SetEwalletPasswordPageRoutingModule);
      };

      SetEwalletPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SetEwalletPasswordPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/set-ewallet-password/set-ewallet-password.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/set-ewallet-password/set-ewallet-password.module.ts ***!
      \***************************************************************************/

    /*! exports provided: SetEwalletPasswordPageModule */

    /***/
    function srcAppPagesSetEwalletPasswordSetEwalletPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SetEwalletPasswordPageModule", function () {
        return SetEwalletPasswordPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _set_ewallet_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./set-ewallet-password-routing.module */
      "./src/app/pages/set-ewallet-password/set-ewallet-password-routing.module.ts");
      /* harmony import */


      var _set_ewallet_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./set-ewallet-password.page */
      "./src/app/pages/set-ewallet-password/set-ewallet-password.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var SetEwalletPasswordPageModule = function SetEwalletPasswordPageModule() {
        _classCallCheck(this, SetEwalletPasswordPageModule);
      };

      SetEwalletPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _set_ewallet_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["SetEwalletPasswordPageRoutingModule"]],
        declarations: [_set_ewallet_password_page__WEBPACK_IMPORTED_MODULE_6__["SetEwalletPasswordPage"]]
      })], SetEwalletPasswordPageModule);
      /***/
    },

    /***/
    "./src/app/pages/set-ewallet-password/set-ewallet-password.page.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/set-ewallet-password/set-ewallet-password.page.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSetEwalletPasswordSetEwalletPasswordPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NldC1ld2FsbGV0LXBhc3N3b3JkL3NldC1ld2FsbGV0LXBhc3N3b3JkLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/pages/set-ewallet-password/set-ewallet-password.page.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/set-ewallet-password/set-ewallet-password.page.ts ***!
      \*************************************************************************/

    /*! exports provided: SetEwalletPasswordPage */

    /***/
    function srcAppPagesSetEwalletPasswordSetEwalletPasswordPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SetEwalletPasswordPage", function () {
        return SetEwalletPasswordPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var SetEwalletPasswordPage = /*#__PURE__*/function () {
        function SetEwalletPasswordPage() {
          _classCallCheck(this, SetEwalletPasswordPage);
        }

        _createClass(SetEwalletPasswordPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SetEwalletPasswordPage;
      }();

      SetEwalletPasswordPage.ctorParameters = function () {
        return [];
      };

      SetEwalletPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-set-ewallet-password',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./set-ewallet-password.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/set-ewallet-password/set-ewallet-password.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./set-ewallet-password.page.scss */
        "./src/app/pages/set-ewallet-password/set-ewallet-password.page.scss"))["default"]]
      })], SetEwalletPasswordPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-set-ewallet-password-set-ewallet-password-module-es5.js.map