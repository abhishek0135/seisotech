(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-order-return-order-return-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-return/order-return.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-return/order-return.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header\n  [titleText]=\"'Order #6'\"\n  [backButton]=\"true\"\n></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 pr5\">\n    <div class=\"p-2 border-bottom\">\n      <span>Which Item do you want to return ?</span>\n    </div>\n    <div class=\"card-body card-contain\">\n      <div class=\"shop-cart-right border-bottom\">\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"6\">\n            <div>\n              <strong>Uniactiv Protein</strong>\n            </div>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding\">\n            <ion-row class=\"ion-no-padding\">\n              <ion-col size=\"10\" class=\"ion-text-right\">\n                {{ quantity }} Qty to Return\n              </ion-col>\n              <ion-col size=\"2\">\n                <ion-icon\n                  name=\"chevron-down-outline\"\n                  tappable\n                  (click)=\"selectQuantity()\"\n                ></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <div class=\"p-1\">\n          <div class=\"d-flex justify-content-between mb-2\">\n            <div class=\"text-secondary\">Price</div>\n            <div class=\"text-secondary\">$1520.00</div>\n          </div>\n          <div class=\"d-flex justify-content-between mb-2\">\n            <div class=\"text-secondary\">Color</div>\n            <div class=\"text-secondary\">Red</div>\n          </div>\n        </div>\n      </div>\n      <div class=\"shop-cart-right border-bottom\">\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"6\">\n            <div>\n              <strong>Uniactiv Protein</strong>\n            </div>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding\">\n            <ion-row class=\"ion-no-padding\">\n              <ion-col size=\"10\" class=\"ion-text-right\">\n                {{ quantity }} Qty to Return\n              </ion-col>\n              <ion-col size=\"2\">\n                <ion-icon\n                  name=\"chevron-down-outline\"\n                  tappable\n                  (click)=\"selectQuantity()\"\n                ></ion-icon>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <div class=\"p-1\">\n          <div class=\"d-flex justify-content-between mb-2\">\n            <div class=\"text-secondary\">Price</div>\n            <div class=\"text-secondary\">$1520.00</div>\n          </div>\n          <div class=\"d-flex justify-content-between mb-2\">\n            <div class=\"text-secondary\">Color</div>\n            <div class=\"text-secondary\">Red</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"card mt15 pr5 p-1\">\n    <div class=\"p-2 border-bottom\">\n      <span>Why are you returning these items ?</span>\n    </div>\n    <div class=\"card-body card-contain p-1\">\n      <ion-item\n        class=\"item-interactive item-input item-has-placeholder item ion-no-padding\"\n      >\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Return Reason\n        </ion-label>\n        <ion-select\n          [(ngModel)]=\"resturnReason\"\n          placeholder=\"select reason\"\n          interface=\"popover\"\n          mode=\"ios\"\n          required\n          [interfaceOptions]=\"{cssClass: 'my-class'}\"\n        >\n          <ion-select-option value=\"1\">Receive Wrong Product</ion-select-option>\n          <ion-select-option value=\"2\">Defective Product</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n    <div class=\"p-1 text-secondary\">\n      <ion-item\n        class=\"item-interactive item-input item-has-placeholder item ion-no-padding\"\n      >\n        <ion-label\n          position=\"stacked\"\n          class=\"sc-ion-label-md-h sc-ion-label-md-s\"\n        >\n          Return Action\n        </ion-label>\n        <ion-select\n          [(ngModel)]=\"resturnAction\"\n          placeholder=\"select action\"\n          interface=\"popover\"\n          mode=\"ios\"\n          required\n        >\n          <ion-select-option value=\"1\">Repair</ion-select-option>\n          <ion-select-option value=\"2\">Return</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </div>\n    <div class=\"p-1 text-secondary\">\n      <ion-row class=\"ion-no-padding\">\n        <ion-col size=\"7\" class=\"ion-no-padding\">\n          <div>\n            upload(Any additional document, scan, etc)\n          </div>\n        </ion-col>\n        <ion-col size=\"5\" class=\"ion-no-padding ion-text-end\">\n          <div>\n            <ion-button>Upload File</ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class=\"p-1 text-secondary\">\n      <ion-label position=\"floating\">Comments</ion-label>\n      <ion-textarea rows=\"4\" placeholder=\"Enter Comments\"></ion-textarea>\n    </div>\n  </div>\n\n  <ion-item\n    [hidden]=\"hideList\"\n    class=\"item-interactive item-input item-has-placeholder item ion-no-padding\"\n  >\n    <ion-label position=\"stacked\">\n      Hello\n    </ion-label>\n    <ion-select\n      interface=\"popover\"\n      [(ngModel)]=\"quantity\"\n      mode=\"ios\"\n      #QtyNumber\n      required\n      class=\"my-class\"\n      [interfaceOptions]=\"{cssClass: 'my-class'}\"\n    >\n      <ion-select-option value=\"1\">1</ion-select-option>\n      <ion-select-option value=\"2\">2</ion-select-option>\n      <ion-select-option value=\"3\">3</ion-select-option>\n      <ion-select-option value=\"4\">4</ion-select-option>\n    </ion-select>\n  </ion-item>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n    Submit Return Request\n  </ion-button>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/order-return/order-return-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/order-return/order-return-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: OrderReturnPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderReturnPageRoutingModule", function() { return OrderReturnPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_return_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-return.page */ "./src/app/pages/order-return/order-return.page.ts");




const routes = [
    {
        path: '',
        component: _order_return_page__WEBPACK_IMPORTED_MODULE_3__["OrderReturnPage"]
    }
];
let OrderReturnPageRoutingModule = class OrderReturnPageRoutingModule {
};
OrderReturnPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderReturnPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/order-return/order-return.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/order-return/order-return.module.ts ***!
  \***********************************************************/
/*! exports provided: OrderReturnPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderReturnPageModule", function() { return OrderReturnPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_return_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-return-routing.module */ "./src/app/pages/order-return/order-return-routing.module.ts");
/* harmony import */ var _order_return_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-return.page */ "./src/app/pages/order-return/order-return.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let OrderReturnPageModule = class OrderReturnPageModule {
};
OrderReturnPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _order_return_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderReturnPageRoutingModule"]
        ],
        declarations: [_order_return_page__WEBPACK_IMPORTED_MODULE_6__["OrderReturnPage"]]
    })
], OrderReturnPageModule);



/***/ }),

/***/ "./src/app/pages/order-return/order-return.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/order-return/order-return.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card-contain {\n  padding: 0px 0px 0.5rem 0.5rem;\n}\n\n.pl10 {\n  padding-left: 10px;\n}\n\n.border-bottom {\n  border-bottom: 1px solid #e8e2e2;\n}\n\n.pr5 {\n  padding-right: 5px;\n}\n\n.mt15 {\n  margin-top: 15px;\n}\n\n.select-reson {\n  padding: 5px 0 5px 0;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXItcmV0dXJuL29yZGVyLXJldHVybi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7QUFFSjs7QUFBQTtFQUNJLGdDQUFBO0FBR0o7O0FBREE7RUFDSSxrQkFBQTtBQUlKOztBQUZBO0VBQ0ksZ0JBQUE7QUFLSjs7QUFIQTtFQUNJLG9CQUFBO0VBQ0EsWUFBQTtBQU1KIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb3JkZXItcmV0dXJuL29yZGVyLXJldHVybi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZC1jb250YWluIHtcbiAgICBwYWRkaW5nOiAwcHggMHB4IDAuNXJlbSAwLjVyZW07XG59XG4ucGwxMCB7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuLmJvcmRlci1ib3R0b20ge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZThlMmUyOyAgXG59XG4ucHI1IHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG4ubXQxNSB7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5zZWxlY3QtcmVzb24ge1xuICAgIHBhZGRpbmc6IDVweCAwIDVweCAwO1xuICAgIGNvbG9yOiBibGFjaztcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/order-return/order-return.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/order-return/order-return.page.ts ***!
  \*********************************************************/
/*! exports provided: OrderReturnPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderReturnPage", function() { return OrderReturnPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let OrderReturnPage = class OrderReturnPage {
    constructor() {
        this.hideList = true;
        this.quantity = 0;
    }
    ngOnInit() {
    }
    selectQuantity() {
        this.countrySelectRef.open();
    }
    changeQuantity(qty) {
        console.log(qty);
    }
};
OrderReturnPage.ctorParameters = () => [];
OrderReturnPage.propDecorators = {
    countrySelectRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['QtyNumber',] }]
};
OrderReturnPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-return',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-return.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-return/order-return.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-return.page.scss */ "./src/app/pages/order-return/order-return.page.scss")).default]
    })
], OrderReturnPage);



/***/ })

}]);
//# sourceMappingURL=pages-order-return-order-return-module-es2015.js.map