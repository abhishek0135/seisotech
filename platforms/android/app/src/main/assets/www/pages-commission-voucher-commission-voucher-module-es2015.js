(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-commission-voucher-commission-voucher-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-voucher/commission-voucher.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-voucher/commission-voucher.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Payout Statement'\" [backButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <!-- PAyout detail section -->\n  <div class=\"card  mb-2\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\">\n        <strong class=\"payout-header\">Payout details</strong>\n      </ion-col>\n    </ion-row>\n    <div class=\"p-2\">\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Company Payout No.</div>\n        <div class=\"text-secondary\">21</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">My Payout No.</div>\n        <div class=\"text-secondary\">21</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">From date</div>\n        <div class=\"text-secondary\">01-01-2020</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">To Date</div>\n        <div class=\"text-secondary\">31/01/2020</div>\n      </div>\n    </div>\n  </div>\n  <!-- Earning section -->\n  <div class=\"card  mb-2\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\">\n        <strong class=\"payout-header\">Earnings</strong>\n      </ion-col>\n    </ion-row>\n    <div class=\"p-2\">\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Performance Income</div>\n        <div class=\"text-secondary\">\n          {{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Director Bonus</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Leadership Bonus</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Fresh Business Bonus</div>\n        <div class=\"text-secondary\">{{124.25 | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Car Fund</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Travel Fund</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">House Fund</div>\n        <div class=\"text-secondary\">\n          {{124.25  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n    </div>\n    <div class=\"d-flex justify-content-between align-items-center px-2 py-2 border-top text-dark\">\n      <h6 class=\"font-weight-bold m-0\">Total</h6>\n      <h6 class=\"font-weight-bold m-0\">\n        {{124.25  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}</h6>\n    </div>\n  </div>\n  <!-- Deduction section -->\n  <div class=\"card  mb-2\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\">\n        <strong class=\"payout-header\">Deductions</strong>\n      </ion-col>\n    </ion-row>\n    <div class=\"p-2\">\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">TDS</div>\n        <div class=\"text-secondary\">{{6.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Admin Charges</div>\n        <div class=\"text-secondary\">\n          {{6.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n    </div>\n    <div class=\"d-flex justify-content-between align-items-center  px-2 py-2 border-top text-dark\">\n      <h6 class=\"font-weight-bold m-0\">Total</h6>\n      <h6 class=\"font-weight-bold m-0\">\n        {{12.05  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}</h6>\n    </div>\n  </div>\n  <!-- Total section -->\n  <div class=\"card  mb-2\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\">\n        <strong class=\"payout-header\">Total</strong>\n      </ion-col>\n    </ion-row>\n    <div class=\"p-2\">\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Amount payable</div>\n        <div class=\"text-secondary\">\n          {{317.76  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}</div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Brought forward</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Total amount payable</div>\n        <div class=\"text-secondary\">\n          {{317.76  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n      <div class=\"d-flex justify-content-between mb-2\">\n        <div class=\"text-secondary\">Carried forward</div>\n        <div class=\"text-secondary\">{{0.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}\n        </div>\n      </div>\n    </div>\n      <div class=\"d-flex justify-content-between align-items-center py-2 px-2 border-top text-dark\">\n        <h6 class=\"font-weight-bold m-0\">Net payable</h6>\n        <h6 class=\"font-weight-bold m-0\">\n          {{318.00  | currency: appValues.currencyCode: 'symbol': appValues.currencyCodeValue}}</h6>\n      </div>\n  </div>\n  <!-- Downline details section  -->\n  <!-- Ventaforce downline details -->\n  <!-- <div class=\"card  mb-2\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\">\n        <strong class=\"payout-header\">Downline details</strong>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"table-header\">\n      <ion-col col-3 textAlignCenter></ion-col>\n      <ion-col col-3 textAlignCenter>Left</ion-col>\n      <ion-col col-3 textAlignCenter>Right</ion-col>\n      <ion-col col-3 textAlignCenter>Total</ion-col>\n    </ion-row>\n    <div class=\"table-container\">\n      <ion-row>\n        <ion-col col-3 textAlignCenter>B.F.</ion-col>\n        <ion-col col-3 textAlignCenter>0</ion-col>\n        <ion-col col-3 textAlignCenter>0</ion-col>\n        <ion-col col-3 textAlignCenter>0</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-3 textAlignCenter>New</ion-col>\n        <ion-col col-3 textAlignCenter>127</ion-col>\n        <ion-col col-3 textAlignCenter>105</ion-col>\n        <ion-col col-3 textAlignCenter>232</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-3 textAlignCenter>Total</ion-col>\n        <ion-col col-3 textAlignCenter>127</ion-col>\n        <ion-col col-3 textAlignCenter>105</ion-col>\n        <ion-col col-3 textAlignCenter>232</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-3 textAlignCenter>Paid</ion-col>\n        <ion-col col-3 textAlignCenter>106</ion-col>\n        <ion-col col-3 textAlignCenter>105</ion-col>\n        <ion-col col-3 textAlignCenter>211</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-3 textAlignCenter>CF</ion-col>\n        <ion-col col-3 textAlignCenter>21</ion-col>\n        <ion-col col-3 textAlignCenter>0</ion-col>\n        <ion-col col-3 textAlignCenter>21</ion-col>\n      </ion-row>\n    </div>\n  </div> -->\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/commission-voucher/commission-voucher-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/commission-voucher/commission-voucher-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: CommissionVoucherPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionVoucherPageRoutingModule", function() { return CommissionVoucherPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _commission_voucher_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./commission-voucher.page */ "./src/app/pages/commission-voucher/commission-voucher.page.ts");




const routes = [
    {
        path: '',
        component: _commission_voucher_page__WEBPACK_IMPORTED_MODULE_3__["CommissionVoucherPage"]
    }
];
let CommissionVoucherPageRoutingModule = class CommissionVoucherPageRoutingModule {
};
CommissionVoucherPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CommissionVoucherPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/commission-voucher/commission-voucher.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/commission-voucher/commission-voucher.module.ts ***!
  \***********************************************************************/
/*! exports provided: CommissionVoucherPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionVoucherPageModule", function() { return CommissionVoucherPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _commission_voucher_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./commission-voucher-routing.module */ "./src/app/pages/commission-voucher/commission-voucher-routing.module.ts");
/* harmony import */ var _commission_voucher_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./commission-voucher.page */ "./src/app/pages/commission-voucher/commission-voucher.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let CommissionVoucherPageModule = class CommissionVoucherPageModule {
};
CommissionVoucherPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _commission_voucher_routing_module__WEBPACK_IMPORTED_MODULE_5__["CommissionVoucherPageRoutingModule"]
        ],
        declarations: [_commission_voucher_page__WEBPACK_IMPORTED_MODULE_6__["CommissionVoucherPage"]]
    })
], CommissionVoucherPageModule);



/***/ }),

/***/ "./src/app/pages/commission-voucher/commission-voucher.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/commission-voucher/commission-voucher.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".payout-header {\n  font-size: 16px;\n}\n\n.table-header {\n  background: #727a87;\n  color: white;\n}\n\n.row-highlight, .table-container > ion-row:nth-of-type(even) {\n  background-color: #e0e0e0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29tbWlzc2lvbi12b3VjaGVyL2NvbW1pc3Npb24tdm91Y2hlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0FBQ0o7O0FBS0U7RUFDRSxtQkFBQTtFQUNBLFlBQUE7QUFGSjs7QUFJRTtFQUNFLG9DQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb21taXNzaW9uLXZvdWNoZXIvY29tbWlzc2lvbi12b3VjaGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wYXlvdXQtaGVhZGVyIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG59XG4udGFibGUtY29udGFpbmVyPmlvbi1yb3c6bnRoLW9mLXR5cGUoZXZlbikge1xuICAgIEBleHRlbmQgLnJvdy1oaWdobGlnaHRcbiAgfVxuXG4gIC50YWJsZS1oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6ICM3MjdhODc7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG4gIC5yb3ctaGlnaGxpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTBlMGUwICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/commission-voucher/commission-voucher.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/commission-voucher/commission-voucher.page.ts ***!
  \*********************************************************************/
/*! exports provided: CommissionVoucherPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionVoucherPage", function() { return CommissionVoucherPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");



let CommissionVoucherPage = class CommissionVoucherPage {
    constructor(appValues) {
        this.appValues = appValues;
    }
    ngOnInit() {
    }
};
CommissionVoucherPage.ctorParameters = () => [
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
CommissionVoucherPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-commission-voucher',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./commission-voucher.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-voucher/commission-voucher.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./commission-voucher.page.scss */ "./src/app/pages/commission-voucher/commission-voucher.page.scss")).default]
    })
], CommissionVoucherPage);



/***/ })

}]);
//# sourceMappingURL=pages-commission-voucher-commission-voucher-module-es2015.js.map