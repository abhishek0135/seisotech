(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-download-document-download-document-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-document/download-document.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-document/download-document.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header titleText=\"Downloadable Document List\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\">\n  <ion-card class=\"download-report-card\" *ngFor=\"let report of downloadableDocuments\">\n    <ion-row class=\"text-black-100\">\n      <ion-col size=\"2\" class=\"left-container\">\n        <ion-row class=\"member-status-image-row\">\n          <ion-col size=\"12\" class=\"member-status-image-col\">\n            <img *ngIf=\"getFileExtension(report.FileName) === 'txt'\"\n              src=\"../assets/imgs/download_files_icons/other.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'docx'\"\n              src=\"../assets/imgs/download_files_icons/word.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'pdf'\"\n              src=\"../assets/imgs/download_files_icons/pdf.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'ppt'\"\n              src=\"../assets/imgs/download_files_icons/ppt.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'doc'\"\n              src=\"../assets/imgs/download_files_icons/word.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'png'\"\n              src=\"../assets/imgs/download_files_icons/image.png\" />\n            <img *ngIf=\"getFileExtension(report.FileName) === 'xls'\"\n              src=\"../assets/imgs/download_files_icons/excel.png\" />\n          </ion-col>\n        </ion-row>\n      </ion-col>\n      <ion-col size=\"10\" class=\"f-12 right-container\">\n        <ion-row>\n          <ion-col size=\"12\" class=\"ion-no-padding font-weight-bolder\">\n            {{report.DocumentName}}\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\" class=\"ion-no-padding text-right\">\n            <span class=\"label-text\">\n              <ion-icon name=\"cloud-download-outline\"></ion-icon>\n            </span>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/download-document/download-document-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/download-document/download-document-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: DownloadDocumentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadDocumentPageRoutingModule", function() { return DownloadDocumentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _download_document_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./download-document.page */ "./src/app/pages/download-document/download-document.page.ts");




const routes = [
    {
        path: '',
        component: _download_document_page__WEBPACK_IMPORTED_MODULE_3__["DownloadDocumentPage"]
    }
];
let DownloadDocumentPageRoutingModule = class DownloadDocumentPageRoutingModule {
};
DownloadDocumentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DownloadDocumentPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/download-document/download-document.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/download-document/download-document.module.ts ***!
  \*********************************************************************/
/*! exports provided: DownloadDocumentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadDocumentPageModule", function() { return DownloadDocumentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _download_document_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./download-document-routing.module */ "./src/app/pages/download-document/download-document-routing.module.ts");
/* harmony import */ var _download_document_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./download-document.page */ "./src/app/pages/download-document/download-document.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let DownloadDocumentPageModule = class DownloadDocumentPageModule {
};
DownloadDocumentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _download_document_routing_module__WEBPACK_IMPORTED_MODULE_5__["DownloadDocumentPageRoutingModule"]
        ],
        declarations: [_download_document_page__WEBPACK_IMPORTED_MODULE_6__["DownloadDocumentPage"]]
    })
], DownloadDocumentPageModule);



/***/ }),

/***/ "./src/app/pages/download-document/download-document.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/download-document/download-document.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rvd25sb2FkLWRvY3VtZW50L2Rvd25sb2FkLWRvY3VtZW50LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/download-document/download-document.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/download-document/download-document.page.ts ***!
  \*******************************************************************/
/*! exports provided: DownloadDocumentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadDocumentPage", function() { return DownloadDocumentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let DownloadDocumentPage = class DownloadDocumentPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.downloadableDocuments = [];
        this.downloadableDocuments = this.appData.getDownloadableDocuments();
    }
    ngOnInit() {
    }
    getFileExtension(filename) {
        const fileExtension = filename.split('.').pop();
        return fileExtension;
    }
};
DownloadDocumentPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
DownloadDocumentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-download-document',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./download-document.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/download-document/download-document.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./download-document.page.scss */ "./src/app/pages/download-document/download-document.page.scss")).default]
    })
], DownloadDocumentPage);



/***/ })

}]);
//# sourceMappingURL=pages-download-document-download-document-module-es2015.js.map