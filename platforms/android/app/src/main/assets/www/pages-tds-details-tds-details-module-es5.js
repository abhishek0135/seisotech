(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tds-details-tds-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tds-details/tds-details.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tds-details/tds-details.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTdsDetailsTdsDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'TDS Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n    <div class=\"card mt15 mb-2\" *ngFor=\"let commissionLedger of tdsDetailsList\">\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <strong>Payout No #{{commissionLedger.payoutNo}}</strong>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <strong>{{commissionLedger.grossIncome  | currency: appConstants.currencyCode: 'symbol': appConstants.currencyCodeValue}}</strong>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">From Date</div>\n                <div>{{commissionLedger.fromDate}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">To Date</div>\n                <div>{{commissionLedger.toDate}}</div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">TDS Deducted</div>\n                <div>{{commissionLedger.TDSDeducted}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">TDS Precentage</div>\n                <div>{{commissionLedger.TDSPercentage}}</div>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tds-details/tds-details-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/tds-details/tds-details-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: TdsDetailsPageRoutingModule */

    /***/
    function srcAppPagesTdsDetailsTdsDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TdsDetailsPageRoutingModule", function () {
        return TdsDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _tds_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tds-details.page */
      "./src/app/pages/tds-details/tds-details.page.ts");

      var routes = [{
        path: '',
        component: _tds_details_page__WEBPACK_IMPORTED_MODULE_3__["TdsDetailsPage"]
      }];

      var TdsDetailsPageRoutingModule = function TdsDetailsPageRoutingModule() {
        _classCallCheck(this, TdsDetailsPageRoutingModule);
      };

      TdsDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TdsDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tds-details/tds-details.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/tds-details/tds-details.module.ts ***!
      \*********************************************************/

    /*! exports provided: TdsDetailsPageModule */

    /***/
    function srcAppPagesTdsDetailsTdsDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TdsDetailsPageModule", function () {
        return TdsDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _tds_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tds-details-routing.module */
      "./src/app/pages/tds-details/tds-details-routing.module.ts");
      /* harmony import */


      var _tds_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./tds-details.page */
      "./src/app/pages/tds-details/tds-details.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var TdsDetailsPageModule = function TdsDetailsPageModule() {
        _classCallCheck(this, TdsDetailsPageModule);
      };

      TdsDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _tds_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["TdsDetailsPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_tds_details_page__WEBPACK_IMPORTED_MODULE_6__["TdsDetailsPage"]]
      })], TdsDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tds-details/tds-details.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/pages/tds-details/tds-details.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTdsDetailsTdsDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Rkcy1kZXRhaWxzL3Rkcy1kZXRhaWxzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/pages/tds-details/tds-details.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/tds-details/tds-details.page.ts ***!
      \*******************************************************/

    /*! exports provided: TdsDetailsPage */

    /***/
    function srcAppPagesTdsDetailsTdsDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TdsDetailsPage", function () {
        return TdsDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var TdsDetailsPage = /*#__PURE__*/function () {
        function TdsDetailsPage(AppData, appConstants) {
          _classCallCheck(this, TdsDetailsPage);

          this.AppData = AppData;
          this.appConstants = appConstants;
          this.tdsDetailsList = [];
          this.tdsDetailsList = this.AppData.getTDSDetails();
        }

        _createClass(TdsDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return TdsDetailsPage;
      }();

      TdsDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      TdsDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tds-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./tds-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tds-details/tds-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./tds-details.page.scss */
        "./src/app/pages/tds-details/tds-details.page.scss"))["default"]]
      })], TdsDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tds-details-tds-details-module-es5.js.map