(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-my-address-my-address-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-address/my-address.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-address/my-address.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMyAddressMyAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'My Address'\" [menuButton]=\"true\" [toShowAddButton]=\"true\"></app-app-header>\n<ion-content class=\"ion-padding my-address-page\" color=\"light\">\n  <div class=\"addresses-item mb-2 card border-primary\">\n    <div class=\"gold-members p-3\">\n      <div class=\"media\">\n        <div class=\"mr-3\">\n          <ion-icon name=\"home-outline\" size=\"large\"></ion-icon>\n        </div>\n        <div class=\"media-body\">\n          <h5 class=\"mb-2 text-dark font-weight-bold\">Home</h5>\n          <p class=\"text-secondary\">\n            Osahan House, Jawaddi Kalan, Ludhiana, Punjab 141002, India\n          </p>\n          <p class=\"mb-0 small\">\n            <a class=\"text-primary mr-3\" tappable (click)=\"editAddress()\">\n              <ion-icon name=\"create-outline\"></ion-icon>\n              EDIT\n            </a>\n            <!-- <a class=\"text-danger\">\n              <ion-icon name=\"trash-outline\"></ion-icon>\n              DELETE\n            </a> -->\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"addresses-item mb-2 card border-primary\">\n    <div class=\"gold-members p-3\">\n      <div class=\"media\">\n        <div class=\"mr-3\">\n          <ion-icon name=\"briefcase-outline\" size=\"large\"></ion-icon>\n        </div>\n        <div class=\"media-body\">\n          <h5 class=\"mb-2 text-dark font-weight-bold\">Work</h5>\n          <p class=\"text-secondary\">\n            NCC, Model Town Rd, Pritm Nagar, Model Town, Ludhiana, Punjab 141002, India\n          </p>\n          <p class=\"mb-0 small\">\n            <a class=\"text-primary mr-3\" tappable (click)=\"editAddress()\">\n              <ion-icon name=\"create-outline\"></ion-icon>\n              EDIT\n            </a>\n            <!-- <a class=\"text-danger\">\n              <ion-icon name=\"trash-outline\"></ion-icon>\n              DELETE\n            </a> -->\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"addresses-item mb-2 card border-primary\">\n    <div class=\"gold-members p-3\">\n      <div class=\"media\">\n        <div class=\"mr-3\">\n          <ion-icon name=\"navigate-outline\" size=\"large\"></ion-icon>\n        </div>\n        <div class=\"media-body\">\n          <h5 class=\"mb-2 text-dark font-weight-bold\">Other</h5>\n          <p class=\"text-secondary\">\n            GTTT, Model Town Rd, Pritm Nagar, Model Town, Ludhiana, Punjab 141002, India\n          </p>\n          <p class=\"mb-0 small\">\n            <a class=\"text-primary mr-3\" tappable (click)=\"editAddress()\">\n              <ion-icon name=\"create-outline\"></ion-icon>\n              EDIT\n            </a>\n            <!-- <a class=\"text-danger\">\n              <ion-icon name=\"trash-outline\"></ion-icon>\n              DELETE\n            </a> -->\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/my-address/my-address-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/my-address/my-address-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: MyAddressPageRoutingModule */

    /***/
    function srcAppPagesMyAddressMyAddressRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyAddressPageRoutingModule", function () {
        return MyAddressPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _my_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./my-address.page */
      "./src/app/pages/my-address/my-address.page.ts");

      var routes = [{
        path: '',
        component: _my_address_page__WEBPACK_IMPORTED_MODULE_3__["MyAddressPage"]
      }];

      var MyAddressPageRoutingModule = function MyAddressPageRoutingModule() {
        _classCallCheck(this, MyAddressPageRoutingModule);
      };

      MyAddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MyAddressPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/my-address/my-address.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/my-address/my-address.module.ts ***!
      \*******************************************************/

    /*! exports provided: MyAddressPageModule */

    /***/
    function srcAppPagesMyAddressMyAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyAddressPageModule", function () {
        return MyAddressPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _my_address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./my-address-routing.module */
      "./src/app/pages/my-address/my-address-routing.module.ts");
      /* harmony import */


      var _my_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./my-address.page */
      "./src/app/pages/my-address/my-address.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var MyAddressPageModule = function MyAddressPageModule() {
        _classCallCheck(this, MyAddressPageModule);
      };

      MyAddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _my_address_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyAddressPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_my_address_page__WEBPACK_IMPORTED_MODULE_6__["MyAddressPage"]]
      })], MyAddressPageModule);
      /***/
    },

    /***/
    "./src/app/pages/my-address/my-address.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/pages/my-address/my-address.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMyAddressMyAddressPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL215LWFkZHJlc3MvbXktYWRkcmVzcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/pages/my-address/my-address.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/my-address/my-address.page.ts ***!
      \*****************************************************/

    /*! exports provided: MyAddressPage */

    /***/
    function srcAppPagesMyAddressMyAddressPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyAddressPage", function () {
        return MyAddressPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var MyAddressPage = /*#__PURE__*/function () {
        function MyAddressPage(navCltr) {
          _classCallCheck(this, MyAddressPage);

          this.navCltr = navCltr;
        }

        _createClass(MyAddressPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "editAddress",
          value: function editAddress() {
            this.navCltr.navigateForward('add-address');
          }
        }]);

        return MyAddressPage;
      }();

      MyAddressPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      MyAddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-address',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./my-address.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-address/my-address.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./my-address.page.scss */
        "./src/app/pages/my-address/my-address.page.scss"))["default"]]
      })], MyAddressPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-my-address-my-address-module-es5.js.map