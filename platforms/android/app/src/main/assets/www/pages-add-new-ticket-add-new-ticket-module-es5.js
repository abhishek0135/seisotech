(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-add-new-ticket-add-new-ticket-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-new-ticket/add-new-ticket.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-new-ticket/add-new-ticket.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAddNewTicketAddNewTicketPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'New Ticket'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding\" style=\"--offset-top: 0px; --offset-bottom: 0px\">\n  <div class=\"card mb-2\">\n    <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">Select department</ion-label>\n        <ion-select [(ngModel)]=\"department\">\n          <ion-select-option value=\"DataWise\">Admin department</ion-select-option>\n        </ion-select>\n      </ion-item>\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n          Full name\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"Enter full name\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n        </ion-input>\n      </ion-item>\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n          Enter Subject\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-input type=\"text\" placeholder=\"Enter Subject\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n        </ion-input>\n      </ion-item>\n      <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n      <ion-item class=\"item-interactive item-input item-has-placeholder item\">\n        <ion-label position=\"stacked\" class=\"sc-ion-label-md-h sc-ion-label-md-s\">\n          Enquiry\n          <ion-text color=\"danger\">*</ion-text>\n        </ion-label>\n        <ion-textarea type=\"text\" rows=\"4\" placeholder=\"Enter enquiry\" class=\"sc-ion-input-md-h sc-ion-input-md-s\">\n        </ion-textarea>\n      </ion-item>\n      <div class=\"mb-2\"></div>\n      <!-- <ion-text color=\"danger\" *ngIf=\"isValidUserName\" class=\"has-error\">Enter User ID</ion-text> -->\n    </ion-list>\n  </div>\n  <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n    Send\n  </ion-button>\n</ion-content>\n<!-- <ion-footer class=\"ion-no-border footer-background\">\n  <ion-button shape=\"round\" expand=\"block\" color=\"primary\">\n    Send\n  </ion-button>\n</ion-footer> -->";
      /***/
    },

    /***/
    "./src/app/pages/add-new-ticket/add-new-ticket-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/add-new-ticket/add-new-ticket-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: AddNewTicketPageRoutingModule */

    /***/
    function srcAppPagesAddNewTicketAddNewTicketRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddNewTicketPageRoutingModule", function () {
        return AddNewTicketPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _add_new_ticket_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-new-ticket.page */
      "./src/app/pages/add-new-ticket/add-new-ticket.page.ts");

      var routes = [{
        path: '',
        component: _add_new_ticket_page__WEBPACK_IMPORTED_MODULE_3__["AddNewTicketPage"]
      }];

      var AddNewTicketPageRoutingModule = function AddNewTicketPageRoutingModule() {
        _classCallCheck(this, AddNewTicketPageRoutingModule);
      };

      AddNewTicketPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AddNewTicketPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/add-new-ticket/add-new-ticket.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/add-new-ticket/add-new-ticket.module.ts ***!
      \***************************************************************/

    /*! exports provided: AddNewTicketPageModule */

    /***/
    function srcAppPagesAddNewTicketAddNewTicketModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddNewTicketPageModule", function () {
        return AddNewTicketPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _add_new_ticket_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./add-new-ticket-routing.module */
      "./src/app/pages/add-new-ticket/add-new-ticket-routing.module.ts");
      /* harmony import */


      var _add_new_ticket_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-new-ticket.page */
      "./src/app/pages/add-new-ticket/add-new-ticket.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var AddNewTicketPageModule = function AddNewTicketPageModule() {
        _classCallCheck(this, AddNewTicketPageModule);
      };

      AddNewTicketPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _add_new_ticket_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddNewTicketPageRoutingModule"]],
        declarations: [_add_new_ticket_page__WEBPACK_IMPORTED_MODULE_6__["AddNewTicketPage"]]
      })], AddNewTicketPageModule);
      /***/
    },

    /***/
    "./src/app/pages/add-new-ticket/add-new-ticket.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/pages/add-new-ticket/add-new-ticket.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAddNewTicketAddNewTicketPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkZC1uZXctdGlja2V0L2FkZC1uZXctdGlja2V0LnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/pages/add-new-ticket/add-new-ticket.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/add-new-ticket/add-new-ticket.page.ts ***!
      \*************************************************************/

    /*! exports provided: AddNewTicketPage */

    /***/
    function srcAppPagesAddNewTicketAddNewTicketPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddNewTicketPage", function () {
        return AddNewTicketPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AddNewTicketPage = /*#__PURE__*/function () {
        function AddNewTicketPage() {
          _classCallCheck(this, AddNewTicketPage);

          this.department = 'DataWise';
        }

        _createClass(AddNewTicketPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AddNewTicketPage;
      }();

      AddNewTicketPage.ctorParameters = function () {
        return [];
      };

      AddNewTicketPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-new-ticket',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-new-ticket.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/add-new-ticket/add-new-ticket.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-new-ticket.page.scss */
        "./src/app/pages/add-new-ticket/add-new-ticket.page.scss"))["default"]]
      })], AddNewTicketPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-add-new-ticket-add-new-ticket-module-es5.js.map