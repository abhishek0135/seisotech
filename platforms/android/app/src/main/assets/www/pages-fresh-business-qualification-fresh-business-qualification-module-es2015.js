(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-fresh-business-qualification-fresh-business-qualification-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Fresh Business Qualification Report'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of freshBusinessQualificationReport\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div><strong>{{report.name}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">ID</div>\n        <div>{{report.id}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Qualification Date</div>\n        <div>{{report.qualificationDate}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/fresh-business-qualification/fresh-business-qualification-routing.module.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/fresh-business-qualification/fresh-business-qualification-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: FreshBusinessQualificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshBusinessQualificationPageRoutingModule", function() { return FreshBusinessQualificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _fresh_business_qualification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fresh-business-qualification.page */ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.ts");




const routes = [
    {
        path: '',
        component: _fresh_business_qualification_page__WEBPACK_IMPORTED_MODULE_3__["FreshBusinessQualificationPage"]
    }
];
let FreshBusinessQualificationPageRoutingModule = class FreshBusinessQualificationPageRoutingModule {
};
FreshBusinessQualificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FreshBusinessQualificationPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.module.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/fresh-business-qualification/fresh-business-qualification.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: FreshBusinessQualificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshBusinessQualificationPageModule", function() { return FreshBusinessQualificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _fresh_business_qualification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fresh-business-qualification-routing.module */ "./src/app/pages/fresh-business-qualification/fresh-business-qualification-routing.module.ts");
/* harmony import */ var _fresh_business_qualification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fresh-business-qualification.page */ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let FreshBusinessQualificationPageModule = class FreshBusinessQualificationPageModule {
};
FreshBusinessQualificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _fresh_business_qualification_routing_module__WEBPACK_IMPORTED_MODULE_5__["FreshBusinessQualificationPageRoutingModule"]
        ],
        declarations: [_fresh_business_qualification_page__WEBPACK_IMPORTED_MODULE_6__["FreshBusinessQualificationPage"]]
    })
], FreshBusinessQualificationPageModule);



/***/ }),

/***/ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZyZXNoLWJ1c2luZXNzLXF1YWxpZmljYXRpb24vZnJlc2gtYnVzaW5lc3MtcXVhbGlmaWNhdGlvbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: FreshBusinessQualificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreshBusinessQualificationPage", function() { return FreshBusinessQualificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let FreshBusinessQualificationPage = class FreshBusinessQualificationPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.freshBusinessQualificationReport = [];
        this.freshBusinessQualificationReport = this.appData.getFreshQualificationReport();
    }
    ngOnInit() {
    }
};
FreshBusinessQualificationPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
FreshBusinessQualificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fresh-business-qualification',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./fresh-business-qualification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./fresh-business-qualification.page.scss */ "./src/app/pages/fresh-business-qualification/fresh-business-qualification.page.scss")).default]
    })
], FreshBusinessQualificationPage);



/***/ })

}]);
//# sourceMappingURL=pages-fresh-business-qualification-fresh-business-qualification-module-es2015.js.map