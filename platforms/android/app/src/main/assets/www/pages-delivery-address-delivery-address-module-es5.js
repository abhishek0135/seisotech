(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-delivery-address-delivery-address-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-address/delivery-address.page.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-address/delivery-address.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDeliveryAddressDeliveryAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Delivery Address'\" [backButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding\">\n    <div class=\"card\">\n        <form>\n            <ion-list class=\"ion-no-margin ion-no-padding\" lines=\"full\">\n                <ion-item>\n                    <ion-label position=\"stacked\"> Name\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Name\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Flat/House/Office No.\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Flat/House/Office..\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Street/Society/Office Name\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Street/Society/Office...\" type=\"text\"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label position=\"stacked\"> Locality\n                        <ion-text color=\"danger\">*</ion-text>\n                    </ion-label>\n                    <ion-input placeholder=\"Enter Locality\" type=\"text\"></ion-input>\n                </ion-item>\n            </ion-list>\n            <ion-list>\n                <ion-radio-group value=\"anchovies\">\n                    <ion-list-header>\n                        <ion-label>Nickname of your address</ion-label>\n                    </ion-list-header>\n                    <ion-item>\n                        <ion-label>Home</ion-label>\n                        <ion-radio color=\"primary\" slot=\"end\" value=\"Home\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label id=\"ion-rb-10-lbl\">Office</ion-label>\n                        <ion-radio color=\"primary\" slot=\"end\" value=\"Office\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label id=\"ion-rb-11-lbl\">Others</ion-label>\n                        <ion-radio color=\"primary\" slot=\"end\" value=\"Others\"></ion-radio>\n                    </ion-item>\n                </ion-radio-group>\n            </ion-list>\n            <div class=\"ion-padding\">\n                <ion-button class=\"ion-no-margin\" expand=\"block\" type=\"submit\" tabindex=\"0\" (click)=\"payment()\">SAVE\n                </ion-button>\n            </div>\n        </form>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/delivery-address/delivery-address-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/delivery-address/delivery-address-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: DeliveryAddressPageRoutingModule */

    /***/
    function srcAppPagesDeliveryAddressDeliveryAddressRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DeliveryAddressPageRoutingModule", function () {
        return DeliveryAddressPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _delivery_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./delivery-address.page */
      "./src/app/pages/delivery-address/delivery-address.page.ts");

      var routes = [{
        path: '',
        component: _delivery_address_page__WEBPACK_IMPORTED_MODULE_3__["DeliveryAddressPage"]
      }];

      var DeliveryAddressPageRoutingModule = function DeliveryAddressPageRoutingModule() {
        _classCallCheck(this, DeliveryAddressPageRoutingModule);
      };

      DeliveryAddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DeliveryAddressPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/delivery-address/delivery-address.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/delivery-address/delivery-address.module.ts ***!
      \*******************************************************************/

    /*! exports provided: DeliveryAddressPageModule */

    /***/
    function srcAppPagesDeliveryAddressDeliveryAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DeliveryAddressPageModule", function () {
        return DeliveryAddressPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _delivery_address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./delivery-address-routing.module */
      "./src/app/pages/delivery-address/delivery-address-routing.module.ts");
      /* harmony import */


      var _delivery_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./delivery-address.page */
      "./src/app/pages/delivery-address/delivery-address.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var DeliveryAddressPageModule = function DeliveryAddressPageModule() {
        _classCallCheck(this, DeliveryAddressPageModule);
      };

      DeliveryAddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _delivery_address_routing_module__WEBPACK_IMPORTED_MODULE_5__["DeliveryAddressPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_delivery_address_page__WEBPACK_IMPORTED_MODULE_6__["DeliveryAddressPage"]]
      })], DeliveryAddressPageModule);
      /***/
    },

    /***/
    "./src/app/pages/delivery-address/delivery-address.page.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/delivery-address/delivery-address.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDeliveryAddressDeliveryAddressPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RlbGl2ZXJ5LWFkZHJlc3MvZGVsaXZlcnktYWRkcmVzcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/pages/delivery-address/delivery-address.page.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/delivery-address/delivery-address.page.ts ***!
      \*****************************************************************/

    /*! exports provided: DeliveryAddressPage */

    /***/
    function srcAppPagesDeliveryAddressDeliveryAddressPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DeliveryAddressPage", function () {
        return DeliveryAddressPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var DeliveryAddressPage = /*#__PURE__*/function () {
        function DeliveryAddressPage(navCltr) {
          _classCallCheck(this, DeliveryAddressPage);

          this.navCltr = navCltr;
        }

        _createClass(DeliveryAddressPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "payment",
          value: function payment() {
            this.navCltr.navigateForward('payment');
          }
        }]);

        return DeliveryAddressPage;
      }();

      DeliveryAddressPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      DeliveryAddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-delivery-address',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./delivery-address.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/delivery-address/delivery-address.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./delivery-address.page.scss */
        "./src/app/pages/delivery-address/delivery-address.page.scss"))["default"]]
      })], DeliveryAddressPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-delivery-address-delivery-address-module-es5.js.map