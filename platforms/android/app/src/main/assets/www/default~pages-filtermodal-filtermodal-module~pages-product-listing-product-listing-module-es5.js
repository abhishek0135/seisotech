(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-filtermodal-filtermodal-module~pages-product-listing-product-listing-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesFiltermodalFiltermodalPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"toolbar\">\n    <ion-row style=\"display: flex;flex-direction: row;\n    justify-content: space-between;align-items: center;padding: 2%;\">\n      <ion-text>Filter</ion-text>\n      <ion-icon (click)=\"closeModal()\" name=\"close-outline\"></ion-icon>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n<div >\n  <ion-row style=\"display: flex;\n  flex-direction: column;\n  padding: 5%;\">\n        <ion-text style=\"font-size: 18px;\n        line-height: 18px;\">Price Range</ion-text>\n\n      <ion-range *ngIf=\"clearInput\" (ionChange)=\"ionRange($event)\" style=\"padding: 0;\" dualKnobs=\"true\" min=\"0\" max=\"10000\" step=\"3\" value=\"0\" \n       snaps=\"true\" pin=\"true\"></ion-range>\n       <ion-range *ngIf=\"!clearInput\" (ionChange)=\"ionRange($event)\" style=\"padding: 0;\" dualKnobs=\"true\" min=\"0\" max=\"10000\" step=\"3\" value=\"0\" \n       snaps=\"true\" pin=\"true\"></ion-range>\n  </ion-row>\n  <div>\n    <ion-row>\n      <ion-text style=\"font-size: 18px;\n      padding: 0 20px;font-weight: bolder;\">Categories</ion-text>\n    </ion-row>\n    <ion-row>\n      <ion-list style=\"width: 100%;\">\n        <ion-item style=\"width: 100%;\" *ngFor=\"let item of categoryArray\">\n          <ion-checkbox [(ngModel)]=\"item.isChecked\" value={{item.id}} (ionChange)=\"ionchange($event,i)\" slot=\"start\" mode=\"md\"></ion-checkbox>\n          <ion-label>{{item.name}}({{item.cnt}})</ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n  </div>\n\n  <div style=\"margin-top: 10%;\"> \n    <ion-row>\n      <ion-text style=\"font-size: 18px;\n      padding: 0 20px;font-weight: bolder;\">Brand</ion-text>\n    </ion-row>\n    <ion-row>\n      <ion-list style=\"width: 100%;\">\n        <ion-item style=\"width: 100%;\" *ngFor=\"let item of brandArray\">\n        <ion-checkbox [(ngModel)]=\"item.isChecked\" value={{item.brand_id}} (ionChange)=\"ionchange1($event)\" slot=\"start\" mode=\"md\"></ion-checkbox>\n          <ion-label>{{item.brand_name}}</ion-label>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n  </div>\n</div>\n\n\n\n\n\n\n\n\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar class=\"toolbar\">\n    <ion-row>\n      <ion-col (click)=\"apply()\" [size]=\"6\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;border-right: 2px solid lightgrey;\">\n        <ion-text style=\"font-size: 16px;\n        line-height: 18px;\n        padding: 0 6px;\">Apply</ion-text>\n      </ion-col>\n      <ion-col (click)=\"clear()\" [size]=\"6\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;\">\n        <ion-text style=\"font-size: 16px;\n        line-height: 18px;\n        padding: 0 6px;\">Clear</ion-text>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./src/app/pages/filtermodal/filtermodal.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/pages/filtermodal/filtermodal.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesFiltermodalFiltermodalPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".toolbar {\n  --background: #003049 !important;\n}\n.toolbar ion-text {\n  color: #FFFFFF;\n  font-size: 20px;\n}\n.toolbar ion-icon {\n  color: #FFFFFF;\n  font-size: 28px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZmlsdGVybW9kYWwvZmlsdGVybW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0NBQUE7QUFDSjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUjtBQUFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUFFUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZpbHRlcm1vZGFsL2ZpbHRlcm1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDMwNDkgIWltcG9ydGFudDtcbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGNvbG9yOiAjRkZGRkZGO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNGRkZGRkY7XG4gICAgICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICB9XG4gIH1cbi8vICAgaW9uLXJhbmdlIHtcbi8vICAgICAtLWJhci1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuLy8gICAgIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiAjMDAzMDQ5O1xuLy8gICB9Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/filtermodal/filtermodal.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/filtermodal/filtermodal.page.ts ***!
      \*******************************************************/

    /*! exports provided: FiltermodalPage */

    /***/
    function srcAppPagesFiltermodalFiltermodalPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FiltermodalPage", function () {
        return FiltermodalPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/app-api.service */
      "./src/app/services/app-api.service.ts"); // import { Storage } from '@ionic/storage';


      var FiltermodalPage = /*#__PURE__*/function () {
        function FiltermodalPage(modalController, httpClient, appApiService) {
          _classCallCheck(this, FiltermodalPage);

          this.modalController = modalController;
          this.httpClient = httpClient;
          this.appApiService = appApiService;
          this.categoryArray = [];
          this.brandArray = [];
          this.brandId = [];
          this.catId = [];
          this.clearInput = false;
        }

        _createClass(FiltermodalPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            // const formData = new FormData();
            // formData.append('product_id','');
            // formData.append('brand_id','');
            // formData.append('cat','');
            // formData.append('price_range','');
            // formData.append('user_id','3');
            this.httpClient.post('http://dev.agrowonmartpos.com/public/api/category', '').subscribe(function (res) {
              console.log(res);
              _this.categoryArray = res.category_list;

              _this.categoryArray.forEach(function (element) {
                element.isChecked = false;
              });

              _this.httpClient.post('http://dev.agrowonmartpos.com/public/api/brand', '').subscribe(function (res) {
                console.log(res);
                _this.brandArray = res.brand_list;

                _this.brandArray.forEach(function (element) {
                  element.isChecked = false;
                });
              });
            });
          }
        }, {
          key: "ionchange",
          value: function ionchange($event) {
            console.log($event);

            if ($event.detail.checked) {
              this.selectedCatId = $event.detail.value;
              this.catId.push(this.selectedCatId);
              console.log(this.catId);
            } else if (!$event.detail.checked) {
              this.selectedCatId = $event.detail.value;
              var index = this.catId.indexOf(this.selectedCatId);
              console.log(index);

              if (index > -1) {
                this.catId.splice(index, 1);
              }

              console.log(this.catId);
            } else {}

            var totalItems = this.categoryArray.length;
            var checked = 0;
            this.categoryArray.map(function (obj) {
              if (obj.isChecked) checked++;
            });

            if (checked > 0 && checked < totalItems) {
              //If even one item is checked but not all
              this.isIndeterminate = true;
              this.masterCheck = false;
            } else if (checked == totalItems) {
              //If all are checked
              this.masterCheck = true;
              this.isIndeterminate = false;
            } else {
              //If none is checked
              this.isIndeterminate = false;
              this.masterCheck = false;
            }
          }
        }, {
          key: "ionchange1",
          value: function ionchange1($event) {
            console.log($event);

            if ($event.detail.checked) {
              this.selectedBrandId = $event.detail.value;
              this.brandId.push(this.selectedBrandId);
              console.log(this.brandId);
            } else if (!$event.detail.checked) {
              this.selectedBrandId = $event.detail.value;
              var index = this.brandId.indexOf(this.selectedBrandId);
              console.log(index);

              if (index > -1) {
                this.brandId.splice(index, 1);
              }

              console.log(this.brandId);
            } else {}

            var totalItems = this.categoryArray.length;
            var checked = 0;
            this.categoryArray.map(function (obj) {
              if (obj.isChecked) checked++;
            });

            if (checked > 0 && checked < totalItems) {
              //If even one item is checked but not all
              this.isIndeterminate = true;
              this.masterCheck = false;
            } else if (checked == totalItems) {
              //If all are checked
              this.masterCheck = true;
              this.isIndeterminate = false;
            } else {
              //If none is checked
              this.isIndeterminate = false;
              this.masterCheck = false;
            }
          }
        }, {
          key: "ionRange",
          value: function ionRange($event) {
            this.clearInput = false;
            console.log($event.detail.value);
            this.minimum = $event.detail.value.lower.toString();
            this.maximum = $event.detail.value.upper.toString();
            console.log(this.minimum);
            console.log(this.maximum);
            console.log(typeof this.minimum);
            console.log(typeof this.maximum);
            this.priceRange = this.minimum + '-' + this.maximum;
            console.log(this.priceRange);
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalController.dismiss();
          }
        }, {
          key: "apply",
          value: function apply() {
            var _this2 = this;

            // const formData = new FormData();
            // formData.append('product_id','');
            // formData.append('brand_id',(this.brandId));
            // formData.append('cat_id',(this.catId));
            // formData.append('price_range',this.priceRange);
            // formData.append('user_id','3');
            // this.httpClient.post<any>('http://pos.agrowonmartpos.com/public/api/product', formData)
            // .subscribe(res => {
            //   console.log(res);
            // });
            this.appApiService.getProducts('', this.brandId, this.catId, this.priceRange).subscribe(function (respData) {
              console.log(respData);

              _this2.modalController.dismiss(respData.ProductWise_List);
            });
          }
        }, {
          key: "clear",
          value: function clear() {
            var _this3 = this;

            this.clearInput = true;
            setTimeout(function () {
              _this3.categoryArray.forEach(function (obj) {
                obj.isChecked = _this3.masterCheck;

                _this3.brandArray.forEach(function (element) {
                  element.isChecked = _this3.masterCheck;
                });
              });
            });
          }
        }]);

        return FiltermodalPage;
      }();

      FiltermodalPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }, {
          type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"]
        }];
      };

      FiltermodalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-filtermodal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./filtermodal.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/filtermodal/filtermodal.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./filtermodal.page.scss */
        "./src/app/pages/filtermodal/filtermodal.page.scss"))["default"]]
      })], FiltermodalPage);
      /***/
    },

    /***/
    "./src/app/services/app-api.service.ts":
    /*!*********************************************!*\
      !*** ./src/app/services/app-api.service.ts ***!
      \*********************************************/

    /*! exports provided: AppApiService */

    /***/
    function srcAppServicesAppApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppApiService", function () {
        return AppApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var _base_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./base-api.service */
      "./src/app/services/base-api.service.ts");

      var AppApiService = /*#__PURE__*/function () {
        function AppApiService(baseApi) {
          _classCallCheck(this, AppApiService);

          this.baseApi = baseApi;
        }

        _createClass(AppApiService, [{
          key: "getCategories",
          value: function getCategories() {
            var formData = new FormData();
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getCategoryList, formData, []);
          }
        }, {
          key: "getProducts",
          value: function getProducts(productId, brandId, catId, priceRange) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('brand_id', brandId);
            formData.append('cat_id', catId);
            formData.append('price_range', priceRange);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductList, formData, []);
          }
        }, {
          key: "getProduct_Search",
          value: function getProduct_Search(search) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('search', search);
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.getProductSearch, formData, []);
          }
        }, {
          key: "addProductToCart",
          value: function addProductToCart(productId) {
            var formData = new FormData();
            formData.append('user_id', _utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].userId.toString());
            formData.append('product_id', productId);
            formData.append('action', 'insert');
            return this.baseApi.postApiCall(_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"].Action.cartAction, formData, []);
          }
        }]);

        return AppApiService;
      }();

      AppApiService.ctorParameters = function () {
        return [{
          type: _base_api_service__WEBPACK_IMPORTED_MODULE_3__["BaseApiService"]
        }];
      };

      AppApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AppApiService);
      /***/
    },

    /***/
    "./src/app/services/base-api.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/base-api.service.ts ***!
      \**********************************************/

    /*! exports provided: BaseApiService */

    /***/
    function srcAppServicesBaseApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BaseApiService", function () {
        return BaseApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../utility/localstorage.utility */
      "./src/app/utility/localstorage.utility.ts");
      /* harmony import */


      var _utility_enums__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../utility/enums */
      "./src/app/utility/enums.ts");

      var BaseApiService = /*#__PURE__*/function () {
        function BaseApiService(commonUtility, http, nav, values, localstorageService, platform) {
          _classCallCheck(this, BaseApiService);

          this.commonUtility = commonUtility;
          this.http = http;
          this.nav = nav;
          this.values = values;
          this.localstorageService = localstorageService;
          this.platform = platform;
          this.API_URL = '';
          this.isNetAvailable = true;
          this.API_URL = this.values.getApiUrl();
        }

        _createClass(BaseApiService, [{
          key: "getApiCall",
          value: function getApiCall(action, parameters) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET, action, parameters);
            var reqHeaderOption = this.getHeaderRequestOptions();
            console.log(reqHeaderOption);
            return this.http.get(apiCallUrl, reqHeaderOption).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "postApiCall",
          value: function postApiCall(action, postData, param) {
            var apiCallUrl = this.generateFullURL(_utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].POST, action, param);
            var reqHeaderOption = this.getHeaderRequestOptions();
            return this.http.post(apiCallUrl, postData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
          }
        }, {
          key: "getHeaderRequestOptions",
          value: function getHeaderRequestOptions() {
            var addHeaders = new Headers(); // addHeaders.append('Content-Type', 'application/json');
            // addHeaders.append(
            //   'AuthorizedToken',
            //   this.localstorageService.getCustomerId()
            // );
            // addHeaders.append('CustomerId', this.localstorageService.getCustomerId());

            return {
              headers: addHeaders
            };
          }
        }, {
          key: "generateFullURL",
          value: function generateFullURL(apiType, action, parameters) {
            var apiCallUrl = this.API_URL;
            var queryString = '';

            if (action !== '') {
              apiCallUrl = apiCallUrl + '/' + action;
            }

            switch (apiType) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].GET:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].PUT:
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["ApiType"].DELETE:
                if (parameters.length > 0) {
                  queryString = '?';

                  for (var index = 0; index < parameters.length; index++) {
                    if (index > 0) {
                      queryString = queryString + '&';
                    }

                    var parameterKeyValue = parameters[index];
                    queryString = queryString + '' + parameterKeyValue[0] + '=' + parameterKeyValue[1];
                  }

                  apiCallUrl = apiCallUrl + '' + queryString;
                }

            }

            return apiCallUrl;
          }
        }, {
          key: "handleError",
          value: function handleError(error) {
            var errorMessage = '';

            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = "Error: ".concat(error.error.message);
            } else {
              // server-side error
              errorMessage = "Error Code: ".concat(error.status, "\nMessage: ").concat(error.message);
            }

            switch (error.status) {
              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].BadRequest:
                errorMessage = error._body;
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Unauthorized:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;

              case _utility_enums__WEBPACK_IMPORTED_MODULE_9__["HttpStatus"].Forbidden:
                errorMessage = error._body;
                this.localstorageService.clearLoginData();
                this.nav.navigateRoot(['/login']);
                break;
            }

            this.commonUtility.showErrorToast(errorMessage);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
          }
        }]);

        return BaseApiService;
      }();

      BaseApiService.ctorParameters = function () {
        return [{
          type: _utility_common_utility__WEBPACK_IMPORTED_MODULE_6__["CommonUtility"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _utility_app_variables__WEBPACK_IMPORTED_MODULE_7__["Values"]
        }, {
          type: _utility_localstorage_utility__WEBPACK_IMPORTED_MODULE_8__["LocalstorageUtility"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }];
      };

      BaseApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], BaseApiService);
      /***/
    },

    /***/
    "./src/app/utility/localstorage.utility.ts":
    /*!*************************************************!*\
      !*** ./src/app/utility/localstorage.utility.ts ***!
      \*************************************************/

    /*! exports provided: LocalstorageUtility */

    /***/
    function srcAppUtilityLocalstorageUtilityTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocalstorageUtility", function () {
        return LocalstorageUtility;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _common_utility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");

      var LocalstorageUtility = /*#__PURE__*/function () {
        function LocalstorageUtility(commonUtility) {
          _classCallCheck(this, LocalstorageUtility);

          this.commonUtility = commonUtility;
        }

        _createClass(LocalstorageUtility, [{
          key: "ProcessValue",
          value: function ProcessValue(value) {
            if (!this.commonUtility.isEmptyOrNull(value)) {
              return value;
            } else {
              return '';
            }
          }
        }, {
          key: "getCustomerId",
          value: function getCustomerId() {
            var value = this.commonUtility.getLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            return this.ProcessValue(value);
          }
        }, {
          key: "clearLoginData",
          value: function clearLoginData() {
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.isLogin);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.customerId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.countryId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.roleId);
            this.commonUtility.removeLocalstorageItem(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].localstorageKeys.fullname);
          }
        }]);

        return LocalstorageUtility;
      }();

      LocalstorageUtility.ctorParameters = function () {
        return [{
          type: _common_utility__WEBPACK_IMPORTED_MODULE_2__["CommonUtility"]
        }];
      };

      LocalstorageUtility = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LocalstorageUtility);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~pages-filtermodal-filtermodal-module~pages-product-listing-product-listing-module-es5.js.map