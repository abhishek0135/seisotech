(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-voucher-voucher-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/voucher/voucher.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/voucher/voucher.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesVoucherVoucherPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Vouchers'\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding\">\n    <div class=\"card mb15 \">\n        <div class=\"card-header float-center custom-header color-grad border-bottom\">\n            <div class=\"ion-text-center font-weight-bold\"><span class=\"rupees-symbol\">₹</span><span>3000</span></div>\n        </div>\n        <div class=\"card-body bt0\">\n            <ion-row class=\"ion-no-padding\">\n                <ion-col class=\"ion-text-center\">\n                    <h6><strong>COUPON CODE</strong></h6>\n                    <h5><strong>191114133343</strong>\n                        <div class=\"copy-icon\" tappable (click)=\"copyCode()\">\n                            <ion-icon name=\"copy-outline\"></ion-icon>\n                        </div>\n                    </h5>\n                </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-no-padding ion-text-center\">\n                <ion-col size=\"12\">\n                    <ion-text class=\"text-secondary\">\n                        <strong>ACHIEVED ON<br></strong>\n                    </ion-text>\n                    <ion-text class=\"text-secondary\">\n                        <span>16/10/2020</span>\n                    </ion-text>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n\n    <div class=\"card mb15\">\n        <div class=\"card-header float-center custom-header color-grad border-bottom\">\n            <div class=\"ion-text-center font-weight-bold\"><span class=\"rupees-symbol\">₹</span><span>5000</span></div>\n        </div>\n        <div class=\"card-body bt0\">\n            <ion-row class=\"ion-no-padding\">\n                <ion-col class=\"ion-text-center\">\n                    <h6><strong>COUPON CODE</strong></h6>\n                    <h5><strong>191114133344</strong>\n                        <div class=\"copy-icon\" tappable (click)=\"copyCode()\">\n                            <ion-icon name=\"copy-outline\"></ion-icon>\n                        </div>\n                    </h5>\n                </ion-col>\n            </ion-row>\n            <ion-row class=\"ion-no-padding ion-text-center\">\n                <ion-col size=\"12\">\n                    <ion-text class=\"text-secondary\">\n                        <strong>ACHIEVED ON<br></strong>\n                    </ion-text>\n                    <ion-text class=\"text-secondary\">\n                        <span>15/10/2020</span>\n                    </ion-text>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/voucher/voucher-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/voucher/voucher-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: VoucherPageRoutingModule */

    /***/
    function srcAppPagesVoucherVoucherRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VoucherPageRoutingModule", function () {
        return VoucherPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _voucher_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./voucher.page */
      "./src/app/pages/voucher/voucher.page.ts");

      var routes = [{
        path: '',
        component: _voucher_page__WEBPACK_IMPORTED_MODULE_3__["VoucherPage"]
      }];

      var VoucherPageRoutingModule = function VoucherPageRoutingModule() {
        _classCallCheck(this, VoucherPageRoutingModule);
      };

      VoucherPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], VoucherPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/voucher/voucher.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/voucher/voucher.module.ts ***!
      \*************************************************/

    /*! exports provided: VoucherPageModule */

    /***/
    function srcAppPagesVoucherVoucherModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VoucherPageModule", function () {
        return VoucherPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _voucher_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./voucher-routing.module */
      "./src/app/pages/voucher/voucher-routing.module.ts");
      /* harmony import */


      var _voucher_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./voucher.page */
      "./src/app/pages/voucher/voucher.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var VoucherPageModule = function VoucherPageModule() {
        _classCallCheck(this, VoucherPageModule);
      };

      VoucherPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _voucher_routing_module__WEBPACK_IMPORTED_MODULE_5__["VoucherPageRoutingModule"]],
        declarations: [_voucher_page__WEBPACK_IMPORTED_MODULE_6__["VoucherPage"]]
      })], VoucherPageModule);
      /***/
    },

    /***/
    "./src/app/pages/voucher/voucher.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/pages/voucher/voucher.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesVoucherVoucherPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".mb15 {\n  margin-bottom: 15px;\n}\n\n.voucher-card {\n  height: 130px;\n  width: 100%;\n  text-align: center;\n  margin-top: -35px;\n}\n\n.border-bottom {\n  border-bottom: 1px solid;\n}\n\n.color-grad {\n  background: linear-gradient(90deg, #06adef 45%, #12d2ff 91%);\n  color: white;\n  font-size: 1.8rem;\n  font-family: sans-serif !important;\n}\n\n.arrow-div {\n  border-top: 2px solid gray;\n  padding-left: 50%;\n}\n\n.card-header ::after {\n  content: \"\";\n  width: 0;\n  height: 0;\n  border-left: 10px solid transparent;\n  border-right: 10px solid transparent;\n  border-top: 10px solid #06adef;\n  position: absolute;\n  top: 65px;\n  left: 47%;\n}\n\n.arrow-down {\n  width: 0;\n  height: 0;\n  border-left: 10px solid transparent;\n  border-right: 10px solid transparent;\n  border-top: 10px solid gray;\n}\n\n.rupees-symbol {\n  font-family: sans-serif !important;\n}\n\n.copy-icon {\n  position: absolute;\n  right: 0;\n  left: 200px;\n  top: 34px;\n  color: #06adef;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdm91Y2hlci92b3VjaGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLHdCQUFBO0FBQ0o7O0FBRUE7RUFDSSw0REFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtDQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxtQ0FBQTtFQUNBLG9DQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBRUE7RUFDSSxRQUFBO0VBQ0EsU0FBQTtFQUNBLG1DQUFBO0VBQ0Esb0NBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0NBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdm91Y2hlci92b3VjaGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYjE1IHtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4udm91Y2hlci1jYXJkIHtcbiAgICBoZWlnaHQ6IDEzMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAtMzVweDtcbn1cblxuLmJvcmRlci1ib3R0b20ge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZDtcbn1cblxuLmNvbG9yLWdyYWQge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg5MGRlZywgcmdiYSg2LCAxNzMsIDIzOSwgMSkgNDUlLCByZ2JhKDE4LCAyMTAsIDI1NSwgMSkgOTElKTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxLjhyZW07XG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWYgIWltcG9ydGFudDtcbn1cblxuLmFycm93LWRpdiB7XG4gICAgYm9yZGVyLXRvcDogMnB4IHNvbGlkIGdyYXk7XG4gICAgcGFkZGluZy1sZWZ0OiA1MCU7XG59XG5cbi5jYXJkLWhlYWRlciA6OmFmdGVyIHtcbiAgICBjb250ZW50OiAnJztcbiAgICB3aWR0aDogMDtcbiAgICBoZWlnaHQ6IDA7XG4gICAgYm9yZGVyLWxlZnQ6IDEwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXJpZ2h0OiAxMHB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci10b3A6IDEwcHggc29saWQgIzA2YWRlZjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA2NXB4O1xuICAgIGxlZnQ6IDQ3JTtcbn1cblxuLmFycm93LWRvd24ge1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogMDtcbiAgICBib3JkZXItbGVmdDogMTBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItcmlnaHQ6IDEwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXRvcDogMTBweCBzb2xpZCBncmF5O1xufVxuXG4ucnVwZWVzLXN5bWJvbCB7XG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWYgIWltcG9ydGFudDtcbn1cblxuLmNvcHktaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwO1xuICAgIGxlZnQ6IDIwMHB4O1xuICAgIHRvcDogMzRweDtcbiAgICBjb2xvcjogIzA2YWRlZjtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/voucher/voucher.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/voucher/voucher.page.ts ***!
      \***********************************************/

    /*! exports provided: VoucherPage */

    /***/
    function srcAppPagesVoucherVoucherPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VoucherPage", function () {
        return VoucherPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var VoucherPage = /*#__PURE__*/function () {
        function VoucherPage(toastController) {
          _classCallCheck(this, VoucherPage);

          this.toastController = toastController;
        }

        _createClass(VoucherPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "copyCode",
          value: function copyCode() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: 'Your coupon code copied.',
                        duration: 2000,
                        cssClass: 'toast-button',
                        buttons: [{
                          side: 'start',
                          icon: 'checkmark-circle-outline',
                          handler: function handler() {
                            console.log('Favorite clicked');
                          }
                        }]
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return VoucherPage;
      }();

      VoucherPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      VoucherPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-voucher',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./voucher.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/voucher/voucher.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./voucher.page.scss */
        "./src/app/pages/voucher/voucher.page.scss"))["default"]]
      })], VoucherPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-voucher-voucher-module-es5.js.map