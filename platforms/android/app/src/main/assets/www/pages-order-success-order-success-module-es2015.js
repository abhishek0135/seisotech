(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-order-success-order-success-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-success/order-success.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-success/order-success.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-no-padding\">\n    <ion-grid class=\"ion-no-padding\">\n        <ion-row class=\"ion-padding ion-align-items-center vh-100\">\n            <ion-col class=\"ion-no-padding\">\n                <div class=\"ion-text-center\">\n                    <img class=\"not-found-img ion-padding-bottom ion-margin-bottom\" src=\"assets/done.svg\">\n                    <h5 class=\"ion-margin-top text-success ion-margin-bottom\">\n                        <h1 class=\"text-primary mb-0\">Congrats!</h1>\n                        <br>Your Order has been Accepted.. </h5>\n                    <p class=\"ion-margin-bottom\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>\n                    <ion-button class=\"ion-margin-top\" color=\"primary\" tabindex=\"0\" (click)=\"home()\">Return to store</ion-button>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/order-success/order-success-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/order-success/order-success-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: OrderSuccessPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderSuccessPageRoutingModule", function() { return OrderSuccessPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_success_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-success.page */ "./src/app/pages/order-success/order-success.page.ts");




const routes = [
    {
        path: '',
        component: _order_success_page__WEBPACK_IMPORTED_MODULE_3__["OrderSuccessPage"]
    }
];
let OrderSuccessPageRoutingModule = class OrderSuccessPageRoutingModule {
};
OrderSuccessPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderSuccessPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/order-success/order-success.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/order-success/order-success.module.ts ***!
  \*************************************************************/
/*! exports provided: OrderSuccessPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderSuccessPageModule", function() { return OrderSuccessPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_success_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-success-routing.module */ "./src/app/pages/order-success/order-success-routing.module.ts");
/* harmony import */ var _order_success_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-success.page */ "./src/app/pages/order-success/order-success.page.ts");







let OrderSuccessPageModule = class OrderSuccessPageModule {
};
OrderSuccessPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _order_success_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderSuccessPageRoutingModule"]
        ],
        declarations: [_order_success_page__WEBPACK_IMPORTED_MODULE_6__["OrderSuccessPage"]]
    })
], OrderSuccessPageModule);



/***/ }),

/***/ "./src/app/pages/order-success/order-success.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/order-success/order-success.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29yZGVyLXN1Y2Nlc3Mvb3JkZXItc3VjY2Vzcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/order-success/order-success.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/order-success/order-success.page.ts ***!
  \***********************************************************/
/*! exports provided: OrderSuccessPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderSuccessPage", function() { return OrderSuccessPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let OrderSuccessPage = class OrderSuccessPage {
    constructor(navCltr) {
        this.navCltr = navCltr;
    }
    ngOnInit() {
    }
    home() {
        this.navCltr.navigateForward('home');
    }
};
OrderSuccessPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
OrderSuccessPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-success',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-success.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-success/order-success.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-success.page.scss */ "./src/app/pages/order-success/order-success.page.scss")).default]
    })
], OrderSuccessPage);



/***/ })

}]);
//# sourceMappingURL=pages-order-success-order-success-module-es2015.js.map