(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-my-profile-my-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'My Profile'\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content class=\"ion-padding my-profile-page\" color=\"light\">\n  <div class=\"mb-2 card bg-white profile-box text-center\">\n    <div class=\"py-4 px-3 border-bottom\">\n      <img\n        alt=\"img\"\n        class=\"img-fluid mt-2 rounded-circle\"\n        src=\"assets/user/1.jpg\"\n      />\n      <h5 class=\"font-weight-bold text-dark mb-1 mt-4\">{{ user.firstName }} {{ user.lastName }}</h5>\n      <p class=\"mb-0 text-muted\">{{ user.emailId }}</p>\n    </div>\n    <div class=\"d-flex\">\n      <div class=\"col-6 border-right p-3\" tabindex=\"0\" (click)=\"editAddress()\">\n        <h6 class=\"font-weight-bold text-dark mb-1\">\n          <ion-icon\n            name=\"navigate-circle-outline\"\n            ng-reflect-name=\"navigate-circle-outline\"\n            class=\"md hydrated\"\n            aria-label=\"navigate circle outline\"\n          ></ion-icon>\n        </h6>\n        <p class=\"mb-0 text-black-50 small\">Edit Address</p>\n      </div>\n      <div class=\"col-6 p-3\" tabindex=\"0\" tappable (click)=\"editProfile()\">\n        <h6 class=\"font-weight-bold text-dark mb-1\">\n          <ion-icon\n            name=\"person-circle-outline\"\n            ng-reflect-name=\"person-circle-outline\"\n            class=\"md hydrated\"\n            aria-label=\"person circle outline\"\n          ></ion-icon>\n        </h6>\n        <p class=\"mb-0 text-black-50 small\">Edit Profile</p>\n      </div>\n    </div>\n    <div\n      class=\"overflow-hidden border-top p-3 d-flex justify-content-between align-items-center\"\n      tabindex=\"0\" tappable (click)=\"myWallet()\">\n      <small class=\"text-secondary font-weight-bold\">\n        <ion-icon\n          name=\"wallet-outline\"\n          ng-reflect-name=\"wallet-outline\"\n          class=\"md hydrated\"\n          aria-label=\"wallet outline\"\n        ></ion-icon>\n        My Balance: </small\n      ><small class=\"text-primary font-weight-bold\">$4,543,00</small>\n    </div>\n  </div>\n  <div class=\"list-group\">\n    <a class=\"list-group-item list-group-item-action active\">\n      <ion-icon\n        class=\"mr-2 md hydrated\"\n        name=\"person-outline\"\n        role=\"img\"\n        aria-label=\"person outline\"\n      ></ion-icon>\n      My Profile </a\n    ><a class=\"list-group-item list-group-item-action\" tappable (click)=\"myAddress()\">\n      <ion-icon\n        class=\"mr-2 md hydrated\"\n        name=\"location-outline\"\n        role=\"img\"\n        aria-label=\"location outline\"\n      ></ion-icon>\n      My Address </a\n    ><a class=\"list-group-item list-group-item-action\" tappable (click)=\"wishlist()\">\n      <ion-icon\n        class=\"mr-2 md hydrated\"\n        name=\"heart-outline\"\n        role=\"img\"\n        aria-label=\"heart outline\"\n      ></ion-icon>\n      Wish List </a\n    ><a\n      class=\"list-group-item list-group-item-action\"\n      ng-reflect-router-link=\"/order-list\"\n      tappable (click)=\"orderList()\"\n    >\n      <ion-icon\n        class=\"mr-2 md hydrated\"\n        name=\"list-outline\"\n        role=\"img\"\n        aria-label=\"list outline\"\n      ></ion-icon>\n      Order List </a\n    ><a class=\"list-group-item list-group-item-action\" tappable (click)=\"myWallet()\">\n      <ion-icon\n        class=\"mr-2 md hydrated\"\n        name=\"wallet-outline\"\n        role=\"img\"\n        aria-label=\"wallet outline\"\n      ></ion-icon>\n      My wallet\n    </a>\n  </div>\n</ion-content>\n<ion-footer class=\"border-0\" role=\"contentinfo\">\n  <ion-button\n    class=\"p-0 m-0\"\n    color=\"primary\"\n    expand=\"full\"\n    size=\"large\"\n    ng-reflect-expand=\"full\"\n  >\n    <ion-icon name=\"lock-closed-outline\"></ion-icon> Logout\n  </ion-button>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/my-profile/my-profile-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/my-profile/my-profile-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: MyProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePageRoutingModule", function() { return MyProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _my_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-profile.page */ "./src/app/pages/my-profile/my-profile.page.ts");




const routes = [
    {
        path: '',
        component: _my_profile_page__WEBPACK_IMPORTED_MODULE_3__["MyProfilePage"]
    }
];
let MyProfilePageRoutingModule = class MyProfilePageRoutingModule {
};
MyProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/my-profile/my-profile.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-profile/my-profile.module.ts ***!
  \*******************************************************/
/*! exports provided: MyProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePageModule", function() { return MyProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-profile-routing.module */ "./src/app/pages/my-profile/my-profile-routing.module.ts");
/* harmony import */ var _my_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-profile.page */ "./src/app/pages/my-profile/my-profile.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let MyProfilePageModule = class MyProfilePageModule {
};
MyProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyProfilePageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_my_profile_page__WEBPACK_IMPORTED_MODULE_6__["MyProfilePage"]]
    })
], MyProfilePageModule);



/***/ }),

/***/ "./src/app/pages/my-profile/my-profile.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-profile/my-profile.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".img-fluid {\n  max-width: 40%;\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbXktcHJvZmlsZS9teS1wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9teS1wcm9maWxlL215LXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1mbHVpZCB7XG4gICAgbWF4LXdpZHRoOiA0MCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/my-profile/my-profile.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-profile/my-profile.page.ts ***!
  \*****************************************************/
/*! exports provided: MyProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyProfilePage", function() { return MyProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utility/app.data */ "./src/app/utility/app.data.ts");




let MyProfilePage = class MyProfilePage {
    constructor(navCltr, appData) {
        this.navCltr = navCltr;
        this.appData = appData;
        this.user = this.appData.getUserData();
        console.log(this.user);
    }
    ngOnInit() {
    }
    myAddress() {
        this.navCltr.navigateForward('my-address');
    }
    wishlist() {
        this.navCltr.navigateForward('wishlist');
    }
    orderList() {
        this.navCltr.navigateForward('order-list');
    }
    myWallet() {
        this.navCltr.navigateForward('my-wallet');
    }
    editProfile() {
        this.navCltr.navigateForward('edit-profile');
    }
    editAddress() {
        this.navCltr.navigateForward('add-address');
    }
};
MyProfilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] }
];
MyProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./my-profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-profile/my-profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./my-profile.page.scss */ "./src/app/pages/my-profile/my-profile.page.scss")).default]
    })
], MyProfilePage);



/***/ })

}]);
//# sourceMappingURL=pages-my-profile-my-profile-module-es2015.js.map