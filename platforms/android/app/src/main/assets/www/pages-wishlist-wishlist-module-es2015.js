(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-wishlist-wishlist-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/wishlist/wishlist.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/wishlist/wishlist.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'My Wishlist'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-padding order-list-page\">\n    <ion-searchbar class=\"p-0 mb-2 \" placeholder=\"Search wishlist\" inputmode=\"text\" type=\"search\"></ion-searchbar>\n\n    <div class=\"card order-item-card mb-2\">\n        <div class=\"d-flex align-items-center p-2\">\n            <div class=\"float-right trash-icon\">\n                <ion-icon name=\"trash-outline\" color=\"danger\" tappable (click)=\"delete()\"></ion-icon>\n            </div>\n            <img alt=\"img\" class=\"not-found-img\" src=\"assets/small/7.jpg\">\n            <div class=\"overflow-hidden ml-3\">\n                <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">Super Store - Ludhiana\n                </h6>\n                <div class=\"text-truncate text-primary\">$233,00</div>\n                <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" class=\"md hydrated\" aria-label=\"cart outline\"></ion-icon> Product ID : GURDEEEP8343743\n                </div>\n            </div>\n        </div>\n        <div class=\"p-3 border-top\">\n            <div class=\"d-flex justify-content-between mb-1\"><small class=\"text-secondary\">Original Payable\n          amount</small><small class=\"text-secondary\">$233,00</small></div>\n            <div class=\"d-flex justify-content-between mb-3\"><small class=\"text-gray\">Final paid amount</small><small class=\"text-gray\">$00,00</small>\n            </div>\n            <ion-button class=\"button-block mb-0 mx-0 ion-color ion-color-primary md button button-solid ion-activatable ion-focusable hydrated\" color=\"primary\" tabindex=\"0\" color=\"primary\">Add To Cart\n            </ion-button>\n        </div>\n    </div>\n    <div class=\"card order-item-card mb-2\">\n        <div class=\"d-flex align-items-center p-2\"><img alt=\"img\" class=\"not-found-img\" src=\"assets/small/5.jpg\">\n            <div class=\"float-right trash-icon\">\n                <ion-icon name=\"trash-outline\" color=\"danger\" tappable (click)=\"delete()\"></ion-icon>\n            </div>\n            <div class=\"overflow-hidden ml-3\">\n                <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">Singh Store - Goa</h6>\n                <div class=\"text-truncate text-primary\">$533,00</div>\n                <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" class=\"md hydrated\" aria-label=\"cart outline\"></ion-icon> Product ID : GURDEEEP8343743\n                </div>\n            </div>\n        </div>\n        <div class=\"p-3 border-top\">\n            <div class=\"d-flex justify-content-between mb-1\"><small class=\"text-secondary\">Original Payable\n          amount</small><small class=\"text-secondary\">$533,00</small></div>\n            <div class=\"d-flex justify-content-between mb-1\"><small class=\"text-secondary\">Delivery charges</small><small class=\"text-secondary\">$510,00</small></div>\n            <div class=\"d-flex justify-content-between mb-3\"><small class=\"text-gray\">Final paid amount</small><small class=\"text-gray\">$644,00</small>\n            </div>\n            <ion-button class=\"button-block mb-0 mx-0 ion-color ion-color-primary md button button-solid ion-activatable ion-focusable hydrated\" color=\"primary\" tabindex=\"0\" color=\"primary\">Add To Cart\n            </ion-button>\n        </div>\n    </div>\n    <div class=\"card order-item-card mb-2\">\n        <div class=\"d-flex align-items-center p-2\"><img alt=\"img\" class=\"not-found-img\" src=\"assets/small/6.jpg\">\n            <div class=\"float-right trash-icon\">\n                <ion-icon name=\"trash-outline\" color=\"danger\" tappable (click)=\"delete()\"></ion-icon>\n            </div>\n            <div class=\"overflow-hidden ml-3\">\n                <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">Osahan Store - Puna</h6>\n                <div class=\"text-truncate text-primary\">$233,00</div>\n                <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" class=\"md hydrated\" aria-label=\"cart outline\"></ion-icon> Product ID : GURDEEEP8343743\n                </div>\n            </div>\n        </div>\n        <div class=\"p-3 border-top\">\n            <div class=\"d-flex justify-content-between mb-1\"><small class=\"text-secondary\">Original Payable\n          amount</small><small class=\"text-secondary\">$233,00</small></div>\n            <div class=\"d-flex justify-content-between mb-3\"><small class=\"text-gray\">Final paid amount</small><small class=\"text-gray\">$00,00</small>\n            </div>\n            <ion-button class=\"button-block mb-0 mx-0 ion-color ion-color-primary md button button-solid ion-activatable ion-focusable hydrated\" color=\"primary\" tabindex=\"0\" color=\"primary\">Add To Cart\n            </ion-button>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/wishlist/wishlist-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: WishlistPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPageRoutingModule", function() { return WishlistPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _wishlist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wishlist.page */ "./src/app/pages/wishlist/wishlist.page.ts");




const routes = [
    {
        path: '',
        component: _wishlist_page__WEBPACK_IMPORTED_MODULE_3__["WishlistPage"]
    }
];
let WishlistPageRoutingModule = class WishlistPageRoutingModule {
};
WishlistPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], WishlistPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.module.ts ***!
  \***************************************************/
/*! exports provided: WishlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPageModule", function() { return WishlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _wishlist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wishlist-routing.module */ "./src/app/pages/wishlist/wishlist-routing.module.ts");
/* harmony import */ var _wishlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wishlist.page */ "./src/app/pages/wishlist/wishlist.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let WishlistPageModule = class WishlistPageModule {
};
WishlistPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _wishlist_routing_module__WEBPACK_IMPORTED_MODULE_5__["WishlistPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_wishlist_page__WEBPACK_IMPORTED_MODULE_6__["WishlistPage"]]
    })
], WishlistPageModule);



/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".not-found-img {\n  width: 60px;\n  height: 60px;\n  border: 1px solid #f4f5f8;\n  border-radius: 3px;\n}\n\n.trash-icon {\n  position: absolute;\n  top: 0.6rem;\n  right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2lzaGxpc3Qvd2lzaGxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNKLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FBQ0E7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FBRUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy93aXNobGlzdC93aXNobGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubm90LWZvdW5kLWltZyB7XG4gICAgd2lkdGg6IDYwcHg7XG5oZWlnaHQ6IDYwcHg7XG5ib3JkZXI6IDFweCBzb2xpZCAjZjRmNWY4O1xuYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLnRyYXNoLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDAuNnJlbTtcbiAgICByaWdodDogMTBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/wishlist/wishlist.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/wishlist/wishlist.page.ts ***!
  \*************************************************/
/*! exports provided: WishlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPage", function() { return WishlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let WishlistPage = class WishlistPage {
    constructor(alertController) {
        this.alertController = alertController;
    }
    ngOnInit() {
    }
    delete() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Warning!',
                message: '<strong>Are You sure To Delete </strong>',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
WishlistPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
WishlistPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-wishlist',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./wishlist.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/wishlist/wishlist.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./wishlist.page.scss */ "./src/app/pages/wishlist/wishlist.page.scss")).default]
    })
], WishlistPage);



/***/ })

}]);
//# sourceMappingURL=pages-wishlist-wishlist-module-es2015.js.map