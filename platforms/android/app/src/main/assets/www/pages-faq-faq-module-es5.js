(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-faq-faq-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesFaqFaqPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"osahan-nav\">\n  <ion-toolbar class=\"in-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n    </ion-buttons>\n    <ion-title>\n      FAQ\n    </ion-title>\n  </ion-toolbar>\n  <ion-toolbar class=\"in-toolbar  toolbar-searchbar\">\n    <ion-searchbar class=\"searchbar-left-aligned\" placeholder=\"Search\" inputmode=\"text\" type=\"search\">\n      <ion-icon aria-hidden=\"true\" class=\"searchbar-search-icon sc-ion-searchbar-md \" role=\"img\"\n        aria-label=\"search sharp\"></ion-icon>\n    </ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-content color=\"light\" style=\"--offset-top:0px; --offset-bottom:0px;\">\n    <ion-card class=\"faq-card\">\n      <ion-card-header class=\"ion-inherit-color \">\n        <ion-card-title role=\"heading\" aria-level=\"2\" class=\"ion-inherit-color \">Where\n          can I get access to Capital IQ?</ion-card-title>\n      </ion-card-header>\n      <ion-card-content class=\"card-content\"> Anim pariatur cliche reprehenderit,\n        enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard\n        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on\n        it squid single-origin coffee nulla assumenda shoreditch et. Nihil helvetica, craf. </ion-card-content>\n    </ion-card>\n    <ion-card class=\"faq-card\">\n      <ion-card-header class=\"ion-inherit-color \">\n        <ion-card-title role=\"heading\" aria-level=\"2\" class=\"ion-inherit-color \">How do\n          I get access to case studies?</ion-card-title>\n      </ion-card-header>\n      <ion-card-content class=\"card-content\"> Enim eiusmod high life accusamus terry\n        richardson ad squid. 3 wolf moon officia aute Anim pariatur cliche reprehenderit, non cupidatat skateboard dolor\n        brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it\n        squid single-origin coffee nulla assumenda shoreditch et. Nihil helvetica, craf. </ion-card-content>\n    </ion-card>\n    <ion-card class=\"faq-card\">\n      <ion-card-header class=\"ion-inherit-color \">\n        <ion-card-title role=\"heading\" aria-level=\"2\" class=\"ion-inherit-color \">How\n          much should I capitalize?</ion-card-title>\n      </ion-card-header>\n      <ion-card-content class=\"card-content\"> Bird on it squid single-origin coffee\n        nulla assumenda shoreditch et. Nihil helvetica, craf. Anim pariatur cliche reprehenderit, enim eiusmod high life\n        accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck\n        quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a </ion-card-content>\n    </ion-card>\n  </ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/faq/faq-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/faq/faq-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: FaqPageRoutingModule */

    /***/
    function srcAppPagesFaqFaqRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FaqPageRoutingModule", function () {
        return FaqPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _faq_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./faq.page */
      "./src/app/pages/faq/faq.page.ts");

      var routes = [{
        path: '',
        component: _faq_page__WEBPACK_IMPORTED_MODULE_3__["FaqPage"]
      }];

      var FaqPageRoutingModule = function FaqPageRoutingModule() {
        _classCallCheck(this, FaqPageRoutingModule);
      };

      FaqPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FaqPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/faq/faq.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/faq/faq.module.ts ***!
      \*****************************************/

    /*! exports provided: FaqPageModule */

    /***/
    function srcAppPagesFaqFaqModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FaqPageModule", function () {
        return FaqPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _faq_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./faq-routing.module */
      "./src/app/pages/faq/faq-routing.module.ts");
      /* harmony import */


      var _faq_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./faq.page */
      "./src/app/pages/faq/faq.page.ts");

      var FaqPageModule = function FaqPageModule() {
        _classCallCheck(this, FaqPageModule);
      };

      FaqPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _faq_routing_module__WEBPACK_IMPORTED_MODULE_5__["FaqPageRoutingModule"]],
        declarations: [_faq_page__WEBPACK_IMPORTED_MODULE_6__["FaqPage"]]
      })], FaqPageModule);
      /***/
    },

    /***/
    "./src/app/pages/faq/faq.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/pages/faq/faq.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesFaqFaqPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZhcS9mYXEucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/faq/faq.page.ts":
    /*!***************************************!*\
      !*** ./src/app/pages/faq/faq.page.ts ***!
      \***************************************/

    /*! exports provided: FaqPage */

    /***/
    function srcAppPagesFaqFaqPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FaqPage", function () {
        return FaqPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var FaqPage = /*#__PURE__*/function () {
        function FaqPage(navController) {
          _classCallCheck(this, FaqPage);

          this.navController = navController;
        }

        _createClass(FaqPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goBack",
          value: function goBack() {
            this.navController.pop();
          }
        }]);

        return FaqPage;
      }();

      FaqPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      FaqPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-faq',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./faq.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./faq.page.scss */
        "./src/app/pages/faq/faq.page.scss"))["default"]]
      })], FaqPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-faq-faq-module-es5.js.map