(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-fund-qualification-report-fund-qualification-report-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fund-qualification-report/fund-qualification-report.page.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fund-qualification-report/fund-qualification-report.page.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Fund Qualification Report'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n    <div class=\"card mt15 mb-2\" *ngFor=\"let fundItem of fundQualificationReport\">\n        <ion-row>\n            <ion-col size=\"12\" class=\"text-left\">\n                <div><strong>{{fundItem.name}}</strong></div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <div class=\"text-secondary\">Type</div>\n                <div>{{fundItem.type}}</div>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <div class=\"text-secondary\">Qualification Date</div>\n                <div>{{fundItem.qualificationDate}}</div>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/fund-qualification-report/fund-qualification-report-routing.module.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/fund-qualification-report/fund-qualification-report-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: FundQualificationReportPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundQualificationReportPageRoutingModule", function() { return FundQualificationReportPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _fund_qualification_report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fund-qualification-report.page */ "./src/app/pages/fund-qualification-report/fund-qualification-report.page.ts");




const routes = [
    {
        path: '',
        component: _fund_qualification_report_page__WEBPACK_IMPORTED_MODULE_3__["FundQualificationReportPage"]
    }
];
let FundQualificationReportPageRoutingModule = class FundQualificationReportPageRoutingModule {
};
FundQualificationReportPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FundQualificationReportPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/fund-qualification-report/fund-qualification-report.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/fund-qualification-report/fund-qualification-report.module.ts ***!
  \*************************************************************************************/
/*! exports provided: FundQualificationReportPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundQualificationReportPageModule", function() { return FundQualificationReportPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _fund_qualification_report_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fund-qualification-report-routing.module */ "./src/app/pages/fund-qualification-report/fund-qualification-report-routing.module.ts");
/* harmony import */ var _fund_qualification_report_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fund-qualification-report.page */ "./src/app/pages/fund-qualification-report/fund-qualification-report.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let FundQualificationReportPageModule = class FundQualificationReportPageModule {
};
FundQualificationReportPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _fund_qualification_report_routing_module__WEBPACK_IMPORTED_MODULE_5__["FundQualificationReportPageRoutingModule"]
        ],
        declarations: [_fund_qualification_report_page__WEBPACK_IMPORTED_MODULE_6__["FundQualificationReportPage"]]
    })
], FundQualificationReportPageModule);



/***/ }),

/***/ "./src/app/pages/fund-qualification-report/fund-qualification-report.page.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/fund-qualification-report/fund-qualification-report.page.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Z1bmQtcXVhbGlmaWNhdGlvbi1yZXBvcnQvZnVuZC1xdWFsaWZpY2F0aW9uLXJlcG9ydC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/fund-qualification-report/fund-qualification-report.page.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/fund-qualification-report/fund-qualification-report.page.ts ***!
  \***********************************************************************************/
/*! exports provided: FundQualificationReportPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FundQualificationReportPage", function() { return FundQualificationReportPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let FundQualificationReportPage = class FundQualificationReportPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.fundQualificationReport = [];
        this.fundQualificationReport = this.appData.getFundQualificationReport();
    }
    ngOnInit() {
    }
};
FundQualificationReportPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
FundQualificationReportPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fund-qualification-report',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./fund-qualification-report.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fund-qualification-report/fund-qualification-report.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./fund-qualification-report.page.scss */ "./src/app/pages/fund-qualification-report/fund-qualification-report.page.scss")).default]
    })
], FundQualificationReportPage);



/***/ })

}]);
//# sourceMappingURL=pages-fund-qualification-report-fund-qualification-report-module-es2015.js.map