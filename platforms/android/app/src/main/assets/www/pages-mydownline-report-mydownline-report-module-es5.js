(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-mydownline-report-mydownline-report-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/mydownline-report/mydownline-report.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/mydownline-report/mydownline-report.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMydownlineReportMydownlineReportPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header titleText=\"My Downline\" [menuButton]=\"true\"></app-app-header>\n\n<ion-content color=\"light\">\n    <ion-slides pager=\"true\">\n        <ion-slide>\n            <ion-card class=\"card w-100 downline-summery-card\">\n                <ion-row>\n                    <ion-col size=\"8\">\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Downline Count</div>\n                                <strong>{{myDownlineReportData.summary.downlineCount}}</strong>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Confirm Downline Count</div>\n                                <strong>{{myDownlineReportData.summary.confirmDownlineCount}}</strong>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <div class=\"right-tree-view-icon\">\n                            <img src=\"../../../assets/tree-view-structure.png\">\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-card>\n        </ion-slide>\n        <ion-slide>\n            <ion-card class=\"card w-100 downline-summery-card\">\n                <ion-row>\n                    <ion-col size=\"8\">\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Downline BV</div>\n                                <strong>{{myDownlineReportData.summary.downlineBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Confirm Downline BV</div>\n                                <strong>{{myDownlineReportData.summary.confirmDownlineBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <div class=\"right-tree-view-icon\">\n                            <img src=\"../../../assets/tree-view-structure.png\">\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-card>\n        </ion-slide>\n        <ion-slide>\n            <ion-card class=\"card w-100 downline-summery-card\">\n                <ion-row>\n                    <ion-col size=\"8\">\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Own BV</div>\n                                <strong>{{myDownlineReportData.summary.ownBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Confirm Own BV</div>\n                                <strong>{{myDownlineReportData.summary.confirmOwnBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <div class=\"right-tree-view-icon\">\n                            <img src=\"../../../assets/tree-view-structure.png\">\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-card>\n        </ion-slide>\n        <ion-slide>\n            <ion-card class=\"card w-100 downline-summery-card\">\n                <ion-row>\n                    <ion-col size=\"8\">\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Group BV</div>\n                                <strong>{{myDownlineReportData.summary.groupBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col size=\"12\" class=\"text-left\">\n                                <div>Confirm Group BV</div>\n                                <strong>{{myDownlineReportData.summary.confirmGroupBV}}</strong>\n                            </ion-col>\n                        </ion-row>\n                    </ion-col>\n                    <ion-col size=\"4\">\n                        <div class=\"right-tree-view-icon\">\n                            <img src=\"../../../assets/tree-view-structure.png\">\n                        </div>\n                    </ion-col>\n                </ion-row>\n            </ion-card>\n        </ion-slide>\n    </ion-slides>\n\n    <ion-card class=\"report-card\" *ngFor=\"let report of myDownlineReportData.downlines\">\n        <ion-row class=\"text-black-100\">\n            <ion-col size=\"3\" class=\"left-container\">\n                <ion-row class=\"member-status-image-row\">\n                    <ion-col size=\"12\" class=\"member-status-image-col\">\n                        <img src=\"data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAFF0lEQVR42rWXf0zUZRzH0dC2Wrna4ucJgg70H2cRGf4KUSs0mmZpuVxaWz+2LKazskaiCA4LJiLYxATJUMIKMSytwSiW0yKhciIYvwK6HRwHHMfx6+7T5/3cPd/73on8Ovxun90Nvs/zen/ez+f5PM95eEzgKTEUzKvtrdzd2l9fyFFUZ/57f3lX0SMed/q50n0xst9ibqfbPIPWAVNVT/n6OwKv7q1IspJVgPBZrM+mtJZYSm3eRgVth8hsMSlCmvpunJhUeIWx5E0JP9OWTtGFQfRk/ixadSqQVuUFUlROAK3IDaDM1vcVEddMlw9MCnzlycD7JDxHu4+e/iaIos8GCRH4/tSZWUIMv0eRWTPp44YXFZf4/dDJsD4XE940/0mrzwXRmuJg8ekqAG5AxBOfaejnzkK5FKXu8qe0D7Q2Y7Jd9c8pApwcKHAWABc2VIYKAWZLj5nn8HRHwD0W65CwM+bH2QIssrcLQPaKgDybANQDRKAoMe6t2qXBE6Y//1tIIATw1hNAdbjaDwEoxKjsALEM3B+EgPjGTRHuODBjwNpPELHmfLAAAizhrtlLATYHesQ4fneOOwKmcyGJGvigfq3IVILVa6+2H9lv/MtWA4bBNhOW0a0q/MmQn4tMuN0KEKAKWAWX2S/L0NCl7u+F/dw/SlDI7u4Eb86kHxN+qftEgACVoYYj++R/XxfZG4c6iccGuN0H8nUHw/WD2j64gCep6VUBVMA5AUrlv3NzpdKEjEMG63f648+4af/pGJ6I5BJsqp6ntF0Fnu0oPDiwtmwOXTWWKS782lX89oTgh1t2zmbrBRwHDwAIAVSBl38+U/kfBCxJ86dFn/rRce0euwgDnW0/GjXuDsj7WM/HK7ELtCxT44DboTIE/KgNjsC7EPH4fl9xdmA5eAmt4+qIDN2FTgYHMCkAMkuncAFjByCWpmtocao/RST7UUPfddHILndfyBpH9nVGZI+9j2yEAJdwAmc6wAiMWXLQnxYm+tKGqlCxjHymYFdMG5XO0JCuIb0oICWrTM2twBHACOnAwn2+3JbryGTppkMt21eMZdvFwjKsPYpJ2jpcOIHTHGCMQ0Qc8KNH43wo6784XNfooiEvgxFTR+JP5ctlEQRgvyMDCVGHAnTJWMIBxlgEBLxRu0gI4A5ZNlprnl5hLL2CA2j7P9GikiVguJBQJ7AdjrEICNhcPV8KuMSM+0cUUN51rhQOoACxfmqIK1BttSsYBYjxYR96CwFIipvSqAKmcL9PwRY8rUul8Hgfx+S3i+RhwHb4Y3t9aMFOL0ps2iq2Ije0r0Y9HbfeCIvUDTQTQk4mIa4hoeqMJRgB+x9+z0vcJ9FT9jS+vGUsp+O9PKAbLmRrE4SFmOwWWKJztmownAv7yJvmv/sQ7W3cLLJv6qtBH3hgTJ2ID531GICBp3QpIlsIwaThu30cEW/7RKYIvIOMF+zwEt/TW3aIOdAH+LoeO56zwJPXLYFvQ+JqZRjU0bftR+i1mnBxIUUnFMVnXwZ8oinhlvTS9bni3oAlhItIhH+woA3fPd4DyZNvPDG8deplJ0M2EIRAp8RJZ4tO+99Nyju4lPKxrH3hj5AtE4Grnwf51Fsd17Axhav496qeX9pqeq/21JmvDfBBY2GXLPx9kH8t9/L/9NztKhMaXznMTq3jsV6TcSWTz11cD4vPd5zYdqHjZBJHBrfrYww8xp8ZP3R8kcwR+3VbxvKgZ2dMG+uk/wMX+LuBWJ8tZAAAAABJRU5ErkJggg==\"\n                        />\n                    </ion-col>\n                </ion-row>\n                <ion-row class=\"member-status-text-row\">\n                    <div>\n                        {{report.status}}\n                    </div>\n                </ion-row>\n            </ion-col>\n            <ion-col size=\"9\" class=\"f-12 right-container\">\n                <ion-row>\n                    <ion-col size=\"12\" class=\"ion-no-padding font-weight-bolder\">\n                        {{report.name}}\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <span class=\"label-text\">ID:</span> {{report.id}}\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <span class=\"label-text\">Sponsor ID:</span> {{report.sponsorId}}\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size=\"6\" class=\"ion-no-padding\">\n                        <span class=\"label-text\">DOJ:</span> {{report.doj}}\n                    </ion-col>\n                    <ion-col size=\"6\" class=\"ion-no-padding text-right\">\n                        <span class=\"label-text\">Level:</span> {{report.level}}\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col size=\"12\" class=\"ion-no-padding\">\n                        <span class=\"label-text\">Current Package:</span> {{report.currentPackage}}\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n    </ion-card>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/mydownline-report/mydownline-report-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/mydownline-report/mydownline-report-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: MydownlineReportPageRoutingModule */

    /***/
    function srcAppPagesMydownlineReportMydownlineReportRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MydownlineReportPageRoutingModule", function () {
        return MydownlineReportPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _mydownline_report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./mydownline-report.page */
      "./src/app/pages/mydownline-report/mydownline-report.page.ts");

      var routes = [{
        path: '',
        component: _mydownline_report_page__WEBPACK_IMPORTED_MODULE_3__["MydownlineReportPage"]
      }];

      var MydownlineReportPageRoutingModule = function MydownlineReportPageRoutingModule() {
        _classCallCheck(this, MydownlineReportPageRoutingModule);
      };

      MydownlineReportPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MydownlineReportPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/mydownline-report/mydownline-report.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/mydownline-report/mydownline-report.module.ts ***!
      \*********************************************************************/

    /*! exports provided: MydownlineReportPageModule */

    /***/
    function srcAppPagesMydownlineReportMydownlineReportModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MydownlineReportPageModule", function () {
        return MydownlineReportPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _mydownline_report_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./mydownline-report-routing.module */
      "./src/app/pages/mydownline-report/mydownline-report-routing.module.ts");
      /* harmony import */


      var _mydownline_report_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./mydownline-report.page */
      "./src/app/pages/mydownline-report/mydownline-report.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var MydownlineReportPageModule = function MydownlineReportPageModule() {
        _classCallCheck(this, MydownlineReportPageModule);
      };

      MydownlineReportPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _mydownline_report_routing_module__WEBPACK_IMPORTED_MODULE_5__["MydownlineReportPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_mydownline_report_page__WEBPACK_IMPORTED_MODULE_6__["MydownlineReportPage"]]
      })], MydownlineReportPageModule);
      /***/
    },

    /***/
    "./src/app/pages/mydownline-report/mydownline-report.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/mydownline-report/mydownline-report.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMydownlineReportMydownlineReportPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".right-tree-view-icon {\n  height: 100px;\n  vertical-align: middle;\n  display: table-cell;\n}\n\n.downline-summery-card {\n  background: url('gray-white-abstract.jpg');\n  background-size: contain;\n  color: #333;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbXlkb3dubGluZS1yZXBvcnQvbXlkb3dubGluZS1yZXBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLDBDQUFBO0VBQ0Esd0JBQUE7RUFDQSxXQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9teWRvd25saW5lLXJlcG9ydC9teWRvd25saW5lLXJlcG9ydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmlnaHQtdHJlZS12aWV3LWljb24ge1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xufVxuXG4uZG93bmxpbmUtc3VtbWVyeS1jYXJkIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2dyYXktd2hpdGUtYWJzdHJhY3QuanBnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgY29sb3I6ICMzMzM7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/mydownline-report/mydownline-report.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/mydownline-report/mydownline-report.page.ts ***!
      \*******************************************************************/

    /*! exports provided: MydownlineReportPage */

    /***/
    function srcAppPagesMydownlineReportMydownlineReportPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MydownlineReportPage", function () {
        return MydownlineReportPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var MydownlineReportPage = /*#__PURE__*/function () {
        function MydownlineReportPage() {
          _classCallCheck(this, MydownlineReportPage);

          this.myDownlineReportData = {};
          this.myDownlineReportData = {
            summary: {
              downlineCount: 9802,
              confirmDownlineCount: 8847,
              downlineBV: 12099954.00,
              confirmDownlineBV: 12101034.00,
              ownBV: 421.00,
              confirmOwnBV: 421.00,
              groupBV: 12100375.00,
              confirmGroupBV: 12101455.00
            },
            downlines: [{
              status: 'Paid',
              name: 'Komal Harshal Bankar',
              id: '100374',
              sponsorId: '461861',
              currentPackage: 'Free Package( 0.00 )',
              doj: '09/03/2020',
              level: '26'
            }, {
              status: 'Paid',
              name: 'Shreekrishn Narayan Londhe',
              id: '100991',
              sponsorId: 'DGYPR3880Q',
              currentPackage: 'Free Package( 0.00 )',
              doj: '12/11/2019',
              level: '15'
            }, {
              status: 'Paid',
              name: 'POPPY BORAH',
              id: '101122',
              sponsorId: '560427',
              currentPackage: 'Free Package( 0.00 )',
              doj: '05/12/2019',
              level: '17'
            }]
          };
        }

        _createClass(MydownlineReportPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return MydownlineReportPage;
      }();

      MydownlineReportPage.ctorParameters = function () {
        return [];
      };

      MydownlineReportPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-mydownline-report',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./mydownline-report.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/mydownline-report/mydownline-report.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./mydownline-report.page.scss */
        "./src/app/pages/mydownline-report/mydownline-report.page.scss"))["default"]]
      })], MydownlineReportPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-mydownline-report-mydownline-report-module-es5.js.map