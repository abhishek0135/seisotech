(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-fasttrack-scheme-qualifiers-fasttrack-scheme-qualifiers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Fast Track Scheme Qualifiers'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of fastTrackSchemeQualifiers\">\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <strong>{{report.name}}</strong>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <strong>{{ report.id}}</strong>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Rank</div>\n        <div>{{ report.rank}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Qualification Date</div>\n        <div>{{ report.qualificationDate}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Year</div>\n        <div>{{ report.year}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Month</div>\n        <div>{{ report.month}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers-routing.module.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: FasttrackSchemeQualifiersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasttrackSchemeQualifiersPageRoutingModule", function() { return FasttrackSchemeQualifiersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _fasttrack_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fasttrack-scheme-qualifiers.page */ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.ts");




const routes = [
    {
        path: '',
        component: _fasttrack_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_3__["FasttrackSchemeQualifiersPage"]
    }
];
let FasttrackSchemeQualifiersPageRoutingModule = class FasttrackSchemeQualifiersPageRoutingModule {
};
FasttrackSchemeQualifiersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FasttrackSchemeQualifiersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: FasttrackSchemeQualifiersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasttrackSchemeQualifiersPageModule", function() { return FasttrackSchemeQualifiersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _fasttrack_scheme_qualifiers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fasttrack-scheme-qualifiers-routing.module */ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers-routing.module.ts");
/* harmony import */ var _fasttrack_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fasttrack-scheme-qualifiers.page */ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let FasttrackSchemeQualifiersPageModule = class FasttrackSchemeQualifiersPageModule {
};
FasttrackSchemeQualifiersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _fasttrack_scheme_qualifiers_routing_module__WEBPACK_IMPORTED_MODULE_5__["FasttrackSchemeQualifiersPageRoutingModule"]
        ],
        declarations: [_fasttrack_scheme_qualifiers_page__WEBPACK_IMPORTED_MODULE_6__["FasttrackSchemeQualifiersPage"]]
    })
], FasttrackSchemeQualifiersPageModule);



/***/ }),

/***/ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Zhc3R0cmFjay1zY2hlbWUtcXVhbGlmaWVycy9mYXN0dHJhY2stc2NoZW1lLXF1YWxpZmllcnMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.ts ***!
  \***************************************************************************************/
/*! exports provided: FasttrackSchemeQualifiersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasttrackSchemeQualifiersPage", function() { return FasttrackSchemeQualifiersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let FasttrackSchemeQualifiersPage = class FasttrackSchemeQualifiersPage {
    constructor(AppData, appConstants) {
        this.AppData = AppData;
        this.appConstants = appConstants;
        this.fastTrackSchemeQualifiers = [];
        this.fastTrackSchemeQualifiers = this.AppData.getFasttrackSchemeQualifiers();
    }
    ngOnInit() {
    }
};
FasttrackSchemeQualifiersPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
FasttrackSchemeQualifiersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fasttrack-scheme-qualifiers',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./fasttrack-scheme-qualifiers.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./fasttrack-scheme-qualifiers.page.scss */ "./src/app/pages/fasttrack-scheme-qualifiers/fasttrack-scheme-qualifiers.page.scss")).default]
    })
], FasttrackSchemeQualifiersPage);



/***/ })

}]);
//# sourceMappingURL=pages-fasttrack-scheme-qualifiers-fasttrack-scheme-qualifiers-module-es2015.js.map