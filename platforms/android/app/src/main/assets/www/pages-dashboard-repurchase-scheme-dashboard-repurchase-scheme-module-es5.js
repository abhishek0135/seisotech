(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-repurchase-scheme-dashboard-repurchase-scheme-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.html":
    /*!*******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.html ***!
      \*******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDashboardRepurchaseSchemeDashboardRepurchaseSchemePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Repurchase Scheme Details'\" [backButton]=\"true\"></app-app-header>\n<ion-content>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme-routing.module.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme-routing.module.ts ***!
      \*************************************************************************************************/

    /*! exports provided: DashboardRepurchaseSchemePageRoutingModule */

    /***/
    function srcAppPagesDashboardRepurchaseSchemeDashboardRepurchaseSchemeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardRepurchaseSchemePageRoutingModule", function () {
        return DashboardRepurchaseSchemePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _dashboard_repurchase_scheme_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./dashboard-repurchase-scheme.page */
      "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.ts");

      var routes = [{
        path: '',
        component: _dashboard_repurchase_scheme_page__WEBPACK_IMPORTED_MODULE_3__["DashboardRepurchaseSchemePage"]
      }];

      var DashboardRepurchaseSchemePageRoutingModule = function DashboardRepurchaseSchemePageRoutingModule() {
        _classCallCheck(this, DashboardRepurchaseSchemePageRoutingModule);
      };

      DashboardRepurchaseSchemePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DashboardRepurchaseSchemePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: DashboardRepurchaseSchemePageModule */

    /***/
    function srcAppPagesDashboardRepurchaseSchemeDashboardRepurchaseSchemeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardRepurchaseSchemePageModule", function () {
        return DashboardRepurchaseSchemePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _dashboard_repurchase_scheme_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./dashboard-repurchase-scheme-routing.module */
      "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme-routing.module.ts");
      /* harmony import */


      var _dashboard_repurchase_scheme_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./dashboard-repurchase-scheme.page */
      "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var DashboardRepurchaseSchemePageModule = function DashboardRepurchaseSchemePageModule() {
        _classCallCheck(this, DashboardRepurchaseSchemePageModule);
      };

      DashboardRepurchaseSchemePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _dashboard_repurchase_scheme_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardRepurchaseSchemePageRoutingModule"]],
        declarations: [_dashboard_repurchase_scheme_page__WEBPACK_IMPORTED_MODULE_6__["DashboardRepurchaseSchemePage"]]
      })], DashboardRepurchaseSchemePageModule);
      /***/
    },

    /***/
    "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.scss":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.scss ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDashboardRepurchaseSchemeDashboardRepurchaseSchemePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC1yZXB1cmNoYXNlLXNjaGVtZS9kYXNoYm9hcmQtcmVwdXJjaGFzZS1zY2hlbWUucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.ts ***!
      \***************************************************************************************/

    /*! exports provided: DashboardRepurchaseSchemePage */

    /***/
    function srcAppPagesDashboardRepurchaseSchemeDashboardRepurchaseSchemePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardRepurchaseSchemePage", function () {
        return DashboardRepurchaseSchemePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var DashboardRepurchaseSchemePage = /*#__PURE__*/function () {
        function DashboardRepurchaseSchemePage() {
          _classCallCheck(this, DashboardRepurchaseSchemePage);
        }

        _createClass(DashboardRepurchaseSchemePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DashboardRepurchaseSchemePage;
      }();

      DashboardRepurchaseSchemePage.ctorParameters = function () {
        return [];
      };

      DashboardRepurchaseSchemePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard-repurchase-scheme',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./dashboard-repurchase-scheme.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./dashboard-repurchase-scheme.page.scss */
        "./src/app/pages/dashboard-repurchase-scheme/dashboard-repurchase-scheme.page.scss"))["default"]]
      })], DashboardRepurchaseSchemePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-dashboard-repurchase-scheme-dashboard-repurchase-scheme-module-es5.js.map