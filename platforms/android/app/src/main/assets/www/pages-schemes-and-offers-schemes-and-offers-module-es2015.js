(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-schemes-and-offers-schemes-and-offers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schemes-and-offers/schemes-and-offers.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schemes-and-offers/schemes-and-offers.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>schemes-and-offers</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/schemes-and-offers/schemes-and-offers-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/schemes-and-offers/schemes-and-offers-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: SchemesAndOffersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchemesAndOffersPageRoutingModule", function() { return SchemesAndOffersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _schemes_and_offers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./schemes-and-offers.page */ "./src/app/pages/schemes-and-offers/schemes-and-offers.page.ts");




const routes = [
    {
        path: '',
        component: _schemes_and_offers_page__WEBPACK_IMPORTED_MODULE_3__["SchemesAndOffersPage"]
    }
];
let SchemesAndOffersPageRoutingModule = class SchemesAndOffersPageRoutingModule {
};
SchemesAndOffersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SchemesAndOffersPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/schemes-and-offers/schemes-and-offers.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/schemes-and-offers/schemes-and-offers.module.ts ***!
  \***********************************************************************/
/*! exports provided: SchemesAndOffersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchemesAndOffersPageModule", function() { return SchemesAndOffersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _schemes_and_offers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./schemes-and-offers-routing.module */ "./src/app/pages/schemes-and-offers/schemes-and-offers-routing.module.ts");
/* harmony import */ var _schemes_and_offers_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./schemes-and-offers.page */ "./src/app/pages/schemes-and-offers/schemes-and-offers.page.ts");







let SchemesAndOffersPageModule = class SchemesAndOffersPageModule {
};
SchemesAndOffersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _schemes_and_offers_routing_module__WEBPACK_IMPORTED_MODULE_5__["SchemesAndOffersPageRoutingModule"]
        ],
        declarations: [_schemes_and_offers_page__WEBPACK_IMPORTED_MODULE_6__["SchemesAndOffersPage"]]
    })
], SchemesAndOffersPageModule);



/***/ }),

/***/ "./src/app/pages/schemes-and-offers/schemes-and-offers.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/schemes-and-offers/schemes-and-offers.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NjaGVtZXMtYW5kLW9mZmVycy9zY2hlbWVzLWFuZC1vZmZlcnMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/schemes-and-offers/schemes-and-offers.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/schemes-and-offers/schemes-and-offers.page.ts ***!
  \*********************************************************************/
/*! exports provided: SchemesAndOffersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchemesAndOffersPage", function() { return SchemesAndOffersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SchemesAndOffersPage = class SchemesAndOffersPage {
    constructor() { }
    ngOnInit() {
    }
};
SchemesAndOffersPage.ctorParameters = () => [];
SchemesAndOffersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-schemes-and-offers',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./schemes-and-offers.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/schemes-and-offers/schemes-and-offers.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./schemes-and-offers.page.scss */ "./src/app/pages/schemes-and-offers/schemes-and-offers.page.scss")).default]
    })
], SchemesAndOffersPage);



/***/ })

}]);
//# sourceMappingURL=pages-schemes-and-offers-schemes-and-offers-module-es2015.js.map