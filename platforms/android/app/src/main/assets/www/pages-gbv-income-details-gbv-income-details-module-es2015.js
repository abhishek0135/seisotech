(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-gbv-income-details-gbv-income-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gbv-income-details/gbv-income-details.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gbv-income-details/gbv-income-details.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'GBV Income Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of gbvIncomeDetailsReport\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div><strong>{{report.legDirectorName}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Business</div>\n        <div>{{report.business}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">GBV Percentage</div>\n        <div>{{report.gbvPercentage}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">GBV Points</div>\n        <div>{{report.gbvPoints}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">GBV Points value</div>\n        <div>{{report.gbvPointValue}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">GBV Income</div>\n        <div>{{report.gbvIncome}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Capping</div>\n        <div>{{report.capping}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div class=\"text-secondary\">Lapsed points</div>\n        <div>{{report.lapsedPoints}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-row>\n    <ion-col size=\"6\" class=\"text-left\">\n      <div class=\"text-secondary\"><strong>GBV Income</strong></div>\n    </ion-col>\n    <ion-col size=\"6\" class=\"text-right\">\n      <div class=\"text-secondary\"><strong>{{124.25  | currency: appConstants.currencyCode: 'symbol': appConstants.currencyCodeValue}}</strong></div>\n    </ion-col>\n  </ion-row>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/gbv-income-details/gbv-income-details-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/gbv-income-details/gbv-income-details-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: GbvIncomeDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GbvIncomeDetailsPageRoutingModule", function() { return GbvIncomeDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _gbv_income_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gbv-income-details.page */ "./src/app/pages/gbv-income-details/gbv-income-details.page.ts");




const routes = [
    {
        path: '',
        component: _gbv_income_details_page__WEBPACK_IMPORTED_MODULE_3__["GbvIncomeDetailsPage"]
    }
];
let GbvIncomeDetailsPageRoutingModule = class GbvIncomeDetailsPageRoutingModule {
};
GbvIncomeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GbvIncomeDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/gbv-income-details/gbv-income-details.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/gbv-income-details/gbv-income-details.module.ts ***!
  \***********************************************************************/
/*! exports provided: GbvIncomeDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GbvIncomeDetailsPageModule", function() { return GbvIncomeDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _gbv_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gbv-income-details-routing.module */ "./src/app/pages/gbv-income-details/gbv-income-details-routing.module.ts");
/* harmony import */ var _gbv_income_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gbv-income-details.page */ "./src/app/pages/gbv-income-details/gbv-income-details.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let GbvIncomeDetailsPageModule = class GbvIncomeDetailsPageModule {
};
GbvIncomeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _gbv_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["GbvIncomeDetailsPageRoutingModule"]
        ],
        declarations: [_gbv_income_details_page__WEBPACK_IMPORTED_MODULE_6__["GbvIncomeDetailsPage"]]
    })
], GbvIncomeDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/gbv-income-details/gbv-income-details.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/gbv-income-details/gbv-income-details.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2didi1pbmNvbWUtZGV0YWlscy9nYnYtaW5jb21lLWRldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/gbv-income-details/gbv-income-details.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/gbv-income-details/gbv-income-details.page.ts ***!
  \*********************************************************************/
/*! exports provided: GbvIncomeDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GbvIncomeDetailsPage", function() { return GbvIncomeDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let GbvIncomeDetailsPage = class GbvIncomeDetailsPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.gbvIncomeDetailsReport = [];
        this.gbvIncomeDetailsReport = this.appData.getGBVIncomeDetails();
    }
    ngOnInit() {
    }
};
GbvIncomeDetailsPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
GbvIncomeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-gbv-income-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./gbv-income-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/gbv-income-details/gbv-income-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./gbv-income-details.page.scss */ "./src/app/pages/gbv-income-details/gbv-income-details.page.scss")).default]
    })
], GbvIncomeDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-gbv-income-details-gbv-income-details-module-es2015.js.map