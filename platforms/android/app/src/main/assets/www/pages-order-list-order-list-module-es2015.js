(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-order-list-order-list-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-list/order-list.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-list/order-list.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header\n  [titleText]=\"'Order Lists'\"\n  [menuButton]=\"true\"\n></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding order-list-page\">\n  <ion-searchbar\n    class=\"p-0 mb-2\"\n    placeholder=\"Search Order\"\n    inputmode=\"text\"\n    type=\"search\"\n  ></ion-searchbar>\n\n  <div class=\"text-center mb-2\"><small>Placed on Fri, 22 Jun 2020</small></div>\n  <div class=\"card order-item-card mb-2\">\n    <div class=\"align-items-center p-3\">\n      <!-- <img alt=\"img\" class=\"not-found-img\" src=\"assets/small/1.jpg\"> -->\n      <div class=\"overflow-hidden\">\n        <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">\n          <!-- <ion-icon name=\"cart-outline\"></ion-icon> -->\n          Order#: 43743\n          <span class=\"float-right\">$233.00</span>\n        </h6>\n        <!-- <div class=\"float-right text-truncate text-primary\">$233.00</div> -->\n        <div class=\"text-truncate text-primary\">\n          8:05 AM\n        </div>\n        <!-- <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" aria-label=\"cart outline\"></ion-icon>\n                    Order ID : GURDEEEP8343743\n                </div> -->\n      </div>\n    </div>\n    <div\n      class=\"d-flex align-items-center border-top border-bottom text-danger py-2 px-3\"\n    >\n      Order Cancelled\n      <ion-icon\n        class=\"ml-auto md hydrated\"\n        name=\"close-circle-outline\"\n        role=\"img\"\n        aria-label=\"close circle outline\"\n      ></ion-icon>\n    </div>\n    <div class=\"p-3\">\n      <div class=\"d-flex justify-content-between mb-1\">\n        <small class=\"text-secondary\">Original Payable amount</small>\n        <small class=\"text-secondary\">$233.00</small>\n      </div>\n      <div class=\"d-flex justify-content-between\">\n        <small class=\"text-gray\">Final paid amount</small>\n        <small class=\"text-gray\">$00.00</small>\n      </div>\n    </div>\n    <div class=\"align-items-center border-bottom\">\n      <ion-row class=\"ion-no-padding ion-text-center border-top\">\n        <ion-col\n          size=\"6\"\n          class=\"float-center ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderReturn()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>\n            RETURN\n          </strong>\n          <ion-icon\n            name=\"reload-outline\"\n            class=\"icon-transform btn-icon\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col\n          size=\"6\"\n          class=\"float-center border-right ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderDetails()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>VIEW DETAILS</strong>\n          <ion-icon name=\"chevron-forward-outline\" class=\"btn-icon\"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n  <div class=\"card order-item-card mb-2\">\n    <div class=\"align-items-center p-3\">\n      <!-- <img alt=\"img\" class=\"not-found-img\" src=\"assets/small/10.jpg\"> -->\n      <div class=\"overflow-hidden ml-2\">\n        <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">\n          ID : 3743\n          <span class=\"float-right\">$233.00</span>\n        </h6>\n        <div class=\"text-truncate text-primary\">\n          9:25 AM\n        </div>\n        <!-- <div class=\"text-truncate text-primary\">$533.00</div> -->\n        <!-- <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" aria-label=\"cart outline\"></ion-icon>\n                    Order ID : GURDEEEP8343743\n                </div> -->\n      </div>\n    </div>\n    <div\n      class=\"d-flex align-items-center py-2 px-3 border-top border-bottom text-success\"\n    >\n      Order successfully\n      <ion-icon\n        class=\"ml-auto md hydrated\"\n        name=\"checkmark-circle-outline\"\n      ></ion-icon>\n    </div>\n    <div class=\"p-3\">\n      <div class=\"d-flex justify-content-between mb-1\">\n        <small class=\"text-secondary\">Original Payable amount</small>\n        <small class=\"text-secondary\">$533.00</small>\n      </div>\n      <div class=\"d-flex justify-content-between mb-1\">\n        <small class=\"text-secondary\">Delivery charges</small>\n        <small class=\"text-secondary\">$510.00</small>\n      </div>\n      <div class=\"d-flex justify-content-between\">\n        <small class=\"text-gray\">Final paid amount</small>\n        <small class=\"text-gray\">$644.00</small>\n      </div>\n    </div>\n    <div class=\"align-items-center border-bottom\">\n      <ion-row class=\"ion-no-padding ion-text-center border-top\">\n        <ion-col\n          size=\"6\"\n          class=\"float-center ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderReturn()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>\n            RETURN\n          </strong>\n          <ion-icon\n            name=\"reload-outline\"\n            class=\"icon-transform btn-icon\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col\n          size=\"6\"\n          class=\"float-center border-right ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderDetails()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>VIEW DETAILS</strong>\n          <ion-icon name=\"chevron-forward-outline\" class=\"btn-icon\"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n  <div class=\"card order-item-card mb-2\">\n    <div class=\"align-items-center p-3\">\n      <!-- <img alt=\"img\" class=\"not-found-img\" src=\"assets/small/3.jpg\"> -->\n      <div class=\"overflow-hidden ml-2\">\n        <h6 class=\"font-weight-bold text-dark mb-0 text-truncate\">\n          ID : 3743\n          <span class=\"float-right\">$233.00</span>\n        </h6>\n        <div class=\"text-truncate text-primary\">\n          10:10 AM\n        </div>\n        <!-- <div class=\"text-truncate text-primary\">$233.00</div> -->\n        <!-- <div class=\"small text-gray-500\">\n                    <ion-icon name=\"cart-outline\" role=\"img\" aria-label=\"cart outline\"></ion-icon>\n                    Order ID : GURDEEEP8343743\n                </div> -->\n      </div>\n    </div>\n    <div\n      class=\"d-flex align-items-center py-2 px-3 border-top border-bottom text-danger\"\n    >\n      Order Cancelled\n      <ion-icon\n        class=\"ml-auto md hydrated\"\n        name=\"close-circle-outline\"\n        role=\"img\"\n        aria-label=\"close circle outline\"\n      ></ion-icon>\n    </div>\n    <div class=\"p-3\">\n      <div class=\"d-flex justify-content-between mb-1\">\n        <small class=\"text-secondary\">Original Payable amount</small>\n        <small class=\"text-secondary\">$233.00</small>\n      </div>\n      <div class=\"d-flex justify-content-between\">\n        <small class=\"text-gray\">Final paid amount</small>\n        <small class=\"text-gray\">$00.00</small>\n      </div>\n    </div>\n    <div class=\"align-items-center border-bottom\">\n      <ion-row class=\"ion-no-padding ion-text-center border-top\">\n        <ion-col\n          size=\"6\"\n          class=\"float-center ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderReturn()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>\n            RETURN\n          </strong>\n          <ion-icon\n            name=\"reload-outline\"\n            class=\"icon-transform btn-icon\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col\n          size=\"6\"\n          class=\"float-center border-right ion-activatable ripple-parent\"\n          tappable\n          (click)=\"orderReturn()\"\n        >\n          <ion-ripple-effect></ion-ripple-effect>\n          <strong>VIEW DETAILS</strong>\n          <ion-icon name=\"chevron-forward-outline\" class=\"btn-icon\"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/order-list/order-list-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/order-list/order-list-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: OrderListPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListPageRoutingModule", function() { return OrderListPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-list.page */ "./src/app/pages/order-list/order-list.page.ts");




const routes = [
    {
        path: '',
        component: _order_list_page__WEBPACK_IMPORTED_MODULE_3__["OrderListPage"]
    }
];
let OrderListPageRoutingModule = class OrderListPageRoutingModule {
};
OrderListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderListPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/order-list/order-list.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/order-list/order-list.module.ts ***!
  \*******************************************************/
/*! exports provided: OrderListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListPageModule", function() { return OrderListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-list-routing.module */ "./src/app/pages/order-list/order-list-routing.module.ts");
/* harmony import */ var _order_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-list.page */ "./src/app/pages/order-list/order-list.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let OrderListPageModule = class OrderListPageModule {
};
OrderListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _order_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderListPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_order_list_page__WEBPACK_IMPORTED_MODULE_6__["OrderListPage"]]
    })
], OrderListPageModule);



/***/ }),

/***/ "./src/app/pages/order-list/order-list.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/order-list/order-list.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".not-found-img {\n  width: 60px;\n  height: 60px;\n  border: 1px solid #f4f5f8;\n  border-radius: 3px;\n}\n\n.icon-transform {\n  transform: scaleX(-1);\n}\n\n.btn-icon {\n  font-size: 10px;\n  margin-left: 5px;\n}\n\n.border-right {\n  border-right: 1px solid #e8e2e2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb3JkZXItbGlzdC9vcmRlci1saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7QUFFSjs7QUFBQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQUdKOztBQURBO0VBQ0ksK0JBQUE7QUFJSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29yZGVyLWxpc3Qvb3JkZXItbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubm90LWZvdW5kLWltZyB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmNGY1Zjg7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuLmljb24tdHJhbnNmb3JtIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlWCgtMSk7XG59XG4uYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLmJvcmRlci1yaWdodCB7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2U4ZTJlMjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/order-list/order-list.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/order-list/order-list.page.ts ***!
  \*****************************************************/
/*! exports provided: OrderListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListPage", function() { return OrderListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let OrderListPage = class OrderListPage {
    constructor(navCltr) {
        this.navCltr = navCltr;
    }
    ngOnInit() {
    }
    orderDetails() {
        this.navCltr.navigateForward('order-details');
    }
    orderReturn() {
        this.navCltr.navigateForward('order-return');
    }
};
OrderListPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
OrderListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-list.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/order-list/order-list.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-list.page.scss */ "./src/app/pages/order-list/order-list.page.scss")).default]
    })
], OrderListPage);



/***/ })

}]);
//# sourceMappingURL=pages-order-list-order-list-module-es2015.js.map