(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-monthly-scheme-details-monthly-scheme-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.html":
    /*!*********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.html ***!
      \*********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMonthlySchemeDetailsMonthlySchemeDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'Monthly Scheme Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of monthlySchemeDetails\">\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <strong>{{report.name}}</strong>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <strong>{{ report.id}}</strong>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Direct Id</div>\n        <div>{{ report.directId}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Direct Name</div>\n        <div>{{ report.directName}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Transaction date</div>\n        <div>{{ report.transactionDate}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Group BV</div>\n        <div>{{ report.groupBV}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-details/monthly-scheme-details-routing.module.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-details/monthly-scheme-details-routing.module.ts ***!
      \***************************************************************************************/

    /*! exports provided: MonthlySchemeDetailsPageRoutingModule */

    /***/
    function srcAppPagesMonthlySchemeDetailsMonthlySchemeDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MonthlySchemeDetailsPageRoutingModule", function () {
        return MonthlySchemeDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _monthly_scheme_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./monthly-scheme-details.page */
      "./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.ts");

      var routes = [{
        path: '',
        component: _monthly_scheme_details_page__WEBPACK_IMPORTED_MODULE_3__["monthlySchemeDetailsPage"]
      }];

      var MonthlySchemeDetailsPageRoutingModule = function MonthlySchemeDetailsPageRoutingModule() {
        _classCallCheck(this, MonthlySchemeDetailsPageRoutingModule);
      };

      MonthlySchemeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MonthlySchemeDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-details/monthly-scheme-details.module.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-details/monthly-scheme-details.module.ts ***!
      \*******************************************************************************/

    /*! exports provided: MonthlySchemeDetailsPageModule */

    /***/
    function srcAppPagesMonthlySchemeDetailsMonthlySchemeDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MonthlySchemeDetailsPageModule", function () {
        return MonthlySchemeDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _monthly_scheme_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./monthly-scheme-details-routing.module */
      "./src/app/pages/monthly-scheme-details/monthly-scheme-details-routing.module.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");
      /* harmony import */


      var _monthly_scheme_details_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./monthly-scheme-details.page */
      "./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.ts");

      var MonthlySchemeDetailsPageModule = function MonthlySchemeDetailsPageModule() {
        _classCallCheck(this, MonthlySchemeDetailsPageModule);
      };

      MonthlySchemeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_6__["ComponentModule"], _monthly_scheme_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["MonthlySchemeDetailsPageRoutingModule"]],
        declarations: [_monthly_scheme_details_page__WEBPACK_IMPORTED_MODULE_7__["monthlySchemeDetailsPage"]]
      })], MonthlySchemeDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMonthlySchemeDetailsMonthlySchemeDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vbnRobHktc2NoZW1lLWRldGFpbHMvbW9udGhseS1zY2hlbWUtZGV0YWlscy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.ts ***!
      \*****************************************************************************/

    /*! exports provided: monthlySchemeDetailsPage */

    /***/
    function srcAppPagesMonthlySchemeDetailsMonthlySchemeDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "monthlySchemeDetailsPage", function () {
        return monthlySchemeDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var monthlySchemeDetailsPage = /*#__PURE__*/function () {
        function monthlySchemeDetailsPage(AppData, appConstants) {
          _classCallCheck(this, monthlySchemeDetailsPage);

          this.AppData = AppData;
          this.appConstants = appConstants;
          this.monthlySchemeDetails = [];
          this.monthlySchemeDetails = this.AppData.getMonthlySchemeDetails();
        }

        _createClass(monthlySchemeDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return monthlySchemeDetailsPage;
      }();

      monthlySchemeDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      monthlySchemeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-monthly-scheme-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./monthly-scheme-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./monthly-scheme-details.page.scss */
        "./src/app/pages/monthly-scheme-details/monthly-scheme-details.page.scss"))["default"]]
      })], monthlySchemeDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-monthly-scheme-details-monthly-scheme-details-module-es5.js.map