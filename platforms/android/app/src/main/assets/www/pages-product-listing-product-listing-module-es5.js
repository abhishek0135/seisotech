(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-product-listing-product-listing-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-listing/product-listing.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-listing/product-listing.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesProductListingProductListingPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <app-app-header\n  [titleText]=\"'Product Listing'\"\n  [backButton]=\"true\"\n  [toShowCartButton]=\"true\">\n<ion-row>\n  <ion-col [size]=\"6\" style=\"display: flex;flex-direction: row;\n  justify-content: center;align-items: center;\">\n    <ion-icon style=\"color: #FFFFFF;font-size: 28px;\" name=\"filter-outline\"></ion-icon>\n    <ion-text style=\"font-size: 16px;\n    line-height: 18px;\n    padding: 0 6px;color: #FFFFFF;\">Sort</ion-text>\n  </ion-col>\n  <ion-col [size]=\"6\" style=\"display: flex;flex-direction: row;\n  justify-content: center;align-items: center;\">\n    <ion-icon style=\"color: #FFFFFF;font-size: 28px;\" name=\"funnel-outline\"></ion-icon>\n    <ion-text style=\"font-size: 16px;\n    line-height: 18px;\n    padding: 0 6px;color: #FFFFFF;\">Filter</ion-text>\n  </ion-col>\n</ion-row>\n</app-app-header> -->\n<ion-header>\n  <ion-toolbar>\n    <ion-row>\n      <ion-col [size]=\"2\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;\">\n        <ion-icon (click)=\"back()\" style=\"color: #0f9fd7;font-size: 28px;\" name=\"arrow-back-outline\"></ion-icon>\n      </ion-col>\n      <ion-col [size]=\"8\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;\">\n        <ion-text style=\"font-size: 16px;\n        line-height: 18px;\n        padding: 0 6px;color: #FFFFFF;\">Product Listing</ion-text>\n      </ion-col>\n      <ion-col [size]=\"2\" style=\"display: flex;flex-direction: row;\n      justify-content: center;align-items: center;\">\n        <ion-icon style=\"color: #FFFFFF;font-size: 28px;\" name=\"cart-outline\"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n  <ion-row>\n    <ion-col (click)=\"presentActionSheet()\" [size]=\"6\" style=\"display: flex;flex-direction: row;\n    justify-content: center;align-items: center;border-right: 2px solid lightgrey;\">\n      <ion-icon style=\"font-size: 28px;\" name=\"filter-outline\"></ion-icon>\n      <ion-text style=\"font-size: 16px;\n      line-height: 18px;\n      padding: 0 6px;\">Sort</ion-text>\n    </ion-col>\n    <ion-col [size]=\"6\" style=\"display: flex;flex-direction: row;\n    justify-content: center;align-items: center;\" (click)=\"presentModal()\">\n      <ion-icon style=\"font-size: 28px;\" name=\"funnel-outline\"></ion-icon>\n      <ion-text style=\"font-size: 16px;\n      line-height: 18px;\n      padding: 0 6px;\">Filter</ion-text>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content\n  color=\"light\"\n  class=\"ion-padding order-list-page ion-color ion-color-light\"\n>\n  <ion-searchbar\n    class=\"p-0 mb-2\"\n    placeholder=\"Search Favorites\"\n    inputmode=\"text\"\n    type=\"search\"\n  ></ion-searchbar>\n  <!-- <div class=\"text-center mb-2\">\n        <small> Placed on Fri, 22 Jun, 8 PM - 11 PM </small>\n    </div> -->\n    \n  <img class=\"rounded shadow-sm mb-2\" src=\"assets/shop.jpg\" />\n  <!-- <div *ngIf=\"!data\" class=\"rounded shadow-sm mb-2\">\n    <ion-skeleton-text animated></ion-skeleton-text>\n  </div> -->\n\n  <div  *ngIf=\"data\">\n    <div\n    class=\"d-flex p-3 shop-cart-item bg-white mb-2\"\n    *ngFor=\"let product of products\"\n    (click)=\"productDetails(product)\">\n\n    <div class=\"shop-cart-left\">\n      <img\n        class=\"not-found-img\"\n        [src]=\"product.img\"\n        onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n      />\n    </div>\n    <div class=\"shop-cart-right\">\n      <h6>{{product.name}}</h6>\n      <h6 class=\"font-weight-bold text-dark mb-2 price\">\n        {{ product.dPrice | currency : 'INR' }}\n        <span\n          class=\"regular-price text-secondary font-weight-normal strick-out\"\n        >\n          {{ product.price | currency : 'INR' }}\n        </span>\n      </h6>\n      <ion-button\n        [disabled]=\"product.addToCartInProgress\"\n        class=\"add-to-cart-btn\"\n        (click)=\"addToCart($event, product)\"\n      >\n        <ion-icon\n          slot=\"start\"\n          name=\"cart-outline\"\n          *ngIf=\"!product.addToCartInProgress\"\n        ></ion-icon>\n        <!-- <img src=\"../../../assets/imgs/addtocart.png\" /> -->\n        <ion-note *ngIf=\"!product.addToCartInProgress\">Add to cart</ion-note>\n        <app-button-spinner\n          *ngIf=\"product.addToCartInProgress\"\n        ></app-button-spinner>\n      </ion-button>\n      <!-- <div class=\"mb-2\" [innerHTML]=\"product.product_description\"></div> -->\n    </div>\n  </div>\n  </div>\n \n\n\n  <div  *ngIf=\"!data\">\n    <div\n    class=\"d-flex p-3 shop-cart-item bg-white mb-2\"\n    *ngFor=\"let product of products\"\n    (click)=\"productDetails(product)\">\n\n    <div class=\"shop-cart-left\">\n      <ion-thumbnail style=\"height: 75px;\n      width: 75px;\">\n        <ion-skeleton-text animated></ion-skeleton-text>\n      </ion-thumbnail>\n      <!-- <img\n        class=\"not-found-img\"\n        [src]=\"product.img\"\n        onerror=\"this.onerror=null;this.src='../../../assets/imgs/default-product.png';\"\n      /> -->\n    </div>\n    <div class=\"shop-cart-right\">\n      <h6> <ion-skeleton-text animated></ion-skeleton-text></h6>\n      <h6 class=\"font-weight-bold text-dark mb-2 price\">\n        <ion-skeleton-text animated></ion-skeleton-text>\n        <span\n          class=\"regular-price text-secondary font-weight-normal strick-out\"\n        >\n        <ion-skeleton-text animated></ion-skeleton-text>\n        </span>\n      </h6>\n      <ion-button\n        [disabled]=\"product.addToCartInProgress\"\n        class=\"add-to-cart-btn\"\n      >\n        <ion-icon\n          slot=\"start\"\n          name=\"cart-outline\"\n          *ngIf=\"!product.addToCartInProgress\"\n        ></ion-icon>\n        <!-- <img src=\"../../../assets/imgs/addtocart.png\" /> -->\n        <ion-note *ngIf=\"!product.addToCartInProgress\">Add to cart</ion-note>\n        <!-- <app-button-spinner\n          *ngIf=\"product.addToCartInProgress\"\n        ></app-button-spinner> -->\n      </ion-button>\n      <!-- <div class=\"mb-2\" [innerHTML]=\"product.product_description\"></div> -->\n    </div>\n  </div>\n  </div>\n\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/product-listing/product-listing-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/product-listing/product-listing-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: ProductListingPageRoutingModule */

    /***/
    function srcAppPagesProductListingProductListingRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPageRoutingModule", function () {
        return ProductListingPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _product_listing_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./product-listing.page */
      "./src/app/pages/product-listing/product-listing.page.ts");

      var routes = [{
        path: '',
        component: _product_listing_page__WEBPACK_IMPORTED_MODULE_3__["ProductListingPage"]
      }];

      var ProductListingPageRoutingModule = function ProductListingPageRoutingModule() {
        _classCallCheck(this, ProductListingPageRoutingModule);
      };

      ProductListingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProductListingPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/product-listing/product-listing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/product-listing/product-listing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: ProductListingPageModule */

    /***/
    function srcAppPagesProductListingProductListingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPageModule", function () {
        return ProductListingPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _product_listing_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./product-listing-routing.module */
      "./src/app/pages/product-listing/product-listing-routing.module.ts");
      /* harmony import */


      var _product_listing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./product-listing.page */
      "./src/app/pages/product-listing/product-listing.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var ProductListingPageModule = function ProductListingPageModule() {
        _classCallCheck(this, ProductListingPageModule);
      };

      ProductListingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _product_listing_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductListingPageRoutingModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]],
        declarations: [_product_listing_page__WEBPACK_IMPORTED_MODULE_6__["ProductListingPage"]]
      })], ProductListingPageModule);
      /***/
    },

    /***/
    "./src/app/pages/product-listing/product-listing.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/product-listing/product-listing.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesProductListingProductListingPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".add-to-cart-btn {\n  --background: #00ab00;\n  color: white;\n}\n.add-to-cart-btn ion-icon {\n  font-size: 28px;\n}\n.add-to-cart-btn ion-note {\n  color: white;\n}\nion-toolbar {\n  --background: #003049 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZHVjdC1saXN0aW5nL3Byb2R1Y3QtbGlzdGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtFQUNBLFlBQUE7QUFDRjtBQUFFO0VBQ0UsZUFBQTtBQUVKO0FBQUU7RUFDRSxZQUFBO0FBRUo7QUFDQTtFQUNFLGdDQUFBO0FBRUYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcm9kdWN0LWxpc3RpbmcvcHJvZHVjdC1saXN0aW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZGQtdG8tY2FydC1idG4ge1xuICAtLWJhY2tncm91bmQ6ICMwMGFiMDA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgfVxuICBpb24tbm90ZSB7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG59XG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogIzAwMzA0OSAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/product-listing/product-listing.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/product-listing/product-listing.page.ts ***!
      \***************************************************************/

    /*! exports provided: ProductListingPage */

    /***/
    function srcAppPagesProductListingProductListingPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductListingPage", function () {
        return ProductListingPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/app-api.service */
      "./src/app/services/app-api.service.ts");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/utility/app.variables */
      "./src/app/utility/app.variables.ts");
      /* harmony import */


      var src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/utility/common.utility */
      "./src/app/utility/common.utility.ts");
      /* harmony import */


      var _utility_app_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../utility/app.data */
      "./src/app/utility/app.data.ts");
      /* harmony import */


      var _pages_filtermodal_filtermodal_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../pages/filtermodal/filtermodal.page */
      "./src/app/pages/filtermodal/filtermodal.page.ts");

      var ProductListingPage = /*#__PURE__*/function () {
        function ProductListingPage(navCltr, appData, appApiService, router, values, commonUtility, modalController, actionSheetController) {
          _classCallCheck(this, ProductListingPage);

          this.navCltr = navCltr;
          this.appData = appData;
          this.appApiService = appApiService;
          this.router = router;
          this.values = values;
          this.commonUtility = commonUtility;
          this.modalController = modalController;
          this.actionSheetController = actionSheetController;
          this.categoryId = '';
          this.products = [];
          this.emptyProduct = [1, 1, 1, 1, 1, 1, 1, 1];
          this.isDataLoaded = false;
          this.data = false;

          if (this.router.getCurrentNavigation().extras.state) {
            this.categoryId = this.router.getCurrentNavigation().extras.state.categoryId;
          }
        }

        _createClass(ProductListingPage, [{
          key: "getProducts",
          value: function getProducts() {
            var _this = this;

            this.appApiService.getProducts('', '', this.categoryId, '').subscribe(function (respData) {
              _this.isDataLoaded = true;
              _this.data = true;
              _this.products = respData.ProductWise_List;
              console.log(_this.products);
            }, function (err) {
              _this.isDataLoaded = true;
              _this.data = true;
              _this.products = _this.appData.getProducts();
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getProducts();
          }
        }, {
          key: "productDetails",
          value: function productDetails(product) {
            var navigationExtras = {
              state: {
                product: JSON.stringify(product)
              }
            };
            this.navCltr.navigateForward(['product-details'], navigationExtras);
          }
        }, {
          key: "addToCart",
          value: function addToCart(event, product) {
            var _this2 = this;

            debugger;
            event.stopPropagation();
            product.addToCartInProgress = true;
            this.appApiService.addProductToCart(product.product_id).subscribe(function (respData) {
              product.addToCartInProgress = false;
              _this2.values.cartCount++;

              _this2.commonUtility.setLocalstorage(src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].localstorageKeys.cartCount, _this2.values.cartCount.toString());

              _this2.commonUtility.showSuccessToast('Product added to cart');
            }, function (err) {
              product.addToCartInProgress = false;
            });
          }
        }, {
          key: "presentModal",
          value: function presentModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal, _yield$modal$onWillDi, data;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _pages_filtermodal_filtermodal_page__WEBPACK_IMPORTED_MODULE_9__["FiltermodalPage"],
                        cssClass: 'filterModal'
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      _context.next = 7;
                      return modal.onWillDismiss();

                    case 7:
                      _yield$modal$onWillDi = _context.sent;
                      data = _yield$modal$onWillDi.data;

                      if (data) {
                        console.log(data);
                        this.products = [];
                        this.products = data;
                      } else {
                        console.log("goes in else");
                      }

                    case 10:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentActionSheet",
          value: function presentActionSheet() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this3 = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.actionSheetController.create({
                        buttons: [{
                          text: 'A-Z',
                          handler: function handler() {
                            function dynamicSort(property) {
                              var sortOrder = 1;

                              if (property[0] === "-") {
                                sortOrder = -1;
                                property = property.substr(1);
                              }

                              return function (a, b) {
                                if (sortOrder == -1) {
                                  return b[property].localeCompare(a[property]);
                                } else {
                                  return a[property].localeCompare(b[property]);
                                }
                              };
                            }

                            _this3.products.sort(dynamicSort("name"));

                            console.log(_this3.products);
                          }
                        }, {
                          text: 'Z-A',
                          handler: function handler() {
                            function dynamicSort(property) {
                              var sortOrder = 1;

                              if (property[0] === "-") {
                                sortOrder = -1;
                                property = property.substr(1);
                              }

                              return function (a, b) {
                                if (sortOrder == -1) {
                                  return b[property].localeCompare(a[property]);
                                } else {
                                  return a[property].localeCompare(b[property]);
                                }
                              };
                            }

                            _this3.products.sort(dynamicSort("-name"));

                            console.log(_this3.products);
                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      actionSheet = _context2.sent;
                      _context2.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "back",
          value: function back() {
            this.router.navigateByUrl('home');
          }
        }]);

        return ProductListingPage;
      }();

      ProductListingPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: _utility_app_data__WEBPACK_IMPORTED_MODULE_8__["AppData"]
        }, {
          type: src_app_services_app_api_service__WEBPACK_IMPORTED_MODULE_4__["AppApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: src_app_utility_app_variables__WEBPACK_IMPORTED_MODULE_6__["Values"]
        }, {
          type: src_app_utility_common_utility__WEBPACK_IMPORTED_MODULE_7__["CommonUtility"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"]
        }];
      };

      ProductListingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-listing',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-listing.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/product-listing/product-listing.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-listing.page.scss */
        "./src/app/pages/product-listing/product-listing.page.scss"))["default"]]
      })], ProductListingPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-product-listing-product-listing-module-es5.js.map