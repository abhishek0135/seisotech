(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-db-income-details-db-income-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-income-details/db-income-details.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-income-details/db-income-details.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesDbIncomeDetailsDbIncomeDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-app-header [titleText]=\"'DB Income Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of dBIncomeDetailsReport\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <!-- <div class=\"text-secondary\">Name</div> -->\n        <div><strong>{{report.name}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">ID</div>\n        <div>{{report.id}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Level NUmber</div>\n        <div>{{report.levelNumber}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Group BV</div>\n        <div>{{report.groupBV}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Points</div>\n        <div>{{report.points}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Point value</div>\n        <div>{{report.pointValue}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Total Income</div>\n        <div>{{report.totalIncome}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-row>\n    <ion-col size=\"6\" class=\"text-left\">\n      <div class=\"text-secondary\"><strong>DB Income</strong></div>\n    </ion-col>\n    <ion-col size=\"6\" class=\"text-right\">\n      <div class=\"text-secondary\"><strong>{{124.25  | currency: appConstants.currencyCode: 'symbol': appConstants.currencyCodeValue}}</strong></div>\n    </ion-col>\n  </ion-row>\n</ion-footer>";
      /***/
    },

    /***/
    "./src/app/pages/db-income-details/db-income-details-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/db-income-details/db-income-details-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: DbIncomeDetailsPageRoutingModule */

    /***/
    function srcAppPagesDbIncomeDetailsDbIncomeDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DbIncomeDetailsPageRoutingModule", function () {
        return DbIncomeDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _db_income_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./db-income-details.page */
      "./src/app/pages/db-income-details/db-income-details.page.ts");

      var routes = [{
        path: '',
        component: _db_income_details_page__WEBPACK_IMPORTED_MODULE_3__["DbIncomeDetailsPage"]
      }];

      var DbIncomeDetailsPageRoutingModule = function DbIncomeDetailsPageRoutingModule() {
        _classCallCheck(this, DbIncomeDetailsPageRoutingModule);
      };

      DbIncomeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DbIncomeDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/db-income-details/db-income-details.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/db-income-details/db-income-details.module.ts ***!
      \*********************************************************************/

    /*! exports provided: DbIncomeDetailsPageModule */

    /***/
    function srcAppPagesDbIncomeDetailsDbIncomeDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DbIncomeDetailsPageModule", function () {
        return DbIncomeDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _db_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./db-income-details-routing.module */
      "./src/app/pages/db-income-details/db-income-details-routing.module.ts");
      /* harmony import */


      var _db_income_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./db-income-details.page */
      "./src/app/pages/db-income-details/db-income-details.page.ts");
      /* harmony import */


      var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/components/component.module */
      "./src/app/components/component.module.ts");

      var DbIncomeDetailsPageModule = function DbIncomeDetailsPageModule() {
        _classCallCheck(this, DbIncomeDetailsPageModule);
      };

      DbIncomeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"], _db_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["DbIncomeDetailsPageRoutingModule"]],
        declarations: [_db_income_details_page__WEBPACK_IMPORTED_MODULE_6__["DbIncomeDetailsPage"]]
      })], DbIncomeDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/db-income-details/db-income-details.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/db-income-details/db-income-details.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesDbIncomeDetailsDbIncomeDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RiLWluY29tZS1kZXRhaWxzL2RiLWluY29tZS1kZXRhaWxzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/pages/db-income-details/db-income-details.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/db-income-details/db-income-details.page.ts ***!
      \*******************************************************************/

    /*! exports provided: DbIncomeDetailsPage */

    /***/
    function srcAppPagesDbIncomeDetailsDbIncomeDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DbIncomeDetailsPage", function () {
        return DbIncomeDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/utility/app.constants */
      "./src/app/utility/app.constants.ts");
      /* harmony import */


      var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/utility/app.data */
      "./src/app/utility/app.data.ts");

      var DbIncomeDetailsPage = /*#__PURE__*/function () {
        function DbIncomeDetailsPage(appData, appConstants) {
          _classCallCheck(this, DbIncomeDetailsPage);

          this.appData = appData;
          this.appConstants = appConstants;
          this.dBIncomeDetailsReport = [];
          this.dBIncomeDetailsReport = this.appData.getDBIncomeDetails();
        }

        _createClass(DbIncomeDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DbIncomeDetailsPage;
      }();

      DbIncomeDetailsPage.ctorParameters = function () {
        return [{
          type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"]
        }, {
          type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"]
        }];
      };

      DbIncomeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-db-income-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./db-income-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/db-income-details/db-income-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./db-income-details.page.scss */
        "./src/app/pages/db-income-details/db-income-details.page.scss"))["default"]]
      })], DbIncomeDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-db-income-details-db-income-details-module-es5.js.map