(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-commission-ledger-commission-ledger-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-ledger/commission-ledger.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-ledger/commission-ledger.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Commission Ledger'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n\n\n    <div class=\"card mt15 mb-2\" *ngFor=\"let commissionLedger of commissionLedgerList\" (click)=\"commissionVoucher()\">\n        <ion-row>\n            <ion-col size=\"6\" class=\"text-left\">\n                <strong>Payout No #{{commissionLedger.payoutNo}}</strong>\n            </ion-col>\n            <ion-col size=\"6\" class=\"text-right\">\n                <strong>{{commissionLedger.amount  | currency: appConstants.currencyCode: 'symbol': appConstants.currencyCodeValue}}</strong>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"9\" class=\"text-left\">\n                <div class=\"text-secondary\">Date</div>\n                <div>{{commissionLedger.date}}</div>\n            </ion-col>\n            <ion-col size=\"3\" class=\"text-right\">\n                    <ion-icon name=\"chevron-forward-outline\" class=\"next-icon\"></ion-icon>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"text-secondary\">Remark</div>\n                <div>{{commissionLedger.remark}}</div>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/commission-ledger/commission-ledger-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/commission-ledger/commission-ledger-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: CommissionLedgerPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionLedgerPageRoutingModule", function() { return CommissionLedgerPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _commission_ledger_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./commission-ledger.page */ "./src/app/pages/commission-ledger/commission-ledger.page.ts");




const routes = [
    {
        path: '',
        component: _commission_ledger_page__WEBPACK_IMPORTED_MODULE_3__["CommissionLedgerPage"]
    }
];
let CommissionLedgerPageRoutingModule = class CommissionLedgerPageRoutingModule {
};
CommissionLedgerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CommissionLedgerPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/commission-ledger/commission-ledger.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/commission-ledger/commission-ledger.module.ts ***!
  \*********************************************************************/
/*! exports provided: CommissionLedgerPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionLedgerPageModule", function() { return CommissionLedgerPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _commission_ledger_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./commission-ledger-routing.module */ "./src/app/pages/commission-ledger/commission-ledger-routing.module.ts");
/* harmony import */ var _commission_ledger_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./commission-ledger.page */ "./src/app/pages/commission-ledger/commission-ledger.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let CommissionLedgerPageModule = class CommissionLedgerPageModule {
};
CommissionLedgerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _commission_ledger_routing_module__WEBPACK_IMPORTED_MODULE_5__["CommissionLedgerPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_commission_ledger_page__WEBPACK_IMPORTED_MODULE_6__["CommissionLedgerPage"]]
    })
], CommissionLedgerPageModule);



/***/ }),

/***/ "./src/app/pages/commission-ledger/commission-ledger.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/commission-ledger/commission-ledger.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".next-icon {\n  font-size: 1.2rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29tbWlzc2lvbi1sZWRnZXIvY29tbWlzc2lvbi1sZWRnZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbW1pc3Npb24tbGVkZ2VyL2NvbW1pc3Npb24tbGVkZ2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXh0LWljb24ge1xuICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/commission-ledger/commission-ledger.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/commission-ledger/commission-ledger.page.ts ***!
  \*******************************************************************/
/*! exports provided: CommissionLedgerPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommissionLedgerPage", function() { return CommissionLedgerPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");





let CommissionLedgerPage = class CommissionLedgerPage {
    constructor(navController, AppData, appConstants) {
        this.navController = navController;
        this.AppData = AppData;
        this.appConstants = appConstants;
        this.commissionLedgerList = [];
        this.commissionLedgerList = this.AppData.getCommissionLedger();
    }
    ngOnInit() {
    }
    commissionVoucher() {
        this.navController.navigateForward(['commission-voucher']);
    }
};
CommissionLedgerPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_4__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"] }
];
CommissionLedgerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-commission-ledger',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./commission-ledger.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/commission-ledger/commission-ledger.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./commission-ledger.page.scss */ "./src/app/pages/commission-ledger/commission-ledger.page.scss")).default]
    })
], CommissionLedgerPage);



/***/ })

}]);
//# sourceMappingURL=pages-commission-ledger-commission-ledger-module-es2015.js.map