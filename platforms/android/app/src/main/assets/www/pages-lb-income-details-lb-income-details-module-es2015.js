(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-lb-income-details-lb-income-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/lb-income-details/lb-income-details.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/lb-income-details/lb-income-details.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'LB Income Details'\" [menuButton]=\"true\"></app-app-header>\n<ion-content color=\"light\" class=\"ion-content-padding-10px\">\n  <div class=\"card mt15 mb-2\" *ngFor=\"let report of LBIncomeDetailsReport\">\n    <ion-row>\n      <ion-col size=\"12\" class=\"text-left\">\n        <div class=\"text-secondary\">Leg Director Name</div>\n        <div><strong>{{report.legDirectName}}</strong></div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">Leg No</div>\n        <div>{{report.legNo}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">Business</div>\n        <div>{{report.business}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">LB Percentage</div>\n        <div>{{report.lbPercentage}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">LB Points</div>\n        <div>{{report.lbpoints}}</div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\" class=\"text-left\">\n        <div class=\"text-secondary\">LB Point Value</div>\n        <div>{{report.lbpointValue}}</div>\n      </ion-col>\n      <ion-col size=\"6\" class=\"text-right\">\n        <div class=\"text-secondary\">LB Income</div>\n        <div>{{report.lbIncome}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border footer-background\">\n  <ion-row>\n    <ion-col size=\"6\" class=\"text-left\">\n      <div class=\"text-secondary\"><strong>LB Income</strong></div>\n    </ion-col>\n    <ion-col size=\"6\" class=\"text-right\">\n      <div class=\"text-secondary\"><strong>{{124.25  | currency: appConstants.currencyCode: 'symbol': appConstants.currencyCodeValue}}</strong></div>\n    </ion-col>\n  </ion-row>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/lb-income-details/lb-income-details-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/lb-income-details/lb-income-details-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: LbIncomeDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LbIncomeDetailsPageRoutingModule", function() { return LbIncomeDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _lb_income_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lb-income-details.page */ "./src/app/pages/lb-income-details/lb-income-details.page.ts");




const routes = [
    {
        path: '',
        component: _lb_income_details_page__WEBPACK_IMPORTED_MODULE_3__["LbIncomeDetailsPage"]
    }
];
let LbIncomeDetailsPageRoutingModule = class LbIncomeDetailsPageRoutingModule {
};
LbIncomeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LbIncomeDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/lb-income-details/lb-income-details.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/lb-income-details/lb-income-details.module.ts ***!
  \*********************************************************************/
/*! exports provided: LbIncomeDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LbIncomeDetailsPageModule", function() { return LbIncomeDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _lb_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./lb-income-details-routing.module */ "./src/app/pages/lb-income-details/lb-income-details-routing.module.ts");
/* harmony import */ var _lb_income_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lb-income-details.page */ "./src/app/pages/lb-income-details/lb-income-details.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let LbIncomeDetailsPageModule = class LbIncomeDetailsPageModule {
};
LbIncomeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"],
            _lb_income_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["LbIncomeDetailsPageRoutingModule"]
        ],
        declarations: [_lb_income_details_page__WEBPACK_IMPORTED_MODULE_6__["LbIncomeDetailsPage"]]
    })
], LbIncomeDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/lb-income-details/lb-income-details.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/lb-income-details/lb-income-details.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xiLWluY29tZS1kZXRhaWxzL2xiLWluY29tZS1kZXRhaWxzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/lb-income-details/lb-income-details.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/lb-income-details/lb-income-details.page.ts ***!
  \*******************************************************************/
/*! exports provided: LbIncomeDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LbIncomeDetailsPage", function() { return LbIncomeDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/utility/app.constants */ "./src/app/utility/app.constants.ts");
/* harmony import */ var src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/utility/app.data */ "./src/app/utility/app.data.ts");




let LbIncomeDetailsPage = class LbIncomeDetailsPage {
    constructor(appData, appConstants) {
        this.appData = appData;
        this.appConstants = appConstants;
        this.LBIncomeDetailsReport = [];
        this.LBIncomeDetailsReport = this.appData.getLBIncomeDetails();
    }
    ngOnInit() {
    }
};
LbIncomeDetailsPage.ctorParameters = () => [
    { type: src_app_utility_app_data__WEBPACK_IMPORTED_MODULE_3__["AppData"] },
    { type: src_app_utility_app_constants__WEBPACK_IMPORTED_MODULE_2__["AppConstants"] }
];
LbIncomeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-lb-income-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./lb-income-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/lb-income-details/lb-income-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./lb-income-details.page.scss */ "./src/app/pages/lb-income-details/lb-income-details.page.scss")).default]
    })
], LbIncomeDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-lb-income-details-lb-income-details-module-es2015.js.map