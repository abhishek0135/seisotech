(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-shopping-cart-shopping-cart-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shopping-cart/shopping-cart.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shopping-cart/shopping-cart.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-app-header [titleText]=\"'Shopping Cart'\" [backButton]=\"showBackButton\" [menuButton]=\"showMenuButton\"></app-app-header>\n\n<ion-content color=\"light\" class=\"ion-padding shop-cart-page\">\n    <div class=\"card mb-2\">\n        <div class=\"p-3\">\n            <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-secondary\">M.R.P.</div>\n                <div class=\"text-secondary\">$233,00</div>\n            </div>\n            <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-secondary\">Products Discount</div>\n                <div class=\"text-success\">-$40,00</div>\n            </div>\n            <!-- <div class=\"d-flex justify-content-between mb-2\">\n                <div class=\"text-info\">\n                    <ion-icon name=\"cash-outline\" ng-reflect-name=\"cash-outline\" role=\"img\" class=\"md hydrated\" aria-label=\"cash outline\"></ion-icon> Club Member Savings\n                </div>\n                <div class=\"text-secondary\">Not a member</div>\n            </div> -->\n            <div class=\"d-flex justify-content-between\">\n                <div class=\"text-secondary\"> Delivery Charges\n                    <div class=\"float-right delivery-charges-help\" (click)=\"helpPopup()\">\n                        <ion-icon class=\"text-danger\" name=\"help-circle-outline\"></ion-icon>\n                    </div>\n                </div>\n                <div class=\"text-danger\">+$90,00</div>\n            </div>\n        </div>\n        <div class=\"d-flex justify-content-between align-items-center py-3 px-3 border-top text-dark\">\n            <h6 class=\"font-weight-bold m-0\">Sub Total</h6>\n            <h6 class=\"font-weight-bold m-0\">$433,00</h6>\n        </div>\n    </div>\n    <div class=\"card mb-2\">\n        <div class=\"d-flex p-3 shop-cart-item-bottom border-bottom\" *ngFor=\"let cartItem of cartData\" tappable (click)=\"productDetails(cartItem)\">\n            <div class=\"shop-cart-left\"><img alt=\"img\" class=\"not-found-img\" [src]=\"cartItem.imageUrl\"></div>\n            <div class=\"shop-cart-right\">\n                <h6 class=\"font-weight-bold text-dark\">{{cartItem.name}}</h6>\n                <h6 class=\"text-dark mb-2\">${{ cartItem.price }}\n                    <span class=\"old-price regular-price text-secondary font-weight-normal\">${{ cartItem.oldPrice }}</span>\n                    <div class=\"float-right\">\n                        <ion-icon name=\"trash-outline\" color=\"danger\" tappable (click)=\"delete($event)\"></ion-icon>\n                    </div>\n                </h6>\n                <!-- <div class=\"mb-2\">{{ cartItem.description }}</div> -->\n                <div class=\"small text-gray-500 d-flex align-items-center justify-content-between\"><small class=\"text-secondary\">30 Capsules</small>\n                    <div class=\"input-group shop-cart-value\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                disabled=\"disabled\" type=\"button\">-</button></span><input class=\"form-control border-form-control form-control-sm input-number\" max=\"10\" min=\"1\" name=\"quant[1]\" type=\"text\" value=\"1\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                type=\"button\">+</button></span></div>\n                </div>\n            </div>\n        </div>\n        <!-- <div class=\"d-flex p-3 shop-cart-item-bottom border-bottom\" tabindex=\"0\" tappable (click)=\"productDetails()\">\n            <div class=\"shop-cart-left\"><img alt=\"img\" class=\"not-found-img\" src=\"assets/small/6.jpg\"></div>\n            <div class=\"shop-cart-right\">\n                <h6 class=\"font-weight-bold text-dark mb-2 price\"> $456.99 <span class=\"regular-price text-secondary font-weight-normal\">$787.99</span>\n                    <ion-badge color=\"success\" ng-reflect-color=\"success\" class=\"ion-color ion-color-success md hydrated\">5% OFF\n                    </ion-badge>\n                    <div class=\"float-right\" ><ion-icon name=\"trash-outline\" color=\"danger\"></ion-icon></div>\n                </h6>\n                <div class=\"mb-2\">Hygienix Anti-Bacterial Hand Sanitizer (Bottle) </div>\n                <div class=\"small text-gray-500 d-flex align-items-center justify-content-between\"><small class=\"text-secondary\">300 ml</small>\n                    <div class=\"input-group shop-cart-value\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                disabled=\"disabled\" type=\"button\">-</button></span><input class=\"form-control border-form-control form-control-sm input-number\" max=\"10\" min=\"1\" name=\"quant[1]\" type=\"text\" value=\"1\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                type=\"button\">+</button></span></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"d-flex p-3 shop-cart-item-bottom border-bottom\" tabindex=\"0\" tappable (click)=\"productDetails()\">\n            <div class=\"shop-cart-left\"><img alt=\"img\" class=\"not-found-img\" src=\"assets/small/11.jpg\"></div>\n            <div class=\"shop-cart-right\">\n                <h6 class=\"font-weight-bold text-dark mb-2 price\">$600.99 <span class=\"regular-price text-secondary font-weight-normal\">$800.99</span>\n                    <div class=\"float-right\" ><ion-icon name=\"trash-outline\" color=\"danger\"></ion-icon></div>\n                </h6>\n                <div class=\"mb-2\">Surf Excel Matic Top Load Detergent Powder (Carton) </div>\n                <div class=\"small text-gray-500 d-flex align-items-center justify-content-between\"><small class=\"text-secondary\">2 Kg</small>\n                    <div class=\"input-group shop-cart-value\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                disabled=\"disabled\" type=\"button\">-</button></span><input class=\"form-control border-form-control form-control-sm input-number\" max=\"10\" min=\"1\" name=\"quant[1]\" type=\"text\" value=\"1\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                type=\"button\">+</button></span></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"d-flex p-3 shop-cart-item \" tabindex=\"0\" tappable (click)=\"productDetails()\">\n            <div class=\"shop-cart-left\"><img alt=\"img\" class=\"not-found-img\" src=\"assets/small/2.jpg\"></div>\n            <div class=\"shop-cart-right\">\n                <h6 class=\"font-weight-bold text-dark mb-2 price\"> $456.99 <span class=\"regular-price text-secondary font-weight-normal\">$787.99</span>\n                    <ion-badge color=\"success\" ng-reflect-color=\"success\" class=\"ion-color ion-color-success md hydrated\">5% OFF\n                    </ion-badge>\n                    <div class=\"float-right\" ><ion-icon name=\"trash-outline\" color=\"danger\"></ion-icon></div>\n                </h6>\n                <div class=\"mb-2\">Hygienix Anti-Bacterial Hand Sanitizer (Bottle) </div>\n                <div class=\"small text-gray-500 d-flex align-items-center justify-content-between\"><small class=\"text-secondary\">300 ml</small>\n                    <div class=\"input-group shop-cart-value\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                disabled=\"disabled\" type=\"button\">-</button></span><input class=\"form-control border-form-control form-control-sm input-number\" max=\"10\" min=\"1\" name=\"quant[1]\" type=\"text\" value=\"1\"><span class=\"input-group-btn\"><button class=\"btn btn-sm\"\n                type=\"button\">+</button></span></div>\n                </div>\n            </div>\n        </div> -->\n    </div>\n</ion-content>\n<ion-footer class=\"border-0 md footer-md hydrated\" role=\"contentinfo\">\n    <button class=\"btn btn-primary btn-lg fix-btn btn-block text-left\" type=\"button\" tabindex=\"0\" (click)=\"verifyMobile()\"><span class=\"float-left\">\n      <ion-icon name=\"cart-outline\" ng-reflect-name=\"cart-outline\" role=\"img\" class=\"md hydrated\"\n        aria-label=\"cart outline\"></ion-icon> Proceed to Checkout\n    </span>\n    <span class=\"float-right\"><strong>$1200.69</strong>\n      <ion-icon name=\"arrow-forward-outline\" ng-reflect-name=\"arrow-forward-outline\" role=\"img\" class=\"md hydrated\"\n        aria-label=\"arrow forward outline\"></ion-icon>\n    </span></button></ion-footer>");

/***/ }),

/***/ "./src/app/pages/shopping-cart/shopping-cart-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/shopping-cart/shopping-cart-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ShoppingCartPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartPageRoutingModule", function() { return ShoppingCartPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _shopping_cart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shopping-cart.page */ "./src/app/pages/shopping-cart/shopping-cart.page.ts");




const routes = [
    {
        path: '',
        component: _shopping_cart_page__WEBPACK_IMPORTED_MODULE_3__["ShoppingCartPage"]
    }
];
let ShoppingCartPageRoutingModule = class ShoppingCartPageRoutingModule {
};
ShoppingCartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ShoppingCartPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/shopping-cart/shopping-cart.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/shopping-cart/shopping-cart.module.ts ***!
  \*************************************************************/
/*! exports provided: ShoppingCartPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartPageModule", function() { return ShoppingCartPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shopping_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shopping-cart-routing.module */ "./src/app/pages/shopping-cart/shopping-cart-routing.module.ts");
/* harmony import */ var _shopping_cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shopping-cart.page */ "./src/app/pages/shopping-cart/shopping-cart.page.ts");
/* harmony import */ var src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/component.module */ "./src/app/components/component.module.ts");








let ShoppingCartPageModule = class ShoppingCartPageModule {
};
ShoppingCartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shopping_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShoppingCartPageRoutingModule"],
            src_app_components_component_module__WEBPACK_IMPORTED_MODULE_7__["ComponentModule"]
        ],
        declarations: [_shopping_cart_page__WEBPACK_IMPORTED_MODULE_6__["ShoppingCartPage"]]
    })
], ShoppingCartPageModule);



/***/ }),

/***/ "./src/app/pages/shopping-cart/shopping-cart.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/shopping-cart/shopping-cart.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".delivery-charges-help {\n  font-size: 16px;\n  padding: 1px;\n}\n\n.my-custom-class {\n  --background: #222;\n}\n\n.old-price {\n  font-size: 0.7rem;\n  text-decoration: line-through;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2hvcHBpbmctY2FydC9zaG9wcGluZy1jYXJ0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSw2QkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2hvcHBpbmctY2FydC9zaG9wcGluZy1jYXJ0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kZWxpdmVyeS1jaGFyZ2VzLWhlbHAge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBwYWRkaW5nOiAxcHg7XG59XG5cbi5teS1jdXN0b20tY2xhc3Mge1xuICAgIC0tYmFja2dyb3VuZDogIzIyMjtcbn1cblxuLm9sZC1wcmljZSB7XG4gICAgZm9udC1zaXplOiAwLjdyZW07XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/shopping-cart/shopping-cart.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/shopping-cart/shopping-cart.page.ts ***!
  \***********************************************************/
/*! exports provided: ShoppingCartPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartPage", function() { return ShoppingCartPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _delivery_charges_popup_delivery_charges_popup_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../delivery-charges-popup/delivery-charges-popup.page */ "./src/app/pages/delivery-charges-popup/delivery-charges-popup.page.ts");
/* harmony import */ var _utility_app_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utility/app.data */ "./src/app/utility/app.data.ts");







let ShoppingCartPage = class ShoppingCartPage {
    constructor(navCltr, activatedRoute, router, modalController, appData, alertController) {
        this.navCltr = navCltr;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.modalController = modalController;
        this.appData = appData;
        this.alertController = alertController;
        this.showBackButton = false;
        this.showMenuButton = false;
        this.cartData = [];
        this.cartData = this.appData.getShoppingCartData();
        console.log(this.cartData);
    }
    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                debugger;
                const showBackValue = this.router.getCurrentNavigation().extras.state.showBack;
                if (showBackValue) {
                    this.showBackButton = true;
                }
            }
            else {
                this.showMenuButton = true;
            }
        });
    }
    productDetails(cartItem) {
        const navigationExtras = {
            state: {
                product: JSON.stringify(cartItem),
            }
        };
        this.navCltr.navigateForward(['product-details'], navigationExtras);
    }
    verifyMobile() {
        this.navCltr.navigateForward('verify-mobile');
    }
    helpPopup() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _delivery_charges_popup_delivery_charges_popup_page__WEBPACK_IMPORTED_MODULE_4__["DeliveryChargesPopupPage"],
                cssClass: 'my-custom-class'
            });
            return yield modal.present();
        });
    }
    delete(evnt) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            evnt.preventDefault();
            evnt.stopPropagation();
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Warning!',
                message: '<strong>Are You sure To Delete </strong>',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
ShoppingCartPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _utility_app_data__WEBPACK_IMPORTED_MODULE_5__["AppData"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
ShoppingCartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shopping-cart',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./shopping-cart.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shopping-cart/shopping-cart.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./shopping-cart.page.scss */ "./src/app/pages/shopping-cart/shopping-cart.page.scss")).default]
    })
], ShoppingCartPage);



/***/ })

}]);
//# sourceMappingURL=pages-shopping-cart-shopping-cart-module-es2015.js.map